<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Заявка на отримання статусу “Перевірена компанія”</title>
</head>
<body>
<h2>Поступила заявка на отримання статусу “Перевірена компанія”.</h2>
    <p>Назва компанії: {{ $user->name }}</p>
    @if($master->website)
    <p>Сайт компанії: {{ $master->website }}</p>
    @endif
    <p>Основний е-мейл: {{ $master->user->email }}</p>
    <p>Ім’я менеджера: {{ $master->user->manager->name }}</p>
    <p>Телефон менеджера: {{ $master->user->manager->phone }}</p>
    <p>Е-мейл менеджера: {{ $master->user->manager->email }}</p>
    <p>Ім’я керівника: {{ $master->user->owner->name }}</p>
    <p>Телефон керівника: {{ $master->user->owner->phone }}</p>
    <p>Е-мейл керівника: {{ $master->user->owner->email }}</p>
    <p><a href="{{ route('admin.master.edit', ['id' => $master->id]) }}">Переглянути деталі</a></p>
</body>
</html>
