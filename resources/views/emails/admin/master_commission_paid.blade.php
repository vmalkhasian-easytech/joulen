<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Спеціаліст щойно сплатив комісію сервісу</title>
</head>
<body>
<p>Спеціаліст “{{$user->name}}” щойно сплатив комісію сервісу у розмірі: {{$order->commission}} грн.</p>
<p><a href="{{route('admin.order.show', ['id' => $order->id])}}">Переглянути деталі</a></p>
<p>{{$order->payment->updated_at}}</p>
</body>
</html>
