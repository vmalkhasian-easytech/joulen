<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$order->title}}</title>
</head>
<body>
<p>Для замовлення (ID:{{$order->id}}) “{{$order->title}}” вибрали спеціаліста.</p>
<p><a href="{{route('admin.order.show',['id' => $order->id])}}">Переглянути деталі</a></p>
</body>
</html>