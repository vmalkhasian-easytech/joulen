<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ваш відгук для бренду “{{ $brand_title }}” відхилено</title>
</head>
<body>
<p>Ваш відгук для бренду <a href="{{ $brand_route }}">“{{ $brand_title }}”</a> відхилено адміністратором.</p>
<p style="word-wrap: break-word; white-space: pre-wrap;">Причина відхилення:<br>{{ $rejection_reason }}</p>
</body>
</html>