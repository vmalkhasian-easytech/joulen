<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Комісія сервісу {{ env('DOMAIN') }} сплачена</title>
</head>
<body>
<h2>Комісія сервісу {{ env('DOMAIN') }} сплачена</h2>
<p>{{$user->name}}</p>
<p>Сума {{$order->commission}} грн.</p>
<p>Дата: {{$order->payment->updated_at}}</p>
</body>
</html>
