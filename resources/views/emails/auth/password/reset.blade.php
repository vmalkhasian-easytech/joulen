<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Реєстрація у сервісі Джоуль</title>
</head>
<body>
<div>Ви надіслали запит на відновлення паролю в сервісі Джоуль.
Щоб змінити пароль, перейдіть за наступним посиланням: <a href="{{ route('password.reset.form', ['token' => $token]) }}?email={{ $email }}">Змінити пароль</a>
</div>
</body>
</html>
