@extends('emails.client.order.show.ses')

@section('battery-capacity')
    <p>Ємність акумуляторів: {{ $order->battery_capacity }} кВт*год</p>
@endsection

@section('ses-area')
    <p>Площа даху: {{ $ses->area }} м.кв.</p>
@endsection

@section('ses-house-comment-title')
    Інша інформація про будинок:
@endsection