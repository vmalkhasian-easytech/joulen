@if($order->orderable->{'about_block'})
    <p>Загальна площа: {{ $energy_audit->area }} кв.м.</p>
    <p>Кількість поверхів: {{$energy_audit->floors}}</p>
    <p>Приміщення {{ !$energy_audit->exploitation ? 'не ' : '' }}експлуатується</p>

    @if($energy_audit->exploitation)
        @yield('energy-audit-residents')        
        
        <p>Споживання електроенергії за рік: {{ $energy_audit->electricity_per_year }}</p>
        <p>Споживання води за рік: {{ $energy_audit->water_per_year }}</p>
        <p>Споживання газу за рік: {{ $energy_audit->gas_per_year }}</p>

        @if(count($energy_audit->energy_audit_heating_type) > 0)
            <p>{{ $energy_audit->energy_audit_heating_type->name }}</p>
        @endif
    @endif

    <p>Область: {{ $order->region->name }}</p>
    <p>Населений пункт: {{ $order->city }}</p>
    @if($order->house_comment != '')
        <p>Інша інформація: {!! nl2br(e($order->house_comment)) !!}</p>
    @endif
@endif

@if($order->orderable->{'problems_block'})
    <p>Проблеми що турбують:</p>
    {!! $energy_audit->high_costs ? '<p>високі витрати на енергоносії</p>' : '' !!}
    {!! $energy_audit->cold_winter ? '<p>холодно взимку</p>' : '' !!}
    {!! $energy_audit->hot_summer ? '<p>спекотно влітку </p>' : '' !!}
    {!! $energy_audit->mold ? '<p>у приміщенні пліснява/грибки</p>' : '' !!}
    {!! $energy_audit->draft ? '<p>протяги</p>' : '' !!}
    {!! $energy_audit->windows_fogging ? '<p>«потіють» вікна</p>' : '' !!}
    {!! $energy_audit->disbalance ? '<p>невідбалансована система опалення (перегрів/недогрів деяких приміщень)</p>' : '' !!}
    {!! $energy_audit->blackout ? '<p>часто вибивають пробки</p>' : '' !!}
    {!! $energy_audit->provider_problems ? '<p>проблеми з енергопостачальниками</p>' : '' !!}

    @if($energy_audit->other_problems != '')
        <p>Інші проблеми: {!! nl2br(e($energy_audit->other_problems)) !!}</p>
    @endif
@endif

@if($order->orderable->{'measures_block'})
    <p>Необхідно провести такі заходи:</p>
    {!! $energy_audit->thermal_examination ? '<p>тепловізійне обстеження</p>' : '' !!}
    {!! $energy_audit->blower_door_test ? '<p>проведення тесту на герметичність (Blower Door Test)</p>' : '' !!}
    {!! $energy_audit->detailed_calculation ? '<p>провести детальний розрахунок: які енергоефективні заходи потрібно впровадити та через скільки вони окупляться</p>' : '' !!}
    {!! $energy_audit->technical_supervision ? '<p>технічний нагляд під час утеплення/реконструкції</p>' : '' !!}
@endif