@extends('layouts.old.app')

@section('content')
    <div id="content">

        <div class="container">
            <div class="error-facebook">
                <div>
                    <p>Ви вже зареєcтровані <br> як майстер</p>
                    <h4>Що тепер робити?</h4>
                    <ol>
                        <li>Перейти на <a href="{{ route('home') }}">головну сторінку</a></li>
                        <li>Перейти на <a href="{{ route('login') }}">сторінку входу</a> і ввійти</li>
                        <li>Продовжуйте дивитись в монітор та через 30 секунд вас буде автоматично переадресовано на головну сторінку</li>
                    </ol>
                </div>
            </div>
        </div>
        <!--# end 404 #-->

    </div>

    @push('footer_scripts')
        {{--<script type="text/javascript">--}}
            {{--setTimeout(function() { window.location.href = '{{ route('home') }}'; }, 30000);--}}
        {{--</script>--}}
    @endpush
@endsection