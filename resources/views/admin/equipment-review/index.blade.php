@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap admin-reviews-page">
        {{--<div class="container">--}}
        <h1 class="main-title">Відгуки</h1>
        <div class="wrapper-page">
            <div class="dealers-brand-wrap">
                {{--<div class="search-items">--}}
                    <form action="{{ route('admin.equipment.review.index.filter') }}" class="search-items">
                        <div class="select-status-review">
                            <label for="status-review">Статус відгуку:</label>
                            <select class='select2' name="status_review" id="status-review">
                                <option value="" {{ !old('status_review') ? 'selected' : '' }}>Всі</option>
                                @foreach($statuses as $status)
                                    @if ($status->slug == 'verified')
                                        <option value="{{ $status->id }}" {{ old('status_review') == $status->id ? 'selected' : '' }}>
                                            Активний
                                        </option>
                                    @elseif($status->slug == 'rejected')
                                        <option value="{{ $status->id }}" {{ old('status_review') == $status->id ? 'selected' : '' }}>
                                            Відхилено
                                        </option>
                                    @else
                                        <option value="{{ $status->id }}" {{ old('status_review') == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <!-- /# -->
                        </div>
                        <!-- /.status-review -->
                        <div class="whose-review">
                            <label for="whose-review">Від кого:</label>
                            <select class='select2' name="whose_review" id="whose-review">
                                <option value="" {{ !old('whose_review') ? 'selected' : '' }}>Всі</option>
                                <option value="client" {{ old('whose_review') == 'client' ? 'selected' : '' }}>Від
                                    клієнтів
                                </option>
                                <option value="master" {{ old('whose_review') == 'master' ? 'selected' : '' }}>Від
                                    компаній інсталяторів
                                </option>
                            </select>
                        </div>
                        <!-- /.whose-review -->
                        <div class="search-brand-review">
                            <label for="brand-review">Бренд:</label>
                            <select class='select2 js-states form-control' name="brand_review" id="brand-review">
                                <option value="" {{ !old('brand_review') ? 'selected' : '' }}>Всі</option>
                                @foreach($equipment_brand as $brand)
                                    <option value="{{ $brand->id }}" {{ old('brand_review') == $brand->id ? 'selected' : '' }}>{{ $brand->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.brand-review -->
                        <div class="accept-btn">
                            <button type="submit" class="">Ок</button>
                            <!-- /. -->
                        </div>
                    </form>
                    <!-- /.accept-btn -->
                {{--</div>--}}
                <!-- /.search-items -->
                <div class="reviews-header-table">
                    <div class="date-review review-item">
                        <p>Дата:</p>
                    </div>
                    <!-- /.date-review -->
                    <div class="text-review review-item">
                        <p>Відгук:</p>
                    </div>
                    <!-- /.text-review -->
                    <div class="brand-review review-item">
                        <p>Бренд:</p>
                    </div>
                    <!-- /.brand-review -->
                    <div class="state-review review-item">
                        <p>Статус:</p>
                    </div>
                    <!-- /.status-review -->
                </div>
                <!-- /.reviews-container -->
                @foreach($reviews as $review)
                    <div class="view-review">
                        <div class="date-review review-item">
                            <p class="header-date">Дата:</p>
                            <span>{{ $review->created_at->format('d.m.Y') }}</span>
                            <div class="stars-wrapper">
                                <div class="stars-off stars">
                                    <div class="stars-on stars" style="width:{{ $review->sum_rating }}%"></div>
                                </div>
                            </div>
                            <!-- /.star-rating -->
                        </div>
                        <!-- /.date-review -->
                        <div class="text-review review-item">
                            <p class="header-review">Відгук:</p>
                            <p><a href="{{ route('admin.equipment.review.edit', ['equipment_type' => array_search($review->equipment->type->slug, $equipment_slug_mapping) ,
                            'brand_slug' => $review->equipment->slug,
                            'id' => $review->id]) }}">{{ $review->title }}</a></p>
                            <span><a href="{{ $review->user->isMaster() ?
                            route('master.show', ['id' => $review->user->master->id])
                            : route('admin.client.edit', ['id' => $review->user->client->id]) }}">{{ $review->user->name }}</a></span>
                            {{ $review->user_IP ? '(' . $review->user_IP . ')' : '' }}
                        </div>
                        <!-- /.text-review -->
                        <div class="brand-review review-item">
                            <p class="header-brand">Бренд:</p>
                            <a href="{{ route('brand.show', ['equipment_type' => array_search($review->equipment->type->slug, $equipment_slug_mapping) ,
                                    'brand_slug' => $review->equipment->slug]) }}" style="color: #32394a">{{ $review->equipment->title }}</a>
                        </div>
                        <!-- /.brand-review -->
                        <div class="state-review review-item">
                            <p class="header-status">Статус:</p>
                            @if ($review->status->slug == 'checking')
                                <p class="status-on-verified">На перевірці</p>
                            @elseif($review->status->slug == 'rejected')
                                <p class="status-rejected">Відхилено</p>
                            @elseif($review->status->slug == 'verified')
                                <p class="status-active">Активний</p>
                            @endif
                        </div>
                        <!-- /.status-review -->
                    </div>
            @endforeach
            <!-- /.view-review -->
            </div>
            <!-- /.dealers-brand-wrap -->

                <div class="pagination-container">
                    {{ $reviews->links() }}
                    <!-- /.pagination -->
                </div>
                <!-- /.pagination -->
            </div>

    </div>
    <!-- /.wrapper-page -->

    {{--</div>--}}
@endsection

@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/equipment-review/index.js') }}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush