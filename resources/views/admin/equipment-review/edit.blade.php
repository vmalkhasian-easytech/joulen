@extends('layouts.master.app')

@section('content')
    <div class="wrapper edit-review-page cabinet-page-wrap">
        <h1 class="main-title">
            Редагувати відгук
        </h1>
        <!-- /.main-title -->
        <div class="wrapper-page">
            <div class="add-brand-wrap">
                <form enctype="multipart/form-data" class="add-brand-form" id="review-form"
                      action="{{ route('admin.equipment.review.update', ['id' => $review->id]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="validation_errors_modal backend_validation_errors">
                        @component('layouts.validation_errors')
                        @endcomponent
                    </div>
                    <div class="form-block review-company-name">
                        <label for="">Відгук про компанію:</label>
                        <input type="hidden" name="equipment_id" value="{{ $brand->id }}">
                        <p>
                            {{ $brand->title }} <span>(Виробник {{ $brand->type_id == 1
                            ? 'інверторів' : 'сонячних панелей'}})</span>
                        </p>
                    </div>
                    <!-- /.form-block -->

                    <div class="form-block">
                        <label for="">Чи рекомендуєте ви обладнання цього бренду?</label>
                        <div class="styled-radio wrap-radio">
                            <input type="radio" name="positive" value="1" id="yes" data-validation="required"
                                    {{ $review->positive ? 'checked' : '' }}>
                            <label for="yes" class="selectItem">Так</label>
                            <input type="radio" name="positive" value="0" id="no" data-validation="required"
                                    {{ !$review->positive ? 'checked' : '' }}>
                            <label for="no" class="selectItem">Ні</label>
                        </div>
                    </div>

                    <div class="form-block">
                        <label for="review-title" class="selectItem">Заголовок відгуку:</label>
                        <div class="counter-symbol-item">
                            <p class="counter-symbol"> Максимум 45 символів<span id="max-review-title">45</span></p>
                            <!--Максимум 45 символів (залишилося 45)-->

                            <input data-validation="required" id="review-title" name="title"
                                   value="{{ $review->title }}">
                            <p>Придумайте короткий заголовок, який у декількох словах підсумує ваш досвід</p>
                        </div>
                        <!-- /.counter-symbol -->
                    </div>

                    <div class="form-block">
                        <label for="name" class="selectItem">Оцініть наступні характеристики:</label>
                        <div class="rating-company">
                            <div class="overal-quality">
                                <span>Якість</span>
                                <span>Ціна</span>
                                <span>Сервіс</span>
                            </div>
                            <div class="stars-wrap">
                                <fieldset class="rating">
                                    <input data-validation="required" type="radio" id="star5" name="quality_rating"
                                           value="5"
                                            {{ $review->quality_rating == 5 ? 'checked' : '' }}/>
                                    <label class="full" for="star5" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star4" name="quality_rating" value="4"
                                            {{ $review->quality_rating == 4 ? 'checked' : '' }}/>
                                    <label class="full" for="star4" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star3" name="quality_rating" value="3"
                                            {{ $review->quality_rating == 3 ? 'checked' : '' }}/>
                                    <label class="full" for="star3" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star2" name="quality_rating" value="2"
                                            {{ $review->quality_rating == 2 ? 'checked' : '' }}/>
                                    <label class="full" for="star2" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star1" name="quality_rating" value="1"
                                            {{ $review->quality_rating == 1 ? 'checked' : '' }}/>
                                    <label class="full" for="star1" title="Sucks big time - 1 star"></label>

                                </fieldset>
                                <fieldset class="rating">
                                    <input data-validation="required" type="radio" id="star10" name="price_rating"
                                           value="5"
                                            {{ $review->price_rating == 5 ? 'checked' : '' }}/>
                                    <label class="full" for="star10" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star9" name="price_rating" value="4"
                                            {{ $review->price_rating == 4 ? 'checked' : '' }}/>
                                    <label class="full" for="star9" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star8" name="price_rating" value="3"
                                            {{ $review->price_rating == 3 ? 'checked' : '' }}/>
                                    <label class="full" for="star8" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star7" name="price_rating" value="2"
                                            {{ $review->price_rating == 2 ? 'checked' : '' }}/>
                                    <label class="full" for="star7" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star6" name="price_rating" value="1"
                                            {{ $review->price_rating == 1 ? 'checked' : '' }}/>
                                    <label class="full" for="star6" title="Sucks big time - 1 star"></label>

                                </fieldset>
                                <fieldset class="rating">
                                    <input data-validation="required" type="radio" id="star15" name="service_rating"
                                           value="5"
                                            {{ $review->service_rating == 5 ? 'checked' : '' }}/>
                                    <label class="full" for="star15" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star14" name="service_rating" value="4"
                                            {{ $review->service_rating == 4 ? 'checked' : '' }}/>
                                    <label class="full" for="star14" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star13" name="service_rating" value="3"
                                            {{ $review->service_rating == 3 ? 'checked' : '' }}/>
                                    <label class="full" for="star13" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star12" name="service_rating" value="2"
                                            {{ $review->service_rating == 2 ? 'checked' : '' }}/>
                                    <label class="full" for="star12" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star11" name="service_rating" value="1"
                                            {{ $review->service_rating == 1 ? 'checked' : '' }}/>
                                    <label class="full" for="star11" title="Sucks big time - 1 star"></label>

                                </fieldset>
                            </div>
                            <!-- /. -->
                        </div>
                        <!-- /.rating-company -->
                    </div>


                    <div class="form-block">
                        <label for="your-review" class="selectItem">Ваш відгук:</label>
                        <div class="counter-symbol-item">
                            <p class="counter-symbol">Необхідно ввести ще <span id="min-your-review">200</span> символів
                            </p>
                            <textarea data-validation="length" id="your-review" name="body"
                                      data-validation-length="200-10000">{{ $review->body }}</textarea>
                            <p>Вкажіть як переваги, так і недоліки цього обладнання. <br>
                                Також напишіть як на вашу думку можна покращити сервіс/якість.</p>
                        </div>
                        <!-- /.counter-symbol -->
                    </div>

                    <div class="form-block view-profile">
                        <label for="">Відгук залишив:</label>
                        <p>{{ $review->user->name }} (<a href="{{ $review->user->isMaster() ?
                            route('master.show', ['id' => $review->user->master->id])
                            : route('admin.client.edit', ['id' => $review->user->client->id]) }}"> переглянути
                                профіль </a>)</p>
                    </div>
                    <!-- /.form-block -->

                    <div class="form-block">
                        <label for="">Статус відгуку:</label>
                        <div class="select-status">
                            <select name="status_id" id="review-status" class="select2">
                                @foreach($statuses as $status)
                                    @if ($status->slug == 'verified')
                                        <option value="{{ $status->id }}" {{ $review->status->id == $status->id ? 'selected' : '' }}>
                                            Активний
                                        </option>
                                    @elseif($status->slug == 'rejected')
                                        <option value="{{ $status->id }}" {{ $review->status->id == $status->id ? 'selected' : '' }}>
                                            Відхилено
                                        </option>
                                    @else
                                        <option value="{{ $status->id }}" {{ $review->status->id == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <!-- /#.select2 -->
                            @if ($review->status->slug == 'checking')
                                <a href="" class="activate-btn review-activate">
                                    Активувати
                                </a>
                                <!-- /.activate-btn -->
                                <a href="#review-reject-form" class="reject-btn">
                                    Відхилити
                                </a>
                        @endif
                        <!-- /.reject-btn -->
                            <div class="accept-btn">
                                <button type="submit" class="">Зберегти</button>
                                <!-- /. -->
                            </div>
                        </div>
                        <!-- /.select-status -->
                    </div>
                    <!-- /.form-block -->
                </form>
            </div>

        </div>
        <!-- /.wrapper-page -->
    </div>
@endsection

@push('modals')
<div class="remodal add-brand-form" id="modal-confirm-reject" data-remodal-id="review-reject-form">
    <h2 class="main-title">
        Причина відхилення
    </h2>
    <div class="form-block">
        <textarea name="rejection_reason"></textarea>
    </div>
    <div class="accept-btn">
        <a class="review-reject" style="padding: 0 25px; margin-right: 36px;">Надіслати</a>
        <button data-remodal-action="cancel"
                style="padding: 0 25px; margin-right: 36px; background-color: rgba(128, 128, 128, 0.62)">Скасувати
        </button>
    </div>
</div>
@endpush

@push('footer_scripts')
<script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
<script src="{{ asset('js/jquery.form-validator.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript">
    var reviewActivateStatusURL = '{{ route('admin.equipment.review.status.update', ['id' => $review->id, 'status_id' => $statuses->keyBy('slug')->get('verified')->id]) }}',
        reviewRejectStatusURL = '{{ route('admin.equipment.review.status.update', ['id' => $review->id, 'status_id' => $statuses->keyBy('slug')->get('rejected')->id]) }}';
</script>
<script type="text/javascript" src="{{ asset('js/admin/equipment-review/edit.js') }}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>

@endpush
