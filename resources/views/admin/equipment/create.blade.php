@extends('layouts.master.app')

@section('content')
    <div class="add-brand-page">
        <div class="cabinet-page-wrap">
            <h1 class="main-title">
                Додати бренд
            </h1>
            <!-- /.main-title -->
            <div class="wrapper-page">
                <div class="add-brand-wrap">
                    <form enctype="multipart/form-data" id="review-form" class="add-brand-form"
                          action="{{ route('admin.equipment.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="validation_errors_modal backend_validation_errors">
                            @component('layouts.validation_errors')
                            @endcomponent
                        </div>

                        <div class="form-block">
                            <label for="">Тип обладнання:</label>
                            <div class="styled-radio wrap-radio">
                                @foreach($equipment_types as $type)
                                    <input data-validation="required" type="radio" name="type_id" value="{{ $type->id }}"
                                           id="{{ $type->slug }}" {{ old('type_id') == $type->id ? 'checked' : '' }}
                                           data-validation-error-msg-required="поле 'Тип обладнання' обов'язкове до заповнення">
                                    <label for="{{ $type->slug }}" class="selectItem">{{ $type->title }}</label>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-block">
                            <label>Логотип:</label>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="file-upload hidden_validate_file" id="equipment_logo"
                                           name="equipment_logo" style="display: none" value="{{ old('equipment_logo') }}">
                                    <img src="" id="equipment_logo_img">
                                </div>
                                <div>
                                    <div id="equipment_logo_download" class="site_icon-download">Завантажити</div>
                                </div>
                                <div>
                                    <a href="" id="equipment-logo-delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" checked id="is_deleted_equipment_logo" value=""
                                           name="is_deleted_equipment_logo"
                                           style="display:none;">
                                </div>
                            </div>
                        </div>

                        <div class="form-block">
                            <label for="name" class="selectItem">Назва:</label>
                            <div class="">
                                <input data-validation="required" id="name" name="title" value="{{ old('title') }}"
                                       data-validation-error-msg-required="поле 'Назва' - обов'язкове до заповнення">
                            </div>
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="addAbout" id="addAbout" {{ old('addAbout') ? 'checked' : '' }}>
                                <label for="addAbout" class="selectItem">Детальний опис:</label>
                            </div>
                            <div class="textarea-wrap">
                            <textarea id="about" name="description" {{ old('description') ? '' : 'disabled' }}
                                      data-validation-error-msg-required="Поле 'Детальний опис' обов'язкове до заповнення">{{ old('equipment_logo') }}</textarea>
                            </div>
                            <!-- /. -->
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="add-site" id="add-site" {{ old('add-site') ? 'checked' : '' }}>
                                <label for="add-site" class="selectItem">Офіційний сайт:</label>
                            </div>
                            <div class="input-wrap">
                                <input type="url" data-validation="url" id="site" name="website" placeholder="http://"
                                       {{ old('website') ? '' : 'disabled' }}
                                       value="{{ old('equipment_logo') }}"
                                       data-validation-error-msg-required="Поле 'Офіційний сайт' обов'язкове до заповнення" >
                            </div>
                            <!-- /.input-wrap -->
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="add-year" id="add-year" {{ old('add-year') ? 'checked' : '' }}>
                                <label for="add-year" class="selectItem">Рік заснування:</label>
                            </div>
                            <div class="input-wrap">
                                <input id="year" name="foundation_year" {{ old('foundation_year') ? '' : 'disabled' }} data-validation="required"
                                       value="{{ old('equipment_logo') }}"
                                       data-validation-error-msg-required="Поле 'Рік заснування' обов'язкове до заповнення">
                            </div>
                            <!-- /.input-wrap -->
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="add-headquarters" id="add-headquarters" {{ old('add-headquarters') ? 'checked' : '' }}>
                                <label for="add-headquarters" class="selectItem">Штаб-квартира:</label>
                            </div>
                            <div class="input-wrap">
                                <input id="headquarters" name="headquarters" {{ old('headquarters') ? '' : 'disabled' }}
                                       value="{{ old('equipment_logo') }}"
                                       data-validation-error-msg-required="Поле 'Штаб-квартира' обов'язкове до заповнення">
                            </div>
                            <!-- /.input-wrap -->
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="add-production" id="add-production" {{ old('add-production') ? 'checked' : '' }}>
                                <label for="add-production" class="selectItem">Виробництво:</label>
                            </div>
                            <div class="input-wrap">
                                <input id="production" name="production"  {{ old('production') ? '' : 'disabled' }}
                                       value="{{ old('equipment_logo') }}"
                                       data-validation-error-msg-required="Поле 'Виробництво' обов'язкове до заповнення">
                            </div>
                        </div>

                        <div class="form-block">
                            <div class="styled-checkbox-blue">
                                <input type="checkbox" name="add-office" id="add-office" {{ old('add-office') ? 'checked' : '' }}>
                                <label for="add-office" class="selectItem">Офіс в Україні:</label>
                            </div>
                            <div class="input-wrap">
                                <input id="office" name="ukraine_office" {{ old('ukraine_office') ? '' : 'disabled' }}
                                       value="{{ old('ukraine_office') }}"
                                       data-validation-error-msg-required="Поле 'Офіс в Україні' обов'язкове до заповнення">
                            </div>
                        </div>

                    <!-- /.dealers-brand-wrap -->

                        <!-- /.reviews-brand-wrap -->

                        <div class="accept-btn">
                            <button type="submit" form="review-form">Зберегти</button>
                        </div>
                        <!-- /.accept-btn -->
                        <div class="cancel-btn">
                            <a href="{{ route('admin.equipment.index') }}">Скасувати</a>
                        </div>

                    </form>
                </div>
                <!-- /.cancel-btn -->
            </div>
            <!--form-->
        </div>
        <!-- /.wrapper-page -->
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}" />
@endpush
@push('footer_scripts')
<script src="{{ asset('js/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/equipment/create.js') }}"></script>
@endpush