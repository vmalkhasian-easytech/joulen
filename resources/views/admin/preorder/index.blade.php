@extends('layouts.old.app')

@section('content')
    <div class="container">
        <div class="table_page">
            <h1 class="title">Повідомлення (каталог)</h1>

            <div class="table_wrap">
                <table width="100%" class="tablesorter tablesorter_new admin-table" id="Table">
                    <thead>
                    <tr class="thead_style">
                        <th class="date small_title"><span>Дата</span> </th>
                        <th class="contact small_title" ><span>Контакти</span></th>
                        <th class="package small_title" ><span>Пакет</span></th>
                        <th class="comment small_title" colspan="1">Коментар</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($preorders as $preorder)
                        <tr>
                            <td>
                                <div class="wrap_date">{{ $preorder->created_at }}</div>
                            </td>
                            <td>
                                <div class="wrap_name">{{ $preorder->preorderable->name }}</div>
                                <div class="wrap_phone">{{ $preorder->preorderable->phone }}</div>
                                <div class="wrap_email"><a href="">{{ $preorder->preorderable->email }}</a></div>
                            </td>
                            <td>
                                <div class="wrap_package">
                                    {{ $preorder->subcategory->slug }}
                                </div>
                            </td>
                            <td>
                                <div class="wrap_comment">
                                    <a href="#modal" class="edit_comment" data-update_route="{{ route('admin.preorder.update', ['id' => $preorder->id]) }}"></a>
                                    <span class="comment">{!! nl2br($preorder->preorderable->comment) !!}</span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="control_table_block center">
            <div class="pagesNav textCenter">
                {{ $preorders->links() }}
            </div>
        </div>

    </div>

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush

    @push('footer_scripts')
        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
        <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
        <script src="{{ asset('js/preorder/index.js') }}"></script>
    @endpush

    @push('modals')
        <div class="remodal audit_modal" data-remodal-id="modal">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Коментар до заявки</h1>
            <form method="POST" action="" class="modal-validate" id="comment_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <textarea name="comment" class="validate_required" id="comment"></textarea>
                <div class="remodal_footer">
                    <input type="submit" value="Зберегти" class="site_button-orange submit">
                    <button data-remodal-action="cancel" class="site_button-transperent">Скасувати</button>
                </div>
            </form>
        </div>
    @endpush
@endsection