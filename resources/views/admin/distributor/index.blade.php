@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap equipment-wrap">
        <div class="add-brand-equipment">
            <h1 class="main-title">Дистриб'ютори</h1>
            <a href="{{ route('admin.distributors.create') }}" class="add-brand">
                + Додати дистриб'ютора
            </a>
            <!-- /.add-brand -->
        </div>
        <!-- /.add-brand -->
        <div class="wrapper-page">
            {{--<div class="search-items">--}}
            <form class="equipment-list search-items" enctype="multipart/form-data" method="POST"
                  action="{{ route('admin.distributors.index.filter') }}" id="admin_distributor_filter">
                {{ csrf_field() }}

                <input type="hidden" name="perpage" value="{{ old('perpage', 20) }}">
                <input type="hidden" name="sortfield" value="{{ old('sortfield', 'company_name') }}">
                <input type="hidden" name="sortorder" value="{{ old('sortorder', 'asc') }}">

                <div class="search-brand-type">
                    <label for="search-brand-type">Бренд:</label>
                    <select class="select2" name="brand" id="search-brand-type">
                        <option value="0" {{ !old('brand') ? 'selected' : '' }}>Всі</option>
                        @foreach($equipment as $brand)
                            <option value="{{ $brand->id }}" {{ old('brand') == $brand->id ? 'selected' : '' }}>
                                @if($brand->type->slug == 'inverter')
                                    Інвертор
                                @elseif($brand->type->slug == 'solar_panel')
                                    Панель
                                @endif
                                {{ $brand->title }}
                            </option>
                        @endforeach
                    </select>
                    <!-- /# -->
                </div>
            </form>
            <!-- /.accept-btn -->
            {{--</div>--}}
        <!-- /.search-items -->
            {{--@if(count($equipment) > 0)--}}
            <table id="myTable" class="distributor-table">
                <thead class="distributor-table_head">
                <tr>
                    <th class="distributor-table_name equipment-item
                        {{ old('sortfield') == 'company_name' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="company_name">
                        <p class="hide-sm">Назва: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <th class="equipment-item {{ old('sortfield') == 'masters_count' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="masters_count">
                        <p class="hide-sm">Дилерів: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <th class="equipment-item {{ old('sortfield') == 'inverters_count' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="inverters_count">
                        <p class="hide-sm">Інвертори: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <!-- /.quantity-photo -->
                    <th class="equipment-item {{ old('sortfield') == 'panels_count' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="panels_count">
                        <p class="hide-sm">Панелі: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <th class="equipment-item {{ old('sortfield') == 'balance' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="balance">
                        <p class="hide-sm">Баланс: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <th class="equipment-item {{ old('sortfield') == 'active' ? old('sortorder', 'desc') : ''}}"
                        data-sortorder="{{ old('sortorder', 'desc') }}" data-sortfield="active">
                        <p class="hide-sm">Статус: <i class="grade demo-icon icon-down-dir"></i></p>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <!-- /.equipment-header-table -->
                <tbody class="distributor-table_body">
                @forelse($distributors as $distributor)
                    <tr class=" ">
                        <td class="">
                            <a href="{{ route('distributor.show', ['slug' => $distributor->slug]) }}"
                               class="distributor-table_title hide-lg">
                                Назва: <i class="grade demo-icon icon-down-dir"></i>
                            </a>
                            <!-- /.company-name-title -->
                            <a href="{{ route('distributor.show', ['slug' => $distributor->slug]) }}">{{ $distributor->company_name }}</a>
                        </td>
                        <!-- /.company-name -->
                        <td class="">
                            <p class="distributor-table_title hide-lg">
                                Дилерів: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.dealers-quantity-title -->
                            <span>{{ $distributor->masters_count }}</span>
                        </td>
                        <!-- /.number-dealers -->
                        <td class=" ">
                            <p class="distributor-table_title hide-lg">
                                Інвертори: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.quantity-photo-title -->
                            <ul>
                                @foreach($distributor->inverters->pluck('title')->toArray() as $invert_name)
                                    <li>{{$invert_name}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <!-- /.quantity-photo -->
                        <td class="">
                            <p class="distributor-table_title hide-lg">
                                Панелі: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <!-- /.quantity-reviews-title -->
                            <ul>
                                @foreach($distributor->panels->pluck('title')->toArray() as $panel_name )
                                    <li>{{$panel_name}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class=" ">
                            <p class="distributor-table_title hide-lg">
                                Баланс: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <span>${{ $distributor->balance }}</span>
                        </td>
                        <td class="">
                            <p class="distributor-table_title hide-lg">
                                Статус: <i class="grade demo-icon icon-down-dir"></i>
                            </p>
                            <span class="distributor-status distributor-status_{{ $distributor->active ? '' : 'in' }}active">{{ \App\Models\User\Distributor::STATUS_MAPPING[$distributor->active] }}
                                <a href=""><img class="hide-lg" src="{{ asset('images/new-edit.png') }}"
                                                alt="image"></a>
                        </span>
                        </td>
                        <td>
                            <a href="{{ route('admin.distributors.edit', ['id' => $distributor->id]) }}"><img
                                        class="hide-sm" src="{{ asset('images/new-edit.png') }}" alt="image"></a>
                        </td>
                    </tr>
                @empty
                    <tr :aria-colspan="6">
                        <td>Дистриб'юторів не знайдено</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
            <div class="pagination-container"> {{ $distributors->links() }} </div>
            <!-- /.pagination -->
        </div>
    </div>
@endsection

@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/distributor/index.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.tablesorter.js') }}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush

@section('site-title')
    Список дистриб'юторів
@endsection