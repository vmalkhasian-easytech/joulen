@extends('layouts.old.app')

@section('content')
    <div id="content">

        <div class="container pageTitle">
            <h1>Список замовлень</h1>
        </div>

        <!--# contentBlock #-->
        <div class="contentBlock bordered">
            <div class="container">
                {{--TODO: move admin form to subtemplate--}}
                <form enctype="multipart/form-data" class="ordersFilter clr" method="post" action="{{ route('admin.order.index.filter') }}" id="admin_order_filter">

                    {{ csrf_field() }}

                    {{--TODO: old request values not working--}}
                    <div>
                        <div class="label">ПІБ КЛІЄНТА</div>
                        <div>
                            <input type="text" name="name" value="{{ old('name') }}" />
                        </div>
                    </div>
                    <div>
                        <div class="label">E-MAIL КЛІЄНТА</div>
                        <div>
                            <input type="text" name="email" value="{{ old('email') }}" />
                        </div>
                    </div>
                    <div>
                        <div class="label">НОМЕР ТЕЛЕФОНУ КЛІЄНТА</div>
                        <div>
                            <input type="text" name="phone" value="{{ old('phone') }}" />
                        </div>
                    </div>
                    <div>
                        <div class="label">ОБЛАСТЬ</div>
                        <div>
                            <select name="region_id">
                                <option value="0" {{ !old('region_id') ? 'selected' : '' }}>Всі</option>
                                @foreach($regions as $region)
                                    <option value="{{ $region->id }}" {{ old('region_id') == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">КАТЕГОРІЯ</div>
                        <div>
                            <select name="subcategory_id">
                                <option value="0" {{ !old('subcategory_id') ? 'selected' : '' }}>Всі</option>
                                @foreach($order_subcategories as $subcategory)
                                    <option value="{{ $subcategory->id }}" {{ old('subcategory_id') == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">СПЕЦІАЛІСТ</div>
                        <div>
                            <select name="master_id">
                                <option value="0" {{ !old('master_id') ? 'selected' : '' }}>Всi</option>
                                @foreach($masters as $master)
                                    <option value="{{ $master->id }}" {{ old('master_id') == $master->id ? 'selected' : '' }}>{{ $master->user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">СТАТУС</div>
                        <div>
                            <select name="status_id">
                                <option value="0" {{ !old('status_id') ? 'selected' : '' }}>Всі</option>
                                @foreach($order_statuses as $status)
                                    <option value="{{ $status->id }}" {{ old('status_id') == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">ID замовлення</div>
                        <div>
                            <select name="id">
                                <option value="" {{ !old('id') ? 'selected' : '' }}>Всі</option>
                                @foreach($orders_id as $order_id)
                                    <option value="{{ $order_id }}" {{ old('id') == $order_id ? 'selected' : '' }}>{{ $order_id }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">Місяць замовлення</div>
                        <div>
                            <select name="created_at">
                                <option value="" {{ !old('created_at') ? 'selected' : '' }}>Всі</option>
                                @foreach($orders_date as $order_date)
                                    <option value="{{ $order_date }}" {{ old('created_at') == $order_date ? 'selected' : '' }}>
                                        {{ $order_date }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if (isset($master_id_requests))
                        <input type="hidden" value="{{ $master_id_requests }}" name="master_id_requests"/>
                    @endif

                    <div class="fullwidth textCenter">
                        <input type="submit" class="site_button-orange onlyBorder" value="Відфільтрувати" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="contentBlock filterResults">
        <div class="container">
            @foreach($orders as $order)
                <div class="filterResult filter_{{ $order->status->slug }}">
                    <div>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <div class="filterDate">{{ $order->created_at }}</div>
                                    <div class="filterStatus">{{ $order->status->name }}</div>
                                    <div class="filterActivatedDate">{{ $order->activated_at_no_time}}</div>
                                </td>
                                <td>
                                    <div class="filterTitle">
                                        <a href="{{ route('admin.order.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
                                    </div>
                                    <div class="filterCity">{{ $order->city }}</div>
                                    <div class="filterDesc">{{ $order->manager_comment }}</div>
                                </td>
                                <td>
                                    {{--TODO: set the right url--}}
                                    {{--TODO get rig of ifs--}}
                                    <a href="{{ route('admin.order.request', ['id' => $order->id]) }}" class="filterQ_applications">
                                        {{ $order->requests_count }}
                                        @if($order->requests_count > 1 && $order->requests_count < 5)
                                            {{ ' заявки' }}
                                        @elseif($order->requests_count == 1)
                                            {{ ' заявка' }}
                                        @else
                                            {{ ' заявок' }}
                                        @endif
                                    </a>
                                </td>
                                <td>

                                    {{--TODO: set the right url (to master page)--}}
                                    @if(count($order->master) > 0)
                                        <a href="{{ route('master.show', ['id' => $order->master->id]) }}" class="filterWorker">{{ $order->master->user->name }}</a>
                                    @endif

                                </td>
                                <td style="padding:0px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endforeach

            {{ $orders->links() }}

        </div>
    </div>

    @push('footer_scripts')
      <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/admin/order/index.js') }}"></script>
    @endpush

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"/>
    @endpush
@endsection

@section('site-title')
    Список замовлень
@endsection