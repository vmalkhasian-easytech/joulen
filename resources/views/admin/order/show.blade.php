@extends('order.show')
@section('user-section')
    {{--TODO: add functionality (it's dummy template)--}}
    <div class="contentBlock">
        <div class="container">
            <div>
                <div class="specialBlock">
                    <form method="POST" id="clientSpecialist" action="{{ route('admin.order.update.meta', ['id' => $order->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="clientSpecialist clr">
                            <div class="clientBlock">
                                <h3>Клієнт</h3>
                                <div class="clientDatas">
                                    <div>
                                        <div class="site_icon-person">
                                            <strong>Ім’я: </strong> <a href="{{ route('admin.client.edit', ['id' => $order->client->id]) }}">{{ $order->client->user->name }}</a>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="site_icon-phone_gray"><strong>Телефон: </strong> <span class="mask-phone">{{ $order->client->user->phone }}</span>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <input {{ $order->show_phone ? 'checked' : '' }} type="checkbox" class="styledChk" id="show_phone" name="show_phone" value="1"/>
                                            <label for="show_phone">показати Перевіреним</label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="site_icon-mail_gray"><strong>E-mail: </strong>
                                            <a href="mailto:{{ $order->client->user->email }}"> {{ $order->client->user->email }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="specialistBlock">
                                <h3>Спеціаліст</h3>
                                <div>
                                    <div class="selectOfSpec">

                                        <select style="" name="master_id" id="selectmaster" name="master_id">
                                            <option value="0" {{ empty($order->master_id) ? 'selected' : '' }} data-commission="0" data-price="0">Не заданий</option>
                                            @if(count($order->requests) > 0)
                                                @foreach($order->requests as $request)
                                                    <option value="{{ $request->master->id }}" {{ $order->master_id == $request->master->id ? 'selected' : '' }} data-commission="{{ $request->commission }}" data-price="{{ $request->price }}">{{ $request->master->user->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                    </div>

                                    <div class="specialist-price">${{ $order->price }}</div>

                                    <div class="specialist-comisia" style="display:block;">
                                        <strong>Комісія:</strong>
                                        {{--TODO: rework comission calculation to respect existing payments--}}
                                        <input type="text" id="uahp" class="uahp" value="{{ $order->commission }}" name="commission">
                                        грн <input type="checkbox" id="specialist-isPaid" class="checkbox-special" name="paid" value="1" {{ $order->paid ? 'checked' : '' }}><label for="specialist-isPaid">Оплачено</label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="clientSpecialist-status">
                                <div class="clientSpecialist-status-label">Статус виконання</div>
                                <select id="statuso" name="status_id">
                                    @foreach($order_statuses as $status)
                                        <option value="{{ $status->id }}"
                                                data-slug="{{ $status->slug }}" {{ $status->slug == $order->status->slug ? 'selected' : '' }}>{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="showHideBlock">
                                <div class="showHideLabel">Коментар менеджера</div>
                                <div class="showHideContent">
                                    <textarea id="kmn" name="manager_comment">{{ $order->manager_comment }}</textarea>
                                </div>
                            </div>
                            <div id="coment_e" class="showHideBlock {!! $order->status->slug == 'rejected' ? '' : 'hidden' !!}">
                                <div class="showHideLabel">Причина відхилення</div>
                                <div class="showHideContent">
                                    <textarea id="coment_errort" name="cancellation_comment">{{ $order->cancellation_comment }}</textarea>
                                </div>
                            </div>

                            <div class="buttonsWrapper">
                                <a href="{{ route('admin.order.index') }}" class="site_button-orange onlyBorder">Назад</a>
                                <a href="{{ route('admin.order.edit', ['id' => $order->id]) }}" class="site_button-orange onlyBorder">Редагувати</a>
                                @if($order->status->slug === 'checking')
                                    <a href="{{ route('admin.order.activate', ['id' => $order->id]) }}" class="site_button-orange onlyBorder">Активувати</a>
                                @elseif ($order->status->slug === \App\Models\OrderStatus::IN_PROGRESS)
                                    <a href="" class="site_button-orange onlyBorder add-commit">Додати відгук</a>
                                @endif
                                <input type="submit" class="site_button-orange" value="Зберегти" id="button_submit"/>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('admin.review.create') }}" method="POST" id="add-commit-form">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_id"  value="{{ $order->id }}">
                        <input type="hidden" name="master_id" value="{{ $order->master_id }}">
                    </form>
                </div>
            </div>
        </div>
    </div>

    @can('leave_review', $order)
        <div id="addcoment" class="contentBlock block-reviews admin-block-reviews">
            <div class="container">
                <h3>Відгук клієнта:</h3>
                <div>
                    <div class="reviewWrapper clr">
                        <div>Відгуку поки що нема.
                            <form action="{{ route('admin.review.create') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
                                <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
                                <input type="submit" class="admin-edit-review" value="Додати відгук"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @if(isset($order->review_by_master) && $order->request_by_master)
        <div class="contentBlock block-reviews">
            <div class="container">
                <h3>Відгук клієнта:</h3>
                <div>
                    <div class="reviewWrapper clr">
                        <div class="review-author">
                            <div class="review-name {{ $order->review_by_master->positive == 1 ? 'sym_plus' : 'sym_minus' }}">{{ $order->client->user->name }}</div>
                            <div class="review-date">{{ $order->review_by_master->updated_at }}</div>
                        </div>
                        <div class="review">
                            <p>{{$order->review_by_master->text}}</p>
                        </div>
                        <div class="review-edit">
                            <a href="javascript:" class="site-icon-edit">
                                <div class="review-edit_button">
                                    <form action="{{ route('admin.review.edit', ['id' => $order->review_by_master->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
                                        <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
                                        <input type="submit" class="admin-edit-review" value="Редагувати відгук"/>
                                    </form>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @push('footer_scripts')
        <script type="text/javascript" src="{{ asset('js/admin/order/show.js')}}"></script>
    @endpush

@endsection
@section('activation-date')
    <div class="activation-date-block">
        <span>
            @if ($order->status_id > 2 && $order->status_id < 6)
                {{ $order->activated_at ?? $order->created_at }}
            @endif
        </span>
    </div>
@endsection

@section('admin-masters-requests')
    @if($order->status->slug === 'done' || $order->status->slug === 'in_progress')
        @component('layouts.order.request', ['request' => $order->request_by_master])
            @slot('final')
                {{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }}
            @endslot
            @slot('buttons')
                <a href="{{ route('admin.request.edit', ['id' => $request->id]) }}" class="site_button-orange button-small leaveFeedback">Редагувати заявку</a>
            @endslot
        @endcomponent
        @component('layouts.admin.order.another_requests', ['requests' => $another_masters_requests])
        @endcomponent
    @else
        @foreach($order->requests as $request)
            @component('layouts.order.request', ['request' => $request])
                @slot('final')
                    {{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }}
                @endslot
                @slot('buttons')
                    <a href="{{ route('admin.request.edit', ['id' => $request->id]) }}" class="site_button-orange button-small leaveFeedback">Редагувати заявку</a>
                @endslot
            @endcomponent
        @endforeach
    @endif
@endsection


@push('footer_scripts')
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
@endpush