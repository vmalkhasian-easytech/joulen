@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="container pageTitle">
            <h1>Редагувати інформацію про {{$master->user->name}}</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div>
            <!--# contentBlock #-->
            <div class="contentBlock">
                <div class="profileInfo">
                    <form method="post"
                          action="{{ route('admin.master.update', ['id' => $master->id]) }}"
                          enctype="multipart/form-data" class="edit-master-form">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть всі фото</span>
                        <input type="hidden" name="profile_stage" value="all">
                        <div class="profileBlock borderedProfile container">
                            <div class="pageTitle">
                                <h1>1. Загальна інформація про компанію</h1>
                            </div>
                            <div class="form-block">
                                <label>Назва компанії</label>
                                <input type="text" value="{{ $master->user->name }}" name="company_name"/>
                            </div>
                            <div class="form-block">
                                <label for="master_avatar">Ваш логотип файли jpg та png розміром не менше
                                    100х100px</label>
                                <div class="logoImgBlock">
                                    <div class="logoFile">
                                        <input type="file" class="file-upload hidden_validate_file" id="master_avatar"
                                               name="master_avatar" style="display:none;"><br>
                                        <img src="{{ $master->getAvatarSrc() }}" alt="master_avatar"
                                             id="master_avatar_img">
                                    </div>
                                    <div>
                                        <div id="master_avatar_download" class="site_icon-download">Завантажити</div>
                                    </div>
                                    <div>
                                        <a href="" id="master-avatar-delete" class="site_icon-delete remove-avatar">Видалити</a>
                                        <input type="checkbox" checked id="is_deleted_master_avatar" value=""
                                               name="is_deleted_master_avatar"
                                               style="display:none;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-block">
                                <label for="about">
                                    Про компанію
                                </label>
                                <textarea name="about">{{ $master->about }}</textarea>
                            </div>
                        </div>
                        <div class="profileBlock borderedProfile container">
                            <div class="pageTitle">
                                <h1>2. Контакти (для клієнтів)</h1>
                            </div>
                            <div>
                                <div class="form-block">
                                    <label>Робочий номер телефону:</label>
                                    <input class="mask-phone" type="text" maxlength="19" name="work_phone"
                                           value="{{ $master->work_phone }}"/>
                                    <a class="add-link add-second-phone">
                                        + додати ще один номер телефону
                                    </a>
                                </div>
                                <div class="site_icon-phone_gray second_phone">
                                    <div class="label">Робочий номер телефону 2:</div>
                                    <div class="field">
                                        <input class="mask-phone" type="text" maxlength="19"
                                               name="second_work_phone"
                                               value="{{ $master->second_work_phone }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-block">
                                <label>Робочий e-mail</label>
                                <input type="text" value="{{ $master->work_email }}" name="work_email"/>
                            </div>
                            <div class="form-block">
                                <label>Сайт</label>
                                <input type="text" name="website" data-validation="required length"
                                       data-validation-length="min2"
                                       data-validation-error-msg="Правильно заповніть це поле"
                                       value="{{ $master->website }}"/>
                            </div>
                            <div class="form-block profile-office-block">
                                <div class="profile-office-link">
                                    <p><strong>Офіс:</strong></p>
                                    <a id="main_office_added" class="add-link">Додати адресу головного офісу</a>
                                    <input type="hidden" name="main_office_added">
                                </div>
                                <div class="profile-office-map {{ $master->main_office->id ? "" : "hide" }}"
                                     id="office_map-1">
                                    <p><strong>Адреса головного офісу</strong>
                                        <a class="hider site_icon-delete" data-hide-id="office_map-1"></a>
                                    </p>

                                    <div class="show-map-block">
                                        <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                        <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                            Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                        </span>
                                        <div class="gmap-block">

                                            <input id="pac-input-1"
                                                   value="{{ $master->main_office->address }}"
                                                   class="search-map-input pac-input"
                                                   type="text" placeholder="Введіть тут адресу офісу">
                                            <div id="gmap-1" class="map img-border"></div>
                                        </div>
                                        <div class="office-input">
                                            <input name="main_office_address"
                                                   type="text" id="output-1" class="office_output"
                                                   value="{{ $master->main_office->address }}" readonly>
                                            <label for="main_office_room">
                                                Офіс №
                                            </label>
                                            <input name="main_office_room" id="main_office_room"
                                                   value="{{ $master->main_office->room }}" class="controls office_room"
                                                   type="text" placeholder="Офіс">
                                        </div>

                                        <input type="hidden" name="main_office_region" class="office-region" value="{{ $master->main_office->region_id ?
                                            explode(' ', $regions->find($master->main_office->region_id)->name )[0] : ''}}">
                                        <input type="hidden" class="geo_coords_lat" id="lat-1" name="main_office_lat"
                                               value="{{ $master->main_office->lat }}">
                                        <input type="hidden" class="geo_coords_lng" id="lng-1" name="main_office_lng"
                                               value="{{ $master->main_office->lng }}">
                                    </div>

                                    <div class="companyLogoBlock">
                                        <div>
                                            <strong>
                                                <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                            </strong>
                                        </div>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id="main_office_photo"
                                                       name="main_office_photo"
                                                       style="display:none;"><br>
                                                <img src="{{ $master->main_office->getPictureSrc() }}"
                                                     id="main_office_photo_img">
                                            </div>
                                            <div>
                                                <div id="main_office_photo_download" class="site_icon-download">
                                                    Завантажити
                                                </div>
                                            </div>
                                            <div>
                                                <a href="" id="main_office_photo_delete"
                                                   class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" id="is_deleted_main_office_photo" value=""
                                                       name="is_deleted_main_office_photo"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="plus_second_map">
                                    <a id="second_office_added"><u>+ Додати ще один офіс</u></a>
                                    <input type="hidden" name="second_office_added">
                                </div>

                                <div class="profile-office-map {{ $master->second_office->id ? "" : "hide" }}" id="office_map-2">
                                    <p><strong>Офіс №2:</strong>
                                        <a class="hider site_icon-delete" data-hide-id="office_map-2"></a>
                                    </p>
                                        <div class="show-map-block">
                                            <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                            <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                                Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                            </span>
                                            <div class="gmap-block" style="display: block; height: 300px;">
                                                <!-- /.marker-info -->
                                                <input value="{{ $master->second_office->address }}"
                                                       id="pac-input-2"
                                                       class="controls pac-input search-map-input" type="text"
                                                       placeholder="Введіть тут адресу офісу">
                                                <div id="gmap-2" class="map"></div>
                                            </div>
                                            <div class="office-input">
                                                <input name="second_office_address"
                                                       type="text" id="output-2" class="office_output" readonly
                                                       value="{{ $master->second_office->address }}">
                                                <label for="main_office_room">
                                                    Офіс №
                                                </label>
                                                <input name="second_office_room" id="second_office_room"
                                                       value="{{ $master->second_office->room }}"
                                                       class="controls office_room" type="text" placeholder="Офіс">
                                            </div>

                                            <input type="hidden" name="second_office_region" class="office-region" value="{{ $master->second_office->region_id ?
                                                explode(' ', $regions->find($master->second_office->region_id)->name )[0] : ''}}">
                                            <input type="hidden" class="geo_coords_lat" id="lat-2"
                                                   name="second_office_lat" value="{{ $master->second_office->lat }}">
                                            <input type="hidden" class="geo_coords_lng" id="lng-2"
                                                   name="second_office_lng" value="{{ $master->second_office->lng }}">
                                        </div>

                                    <div class="companyLogoBlock">
                                        <div>
                                            <strong>
                                                <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                            </strong>
                                        </div>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id="second_office_photo"
                                                       name="second_office_photo"
                                                       style="display:none;"><br>
                                                <img src="{{ $master->second_office->getPictureSrc() }}" alt="" id="second_office_photo_img">
                                            </div>
                                            <div>
                                                <div id="second_office_photo_download" class="site_icon-download">
                                                    Завантажити
                                                </div>
                                            </div>
                                            <div>
                                                <a href="" id="second_office_photo_delete"
                                                   class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" checked id="is_deleted_second_office_photo"
                                                       value=""
                                                       name="is_deleted_second_office_photo"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="plus_third_map">
                                    <a id="third_office_added"><u>Додати ще один офіс</u></a>
                                    <input type="hidden" name="third_office_added">
                                </div>
                                <div class="profile-office-map {{ $master->third_office->id ? "" : "hide" }}" id="office_map-3">
                                    <p><strong>Офіс №3:</strong>
                                        <a class="hider site_icon-delete" data-hide-id="office_map-3"></a>
                                    </p>
                                    <input type="hidden" name="third_office_added">
                                        <div class="show-map-block">
                                            <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                            <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                                Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                            </span>
                                            <div class="gmap-block" style="display: block; height: 300px;">
                                                <!-- /.marker-info -->
                                                <input value="{{ $master->third_office->address }}"
                                                       id="pac-input-3"
                                                       class="controls pac-input search-map-input" type="text"
                                                       placeholder="Введіть тут адресу офісу">
                                                <div id="gmap-3" class="map"></div>
                                            </div>
                                            <div class="office-input">
                                                <input name="third_office_address"
                                                       type="text" id="output-3" class="office_output" readonly
                                                       value="{{ $master->third_office->address }}">
                                                <label for="main_office_room">
                                                    Офіс №
                                                </label>
                                                <input name="third_office_room" id="third_office_room"
                                                       value="{{ $master->third_office->room }}"
                                                       class="controls office_room" type="text" placeholder="Офіс">
                                            </div>

                                            <input type="hidden" name="third_office_region" class="office-region" value="{{ $master->third_office->region_id ?
                             explode(' ', $regions->find($master->third_office->region_id)->name )[0] : ''}}">
                                            <input type="hidden" class="geo_coords_lat" id="lat-3"
                                                   name="third_office_lat" value="{{ $master->third_office->lat }}">
                                            <input type="hidden" class="geo_coords_lng" id="lng-3"
                                                   name="third_office_lng" value="{{ $master->third_office->lng }}">
                                        </div>

                                    <div class="companyLogoBlock">
                                        <div>
                                            <strong>
                                                <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                            </strong>
                                        </div>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id='third_office_photo'
                                                       name="third_office_photo"
                                                       style="display:none;"><br>
                                                <img src="{{ $master->third_office->getPictureSrc() }}" alt="" id="third_office_photo_img">
                                            </div>
                                            <div>
                                                <div id="third_office_photo_download" class="site_icon-download">
                                                    Завантажити
                                                </div>
                                            </div>
                                            <div>
                                                <a href="" id="third_office_photo_delete"
                                                   class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" checked id="is_deleted_third_office_photo"
                                                       value=""
                                                       name="is_deleted_third_office_photo"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="profileBlock borderedProfile container">
                            <div class="pageTitle">
                                <h1>3. Дані для співпраці із сервісом</h1>
                            </div>
                            <div class="form-block form-contact-block">
                                <h3>Контакти менеджера, який обробляє заявки на сервісі</h3>

                                <label>
                                    Ім'я:
                                </label>

                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       value="{{  $master->user->manager->name }}"
                                       data-validation="length" data-validation-length="min3"
                                       name="manager_name"/>


                                <label>Телефон:</label>

                                <input id="manager-phone" data-validation-error-msg="Правильно заповніть це поле"
                                       class="mask-phone" type="text"
                                       data-validation="number" data-validation-ignore="(,),-, ,+"
                                       maxlength="19" name="manager_phone"
                                       value="{{ $master->user->manager->phone }}"/>


                                <label>Е-mail:</label>

                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="email" name="manager_email" value="{{ $master->user->manager->email }}"/>

                                <div class="styled-checkbox">
                                    <input type="checkbox" id="manager_mailing" value="1"
                                           {{ $master->user->manager->mailing == 1 ? 'checked' : '' }} name="manager_mailing">
                                    <label class="selectItem" for="manager_mailing">
                                        Отримувати
                                        розсилку на цей e-mail</label>
                                </div>
                                <!-- /.styled-checkbox -->

                            </div>
                            <div class="form-block form-contact-block">
                                <h3>Контакти керівника компанії, з яким можна обговорювати деталі
                                    стратегічного партнерства:</h3>

                                <label>
                                    Iм'я:
                                </label>
                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="length" data-validation-length="min3"
                                       value="{{ $master->user->owner->name }}"
                                       name="owner_name"/>

                                <label>Телефон:</label>
                                <input id="chief-phone" data-validation-error-msg="Правильно заповніть це поле"
                                       class="mask-phone" type="text" maxlength="19"
                                       name="owner_phone"
                                       data-validation="number" data-validation-ignore="(,),-, ,+"
                                       value="{{ $master->user->owner->phone }}"/>

                                <label>Е-mail:</label>
                                <input type="text" data-validation-error-msg="Правильно заповніть це поле"
                                       data-validation="email"
                                       value="{{ $master->user->owner->email }}" name="owner_email"/>

                                <div class="styled-checkbox">
                                    <input type="checkbox" value="1" id="owner_mailing"
                                           {{ $master->user->owner->mailing == 1 ? 'checked' : '' }} name="owner_mailing">
                                    <label for="owner_mailing">
                                        Отримувати
                                        розсилку на цей e-mail
                                    </label>
                                </div>

                            </div>
                            <div class="form-block">
                                <p>Ваша компанія зареєстрована як юридична особа?</p>

                                <div class="styled-radio">
                                    <input type="radio" value="1" id="legal"
                                           {{ $master->edrpou ? 'checked' : ''}} name="legal"/>
                                    <label for="legal" class="selectItem">Так</label>
                                </div>
                                <div class="styled-radio">
                                    <input type="radio" value="0" id="legaln"
                                           {{ !$master->edrpou ? 'checked' : ''}} name="legal"/>
                                    <label for="legaln" class="selectItem">Ні</label>
                                </div>
                                <div class="codeLegal" @if(!empty($master->edrpou)) style="display: block" @endif>
                                    <label class="labelCode">Код ЄДРПОУ юридичної особи:</label>
                                    <input type="text" data-validation="number"
                                           data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 8 цифр"
                                           class="edrpuo" value="{{ $master->edrpou }}" name="edrpou"
                                           placeholder="8 цифр">
                                </div>
                            </div>
                            <div class="form-block">
                                <p>Ваша компанія зареєстрована як ФОП?</p>
                                <div class="styled-radio">
                                    <input type="radio" value="1" id="physical"
                                           {{ $master->ipn ? 'checked' : ''}} name="physical"/>
                                    <label for="physical" class="selectItem">Так</label>
                                </div>
                                <div class="styled-radio">
                                    <input type="radio" value="0" id="physicaln"
                                           {{ !$master->ipn ? 'checked' : ''}} name="physical"/>
                                    <label for="physicaln" class="selectItem">Ні</label>
                                </div>
                                <div class="codeFOP" @if(!empty($master->ipn)) style="display: block" @endif>
                                    <label>Індивідуальний податковий номер:</label>
                                    <input type="text"
                                           data-validation-error-msg="Введіть код ЄДРПОУ вашої компанії - 10 цифр"
                                           data-validation="number" value="{{ $master->ipn }}" class="ipn" name="ipn"
                                           placeholder="10 цифр">
                                </div>
                            </div>
                        </div>
                        <div class="profileBlock borderedProfile container">
                            <div class="pageTitle">
                                <h1>4. Сервіс та обслуговування</h1>
                            </div>
                            <div class="profileService">
                                <div class="form-block ses-type">
                                    <p>Станції якого типу будує ваша компанія?</p>
                                    <div class="styled-checkbox">

                                        <input type="checkbox" id="home_ses"
                                               @if($master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both') checked
                                               @endif value="1" name="home_ses" data-validation="checkbox_group"
                                               data-validation-qty="min1">
                                        <label for="home_ses" class="selectItem">Домашні СЕС</label>
                                        <span class="form-info">У вашому портфоліо повинно бути як мінімум 3 домашні станції</span>
                                    </div>
                                    <!-- /.styled-checkbox -->
                                    <div class="styled-checkbox">
                                        <input type="checkbox" id="commercial_ses"
                                               @if($master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both') checked=""
                                               @endif value="1" name="commercial_ses">
                                        <label class="selectItem" for="commercial_ses">Комерційні
                                            СЕС</label>
                                        <span class="form-info">У вашому портфоліо повинно бути як мінімум 2 комерційні станції</span>

                                    </div>
                                    <!-- /.styled-checkbox -->

                                </div>
                                <div class="form-block exchangeFond">
                                    <p>Чи є на складах вашої компанії
                                        <span class="tooltip">
                                        @component('layouts.profile_help')
                                            @endcomponent
                                            обмінний фонд

                                    </span> обладнання на випадок
                                        настання гарантійного випадку у клієнта?
                                    </p>

                                    <div class="styled-radio">
                                        <input type="radio" id="exchange_fund"
                                               {{ $master->exchange_fund ? 'checked' : ''}} name="exchange_fund"
                                               value="1">
                                        <label for="exchange_fund">Так</label>
                                    </div>
                                    <!-- /.styled-radio -->
                                    <div class="styled-radio">
                                        <input type="radio" id="exchange_fundn"
                                               {{ !$master->exchange_fund ? 'checked' : ''}} name="exchange_fund"
                                               value="0">
                                        <label for="exchange_fundn">Ні</label>
                                    </div>
                                    <!-- /.styled-radio -->

                                    <div class="brandName public-info {{ $master->exchange_fund ? '' : "hide" }}">
                                        <label>Яке обладнання (вкажіть бренди) та в яких кількостях зарезервовано на
                                            складі
                                            вашої компанії?</label>
                                        <textarea data-validation="length" data-validation-length="min50"
                                                  data-validation-error-msg="Заповніть це поле - не менше 50 символів"
                                                  name="exchange_fund_text">{{ $master->exchange_fund }}</textarea>
                                    </div>
                                </div>
                                <div class="form-block corporateCar">
                                    <p>Чи є у вашій компанії корпоративний автомобіль для сервісного
                                        обслуговування клієнтів?
                                    </p>
                                    <div class="styled-radio">
                                        <input type="radio" {{ $master->car ? 'checked' : ''}} name="car" id="car"
                                               value="1">
                                        <label for="car">Так</label>
                                    </div>
                                    <!-- /.styled-radio -->

                                    <div class="styled-radio">
                                        <input type="radio" id="nocar"
                                               {{ !$master->car ? 'checked' : ''}} name="car"
                                               value="0">
                                        <label for="nocar">Ні</label>
                                    </div>
                                    <!-- /.styled-radio -->

                                    <div class="photo-car {{ $master->car ? '' : "hide" }}">
                                        <p>Прикріпіть фото автомобіля</p>
                                        <span class="form-info">Наявність власного брендованого автомобіля підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id="car_photo"
                                                       name="car_photo" style="display:none;"><br>
                                                <img src="{{ $master->getCarPhotoSrc() }}" id="car_photo_img">
                                            </div>
                                            <div>
                                                <div id="car_photo_download" class="site_icon-download">Завантажити
                                                </div>
                                            </div>
                                            <div>
                                                <a href="" id="car_photo_delete" class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" checked id="is_deleted_car_photo" value="0"
                                                       name="is_deleted_car_photo"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                        <div class="styled-checkbox">
                                            <input type="checkbox" value="1"
                                                   name="public_car_photo" id="public_car_photo">
                                            <label for="public_car_photo">Додати
                                                це фото у ваше портфоліо</label>
                                        </div>
                                        <!-- /.styled-checkbox -->

                                    </div>
                                </div>
                                <div class="form-block projectInfo {{ $master->commercial ? '' : 'hide'}}">
                                    <div>
                                        <p>Чи є у вашій компанії власний проектний відділ (не на аутсорсінгу)?</p>
                                        <div class="styled-radio">
                                            <input type="radio"
                                                   {{ $master->project_department ? 'checked' : '' }} name="project_department"
                                                   value="1" id="project_department">
                                            <label for="project_department">Так</label>
                                        </div>
                                        <!-- /.styled-radio -->
                                        <div class="styled-radio">
                                            <input type="radio" @if(!$master->project_department) checked
                                                   @endif name="project_department" value="0" id="no_department">
                                            <label for="no_department">Ні</label>
                                        </div>
                                        <!-- /.styled-radio -->
                                    </div>
                                </div>
                                <div class="form-block machinery-block {{ $master->commercial ? '' : 'hide'}}">
                                    <p>Чи є у вашій компанії власна будівельна техніка?</p>
                                    <div class="styled-radio">
                                        <input type="radio" id="machinery"
                                               {{ $master->construction_machinery ? 'checked' : ''}} name="machinery"
                                               value="1">
                                        <label for="machinery">Так</label>
                                    </div>
                                    <!-- /.styled-radio -->
                                    <div class="styled-radio">
                                        <input type="radio" id="no_machinery"
                                               {{ !$master->construction_machinery ? 'checked' : ''}} name="machinery"
                                               value="0">
                                        <label for="no_machinery">Ні</label>
                                    </div>
                                    <!-- /.styled-radio -->

                                    <div class="photo-machinery {{ $master->construction_machinery ? '' : "hide" }}">
                                        <p>Прикріпіть фото власної будівельної техніки</p>
                                        <span class="form-info">Наявність власного будівельної техніки підвищить позиції вашої компанії у внутрішньому рейтингу сервісу Джоуль.</span>
                                        <div class="logoImgBlock">
                                            <div class="logoFile">
                                                <input type="file" class="hidden_validate_file" id="machinery_photo"
                                                       name="machinery_photo" style="display: none"><br>
                                                <img src="{{ $master->getMachineryPhotoSrc() }}"
                                                     id="machinery_photo_img">
                                            </div>
                                            <div>
                                                <div id="machinery_photo_download" class="site_icon-download">
                                                    Завантажити
                                                </div>
                                            </div>
                                            <div>
                                                <a href="" id="machinery_photo_delete"
                                                   class="site_icon-delete remove-avatar">Видалити</a>
                                                <input type="checkbox" checked id="is_deleted_machinery_photo"
                                                       value="0"
                                                       name="is_deleted_machinery_photo"
                                                       style="display:none;">
                                            </div>
                                        </div>
                                        <!-- /.styled-checkbox -->

                                    </div>
                                </div>
                                <div class="form-block companyBrand">
                                    <p>Обладнання яких брендів пропонує ваша компанія?</p>
                                    <div class="invertor">
                                        <p>Інвертори:</p>
                                        <div class="brand-list">
                                            <ul>
                                                @foreach($master->equipment as $invertor)
                                                    @if($invertor->type_id == 1)
                                                        <li>
                                                            <a href="#view-brands" data-id="{{ $invertor->id }}"
                                                               class="brand-item">{{ $invertor->title }}</a>
                                                            <a data-id="{{ $invertor->id }}"
                                                               class="brand-edit edit open-update-invertor open-modal"
                                                               href="#update-brand-invertor"></a>
                                                            <a class="brand-delete delete delete-equipment delete-invertor"
                                                               href="#modal-confirm-delete"
                                                               data-id="{{ $invertor->id }}"
                                                               data-url="{{ route('master.delete.equipment', ['id' => $invertor->id]) }}"></a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <a href="#add-brand-invertor" class="open-modal open-add-invertor add-link open-brand" data-id="1">
                                                + додати бренд
                                            </a>
                                        </div>
                                    </div>
                                    <div class="sun-panel">
                                        <p>Сонячні панелі:</p>
                                        <div class="brand-list">
                                            <ul>
                                                @foreach($master->equipment as $panel)
                                                    @if($panel->type_id == 2)
                                                        <li><a href="#view-brands" data-id="{{$panel->id}}"
                                                               class="brand-item">{{ $panel->title }}</a>
                                                            <a data-id="{{ $panel->id }}"
                                                               class="brand-edit edit open-update-panel open-modal"
                                                               href="#update-brand-panel"></a>
                                                            <a class="brand-delete delete delete-equipment delete-panel"
                                                               href="#modal-confirm-delete" data-id="{{$panel->id}}"
                                                               data-url="{{ route('master.delete.equipment',['id' => $panel->id]) }}"></a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <a href="#add-brand-panel" class="open-modal add-link open-add-panel open-brand" data-id="2">
                                                <span class="orange">+</span> додати бренд
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-block contract"
                                     @if($master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both') style="display: block;" @endif>
                                    <p>Прикріпити зразок типового договору, який заключає ваша компанія
                                        з клієнтом на будівництво домашньої СЕС?
                                    </p>
                                    <span class="form-info">Цю інформацію бачитиме лише адміністратор сайту</span>

                                    <div class="inputfile-styled">
                                        <label class="input-file">
                                            <input type="file" name="contract" value="0"
                                                   style="display:none;">Прикріпити
                                            договір</label>
                                        <a class="file-name" href="{{ $master->getContractSrc() }}" download>
                                            {{ $master->contract->file_name }}</a>
                                        <div>
                                            <a href="" id="company-contract-delete"
                                               class="site_icon-delete">Видалити</a>
                                            <input type="checkbox" checked id="is_deleted_contract" value="0"
                                                   name="is_deleted_contract"
                                                   style="display:none;">
                                        </div>
                                    </div>

                                    {{--<label class="site_icon-download"><input type="file" name="contract" value="0"--}}
                                    {{--style="display:none;">Прикріпити--}}
                                    {{--договір</label>--}}
                                    {{--<div>--}}
                                    {{--<a href="" id="company-contract-delete" class="site_icon-delete">Видалити</a>--}}
                                    {{--<input type="checkbox" checked id="is_deleted_contract" value="0"--}}
                                    {{--name="is_deleted_contract"--}}
                                    {{--style="display:none;">--}}
                                    {{--</div>--}}
                                </div>
                                <div class="form-block">
                                    <p>У яких областях ви надаєте послуги?</p>
                                    <div class="service-region checkbox-validation-block">
                                        @foreach ($regions as $region)
                                            <div class="styled-checkbox">
                                                <input {{ isset($master->regions->keyBy('id')[$region->id]) ? 'checked' : '' }}
                                                       type="checkbox" value="{{ $region->id }}"
                                                       name="work_location[{{ $region->id }}]"
                                                       class="styledChk regionChk" id='{{ $region->slug }}'
                                                />
                                                <label for="{{ $region->slug }}">{{ $region->name }}</label>
                                            </div>
                                        @endforeach
                                        <input type="checkbox" style="display: none" name="validation-work-location"
                                               class="validation-location">
                                    </div>
                                </div>
                                <div class="form-block orderRegion order-region-wrap">
                                    <p>Із яких областей ви б хотіли отримувати замовлення перш <br> за
                                        все? <span class="form-info"> (до трьох областей)</span>
                                    </p>
                                    <ol>
                                        <li>
                                            <label for="Region1">1:</label>
                                            <select data-validation="required" name="favorite_work_location[1]"
                                                    id="Region1"
                                                    class="select2">
                                                <option selected value="">Не вказано</option>
                                                @foreach($regions as $region)
                                                    @if($region->slug != 'krym')
                                                        <option {{ $master->getFavoriteRegions()->count() > 0 &&
                                                                $master->getFavoriteRegions()->toArray()[0] == $region->id ? 'selected' : '' }}
                                                                {{ ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ||
                                                                ($master->getFavoriteRegions()->count() > 2 &&
                                                                $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                                value="{{ $region->id }}">{{ $region->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </li>
                                        <li>
                                            <label for="Region2">2:</label>
                                            <select data-validation="required" name="favorite_work_location[2]"
                                                    class="select2"
                                                    id="Region1">
                                                <option selected value="">Не вказано</option>
                                                @foreach($regions as $region)
                                                    @if($region->slug != 'krym')
                                                        <option {{ $master->getFavoriteRegions()->count() > 1 &&
                                                                $master->getFavoriteRegions()->toArray()[1] == $region->id ? 'selected' : '' }}
                                                                {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                                ($master->getFavoriteRegions()->count() > 2 && $master->getFavoriteRegions()->toArray()[2] == $region->id) ? 'disabled' : '' }}
                                                                value="{{ $region->id }}">{{ $region->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </li>
                                        <li>
                                            <label for="Region3">3:</label>
                                            <select data-validation="required" name="favorite_work_location[3]"
                                                    class="select2"
                                                    id="Region3">
                                                <option selected value="">Не вказано</option>
                                                @foreach($regions as $region)
                                                    @if($region->slug != 'krym')
                                                        <option {{ $master->getFavoriteRegions()->count() > 2 &&
                                                                $master->getFavoriteRegions()->toArray()[2] == $region->id ? 'selected' : '' }}
                                                                {{ ($master->getFavoriteRegions()->count() > 0 && $master->getFavoriteRegions()->toArray()[0] == $region->id) ||
                                                                ($master->getFavoriteRegions()->count() > 1 && $master->getFavoriteRegions()->toArray()[1] == $region->id) ? 'disabled' : '' }}
                                                                value="{{ $region->id }}">{{ $region->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </li>
                                    </ol>

                                </div>
                                <div class="form-block accreditation">
                                    <p>Ваша компанія отримала акредитацію від Укргазбанку по програмі
                                        кредитування "Еко-енергія"?
                                    </p>
                                    <div class="styled-radio">
                                        <input type="radio" id="eco_energy"
                                               @if($master->eco_energy == '1') checked
                                               @endif name="eco_energy" value="1">
                                        <label for="eco_energy" class="selectItem">Так</label>
                                    </div>
                                    <!-- /.styled-checkbox -->
                                    <div class="styled-radio">
                                        <input type="radio" id="no_eco_energy"
                                               @if($master->eco_energy == '0') checked
                                               @endif name="eco_energy" value="0">
                                        <label for="no_eco_energy">Ні</label>
                                    </div>
                                    <!-- /.styled-checkbox -->

                                </div>
                                <div class="form-block  startInstall">
                                    <p>В якому році ваша компанія почала займатися монтажем сонячних
                                        електростанцій?
                                    </p>
                                    <div class="field">
                                        @php
                                            $currentYear = date("Y");
                                            $yearArray = range($currentYear, 1991);
                                        @endphp
                                        <select data-validation="required" name="year_started" class="select2"
                                                id="install">
                                            <option disabled selected>Рік</option>

                                            @foreach ($yearArray as $year)
                                                <option @if ($year == $master->year_started) selected
                                                        @endif value="{{$year}}">{{$year}}</option>;
                                            @endforeach
                                        </select>
                                    </div>
                                    <span class="form-info">Ви повинні додати у ваше портфоліо станцію введену в експлуатацію відповідного року</span>
                                </div>
                            </div>
                        </div>
                        <div class="profileBlock borderedProfile container">
                            <div class="pageTitle">
                                <h1>5. Портфоліо</h1>
                            </div>
                            <div class="contentBlock block-portfolio">
                                <div>
                                    <div class="clr portfolioContainer">
                                        <div class="alert alert-primary">
                                            <strong>Увага!</strong>
                                            <p>
                                                Сервіс автоматично рекомендуватиме клієнтам ті компанії, які вже збудували станції у їхньому або найближчому населеному пункті.
                                                Тому чим більше станцій ви додасте у портфоліо, тим більше шансів у вашої компанії отримати нове замовлення.
                                            </p>
                                            <strong>Звертаємо увагу!</strong>
                                            <p>Якщо під час перевірки з’ясується, що ви додали не існуючу станцію - ваш аккаунт буде заблоковано без можливості відновлення.
                                            </p>

                                        </div>
                                    </div>
                                    <div class="home-ses-block {{ $master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                                        <h3>Домашні СЕС:</h3>
                                        <div class="table-block">
                                        <table class="table home-ses-table">
                                            @foreach($master->getHomePortfolios() as $object)
                                                @if($object->subcategory_id != null)
                                                    <tr class='tr_id' id="{{ $object->id }}">
                                                        <td data-title="Дата" data-breakpoints="xs">
                                                            {{ $object->month }} {{ $object->year }}
                                                        </td>

                                                        <td data-title="Тип" data-breakpoints="">
                                                            <a href="#show-portfolio" class="get-portfolio" data-id="{{ $object->id }}">
                                                            {{ $subcategories[$object->subcategory_id] }}
                                                            </a>
                                                        </td>
                                                        <td data-title="Потужність" data-breakpoints="">
                                                            {{ $object->power }} кВт
                                                        </td>
                                                        <td data-title="Адреса" data-breakpoints="xs sm md">
                                                            {{ $object->getCity() }}
                                                        </td>

                                                        <td data-title="Обладнання" data-breakpoints="xs">
                                                            @if($object->delivery == 1)
                                                                <img src="{{ asset('images/delivery.png') }}" alt="">
                                                            @else
                                                                <img class="opacity-img"
                                                                     src="{{ asset('images/delivery.png') }}" alt="">
                                                            @endif

                                                        </td>
                                                        <td data-title="Монтаж" data-breakpoints="xs">

                                                            @if($object->installation == 1)
                                                                <img src="{{ asset('images/installation.png') }}" alt="">
                                                            @else
                                                                <img class="opacity-img"
                                                                     src="{{ asset('images/installation.png') }}" alt="">
                                                            @endif

                                                        </td>
                                                        <td data-title="Документування" data-breakpoints="xs">

                                                            @if($object->documentation == 1)
                                                                <img src="{{ asset('images/designing.png') }}" alt="">
                                                            @else
                                                                <img class="opacity-img"
                                                                     src="{{ asset('images/designing.png') }}" alt="">
                                                            @endif

                                                        </td>
                                                        <td data-title="Редагувати" data-breakpoints=""
                                                            class="edit_block">
                                                            <a class="open-update-ses update-home site-icon-edit"
                                                               data-id="{{ $object->id }}"
                                                               data-remodal-target="add-ses"></a>

                                                        </td>
                                                        <td data-title="Видалити" data-breakpoints=""
                                                            class="remove_block">
                                                            <a class="delete_ses site_icon-delete"
                                                               data-id="{{ $object->id }}"
                                                               data-remodal-target="delete-ses"></a>
                                                        </td>
                                                        <td data-title="Статус" data-breakpoints="xs sm"
                                                            class="application_status">
                                                            @if($object->status->slug == 'verified')
                                                                <p class="active-portfolio">Aктивна</p>
                                                            @elseif($object->status->slug == 'checking')
                                                                <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                                            @else
                                                                <p class="rejected-request">{{ $object->status->name }}</p>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                        <a id='add-home-ses-open' class="add-home open-modal add-link"
                                           data-remodal-target="add-ses">+ додати об'єкт</a>
                                        <div class="alert-wrap">
                                            <div class="alert alert-warning validation-home hide">
                                                <p>Потрібно обов'язково додати як мінімум три домашні станції із
                                                    фотографіями.
                                                    Якщо ваша компанія ще не будувала домашні СЕС, то вам потрібно
                                                    <a class="returnToStepFour" href="">повернутися до</a>
                                                     кроку №4 цієї анкети і деактивувати пункт "Домашні СЕС".</p>
                                            </div>
                                        </div>
                                        <!-- /.alert-wrap -->

                                    </div>
                                        <div id="gmap-4" class="map map_portfolio"></div>
                                    </div>
                                    <div class="commercial-ses-block {{ $master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both' ? '' : 'hide' }}">
                                        <h3>Комерційні СЕС:</h3>
                                        <div class="table-block">
                                        <table class="table commercial-ses-table">

                                            @foreach($master->getCommercialPortfolios() as $object)
                                                <tr class='tr_id' id="{{ $object->id }}">
                                                    <td data-title="Дата" data-breakpoints="xs">
                                                        {{ $object->month }} {{ $object->year }}
                                                    </td>
                                                    <td data-title="Тип" data-breakpoints="xs">
                                                        <a href="#show-portfolio" class="get-portfolio" data-id="{{ $object->id }}">
                                                        @if($object->ground == 1)
                                                            Наземна
                                                        @else
                                                            Дахова
                                                        @endif
                                                        </a>
                                                    </td>
                                                    <td data-title="Потужність" data-breakpoints="">
                                                        {{ $object->power }} кВт
                                                    </td>
                                                    <td data-title="Адреса" data-breakpoints="xs sm md">
                                                        {{ $object->getCity() }}
                                                    </td>
                                                    <td data-title="Генпіряд" data-breakpoints="xs">
                                                        @if($object->general_contractor == 1)
                                                            <img src="{{ asset('images/gen_contractor.png') }}" alt="">
                                                        @else
                                                            <img class="opacity-img"
                                                                 src="{{ asset('images/gen_contractor.png') }}" alt="">
                                                            {{--генпідряд--}}
                                                        @endif

                                                    </td>
                                                    <td data-title="Проектування" data-breakpoints="xs">

                                                        @if($object->designing == 1)
                                                            <img src="{{ asset('images/designing.png') }}" alt="">
                                                        @else
                                                            <img class="opacity-img" src="{{ asset('images/designing.png') }}"
                                                                 alt="">

                                                            {{--проек--}}
                                                        @endif

                                                    </td>
                                                    <td data-title="Обладнання" data-breakpoints="xs">
                                                        @if($object->delivery == 1)
                                                            <img src="{{ asset('images/delivery.png') }}" alt="">
                                                        @else
                                                            <img class="opacity-img" src="{{ asset('images/delivery.png') }}"
                                                                 alt="">
                                                            {{--обл--}}
                                                        @endif

                                                    </td>
                                                    <td data-title="Монтаж" data-breakpoints="xs">

                                                        @if($object->installation == 1)
                                                            <img src="{{ asset('images/installation.png') }}" alt="">
                                                        @else
                                                            <img class="opacity-img"
                                                                 src="{{ asset('images/installation.png') }}" alt="">
                                                            {{--монт--}}
                                                        @endif

                                                    </td>
                                                    <td data-title="едит" data-breakpoints="" class="edit_block">
                                                        <a class="open-update-ses update-commercial site-icon-edit"
                                                           data-id="{{ $object->id }}" data-remodal-target="add-ses"></a>

                                                    </td>
                                                    <td data-title="делейт" data-breakpoints=""
                                                        class="remove_block">
                                                        <a class="delete_ses site_icon-delete"
                                                           data-id="{{ $object->id }}"
                                                           data-remodal-target="delete-ses"></a>
                                                    </td>
                                                    <td data-title="Статус" data-breakpoints="xs sm"
                                                        class="application_status">
                                                        @if($object->status->slug == 'verified')
                                                            <p class="active-portfolio">Aктивна</p>
                                                        @elseif($object->status->slug == 'checking')
                                                            <p class="on-check-portfolio">{{ $object->status->name }}</p>
                                                        @else
                                                            <p class="rejected-request">{{ $object->status->name }}</p>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </table>
                                        <a id='add-commercial-ses-open' class='open-modal add-link add-commercial'
                                           data-remodal-target="add-ses">+ додати об'єкт</a>
                                        <div class="alert-wrap">
                                            <div class="alert alert-warning validation-commercial hide">
                                                <p>Потрібно обов'язково додати як мінімум дві комерційі станції із
                                                    фотографіями.
                                                    Якщо ваша компанія ще не будувала комерційні СЕС, то вам
                                                    потрібно <a class="returnToStepFour" href="">повернутися до</a>
                                                    до кроку №4 цієї анкети і скасувати пункт "Комерційні СЕС".</p>
                                            </div>
                                        </div>
                                    </div>
                                        <div id="gmap-5" class="map map_portfolio img-border"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="profileBlock otherInfo">
                                <div class="form-block news">
                                    <h3>Новини про компанію (з Інтернету):</h3>
                                    @foreach($master->news as $news)
                                        <div class='added-news' style="border: 1px solid black; padding: 20px">
                                            <div class="wrap_profile">
                                                <label>{{ $news->id }}. Заголовок:</label>
                                                <input type="text" value="{{ $news->title }}"
                                                       name="news_title_updated[{{ $news->id }}]"/>
                                            </div>
                                            <div class="wrap_profile">
                                                <label>URL:</label>
                                                <input type="text" value="{{ $news->url }}"
                                                       name="news_url_updated[{{ $news->id }}]"/>
                                                <a class="remove-item remove-news" data-news-id="{{ $news->id }}">Видалити</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-block  add-news-block">
                                    <div class="wrap_profile">
                                        <label>1. Заголовок:</label>
                                        <input type="text" value="" name="news_title_added[]"/>
                                    </div>
                                    <div class="wrap_profile">
                                        <label>URL:</label>
                                        <input type="text" value="" name="news_url_added[]"/>
                                        <a class="remove-item remove-news">Видалити</a>
                                    </div>
                                </div>
                                <a class="add-home open-modal add-link add-news-link">
                                    <span class="orange">+</span> додати новину
                                </a>
                                <div class="form-block">
                                    <div class="wrap_profile main_email">
                                        <label>Основний e-mail:</label>
                                        <input type="text" value="{{ $master->user->email }}" name="main_email"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-block">
                                <label>Розсилка:</label>
                                <div class="checkbox-validation-block">
                                    <div class="styled-checkbox">
                                        <input value="1" id="home_ses_mailing" type="checkbox"
                                               class="styledChk" name="home_ses_mailing"
                                                {{ $master->home_mailing ? 'checked' : ''}}/>
                                        <label for="home_ses_mailing">на домашні СЕС</label>
                                    </div>
                                    <div class="styled-checkbox spacing-top mailing-block mailing-profile">
                                        <input value="1" id="commercial_ses_mailing" type="checkbox"
                                               class="styledChk" name="commercial_ses_mailing"
                                                {{ $master->commercial_mailing ? 'checked' : ''}}/>
                                        <label for="commercial_ses_mailing">на комерційні СЕС</label>
                                    </div>
                                    <div class="styled-checkbox form-block spacing-top mailing-block mailing-profile">
                                        <input value="1" id="service_news" type="checkbox"
                                               class="styledChk" name="service_news"/>
                                        <label for="service_news">Новини сервісу</label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-block spacing-top wrap_profile">
                                    <label>
                                        Статус компанії:
                                    </label>
                                    <div class="field">
                                        <select name="company_status" class="custom_select" id="company_status">
                                            <option value="0">Виберіть статус</option>
                                            @foreach($master_statuses as $status)
                                                <option {{ $master->status_id == $status->id ? 'selected' : '' }} value="{{ $status->id }}">{{ $status->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($master->status->slug == 'inactive' || $master->status->slug == 'wait_activation')
                                        <div class="master-activation-block">
                                            <a href="" class="add-home open-modal add-link master-activate">Активувати</a>
                                            <a data-remodal-target="modal-confirm-reject" class="open_comment_decline remove-item open-master-reject">Відхилити</a>
                                        </div>
                                    @endif
                                    <div class="styled-checkbox form-block spacing-top" style="margin-top: 10px">
                                        <input value="1" id="blocked" type="checkbox"
                                               class="styledChk" name="blocked"
                                                {{ $master->user->blocked ? 'checked' : '' }}/>
                                        <label for="blocked">Заблокований</label>
                                    </div>
                                    <div class="styled-checkbox form-block spacing-top">
                                        <input value="1" id="deleted" type="checkbox"
                                               class="styledChk" name="deleted"
                                                {{ $master->user->deleted ? 'checked' : '' }}/>
                                        <label for="deleted">Видалився</label>
                                    </div>
                                </div>
                            </div>
                            @if($master->user->profileType->profile_url)
                                <div>
                                    <div class="spacing-top wrap_profile">
                                        <div class="label">
                                            Facebook-профіль:
                                        </div>
                                        <a href="{{ $master->user->profileType->profile_url }}" class="add-home open-modal add-link">Переглянути</a>
                                        <a href="" class="open_comment_decline remove-item">Видалити</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class='space-top container'>
                            <button class='btn btn-primary edit-master-submit'>Зберегти</button>
                            <a href='{{ route('admin.master.index') }}'
                               class='step_back btn btn-secondary'>Скасувати</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="hellopreloader" style="display: none">
        <div id="hellopreloader_preload">
            <p class="preloader">Зачекайте, іде завантаження</p>
        </div>
    </div>


    @push('footer_scripts')
    <script>
        var master = {!! $master !!},
            equipment = {!! $master->equipment->keyBy('id') !!},
            guarantee_repairs = {!! $guarantee_repairs !!},
            exchange_funds = {!! $exchange_funds !!},
            defaultAvatar = '{{ asset('images/specialist.png') }}', // TODO: use imgDriectoryPath var
            markerSrc = '{{ asset('images/map-marker.png') }}',// TODO: use imgDriectoryPath var
            imgDirectoryPath = '{{ asset('images/') }}',
            getEquipmentURL = '{{ route('admin.master.get.equipment') }}',
            deleteEquipmentURL = '{{ route('admin.master.delete.equipment') }}',
            getBrandsURL = '{{ route('admin.master.get.brands') }}',
            getPortfolioURL = '{{ route('admin.master.get.portfolio') }}',
            storePortfolioURL = '{{ route('admin.master.store.portfolio') }}',
            updatePortfolioURL = '{{ route('admin.master.update.portfolio') }}',
            deletePortfolioURL = '{{ route('admin.master.delete.portfolio') }}',
            subcategories = {!! $subcategories !!},
            invertor_id = {!! $invertor_type_equipment->keyBy('id') !!},
            panel_id = {!! $panel_type_equipment->keyBy('id') !!},
            masterOrderRoute = '{{ route('master.order.index') }}',
            homePortfolioCoordinates = {!! $master->home_portfolios_coordinates !!},
            commercialPortfolioCoordinates = {!! $master->commercial_portfolios_coordinates !!},
            masterActivateURL = '{{ route('admin.master.status.update', [ 'id' => $master->id, 'status_id' => $master_statuses->get('unverified')->id ]) }}',
            masterRejectURL = '{{ route('admin.master.status.update', [ 'id' => $master->id, 'status_id' => $master_statuses->get('canceled')->id ]) }}',
            regions = {!! $regions->keyBy('id') !!},
            count_regions = {!! $regions->count() !!},
            invertorName = {!! $invertor_type_equipment->keyBy('id') !!},
            panelName = {!! $panel_type_equipment->keyBy('id') !!},
            masterStatus = {!! $master_statuses !!};
    </script>
    <script src="{{ asset('libs/footable/js/footable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/portfolio_modals.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/profile-form-handler.js') }}"></script>
    <script src="{{ asset('js/jquery.nice-select.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
            defer></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script src="{{ asset('js/master/mask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/profile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/master/edit.js') }}"></script>
    @endpush
    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/footable/css/footable.standalone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/preloader.css') }}"/>
    @endpush

    @push('modals')
    @component('layouts.master.profile.add_invertor_brand', [
    'invertor_type_equipment' => $invertor_type_equipment,
    'guarantee_repairs' => $guarantee_repairs,
    'exchange_funds' => $exchange_funds,
    'master' => $master
    ])
    @endcomponent

    @component('layouts.master.profile.update_invertor_brand', [
    'guarantee_repairs' => $guarantee_repairs,
    'exchange_funds' => $exchange_funds,
    'master' => $master
    ])
    @endcomponent

    @component('layouts.master.profile.add_panel_brand', [
    'panel_type_equipment' => $panel_type_equipment,
    'guarantee_repairs' => $guarantee_repairs,
    'exchange_funds' => $exchange_funds,
    'master' => $master
    ])
    @endcomponent

    @component('layouts.master.profile.update_panel_brand', [
    'guarantee_repairs' => $guarantee_repairs,
    'exchange_funds' => $exchange_funds,
    'master' => $master
    ])
    @endcomponent

    @component('layouts.master.profile.ses_portfolio', [
    'subcategories' => $subcategories,
    'invertor_type_equipment' => $invertor_type_equipment,
    'panel_type_equipment' => $panel_type_equipment,
    'master' => $master
    ])
    @endcomponent

    @component('layouts.master.profile.view_brands')
    @endcomponent

    <div class="remodal" data-remodal-id="delete-ses" id="delete-ses">
        <div class="modal_header">
            <p>Ви дійсно бажаєте видалити цю СЕС з портфоліо?</p>
        </div>
        <div class='space-top remove-btn'>
            <a class='site_button-orange onlyBorder button-small delete_ses_confirm'
               data-remodal-action="confirm">Так</a>
            <button class='site_button-orange button-small' data-remodal-action="cancel">Ні</button>
        </div>
    </div>

    <div class="remodal" id="modal-confirm-delete" data-remodal-id="modal-confirm-delete">
        <div class="modal_header">
            <p>Ви дійсно бажаєте видалити цей бренд зі списку?</p>
        </div>
        <div class="space-top remove-btn">
            <a class="btn btn-primary delete-equipment-confirm" data-remodal-action="confirm">Так</a>
            <button class='btn btn-secondary' data-remodal-action="cancel">Ні</button>
        </div>
    </div>

    <div class="remodal" id="modal-confirm-reject" data-remodal-id="modal-confirm-reject">
        <div class="modal_header">
            <p>Причина відхилення</p>
        </div>
        <div class="form-block" style="margin: 10px auto 0">
            <textarea name="rejection_reason"></textarea>
        </div>
        <div class="space-top remove-btn">
            <button class='btn btn-secondary' data-remodal-action="cancel">Скасувати</button>
            <a class="btn btn-primary master-reject" data-remodal-action="confirm">Надіслати</a>
        </div>
    </div>

    @component('layouts.master.profile.view_portfolio')
    @endcomponent
    @endpush

@endsection