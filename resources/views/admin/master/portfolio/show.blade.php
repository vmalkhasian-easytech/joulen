@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">

        <div class="page-title">
            <h1>{{ $portfolio->subcategory->name }} СЕС, {{$portfolio->power}} кВт</h1>
        </div>

        <!--# contentBlock #-->
        <div class="cabinet-portfolio">
                <div>
                    <p>{{ $portfolio->address }}</p>
                </div>
                <div id="map" class="map"></div>
                <div class="photoBlock">
                    @if($portfolio->panel_photo_src)
                        <div>
                            <img src="{{ $portfolio->panel_photo_src }}" alt="image">
                        </div>
                    @endif
                    @if($portfolio->invertor_photo_src)
                        <div>
                            <img src="{{ $portfolio->invertor_photo_src }}" alt="image">
                        </div>
                    @endif
                </div>
                <div>Введено в експлуатацію: {{ $portfolio->month }} {{ $portfolio->year }}</div>
                <div>Інвертор: {{ $portfolio->invertor->title }}</div>
                <div>Панелі: {{ $portfolio->panel->title }}</div>
                <div>
                    Виконавець: {{  $portfolio->master->user->name }}
                    (
                    @if ($portfolio->delivery)
                        поставка обладнання,
                    @endif
                    @if ($portfolio->installation)
                        монтаж,
                    @endif
                    @if($portfolio->documentation)
                        документальний супровід
                    @endif
                    )
                </div>
                <form class="publicInfo" action="{{ route('admin.portfolio.update', $portfolio->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div>Статус:
                    <div class="field">
                        <select name="status">
                            <option value="1" {{ $portfolio->active ? 'selected' : '' }}>Активна</option>
                            <option value="0" {{ !$portfolio->active ? 'selected' : '' }}>Неактивна</option>
                        </select>
                    </div>
                    </div>
                    <div><a class="delete_portfolio" data-id='{{ $portfolio->id }}' href="#portfolio-confirm-delete">Видалити об'єкт</a></div>

                    <div class='space-top'>
                        <input type="submit" value="Зберегти" class='btn btn-primary'>
                        <a href="{{ route('admin.portfolio.index') }}" class='btn btn-secondary'>Скасувати</a>
                    </div>
                </form>
            </div>
    </div>

    @push('modals')
    <div class="remodal" id="portfolio-confirm-delete" data-remodal-id="portfolio-confirm-delete">
        <div class="modal_header">
            <p>Ви дійсно бажаєте видалити це портфоліо?</p>
        </div>
        <div class="space-top">
            <form method="POST" action="{{ route('admin.portfolio.destroy', $portfolio->id) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <input type="submit" value="Так" class="site_button-orange button-small delete-portfolio-confirm">
            </form>
            <button class='site_button-orange onlyBorder button-small' data-remodal-action="cancel">Ні</button>
        </div>
    </div>
    @endpush
    @push('footer_scripts')
    <script>
        var portfolio = {!! $portfolio !!};
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
            defer></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script src="{{ asset('js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('js/admin/master/portfolio/show.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    @endpush

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush
@endsection

@section('site-title')
    Редагувати портфоліо
@endsection