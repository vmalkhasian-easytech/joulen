@extends('layouts.old.app')

@section('content')
    <div class="container pageTitle">
        <h1>Параметри облікового запису</h1>
    </div>

    <div class="contentBlock userData">
        <div class="container">
            <form id="formparams" class="validate_form" method="POST" action="{{ route('admin.client.update', ['id' => $client->id]) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                @if($client->user->deleted)
                    <a href="{{ route('admin.client.restore', ['id' => $client->user->id]) }}">Відновити користувача</a>
                @endif

                <div class="">
                    <input name="blocked" type="checkbox" {{ $client->user->blocked ? 'checked' : '' }} class="styledChk" value="1" id="blocked">
                    <label for="blocked">Заблокувати користувача</label>
                </div>

                <div>
                    <div class="site_icon-person">
                        <div class="label">Ім'я</div>
                        <div class="field">
                            <input name="name" class="validate_required" type="text" value="{{ $client->user->getOriginal('name') }}">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="site_icon-mail_gray">
                        <div class="label">E-mail</div>
                        <div class="field">
                            <input name="email" type="text" value="{{ $client->user->email }}">
                        </div>
                    </div>
                </div>

                <div style="display:block;">
                    <div class="site_icon-phone_gray">
                        <div class="label">Телефон</div>
                        <div class="field">
                            <input name="phone" class="mask-phone" type="text" value="{{ $client->user->phone }}">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="site_icon-password_gray">
                        <div class="label">Новий пароль</div>
                        <div class="field">
                            <input name="password" type="text" value="" >
                        </div>
                    </div>
                </div>

                <div>
                    <div class="spacing-top">
                        <input value="1" name="mailing" type="checkbox" class="styledChk"
                               id="mailing" {{  $client->mailing ? 'checked' : '' }}>
                        <label for="mailing">Клієнт підписаний на розсилку про нові замовлення</label>
                    </div>
                </div>

                <div class="submitBtns">
                    <a href="{{ route('admin.order.index') }}" class="site_button-orange button-small onlyBorder">Відмінити</a>
                    <input type="submit" value="Зберегти" class="site_button-orange button-small">
                </div>
            </form>
        </div>
    </div>

    @push('footer_scripts')
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    @endpush

@endsection

@section('site-title')
    Параметри облікового запису
@endsection