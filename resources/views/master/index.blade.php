@extends('layouts.app')

@section('content')
    <div class="catalog-station-wrap select-category">
        <div class="container">
            <div class="catalog select-station">
                <h2 class="gray">Каталог монтажних організацій</h2>
                <div class="select-organiation">
                    <div class="select-region select-item">
                        <label for="filterRegion">Оберіть регіон:</label>
                        <select class="select2" name="region_slug"  id="filterRegion">
                            <option value="all" {{ !array_key_exists('region', $filter) ? 'selected' : '' }}>Вся Україна</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->slug }}" {{ array_key_exists('region_slug', $filter) && $filter['region_slug'] == $region->slug ? 'selected' : '' }}>{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="select-specialization select-item">
                        <label for="select-specialization">Спеціалізація:</label>
                        <select class="select2" name="subcategory" id="select-specialization">
                            <option value="all" {{ !array_key_exists('subcategory', $filter) ? 'selected' : '' }}>Всі типи СЕС</option>
                            <option value="home_ses" {{ array_key_exists('subcategory', $filter) && $filter['subcategory'] == "home_ses" ? 'selected' : '' }}>Домашні</option>
                            <option value="commercial_ses" {{ array_key_exists('subcategory', $filter) && $filter['subcategory'] == "commercial_ses" ? 'selected' : '' }}>Комерційні</option>
                        </select>
                    </div>
                    <div class="visible-proven-companies">
                        <input  name="verified_masters" id="proven-companies" type="checkbox" value="verified"
                                {{ array_key_exists('verified_masters', $filter) && $filter['verified_masters'] == 'verified' ? 'checked' : '' }}>
                        <label for="proven-companies">Показати лише перевірені компанії</label>
                    </div>
                </div>

                @foreach ($masters as $master)
                    <div class="visible-catalog-organization">
                        <div class="name-organization">
                            <h3><a href="{{ route('master.show', ['id' => $master->id])}}">{{$master->user->name}}</a></h3>
                        </div>
                        <!-- /.name-organization -->
                        @if($master->status->slug == 'verified')
                        <div class="proven-organization">
                            <img src="{{ asset('images/check.png') }}" alt="image">
                            <span>Компанія перевірена</span>
                        </div>
                        @endif

                        <div class="about-company">
                            <div class="logo-organization">
                                <a href="{{ route('master.show', ['id' => $master->id])}}">
                                    <img class="border-img" src="{{ $master->getAvatarSrc() }}" alt="image"></a>
                            </div>
                            <div class="info-organization">
                                <div class="location-organization">
                                    <img src="{{ asset('images/location.svg') }}" alt="image">
                                    <span>{{ explode(',', $master->main_office->address)[0] }}</span>
                                </div>
                                <!-- /.location-organization -->

                                <div class="quantity-rewiews">
                                    <a class="review" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                        {{ $master->reviews_count }}</a>
                                    <p>відгуків
                                        @if($master->negative_reviews_count > 0)
                                        <a class="negative-review" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                            (-{{$master->negative_reviews_count}})</a></p>
                                        @endif
                                </div>
                                <!-- /.quantity-rewiews -->
                                <div class="in-process">
                                    <p>В процесі: <a href="{{ route('master.show.orders_in_progress', ['id' => $master->id]) }}">
                                            {{ $master->orders_in_progress_count}}</a></p>
                                </div>
                            </div>
                            <!-- /.info-organization -->
                        </div>
                        <div class="img-station-wrap">
                            @if($master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both')
                            <div class="sun-station tooltip-btn">
                                <img src="{{ asset('images/solar_installation.svg') }}" alt="image">
                                <span class="tooltip small-tooltip">Компанія будує домашні CEC</span>
                            </div>
                            @endif
                            @if($master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both')
                            <div class="terrestrial-station tooltip-btn">
                                <img src="{{ asset('images/terrestrial_station.svg') }}" alt="image">
                                <span class="tooltip small-tooltip">Компанія будує комерційні СЕС</span>
                            </div>
                            @endif
                        </div>
                    </div>

                <div class="catalog-organization">
                    <!--<div class="about-company-wrap"> -->
                    <div class="logo-organization">
                        <a href="{{ route('master.show', ['id' => $master->id])}}">
                            <img class="border-img" src="{{ $master->getAvatarSrc() }}" alt="image"></a>
                    </div>
                    <div class="about-organization">
                        <h3><a href="{{ route('master.show', ['id' => $master->id])}}">{{$master->user->name}}</a></h3>
                        @if($master->main_office->address)
                        <img src="{{ asset('images/location.png') }}" alt="image">
                        <span>{{ explode(',', $master->main_office->address)[0] }}</span>
                        @endif
                    </div>
                    <!--</div>-->
                    <!-- /.about-company-wrap -->

                    <div class="proven-organization-wrap">
                        @if($master->status->slug == 'verified')
                        <div class="proven-organization">
                            <img src="{{ asset('images/check.png') }}" alt="image">
                            <span>Компанія перевірена</span>
                        </div>
                        @endif
                    </div>

                    <div class="reviews-organization">
                        <div class="quantity-rewiews">
                            <a class="review" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                {{ $master->reviews_count }}</a>
                            <p>відгуків
                                @if($master->negative_reviews_count > 0)
                                <a class="negative-review" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                    (-{{$master->negative_reviews_count}})</a>
                                @endif
                            </p>
                        </div>
                        <!-- /.quantity-rewiews -->
                        <div class="in-process">
                            <p>В процесі: <a href="{{ route('master.show.orders_in_progress', ['id' => $master->id]) }}">
                                    {{ $master->orders_in_progress_count}}</a></p>
                        </div>

                    </div>
                    <div class="img-station-wrap">
                        @if($master->getSesTypes() == 'home_ses' || $master->getSesTypes() == 'both')
                        <div class="sun-station tooltip-btn">
                            <img src="{{ asset('images/solar_installation.svg') }}" alt="image">
                            <span class="tooltip small-tooltip">Компанія будує домашні CEC</span>
                        </div>
                        @endif
                        @if($master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'both')
                        <div class="terrestrial-station tooltip-btn">
                            <img src="{{ asset('images/terrestrial_station.svg') }}" alt="image">
                            <span class="tooltip small-tooltip">Компанія будує комерційні СЕС</span>
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>

            {{ $masters->links() }}
        </div>

    </div>
    @push('footer_scripts')
        <script>
            var masterIndexRoute = '{{ route('master.index') }}';
        </script>
        <script type="text/javascript" src="{{ asset('js/master/index.js') }}"></script>
    <script >
    </script>
    @endpush


@endsection

@section('site-title')
    {{ $site_title }}
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/select2/select2.min.css') }}"/>
@endpush