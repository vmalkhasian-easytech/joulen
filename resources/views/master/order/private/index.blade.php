@extends('layouts.master.app')

@section('content')
    <div id="content">

        <div class="container pageTitle withOption bordered">
            <h1>Закріплені за мною</h1>
        </div>

        <!--# contentBlock #-->
        <div class="contentBlock cabinet-page-wrap">
            <div class="container">
                <div class="table-block table-block-filter">
                    <div class="site_table applicationTable">
                        @if(count($orders) == 0)
                            <span>Замовлень не знайдено</span>
                        @endif
                        <table cellpadding="0" cellspacing="0" border="0" class="table home-ses-table">
                            <tbody>
                            @foreach($orders as $order)
                                <tr class="tr_id">
                                    <td>{{ $order->created_at }}</td>
                                    <td class="">
                                        <a href="{{ route('master.order.private.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
                                        <img class='open-table' src="<?php echo e(asset('images/arrow-prev.png')); ?>">
                                    </td>
                                    <td>
                                        {{--TODO: Phrase "Монтаж не потрібен" must be outputted in some conditions--}}
                                        <div class="table-address">
                                            <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    width="0.423cm" height="0.635cm">
                                                <path fill-rule="evenodd" fill="rgb(200, 203, 212)"
                                                      d="M10.242,1.847 C9.109,0.657 7.602,0.001 6.000,0.001 C4.397,0.001 2.891,0.657 1.757,1.847 C-0.340,4.050 -0.601,8.195 1.193,10.706 L6.000,17.998 L10.800,10.716 C12.600,8.195 12.340,4.050 10.242,1.847 ZM6.055,8.547 C4.848,8.547 3.865,7.514 3.865,6.246 C3.865,4.978 4.848,3.945 6.055,3.945 C7.263,3.945 8.245,4.978 8.245,6.246 C8.245,7.514 7.263,8.547 6.055,8.547 Z"/>
                                            </svg>{{ $order->city }}</div>
                                    </td>
                                    <td class=" textRight spacing_big-right">
                                        <div class="blue"><strong>${{ round($order->price) }}</strong></div>
                                    </td>
                                    <td class="">
                                        <span class="site_icon-status_{{ $order->status->slug }}">{{ $order->status->name }}</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--# contentBlock #-->
        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@section('site-title')
    Закріплені за мною
@endsection

@push('footer_scripts')
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush