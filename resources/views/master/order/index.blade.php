@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>
                @if(array_key_exists('category', $filter))
                    Замовлення на {{ $order_categories[$filter['category']]->name }}
                @else
                    Всі замовлення
                @endif</h1>
        </div>
        <div class="filter-order">
            <p>Фільтр:</p>
            <form method="GET" id="formsetfilter" action="/">
                @foreach($order_categories as $category)
                    @if($category->slug == 'ses')
                        <div class="filter-radio formsetfilter_type" data-category="{{ $category->slug }}">
                            <input {{ array_key_exists('category', $filter) && $filter['category'] == 'ses' ? 'checked' : ''}} id="all"
                                   type="radio" name="filter" value="all" {{ $master->getSesTypes() == 'both' ? 'checked' : ''}}
                                    {{ $master->getSesTypes() == 'commercial_ses' || $master->getSesTypes() == 'home_ses' ? 'disabled' : ''}}>
                            <label for="all"><span class="choose-ses">Всі CEC</span></label>
                        </div>
                        <img class='open-table' src="<?php echo e(asset('images/arrow-next.png')); ?>">
                        @foreach($category->subcategories as $subcategory)
                            @if($subcategory->slug == 'commercial')
                                <div class="filter-radio formsetfilter_type_subcategory"
                                     data-subcategory="{{ $subcategory->slug }}">
                                    <input {{ array_key_exists('subcategory', $filter) && $filter['subcategory'] == $subcategory->slug ? 'checked' : ''}} id="commercial"
                                           type="radio" name="filter" value="commercial" {{ $master->getSesTypes() == 'commercial_ses' ? 'checked' : ''}}
                                            {{ $master->getSesTypes() == 'home_ses' ? 'disabled' : ''}}>
                                    <label class="checked" for="commercial">Комерційні</label>
                                </div>
                            @endif
                        @endforeach
                        <div class="filter-radio formsetfilter_type_home" data-home="home">
                            <input {{ array_key_exists('home', $filter) && $filter['home'] ? 'checked' : ''}} id="home"
                                   type="radio" name="filter" value="home" {{ $master->getSesTypes() == 'home_ses' ? 'checked' : ''}}
                                    {{ $master->getSesTypes() == 'commercial_ses' ? 'disabled' : ''}}>
                            <label for="home">Домашні</label>
                        </div>
                    @endif
                @endforeach
                <div class="filter-checkbox">
                    Показувати тільки з тих регіонів, на які я
                    відмітився
                    <label class="switch" for="regionFilterChk">
                        <input id="regionFilterChk" type="checkbox" name="filter"
                               value="region_filter_on" {{ array_key_exists('region_filter_on', $filter) && $filter['region_filter_on'] ? 'checked' : ''}} />
                        <span class="slider round"></span>
                    </label>
                </div>
            </form>
        </div>
        <div class="cabinet-profile">
            <div class="cabinet-item">
                <div class="table-block table-block-filter">
                    @if(count($orders) == 0)
                        <span>Замовлень не знайдено</span>
                    @endif
                    <table class="table home-ses-table footable footable-1 breakpoint-lg" style="display: table;">
                        <tbody>
                        @foreach($orders as $order)
                            <tr class="tr_id">
                                <td class="type-ses" data-title="Дата" data-breakpoints="xs sm md"
                                    class="footable-first-visible"
                                    style="display: table-cell;">
                                    {{ $order->activated_at_no_time ?? $order->created_at }}
                                </td>
                                <td data-title="Тип" data-breakpoints="xs sm md" style="display: table-cell;">
                                    <a href="{{ route('master.order.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
                                    <img class='open-table' src="<?php echo e(asset('images/arrow-prev.png')); ?>">
                                </td>
                                <td class="table-address" data-title="Адреса" data-breakpoints="xs sm md"
                                    style="display: table-cell;">
                                    <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            width="0.423cm" height="0.635cm">
                                        <path fill-rule="evenodd" fill="rgb(200, 203, 212)"
                                              d="M10.242,1.847 C9.109,0.657 7.602,0.001 6.000,0.001 C4.397,0.001 2.891,0.657 1.757,1.847 C-0.340,4.050 -0.601,8.195 1.193,10.706 L6.000,17.998 L10.800,10.716 C12.600,8.195 12.340,4.050 10.242,1.847 ZM6.055,8.547 C4.848,8.547 3.865,7.514 3.865,6.246 C3.865,4.978 4.848,3.945 6.055,3.945 C7.263,3.945 8.245,4.978 8.245,6.246 C8.245,7.514 7.263,8.547 6.055,8.547 Z"/>
                                    </svg>
                                    {{--TODO: Phrase "Монтаж не потрібен" must be outputted in some conditions--}}
                                    {{ $order->city }}
                                </td>
                                <td data-title="Сумма" data-breakpoints="xs sm md" style="display: table-cell;">
                                    @if(count($order->requests) > 0)
                                        Ваша {{ $order->requests[0]->final == true ? 'остаточна' : 'попередня' }} заявка
                                        ${{ $order->requests[0]->price }}
                                    @endif
                                </td>
                                <td data-title="Кількість заявок" data-breakpoints="xs sm md"
                                    style="display: table-cell;">
                                    {{--TODO: get rid of if's--}}
                                    {{ $order->requests_count . ' ' }}
                                    @if($order->requests_count == 1)
                                        заявка
                                    @elseif($order->requests_count > 1 && $order->requests_count < 5)
                                        заявки
                                    @elseif($order->requests_count == 0 || $order->requests_count >= 5)
                                        заявок
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--<div class="pagination">--}}
                    {{--<ul>--}}
                    {{--<li class="active"><a href="#">1</a></li>--}}
                    {{--<li><a href="#">2</a></li>--}}
                    {{--<li><a href="#">3</a></li>--}}
                    {{--<li><a href="#">4</a></li>--}}
                    {{--<li><a href="#">5</a></li>--}}
                    {{--<li><a href="#">...</a></li>--}}
                    {{--<li><a href="#">25</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}

                    {{ $orders->links() }}
                </div>
            </div>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>

    @push('footer_scripts')
    <script>
        var masterOrderRoute = '{{ route("master.order.index") }}';
    </script>
    <script type="text/javascript" src="{{ asset('js/master/order/index.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
    @endpush
@endsection

@section('site-title')
    Список доступних замовлень
@endsection