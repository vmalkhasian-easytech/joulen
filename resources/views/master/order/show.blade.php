@extends('order.show')

@section('user-section')
    <div class="contentBlock listOfApp">
        <div class="container">
        @yield('client-contacts')

            {{--TODO: move request to external template--}}
            <h3>Ваша заявка</h3>
            <div>
                <div class="app-item">
                    <div class="app-item-author clr">
                        <div class="app-item-author_info clr">
                            <h3><a href="{{ route('master.show', ['id' => $request->master->id]) }}">{{ $request->master->user->name }}</a></h3>
                            <div class="app-item-reviews site_icon-reviews">Відгуків: <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="">{{ count($request->master->reviews) }}</a>
                                @if($request->master->negative_reviews_count > 0)
                                    <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="site_color-red" title="Негативні відгуки">(-{{ $request->master->negative_reviews_count }})</a>
                                @endif
                            </div>
                            <a href="{{ route('master.show', ['id' => $request->master->id]) }}" class="app-item-author_img">
                                <img src="{{ $request->master->getAvatarSrc() }}" alt="{{ $request->master->user->name }}">
                            </a>
                            {{--TODO: make url filter maker--}}
                            <div class="app-item-loc site_icon-location">
                                {{ $request->master->main_office->address ? explode(',', $request->master->main_office->address)[0] : $request->master->region->name }}
                            </div>
                        </div>
                        <div class="app-item-price">{{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }} <span>${{ $request->price }}</span></div>
                    </div>
                    <div class="app-item-content">
                        @foreach($request->prices as $price)
                            <h4>{{ $price->type->name }}: ${{ $price->price }}</h4>
                            <p style="margin-left:40px;white-space:pre-wrap;">{!! nl2br(e($price->comment)) !!}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection