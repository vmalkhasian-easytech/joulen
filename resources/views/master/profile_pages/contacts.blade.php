@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Контакти</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div class="cabinet-item">
            <form class="publicInfo" method="POST" id="contacts"
                  action="{{ route('master.profile_pages.contact.update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" value="2" name="profile_stage">
                <span class="show-error-hidden-file form-error" style="display: none">Обов’язково прикріпіть всі фото</span>
                <div class="form-block">
                    <label>Робочий номер телефону:</label>

                    <input class="mask-phone" data-validation-error-msg="Правильно заповніть це поле"
                           data-validation="number" data-validation-ignore="(,),-, ,+"
                           type="text" maxlength="19" name="work_phone"
                           value="{{ $master->work_phone }}"/>
                    <a class="add-link add-second-phone  {{ $master->second_work_phone ? 'hide' : ''}}">
                        + додати ще один номер телефону
                    </a>
                </div>
                <div class="form-block second_phone" {{ $master->second_work_phone ? 'style=display:block' : ''}}>
                    <label>Робочий номер телефону 2:</label>
                    <input class="mask-phone" type="text" maxlength="19"
                           data-validation="number" data-validation-ignore="(,),-, ,+"
                           name="second_work_phone"
                           value="{{ $master->second_work_phone }}">
                    <a class="remove-second-phone {{ $master->second_work_phone ? '' : 'hide'}}">
                        - видалити
                    </a>
                </div>

                <div class="form-block">
                    <label>Робочий e-mail</label>
                    <input type="text" data-validation="email" value="{{ $master->work_email }}"
                           name="work_email"/>
                </div>

                <div class="form-block">
                    <label>Сайт</label>
                    <input type="text" name="website" data-validation="required length"
                           data-validation-length="min2"
                           data-validation-error-msg="Правильно заповніть це поле"
                           value="{{ $master->website }}"/>
                </div>
                <div class="form-block profile-office-block">
                    <div class="profile-office-link">
                        <p><strong>Офіс:</strong></p>
                        <a id="main_office_added" class="add-link">Додати адресу головного офісу</a>
                        <input type="hidden" name="main_office_added">
                    </div>
                    <div class="profile-office-map {{ $master->main_office->id ? "" : "hide" }}"
                         id="office_map-1">
                        <p><strong>Адреса головного офісу</strong> <a class="hider site_icon-delete" data-hide-id="office_map-1"></a>
                        </p>
                        <div class="show-map-block">
                            <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                            <span class="error-block-office" style="display:none;color:red; text-align: left;">
                            Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                            </span>
                            <div class="gmap-block">

                                <input data-validation="required"
                                       data-validation-error-msg="Обов'язково заповніть поле Адреса головного офісу"
                                       id="pac-input-1"
                                       value="{{ $master->main_office->address }}"
                                       class="search-map-input pac-input"
                                       type="text" placeholder="Пошук по місту">
                                <div id="gmap-1" class="map img-border"></div>
                            </div>
                            <div class="office-input">
                                <input name="main_office_address"
                                       type="text" id="output-1" class="office_output"
                                       value="{{ $master->main_office->address }}" readonly>
                                <label for="main_office_room">
                                    Офіс №
                                </label>
                                <input name="main_office_room" id="main_office_room"
                                       value="{{ $master->main_office->room }}" class="controls office_room"
                                       type="text">
                            </div>

                            <input type="hidden" name="main_office_region" class="office-region" value="{{ $master->main_office->region_id ?
                             explode(' ', $regions->find($master->main_office->region_id)->name )[0] : ''}}">
                            <input type="hidden" class="geo_coords_lat" id="lat-1" name="main_office_lat"
                                   value="{{ $master->main_office->lat }}">
                            <input type="hidden" class="geo_coords_lng" id="lng-1" name="main_office_lng"
                                   value="{{ $master->main_office->lng }}">
                        </div>


                        <div class="companyLogoBlock">
                            <div>
                                <strong>
                                    <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                </strong>
                            </div>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="main_office_photo"
                                           name="main_office_photo" style="display:none;"><br>
                                    <img src="{{ $master->main_office->getPictureSrc() }}" id="main_office_photo_img">
                                </div>
                                <div>
                                    <div id="main_office_photo_download" class="site_icon-download">Завантажити
                                    </div>
                                </div>
                                <div>
                                    <a href="" id="main_office_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" id="is_deleted_main_office_photo" value=""
                                           name="is_deleted_main_office_photo"
                                           style="display:none;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="plus_second_map">
                        <a id="second_office_added" class="add-link">+ Додати ще один офіс</a>
                        <input type="hidden" name="second_office_added">
                    </div>
                    <div class="profile-office-map {{ $master->second_office->id ? "" : "hide" }}"
                         id="office_map-2">
                        <p><strong>Офіс №2:</strong><a class="hider site_icon-delete" data-hide-id="office_map-2"></a>
                        </p>
                            <div class="show-map-block">
                                <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                    Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                </span>
                                <div class="gmap-block">
                                    <!-- /.marker-info -->
                                    <input data-validation="required"
                                           data-validation-error-msg="Обов'язково заповніть поле Офіс №2"
                                           value="{{ $master->second_office->address }}"
                                           id="pac-input-2"
                                           class="controls pac-input search-map-input" type="text"
                                           placeholder="Пошук по місту">
                                    <div id="gmap-2" class="map img-border"></div>
                                </div>
                                <div class="office-input">
                                    <input name="second_office_address"
                                           type="text" id="output-2" class="office_output " readonly
                                           value="{{ $master->second_office->address }}">
                                    <label for="main_office_room">
                                        Офіс №
                                    </label>
                                    <input name="second_office_room" id="second_office_room"
                                           value="{{ $master->second_office->room }}"
                                           class="controls office_room" type="text">
                                </div>

                                <input type="hidden" name="second_office_region"  class="office-region" value="{{ $master->second_office->region_id ?
                                    explode(' ', $regions->find($master->second_office->region_id)->name )[0] : ''}}">
                                <input type="hidden" class="geo_coords_lat" id="lat-2" name="second_office_lat"
                                       value="{{ $master->second_office->lat }}">
                                <input type="hidden" class="geo_coords_lng" id="lng-2" name="second_office_lng"
                                       value="{{ $master->second_office->lng }}">
                            </div>

                        <div class="companyLogoBlock">
                            <div>
                                <strong>
                                    <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                </strong>
                            </div>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id="second_office_photo"
                                           name="second_office_photo" style="display: none"><br>
                                    <img src="{{ $master->second_office->getPictureSrc() }}" id="second_office_photo_img">
                                </div>
                                <div>
                                    <div id="second_office_photo_download" class="site_icon-download">
                                        Завантажити
                                    </div>
                                </div>
                                <div>
                                    <a href="" id="second_office_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" id="is_deleted_second_office_photo" value=""
                                           name="is_deleted_second_office_photo"
                                           style="display:none;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="plus_third_map">
                        <a id="third_office_added"><u>Додати ще один офіс</u></a>
                        <input type="hidden" name="third_office_added">
                    </div>
                    <div class="profile-office-map {{ $master->third_office->id ? "" : "hide" }}"
                         id="office_map-3">
                        <p><strong>Офіс №3:</strong>
                            <a class="hider site_icon-delete" data-hide-id="office_map-3"></a></p>
                            <div class="show-map-block">
                                <span class="showgmap">Вкажіть точне розміщення вашого офісу на карті:</span>
                                <span class="error-block-office" style="display:none;color:red; text-align: left;">
                                    Будь-ласка, вкажіть точне розміщення офісу, пересунувши маркер на карті або вибравши адресу із випадаючого списку.
                                </span>
                                <div class="gmap-block">
                                    <!-- /.marker-info -->
                                    <input data-validation="required"
                                           data-validation-error-msg="Обов'язково заповніть поле Офіс №3"
                                           value="{{ $master->third_office->address }}"
                                           id="pac-input-3"
                                           class="controls pac-input search-map-input" type="text"
                                           placeholder="Пошук по місту">
                                    <div id="gmap-3" class="map img-border"></div>
                                </div>
                                <div class="office-input">
                                    <input name="third_office_address"
                                           type="text" id="output-3" class="office_output" readonly
                                           value="{{ $master->third_office->address }}">
                                    <label for="main_office_room">
                                        Офіс №
                                    </label>
                                    <input name="third_office_room" id="third_office_room"
                                           value="{{ $master->third_office->room }}"
                                           class="controls office_room" type="text">
                                </div>

                                <input type="hidden" name="third_office_region" class="office-region" value="{{ $master->third_office->region_id ?
                                    explode(' ', $regions->find($master->third_office->region_id)->name )[0] : ''}}">
                                <input type="hidden" class="geo_coords_lat" id="lat-3" name="third_office_lat"
                                       value="{{ $master->third_office->lat }}">
                                <input type="hidden" class="geo_coords_lng" id="lng-3" name="third_office_lng"
                                       value="{{ $master->third_office->lng }}">
                            </div>

                        <div class="companyLogoBlock">
                            <div>
                                <strong>
                                    <span>Прикріпіть фото вашого офісу (всередині або з вулиці)</span>
                                </strong>
                            </div>
                            <div class="logoImgBlock">
                                <div class="logoFile">
                                    <input type="file" class="hidden_validate_file" id='third_office_photo'
                                           name="third_office_photo" style="display: none;"><br>
                                    <img src="{{ $master->third_office->getPictureSrc() }}" id="third_office_photo_img">
                                </div>
                                <div>
                                    <div id="third_office_photo_download" class="site_icon-download">Завантажити
                                    </div>
                                </div>
                                <div>
                                    <a href="" id="third_office_photo_delete"
                                       class="site_icon-delete remove-avatar">Видалити</a>
                                    <input type="checkbox" id="is_deleted_third_office_photo" value=""
                                           name="is_deleted_third_office_photo"
                                           style="display:none;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class='btn btn-primary' id="contacts-confirm">Зберегти
                </button>
                <a class='btn btn-secondary step_back'>Скасувати</a>
            </form>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
<script>
    var markerSrc = '{{ asset('images/map-marker.png') }}',
        masterOffices = {!! $master->offices !!};
</script>
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
<script src="{{ asset('js/master/mask.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile_pages/contacts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
        defer></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>
@endpush
