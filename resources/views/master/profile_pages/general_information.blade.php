@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Профіль компанії</h1>
        </div>

        @component('layouts.validation_errors')
        @endcomponent

        <div class="cabinet-item">
            <form method="POST" id="general-info"
                  action="{{ route('master.profile_pages.general.update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" value="1" name="profile_stage">
                <div class="form-block">
                    <label for="company_name">
                        Назва компанії
                    </label>
                    <input data-validation-error-msg="Введіть назву компанії - мінімум 3 символи"
                           data-validation="length" data-validation-length="min3"
                           id="company_name" type="text" value="{{ $master->user->name }}"
                           name="company_name" class="company-name">

                </div>
                <div class="form-block">
                    <label for="master_avatar">
                        Ваш логотип файли <span class="download-size">(jpg та png розміром не менше 100х100px)</span>
                    </label>
                    <span class="show-error form-error" style="display: none">Обов’язково прикріпіть логотип вашої компанії</span>
                    <div class="logoImgBlock">
                        <div class="logoFile">
                            <input type="file" class="file-upload hidden_validate_file" id="master_avatar"
                                   name="master_avatar" style="display: none">
                            <img src="{{ $master->getAvatarSrc() }}" id="master_avatar_img">
                        </div>
                        <div>
                            <div id="master_avatar_download" class="site_icon-download">Завантажити</div>
                        </div>
                        <div>
                            <a href="" id="master-avatar-delete"
                               class="site_icon-delete remove-avatar">Видалити</a>
                            <input type="checkbox" checked id="is_deleted_master_avatar" value=""
                                   name="is_deleted_master_avatar"
                                   style="display:none;">
                        </div>
                    </div>
                </div>

                <div class="form-block">
                    <label for="about">
                        Про компанію
                    </label>

                    <textarea data-validation="length"
                              data-validation-error-msg="У цьому полі можна ввести не більше 2500 символів"
                              data-validation-length="max2500" name="about"
                              id="about">{{ $master->about }}</textarea>
                </div>

                <button class='btn btn-primary' type="submit">Зберегти</button>
                <a class='btn btn-secondary step_back'>Скасувати</a>
            </form>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
<script src="{{ asset('libs/formvalidator/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/profile_pages/general_information.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/validator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('libs/formvalidator/theme-default.css') }}"/>
@endpush
