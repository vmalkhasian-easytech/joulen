@extends('layouts.old.app')

@section('content')
    <div id="content">

        <div class="container pageTitle">
            <h1>Публічна інформація про Вас</h1>
        </div>

        <!--# contentBlock #-->
        <div class="contentBlock">
            <div class="container">
                <form class="publicInfo" method="post" action="{{ route('master.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li style="color: red; list-style: none; margin-top: 10px;">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="publicInfoChoice">
                        <span>Ви:</span>
                        <div class="clr">
                            <div class="checkboxChoices">
                                <input type="checkbox" id="isCompany" value="1" name="is_company" {{ old('is_company') ? 'checked' : '' }} />
                                <label for="isCompany">
                                    <div class="clr">
                                        <span>Приватний спеціаліст</span>
                                        <span>Компанія</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-person">
                            <div class="label">
                                <span class="companyLabel">Назва компанії</span>
                                <span class="privateLabel">Ім'я, Прізвище</span>
                            </div>
                            <div class="field">
                                <input type="text" value="{{ old('name') }}" name="name" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-phone_gray">
                            <div class="label">Телефон</div>
                            <div class="field">
                                <input id="phone-number" class="mask-phone" type="text" maxlength="19" name="phone" value="{{ old('phone') }}" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-mail_gray">
                            <div class="label">Робочий e-mail</div>
                            <div class="field">
                                <input type="text" value="{{ old('work_email') }}" name="work_email" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-link">
                            <div class="label">Сайт</div>
                            <div class="field">
                                <input type="text" value="{{ old('site') }}" name="site" />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-loc">
                            <div class="label">
                                <span class="companyLabel">Де знаходиться ваш центральний офіс</span>
                                <span class="privateLabel">Звідки ви?</span>
                            </div>
                            <div class="field">
                                <select name="location" class="custom_select hidden_validate">
                                    <option disabled selected data-display="Виберіть область">Виберіть область</option>
                                    @foreach ($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="services_block">
                        <input type="checkbox"
                               name="services[ses]" value="1"
                               class="styledChk servicesChk" id="ses" checked>
                    </div>
                    <div>
                        <div class="site_icon-info">
                            <div class="label">
                                <span class="companyLabel">Про компанію</span>
                                <span class="privateLabel">Про вас</span>
                            </div>
                            <span class="field_info">Не більше 500 символів</span>
                            <div class="field">
                                <textarea name="about">{{ old('about') }}</textarea>
                                <div class="companyLogoBlock">
                                    <div>
                                        <strong>
                                            <span class="companyLabel">Ваш логотип</span>
                                            <span class="privateLabel">Ваше фото</span>
                                        </strong> файли jpg та png розміром не менше 100х100px
                                    </div>
                                    <div class="logoImgBlock">
                                        <div class="logoFile">
                                            <input type="file" class="hidden_validate_file" id="your_photo" name="master_avatar"  style="display:none;"><br>
                                            <img src="{{ asset('images/specialist.png') }}" alt="" id="avatar">
                                        </div>
                                        <div>
                                            <div class="site_icon-download">Завантажити</div>
                                        </div>
                                        <div>
                                            <a href="" class="site_icon-delete remove-avatar">Видалити</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="site_icon-map">
                            <div class="label">В яких областях ви готові надавати свої послуги</div>
                            <div class="field clr checkbox-validation-block">
                                @foreach ($regions as $region)
                                    <div class="col3">

                                        <input type="checkbox" value="{{ $region->id }}" name="work_location[{{ $region->id }}]" class="styledChk regionChk" id='{{ $region->slug }}'
                                                {{--@if( is_array(old('work_location') ) && in_array(1, old('work_location'))) checked @endif  --}}
                                        />
                                        <label for="{{ $region->slug }}">{{ $region->name }}</label>
                                    </div>
                                @endforeach
                                <input type="checkbox" style="display: none" name="validation-work-location" class="validation-location">
                            </div>
                        </div>
                    </div>


                    <div class="container controlBtn textCenter">
                        <input type="submit" class="site_button-orange validation-btn" value="Зберегти">
                        <a href="#" class="site_button-orange onlyBorder">Скасувати</a>
                        {{--<a href="#" class="site_button-orange">Зберігти</a>--}}

                    </div>
                </form>
            </div>
        </div>
        <!--# end contentBlock #-->
    </div>


    @push('footer_scripts')

        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
        <script src="{{ asset('js/jquery.nice-select.js') }}"></script>
        <script src="{{ asset('js/master/edit-validation-rules.js') }}"></script>
        <script src="{{ asset('js/master/master.js') }}"></script>
        <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
        <script src="{{ asset('js/client-register/phoneMask.js') }}"></script>
        <script>
            var defaultAvatar = '{{ asset('images/specialist.png') }}';
        </script>
    @endpush

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('css/nice-select.css') }}"/>
    @endpush
@endsection
