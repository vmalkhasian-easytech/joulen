@extends('layouts.old.app')
@section('content')


<!--# contentWrapper #-->
<div id="content">

    <!--# contentBlock #-->
    <div class="contentBlock searchProfHeader">
        <div class="container">

            <h1>Рахунки за компослуги можна скоротити в 2 рази!</h1>

        </div>
    </div>
    <!--# end contentBlock #-->

    <div class="contentBlock">
        <div class="container">
            <h2 class="search_auditor_title">Де потрібно провести енергоаудит?</h2>
            <div class="search_auditor_wrap">
                <div class="search_auditor_item">
                    <a href="{{ route('client.order.create', ['category' => 'energy_audit', 'subcategory' => 'private']) }}">
                        <img src="{{ asset('images/pic-privat-house.png') }}" alt="img">
                        <p>Приватний будинок</p>
                    </a>
                </div>
                <div class="search_auditor_item">
                    <a href="{{ route('client.order.create', ['category' => 'energy_audit', 'subcategory' => 'osbb']) }}">
                        <img src="{{ asset('images/pic-osbb.png') }}" alt="img">
                        <p>ОСББ</p>
                    </a>
                </div>
                <div class="search_auditor_item">
                    <a href="{{ route('client.order.create', ['category' => 'energy_audit', 'subcategory' => 'municipal']) }}">
                        <img src="{{ asset('images/pic-budget.png') }}" alt="img">
                        <p>Бюджетний заклад</p>
                    </a>
                </div>
                <div class="search_auditor_item">
                    <a href="{{ route('client.order.create', ['category' => 'energy_audit', 'subcategory' => 'commerce']) }}">
                        <img src="{{ asset('images/pic-trade-office.png') }}" alt="img">
                        <p>Торгово-офісний центр</p>
                    </a>
                </div>
                <div class="search_auditor_item">
                    <a href="{{ route('client.order.create', ['category' => 'energy_audit', 'subcategory' => 'industrial']) }}">
                        <img src="{{ asset('images/pic-factory.png') }}" alt="img">
                        <p>Промислове підприємство</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
<!--# end contentWrapper #-->


@endsection

@section('site-title')
    Пошук енергоаудитора в Україні. Дізнатись ціни
@endsection
