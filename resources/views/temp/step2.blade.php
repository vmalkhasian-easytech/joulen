@extends('layouts.app')

@section('content')
<div class="home-station-wrap select-category">
    <div class="container">
        <div class="home-station select-station">
            <h2 class="gray">Який тип домашньої станції вас цікавить?</h2>
            <div class="home-types-station">
                <!-- /.home-types-station -->
                <a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'network']) }}" class="home-station-items">
                    <div class="voltage-station">
                        <img src="{{ asset('images/merezheva_ses.svg') }}" alt="image">
                        <h3>Мережева</h3>
                        <p>Для продажу електрики по «зеленому тарифу», та для власного споживання.
                            Без акумуляторів</p>
                    </div>
                    <!-- /.network-station -->
                </a>
                <a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'autonomous']) }}" class="home-station-items">
                    <div class="voltage-station">
                        <img src="{{ asset('images/avtonomna_ses.svg') }}" alt="image">
                        <h3>Автономна</h3>
                        <p>Використовується коли нема підключення до центральної електромережі.
                            Потрібні акумулятори</p>
                    </div>
                    <!-- /.network-station -->
                </a>
                <a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'hybrid']) }}" class="home-station-items">
                    <div class="voltage-station">
                        <img src="{{ asset('images/gibridna_ses.svg') }}" alt="image">
                        <h3>Гібридна</h3>
                        <p>Використовується коли електромережа нестабільна. Надлишки електроенергії продаються.
                            Потрібні аккумулятори</p>
                    </div>
                    <!-- /.network-station -->
                </a>
            </div>
            <!-- /.home-ses-items -->
        </div>
        <!-- /.home-ses -->
    </div>
    <!-- /.container -->
</div>
@endsection

@section('site-title')
    Сонячні електростанції для дому: ціни, каталог монтажників. Отримання зеленого тарифу
@endsection