@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="distributor-page">
            <div class="distributor-container">
                <div class="distributor-main-info">
                    <div class="distributor-logo border-gray">
                        <img src="{{ $distributor->getAvatarSrc() }}" alt="image">
                    </div>
                    <!-- /.distributor_logo -->
                    <div class="distributor-info">
                        <h1 class="distributor_name">
                            {{ $distributor->company_name }}
                            @can('edit', $distributor)
                                <a class="site-icon-edit" href="{{ route('admin.distributors.edit', $distributor->id)}}"></a>
                            @endcan
                        </h1>
                        <!-- /.distributor_name -->
                        <p class="distributor-description">
                            Офіційний дистриб’ютор в Україні
                        </p>
                        <!-- /.distributor_info -->
                    </div>
                    <!-- /.distributor_info -->

                    <div class="distributor-sidebar">
                        <div class="distributor-sidebar_item distributor-check">
                            <div class="svg-icon-wrap">
                                <svg class="green_mark"
                                     width="73px" height="73px">
                                    <path fill-rule="evenodd" fill="rgb(146, 179, 44)"
                                          d="M50.835,48.835 C59.036,40.634 59.036,27.342 50.835,19.140 C42.634,10.939 29.342,10.939 21.141,19.140 C12.939,27.342 12.939,40.634 21.141,48.835 C29.342,57.036 42.642,57.036 50.835,48.835 ZM28.459,30.779 L33.181,35.501 L43.525,25.165 L47.184,28.824 L36.849,39.160 L33.181,42.819 L29.522,39.160 L24.800,34.438 L28.459,30.779 Z"/>
                                </svg>
                            </div>
                            <span class="distributor-check_title">Офіційний дистриб’ютор!</span>
                            <p>Виробники обладнання надали письмове підтвердження, що ця компанія є їхнім офіційним дистриб'ютором в Україні.</p>
                        </div>

                        <div class="distributor-sidebar_item distributor-contacts">

                            <svg class="distributor-contacts_icon"
                                    width="56px" height="56px">
                                <path fill-rule="evenodd" fill="rgb(240, 165, 79)"
                                      d="M27.499,37.999 C20.596,37.999 15.000,32.403 15.000,25.500 C15.000,18.596 20.596,13.000 27.499,13.000 C34.403,13.000 40.000,18.596 40.000,25.500 C40.000,32.403 34.403,37.999 27.499,37.999 ZM32.579,31.094 L32.579,20.215 L32.579,19.351 C32.579,18.873 32.192,18.486 31.715,18.486 L30.850,18.486 L26.079,18.486 L23.069,18.486 C22.114,18.486 21.340,19.260 21.340,20.215 L21.340,31.094 C21.340,32.049 22.114,32.823 23.069,32.823 L26.079,32.823 L30.850,32.823 L31.715,32.823 C32.192,32.823 32.579,32.436 32.579,31.959 L32.579,31.094 ZM34.308,31.094 L34.308,20.215 L34.308,19.351 C34.308,18.873 33.921,18.486 33.444,18.486 L33.210,18.486 C33.358,18.741 33.444,19.036 33.444,19.351 L33.444,20.215 L33.444,31.094 L33.444,31.959 C33.444,32.274 33.358,32.568 33.210,32.823 L33.444,32.823 C33.921,32.823 34.308,32.436 34.308,31.959 L34.308,31.094 ZM29.227,30.543 L26.959,30.543 L24.692,30.543 C23.608,30.543 23.976,29.551 23.976,28.326 C23.976,27.101 25.600,26.108 26.684,26.108 L26.959,26.108 L27.235,26.108 C28.318,26.108 29.943,27.101 29.943,28.326 C29.943,29.551 30.311,30.543 29.227,30.543 ZM26.959,25.833 C25.507,25.833 24.983,24.532 24.983,23.441 C24.983,22.349 25.868,21.464 26.959,21.464 C28.051,21.464 28.936,22.349 28.936,23.441 C28.936,24.532 28.555,25.833 26.959,25.833 Z"/>
                            </svg>
                            <span class="distributor-contacts_title">Контакти</span>
                            <ul class="distributor-contact_list">
                                @if(is_null($distributor->second_phone))
                                    <li>Телефон: <span class="contact-number mask-phone">{{ $distributor->phone }}</span></li>
                                @else
                                    <li>Телефон 1: <span class="contact-number mask-phone">{{ $distributor->phone }}</span></li>
                                    <li>Телефон 2: <span
                                                class="contact-number mask-phone">{{ $distributor->second_phone }}</span></li>
                                @endif
                                <li>E-mail: <span>{{ $distributor->corporate_email }}</span></li>
                                @if ($distributor->website)
                                    <li>Сайт: <a class="cut-website" target="_blank" rel="nofollow"
                                                 href="{{ $distributor->website }}">{{ $distributor->website }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- /.sidebar -->

                    <p class="distributor-text">{!! nl2br($distributor->description) !!}</p>
                    <!-- /.distributor_text -->
                </div>
                @if ($distributor->inverters->isNotEmpty() || $distributor->panels->isNotEmpty())
                    <div class="company-distributor">
                    <h2>Компанія є офіційним дистриб’ютором брендів</h2>
                    @if($distributor->panels->isNotEmpty())
                        <div class="company-distributor_brand border-gray">
                        <h3 class="company-distributor_type">Сонячні панелі</h3>
                        <!-- /.company-distributor_type -->
                        <div class="company-distributor_items">
                            @foreach($distributor->panels as $panel)
                                <div class="company-distributor_item">
                                    <a href="{{ route('brand.show', ['equipment_type' => 'solar-panel',
                                    'brand_slug' => $panel->slug]) }}" class="company-distributor_logo border-gray">
                                        <img src="{{ $panel->logo ? $panel->logo->getPath(true) : asset('images/default_photo.jpg') }}" alt="image">
                                    </a>
                                    <!-- /.border-gray -->
                                    <a href="{{ route('brand.show', ['equipment_type' => 'solar-panel',
                                    'brand_slug' => $panel->slug]) }}" class="company-distributor_name">{{ $panel->title }}</a>
                                    <!-- /.company-distributor_name -->
                                </div>
                            @endforeach
                        </div>
                        <!-- /.company-distributor_item -->
                    </div>
                    @endif
                    <!-- /.company_distributor_brand -->
                    @if($distributor->inverters->isNotEmpty())
                        <div class="company-distributor_brand border-gray">
                        <h3 class="company-distributor_type">Інвертори</h3>
                        <!-- /.company-distributor_type -->
                        <div class="company-distributor_items">
                            @foreach($distributor->inverters as $inverter)
                                <div class="company-distributor_item">
                                    <a href="{{ route('brand.show', ['equipment_type' => 'inverter',
                                        'brand_slug' => $inverter->slug]) }}" class="company-distributor_logo border-gray">
                                        <img src="{{ $inverter->logo ? $inverter->logo->getPath(true) : asset('images/default_photo.jpg') }}" alt="image">
                                    </a>
                                    <!-- /.border-gray -->
                                    <a href="{{ route('brand.show', ['equipment_type' => 'inverter',
                                        'brand_slug' => $inverter->slug]) }}" class="company-distributor_name">{{ $inverter->title }}</a>
                                    <!-- /.company-distributor_name -->
                                </div>
                            @endforeach
                        </div>
                        <!-- /.company-distributor_item -->
                    </div>
                    @endif
                    <!-- /.company_distributor_brand -->
                </div>
                @endif
                <!-- /.company_distributor -->

                @if ($masters->isNotEmpty())
                    <div class="company-dealers" style="margin-top: 50px;">
                    <h2>Регіональні дилери компанії “{{ $distributor->company_name }}”</h2>
                    <div class="company-dealer_items">
                        @foreach($masters as $master)
                            <div class="company-dealer_item dealer-item-{{ $master->id }}">
                            <a href="{{ route('master.show', ['id' => $master->id])}}" class="company-dealer_name hide-lg">{{ $master->user->name }}</a>
                            <div class="company-dealer_group">
                                <a href="{{ route('master.show', ['id' => $master->id])}}" class="company-dealer_logo border-gray">
                                    <img src="{{ $master->getAvatarSrc() }}" alt="image">
                                </a>
                                <!-- /.company-dealer_logo -->
                                <div class="company-dealer_info">
                                    <a href="{{ route('master.show', ['id' => $master->id])}}" class="company-dealer_name hide-sm">{{ $master->user->name }}</a>
                                    <!-- /.company-dealer_name -->
                                    <div class="company-dealer_location hide-sm">
                                        <img class="icon-location" src="{{ asset('images/location.png') }}" alt="image">
                                        <span class="company-dealer_city">{{ explode(',', $master->main_office->address)[0] }}</span>
                                    </div>
                                    <!-- /.company-dealer_location -->
                                </div>
                                <!-- /.company-dealer_info -->
                            </div>
                            <!-- /.company-dealer_group -->
                            <div class="company-dealer_group">
                                <div class="company-dealer_review">
                                    <div class="company-dealer_location hide-lg">
                                        <img class="icon-location" src="{{ asset('images/location.png') }}" alt="image">
                                        <span class="company-dealer_city">{{ explode(',', $master->main_office->address)[0] }}</span>
                                    </div>
                                    <div class="dealer-quantity_reviews">
                                        <a class="dealer-quantity_review" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                            {{ $master->reviews_count }}</a>
                                        <p>відгуків
                                            @if($master->negative_reviews_count > 0)
                                                <a class="dealer-quantity_review_negative" href="{{ route('master.show.reviews', ['id' => $master->id]) }}">
                                                    (-{{ $master->negative_reviews_count }})</a>
                                            @endif
                                        </p>
                                    </div>
                                    <div class="dealer_in-process">
                                        <p>В процесі: <a href="{{ route('master.show.orders_in_progress', ['id' => $master->id]) }}">
                                                {{ $master->orders_in_progress_count }}</a></p>
                                    </div>
                                    <!-- /.company-dealer_project -->
                                </div>
                                <!-- /.company-dealer_review -->
                            </div>
                        </div>
                        @endforeach
                    </div>

                    @if ($masters->count() > 20)
                        <div id="pagination-container" class="pagination"></div>
                    @endif
                    <!-- /.company-dealer_items -->
                </div>
                @endif
                <!-- /.company-dealers -->
            </div>
            <!-- /.distributor-container -->
        </div>
    </div>
@endsection

@section('site-title')
    “{{ $distributor->company_name }}”: вся інформація про дистриб’ютора, бренди, дилери, відгуки, контакти
@endsection

@section('site-meta-description')
    Ознайомтеся із діяльністю офіційного дистриб’ютора “{{ $distributor->company_name }}”: контакти, список регіональних дилерів, брендами обладнання
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('libs/pagination.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/client-register/phoneMask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/distributor/show.js') }}"></script>
    <script type="text/javascript">
        var dealers = {!! $masters !!};
    </script>
@endpush