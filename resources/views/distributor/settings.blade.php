@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <div class="page-title">
            <h1>Параметри облікового запису</h1>
        </div>

        <div class="cabinet-item private-item">
            <div class="change-pass-link form-block">
                <a href="{{ route('password.change') }}" class="site_icon-edit">Змінити пароль</a>
            </div>

            <form id="formparams" method="POST" action="{{ route('distributor.settings.update') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-block">
                    <label>E-mail:</label>
                    <input type="text" value="{{ Auth::user()->email }}" disabled=""/>
                </div>

                <div class="change-pass-link form-block">
                    @include('auth.new_attach_social_account')
                </div>
            </form>
        </div>

        <div id="hellopreloader">
            <div id="hellopreloader_preload">
                <p class="preloader">Зачекайте, іде завантаження</p>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/master/settings/private.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/preloader.js') }}"></script>
@endpush
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/new-site.css') }}"/>
@endpush

@section('site-title')
    Параметри облікового запису
@endsection