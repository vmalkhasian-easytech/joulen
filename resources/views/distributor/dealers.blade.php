@extends('layouts.master.app')

@section('content')
    <div class="cabinet-page-wrap">
        <h1 class="main-title">Мої дилери</h1>
        <!-- /.main-title -->
        <div class="wrapper-page">
            <form action="{{ route('distributor.masters.update') }}" method="POST">
                {{ csrf_field() }}
                <div class="dealers">
                    <ul class="dealers-list border-gray">
                        @foreach($masters as $master)
                            <li class="dealers-list_item">
                                <input type="checkbox" id="dealer{{$loop->index}}" name="masters[]" value="{{ $master->id }}"
                                    {{ $selected_master_ids->contains($master->id) ? 'checked' : '' }}>
                                <label class="custom-checkbox border-gray" for="dealer{{$loop->index}}"></label>

                                <label for="dealer{{$loop->index}}">{{ $master->user->name }}</label>
                            </li>
                        @endforeach
                    </ul>
                    <!-- /.dealers-list -->
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                    <!-- /.btn-primary -->
                </div>
            </form>
            <!-- /.my-dealers -->
        </div>
    </div>
    <!-- /.cabinet-page-wrap -->
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-style.min.css') }}"/>
@endpush