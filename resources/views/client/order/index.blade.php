@extends('layouts.old.app')

@section('content')
    <div id="content">
        <div class="container">
            <div class="order_list_wrap">
                <div class="order_list client_order_list">
                    <div class="container pageTitle bordered">
                        <h1>Мої проекти</h1>
                        <div class="additionalBlock">
                            {{--TODO: change to right url when order creation will be implemented--}}
                            <a href="{{url('/user/search/step1')}}" class="site_button-orange button-small onlyBorder withSym">
                                <span class="orange">+</span> Новий проект для оцінки
                            </a>
                        </div>
                    </div>
                    <div class="contentBlock">
                        <div class="container">
                            <div>
                                <div class="site_table">
                                    @if(count($orders) == 0)
                                        <span>Замовлень не знайдено</span>
                                    @endif
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td class="td_date">{{ $order->created_at }}</td>
                                                <td class="">
                                                    <a href="{{ route('client.order.show', ['id' => $order->id]) }}">{{ $order->title }}</a>
                                                </td>
                                                <td class="">
                                                    {{--TODO get rig of ifs--}}
                                                    <a href="{{ route('client.order.show', ['id' => $order->id]) . '#master' }}">
                                                        {{ $order->requests_count }}
                                                        @if($order->requests_count == 1)
                                                        {{ ' заявка' }}
                                                        @elseif($order->requests_count > 1 && $order->requests_count < 5 )
                                                            {{ ' заявки' }}
                                                        @else
                                                            {{ ' заявок' }}
                                                        @endif
                                                    </a>
                                                </td>
                                                <td class=" textRight spacing_big-right">
                                                    @if($order->status->slug == 'done' || $order->status->slug == 'in_progress')
                                                        <div class="blue"><strong>${{ round($order->price) }}</strong></div>
                                                    @endif
                                                </td>
                                                <td class="td_status textRight">
                                                    <span class="projectStatus site_icon-status_{{ $order->status->slug }}">{{ $order->status->name }}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{ $orders->links() }}
                </div>
                <div style="display: none" class="pageTitle-sorting">
                    <form method="GET" id="formsetfilter" action="/">
                        <h3>Фільтр:</h3>
                        <ul>
                            <li>
                                <a class="formsetfilter_type {{ !array_key_exists('category', $filter) ? 'active' : '' }}"
                                   href="{{ route('client.order.index') }}" data-category="all">
                                    Всі замовлення
                                    <span></span>
                                </a>
                            </li>
                            @foreach($order_categories as $category)
                                <li>
                                    <a class="formsetfilter_type {{ array_key_exists('category', $filter) && $filter['category'] == $category->slug ? 'active' : ''}}"
                                       href="{{ route('client.order.index') }}" data-category="{{ $category->slug }}">
                                        {{ $category->name }}
                                        <span></span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('footer_scripts')
        <script type="text/javascript" src="{{ asset('js/client/order/index.js') }}"></script>
    @endpush
@endsection

@section('site-title')
    Мої проекти
@endsection