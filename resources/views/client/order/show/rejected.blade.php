@extends('client.order.show')

@section('buttons')
    <div class="contentBlock bordered refusedModerator">
        <div class="container">
            <h3><span>Проект відхилено модератором</span></h3>
            <div>
                <p></p>
            </div>
        </div>
    </div>

    @component('layouts.client.order.buttons', ['order' => $order])
        @slot('button')
            <a href="{{ route('client.order.run.checking', ['id' => $order->id]) }}" class="site_button-orange">Подати повторно</a>
        @endslot
    @endcomponent
@endsection