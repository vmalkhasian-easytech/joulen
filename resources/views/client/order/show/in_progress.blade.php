@extends('client.order.show')

@section('selected-master-request')
    <div class="contentBlock" id="master">
        <div class="container">
            <h3>Виконавець</h3>
        </div>
    </div>

    @component('layouts.order.request', ['request' => $order->request_by_master])
        @slot('final')
        @endslot

        @slot('buttons')
            @can('leave_review', $order)
                <form action="{{ route('client.review.create') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
                    <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
                    <input type="submit" class="site_button-orange button-small leaveFeedback" value="Залишити відгук"/>
                </form>
            @endcan
        @endslot
    @endcomponent
@endsection

@section('order-header-additional-block')
    @can('leave_review', $order)
        <form action="{{ route('client.review.create') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
            <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
            <input type="submit" class="site_button-orange button-small leaveFeedback" value="Залишити відгук"/>
        </form>
    @endcan
    {{--TODO: change cannot leave review to can update review--}}
    @cannot('leave_review', $order)
        <form action="{{ route('client.review.edit', ['id' => $order->review_by_master->id]) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
            <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
            <input type="submit" class="site_button-orange button-small onlyBorder" value="Редагувати відгук"/>
        </form>
    @endcannot
@endsection

@section('requests')
    @component('layouts.order.another_requests', ['requests' => $another_masters_requests])
    @endcomponent
@endsection
