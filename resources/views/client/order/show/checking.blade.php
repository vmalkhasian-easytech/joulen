@extends('client.order.show')

@section('buttons')
    @component('layouts.client.order.buttons', ['order' => $order])
        @slot('button')
            <a href="{{ route('client.order.cancel', ['id' => $order->id]) }}" class="site_button-orange">Скасувати проект</a>
        @endslot
    @endcomponent
@endsection