@extends('client.order.show')

@section('selected-master-request')
    <div class="contentBlock" id="master">
        <div class="container">
            <h3>Виконавець</h3>
        </div>
    </div>

    @component('layouts.order.request', ['request' => $order->request_by_master])
        @slot('final')
        @endslot

        @slot('buttons')
            @can('leave_review', $order)
                <form action="{{ route('client.review.create') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
                    <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
                    <input type="submit" class="site_button-orange button-small leaveFeedback" value="Залишити відгук"/>
                </form>
            @endcan
        @endslot
    @endcomponent
@endsection

@section('selected-master-review')
    @cannot('leave_review', $order)
        <div class="contentBlock block-reviews noborderTitle">
            <div class="container">
                <h3>Ваш відгук</h3>
                <div>
                    <div class="reviewWrapper clr">
                        <div>
                            <span class="review-name sym_{{ $order->review_by_master->positive ? 'plus' : 'minus' }}"></span>
                        </div>
                        <div class="review">
                            <p style="margin-left: 30px; white-space: pre-wrap;">{{$order->review_by_master->text}}</p>
                        </div>
                        <div class="review-edit_button">
                            <form action="{{ route('client.review.edit', ['id' => $order->review_by_master->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="master_id" value="{{ $order->request_by_master->master->id }}">
                                <input type="hidden" name="order_id" value="{{ $order->request_by_master->order->id }}">
                                <input type="submit" class="site_button-orange button-small onlyBorder" value="Редагувати відгук"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcannot
@endsection

@section('requests')
    @component('layouts.order.another_requests', ['requests' => $another_masters_requests])
    @endcomponent
@endsection
