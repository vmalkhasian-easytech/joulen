@extends('client.order.show')

@section('buttons')
    @component('layouts.client.order.buttons', ['order' => $order])
        @slot('button')
            <a href="{{ route('client.order.close', ['id' => $order->id]) }}" class="site_button-orange">Закрити проект</a>
        @endslot
    @endcomponent
@endsection

@section('requests')
    <div id="requests">
        @foreach($order->requests_from_active_masters as $request)
            @component('layouts.order.request', ['request' => $request])
                @slot('final')
                    {{ $request->final ? 'Остаточна заявка' : 'Попередня заявка' }}
                @endslot

                @slot('buttons')
                    <div class="master-message-buttons">
                        <a href="#mail-to-master" class="mail-to-master site_button-orange onlyBorder button-small" data-name="{{ $request->master->user->name }}" data-region="{{ $request->master->region->name }}" data-img="{{ $request->master->getAvatarSrc() }}" data-id="{{ $request->master->id }}">Написати повідомлення</a>
                        <a href="#modalsWrapper" class="site_button-orange button-small select-master {{ $request->final ? 'showModal' : 'disabled' }}" data-ok="no" data-modal="#appWarning" data-master_id="{{ $request->master->id }}">
                            Обрати виконавцем
                            <span class="master_tooltip tooltip_info">
                                Щоб вибрати спеціаліста виконавцем, він повинен змінити статус його заявки з попередньої на остаточну.
                            </span>
                            <!-- /.master_tooltip -->
                        </a>
                    </div>
                @endslot
            @endcomponent
        @endforeach
    </div>

    {{--TODO: move email sending functionality to external file if it will be imlplemented on master page--}}
    @push('footer_scripts')
        <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/client/order/considering.js') }}"></script>
        <script type="text/javascript">
                var url = '{{ route('client.email.send') }}';
        </script>
        <script type="text/javascript" src="{{ asset('js/client/order/client_email.js') }}"></script>
    @endpush

    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush

    @push('modals')
        <div  class="remodal modal-client-mail" data-remodal-id="mail-to-master">
            <div class="modal_header">
                <p>Написати повідомлення</p>
            </div>
            <div class="modal_body">
                {{--<form method="POST" action="{{ route('client.email.send') }}" id="message-to-master-form">--}}
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                    <input type="hidden" name="master_id" value="" class="company-id">
                    <div class="form_block">
                        <label for="">Кому</label>
                        <div class="user_block">
                            <div class="company-img user_img">
                                <img src="" alt="image">
                            </div>
                            <p class="company-name name"></p>
                            <p class="company-region region site_icon-location"></p>
                        </div>
                    </div>
                    <div class="form_block">
                        <label for="">Проект</label>
                        <input type="text" placeholder="{{ $order->title }}" disabled>
                    </div>
                    <div class="form_block">
                        <label for="">Повідомлення</label>
                        <textarea name="text" id="message-to-master-text"></textarea>
                    </div>
                    <p class="info">
                        <span>УВАГА!</span> Спеціаліст надішле Вам відповдь на Ваш е-мейл: <a href="#">{{ Auth::user()->email }}</a>
                    </p>
                    <div class="btn_block">
                        <button class='btn_cancel' data-remodal-action="cancel" >Скасувати</button>
                        <button class='btn_confirm submit'>Надіслати</button>
                    </div>
                {{--</form>--}}
            </div>
        </div>

        <div class="remodal modal-client-mail" data-remodal-id="confirm-mail-to-master">
            <div class="modal_header">
                <p>Ваш лист надіслано</p>
            </div>
        </div>

        <div class="remodal modal-client-mail" data-remodal-id="fail-mail-to-master">
            <div class="modal_header">
                <p>Виникла помилка. Спробуйте надіслати лист пізніше.</p>
            </div>
        </div>

        <div id="modalsWrapper">
            <div id="appWarning" class="modal" style="height:auto;">
                {{--<div class="specialBlock" id="message1">--}}
                    {{--<p>Щоб вибрати спеціаліста виконавцем, він повинен змінити статус його заявки з <span style="text-decoration:underline;">попередньої</span> на <span style="text-decoration:underline;">остаточну</span></p>--}}
                {{--</div>--}}
                <div class="specialBlock"  id="message2">
                    <form action="{{ route('client.order.select.master', ['id' => $order->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="master_id" value="">
                        <p><p>Зверніть увагу!</p>
                        Перш ніж обрати спеціаліста виконавцем, ви повинні з ним <b>узгодити остаточну вартість послуг</b>.
                        Оскільки в подальшому її можна буде змінити тільки через адміністрацію сайту.<br><br>
                        Ви дійсно бажаєте вибрати виконавцем цього спеціаліста?<br><br>
                        </p>
                        <div class="btn-message-wrap">
                            <a href="javascript:" class="closemw">Скасувати</a>
                            <input class="site_button-orange" type="submit" value="Так, все вірно">
                        </div>
                        <!-- /.btn-wrap -->

                    </form>
                </div>
                <span class="closeModal"></span>
            </div>
        </div>
    @endpush

@endsection

@section('order-header-additional-block')
    <div class="applicatioQ">Заявок від спеціалістів: {{ $order->requests_from_active_masters_count }}</div>
    <a href="#requests" class="site_button-orange onlyBorder button-small">Перейти до заявок</a>
@endsection