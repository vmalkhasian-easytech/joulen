@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="container">
            <div class="wrapper-page">
                <div class="catalog-invertor-wrap">
                    <h1 class="main-title">Каталог виробників @yield('page-title')</h1>
                    <form class="search-items"
                          action="{{ route('brand.index', ['equipment_type' => $equipment_type]) }}" method="post"
                          enctype="multipart/form-data" id="main-filter">
                        {{ csrf_field() }}
                        <div class="search-brand-invertor">
                            <label for="id">
                                Знайти бренд @yield('filter-title'):
                            </label>
                            <select name="id" class="select2 dropdown-hide">
                                <option value="" {{ !old('id') ? 'selected' : '' }}>Всі</option>
                                @yield('brand-name-select-options')
                            </select>
                            {{--<input type="text" id="search-brand" placeholder="Почніть вводити назву">--}}
                        </div>
                        <!-- /.search-brand-invertor -->
                        <div class="sort-brand-invertor">
                            <label for="order">
                                Сортувати:
                            </label>
                            <select name="order" class="select2 dropdown-hide">
                                <option value="masters_count" {{ !old('order') || old('order') == 'masters_count' ? 'selected' : '' }}>
                                    по кількості дилерів
                                </option>
                                <option value="reviews_count" {{ old('order') == 'reviews_count' ? 'selected' : '' }}>по
                                    кількості відгуків
                                </option>
                                <option value="photos_count" {{ old('order') == 'photos_count' ? 'selected' : '' }}>по
                                    кількості фото
                                </option>
                                <option value="rating" {{ old('order') == 'rating' ? 'selected' : '' }}>по рейтингу
                                </option>
                            </select>
                        </div>
                        <!-- /.sort-brand -->
                        <div class="view_official_brand">
                            <input id="view_official_brand" type="checkbox" name="only_official_distributors"
                                    {{ old('only_official_distributors') ? 'checked' : '' }}>
                            <label for="view_official_brand" class="custom-checkbox border-gray"></label>
                            <p>Показати лише офіційно представлені бренди</p>
                        </div>
                    </form>
                    <!-- /.sort-brand-invertor -->

                    <div class="catalog-manufacturers-invertor">
                        @foreach($equipment as $item)
                            <div class="manufacturers-invertor-item manufacturers-invertor-item-lg">
                                <div class="manufacturer-info-wrap">
                                    <div class="logo-organization img-border">
                                        <a href="{{ route('brand.show', ['equipment_type' => $equipment_type,
                                    'brand_slug' => $item->slug]) }}">
                                            {{--TODO: set default image--}}
                                            <img src="{{ $item->logo ? $item->logo->getPath(true) : asset('images/default_photo.jpg') }}"
                                                 alt="image">
                                        </a>
                                    </div>
                                    <!-- /.logo-organization -->
                                    <div class="about-organization">
                                        <h3><a href="{{ route('brand.show', ['equipment_type' => $equipment_type,
                                    'brand_slug' => $item->slug]) }}">{{ $item->title }}</a></h3>
                                        <div class="total-rating">
                                            @if($item->reviews_count)
                                                <span class="value-evaluation">{{ number_format($item->sum_rating * 5 / 100, 2) }}</span>
                                            @endif
                                            <div class="stars-wrapper">
                                                <div class="stars-off stars">
                                                    <div class="stars-on stars"
                                                         style="width:{{ $item->sum_rating }}%"></div>
                                                </div>
                                            </div>
                                            <!-- /. -->
                                            <p>Відгуків <a href="{{ route('brand.show', ['equipment_type' => $equipment_type,
                                                'brand_slug' => $item->slug]) . '#equipment-reviews' }}">{{ $item->reviews_count }}</a>
                                            </p>
                                        </div>
                                        <!-- /.total-rating -->
                                    </div>
                                </div>
                                <!-- /.about-organization -->
                                <div class="manufacturer-dealers-wrap">
                                    @if ($item->distributors_count > 0)
                                        <div class="official-distributor">
                                            <img src="{{asset('images/official-distributor.png')}}" alt="image">
                                            <p>Є офіційний  дистрибютор в Україні</p>
                                        </div>
                                    @endif
                                    <!-- /.official-distributor -->
                                    <a href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-dealers' }}"
                                       class="number-dealers img-border">
                                        <span>{{ $item->masters_count }}</span>
                                        <p>дилерів / партнерів в Україні</p>
                                    </a>
                                    <!-- /.number-dealers -->

                                    <div class="photo-invertors">
                                        @if($item->portfolios->count() > 0)
                                            {{--TODO: set default image--}}
                                            <img src="{{ asset('images/picture.png') }}" alt="image">
                                            <p>
                                                <a href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-photos' }}">{{ $item->portfolios->count() }}</a>
                                                фото</p>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.photo -->
                            </div>
                            <!-- /.manufacturers-invertor-item -->

                            <div class="manufacturers-invertor-item manufacturers-invertor-item-sm">
                                <h3><a href="{{ route('brand.show', ['equipment_type' => $equipment_type,
                                    'brand_slug' => $item->slug]) }}">{{ $item->title }}</a></h3>
                                <div class="manufacturer-info-wrap">
                                    <div class="logo-organization img-border">
                                        <a href="{{ route('brand.show', ['equipment_type' => $equipment_type,
                                    'brand_slug' => $item->slug]) }}">
                                            {{--TODO: set default image--}}
                                            <img src="{{ $item->logo ? $item->logo->getPath(true) : asset('images/default_photo.jpg') }}"
                                                 alt="image">
                                        </a>
                                    </div>
                                    <!-- /.logo-organization -->
                                    <div class="about-organization">
                                        <div class="total-rating">
                                            @if($item->reviews_count)
                                                <span class="value-evaluation">{{ number_format($item->sum_rating * 5 / 100, 2) }}</span>
                                            @endif
                                            <div class="stars-wrapper">
                                                <div class="stars-off stars">
                                                    <div class="stars-on stars"
                                                         style="width:{{ $item->sum_rating }}%"></div>
                                                </div>
                                            </div>
                                                <p>Відгуків <a
                                                            href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-reviews' }}">{{ $item->reviews_count }}</a>
                                                </p>
                                        </div>
                                        <!-- /.total-rating -->
                                        <div class="photo-invertors hide-lg">
                                            @if($item->portfolios->count() > 0)
                                                {{--TODO: set default image--}}
                                                <img src="{{ asset('images/picture.png') }}" alt="image">
                                                <p>
                                                    <a href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-photos' }}">{{ $item->portfolios->count() }}</a>
                                                    фото</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.about-organization -->
                                <div class="manufacturer-dealers-wrap">
                                    @if ($item->distributors_count > 0)
                                        <div class="official-distributor">
                                            <img src="{{asset('images/official-distributor.png')}}" alt="image">
                                            <p>Є офіційний дистрибютор в Україні</p>
                                        </div>
                                    @endif
                                    <!-- /.official-distributor -->
                                    <a href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-dealers' }}"
                                       class="number-dealers img-border">
                                        <span>{{ $item->masters_count }}</span>
                                        <p>дилерів / партнерів в Україні</p>
                                    </a>
                                    <!-- /.number-dealers -->

                                    <div class="photo-invertors hide-sm">
                                        @if($item->portfolios->count() > 0)
                                            {{--TODO: set default image--}}
                                            <img src="{{ asset('images/picture.png') }}" alt="image">
                                            <p>
                                                <a href="{{ route('brand.show', ['equipment_type' => $equipment_type, 'brand_slug' => $item->slug]) . '#equipment-photos' }}">{{ $item->portfolios->count() }}</a>
                                                фото</p>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.photo -->
                            </div>
                            <!-- /.manufacturers-invertor-item -->
                        @endforeach
                    </div>
                    <!-- /.catalog-manufacturers-invertor -->

                    <div class="pagination-container"> {{ $equipment->links() }} </div>
                    <!-- /.pagination -->
                </div>
                <!-- /.select-brand -->
            </div>
            <!-- /.wrapper-page -->
        </div>
        <!-- /.catalog-station-wrap -->
    </div>
    <!-- /.container -->
    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"/>
    @endpush
    @push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/equipment/index.js') }}"></script>
    @endpush
@endsection