@extends('equipment.index')

@section('page-title', 'інверторів')

@section('filter-title', 'інвертора')

@section('brand-name-select-options')
    @foreach($invertors as $item)
        <option value="{{ $item->id }}" {{ old('id') == $item->id ? 'selected' : '' }}>{{ $item->title }}</option>
    @endforeach
@endsection

@section('site-title', 'Каталог виробників інверторів: рейтинг, відгуки, купити в Україні')

@section('site-meta-description', 'Список всіх виробників інверторів, доступних в Україні: рейтинг, відгуки, де купити, отзывы')