<li>
    <a class="main_menu_item orders_menu" href="#">Замовлення</a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="secondary_menu">
        <li>
            <a href="{{ route('master.order.index') }}">Всі замовлення</a>
        </li>
        <li>
            <a href="{{ route('master.order.private.index') }}">Закріплені за мною</a>
        </li>
    </ul>
</li>
<li>
    <a class="main_menu_item company_menu" href="#">Ваша компанія</a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="secondary_menu">
        <li>
            <a href="{{route('master.profile_pages.general')}}">Загальна інформація</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.contact')}}">Ваші контакти</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.services')}}">Послуги вашої компанії</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.portfolio')}}">Портфоліо</a>
        </li>
        <li>
            <a href="{{ route('master.commission')}}">Комісія сервісу</a>
        </li>
    </ul>
</li>
<li>
    <a class="main_menu_item settings_menu" href="#">Налаштування</a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="secondary_menu">
        <li>
            <a href="{{route('master.profile_pages.subscribe')}}">Розсилка</a>
        </li>
        <li>
            <a href="{{route('master.settings.private')}}">Обліковий запис</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.partnership')}}">Дані для співпраці</a>
        </li>
        <li>
            <a href="{{ route('master.commission')}}">Комісія сервісу</a>
        </li>
        @if(Auth::user()->master->status->slug == 'unverified' )
            <li>
                <a href="{{route('master.profile_pages.verification_request')}}">Стати "Перевіреним"</a>
            </li>
        @endif
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>
<li>
    <a class="faq_menu" href="{{ route('master.faq') }}">Поширенні запитання</a>
</li>