<li><a href="{{ route('master.index') }}">Каталог спеціалістів</a></li>
<li>
    <a href="#">Мій профіль</a>
    <span class="showSubMenu"></span>
    <ul>
        <li>
            <a href="{{ route('master.create') }}">Публічна інформація про Вас</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>