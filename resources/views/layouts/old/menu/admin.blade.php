<li><a href="{{ route('master.index') }}">Каталог спеціалістів</a></li>
<li>
    <a href="#">Адміністрування</a>
    <span class="showSubMenu"></span>
    <ul>
        @include('layouts.admin_menu_list')
    </ul>
</li>
<li>
    <a href="#">Мій профіль</a>
    <span class="showSubMenu"></span>
    <ul>
        <li>
            <a href="{{ route('admin.settings') }}">Параметри облікового запису</a>
        </li>
        <li class="logoutLink">
            <a href="{{ route('logout') }}">Вихід</a>
        </li>
    </ul>
</li>