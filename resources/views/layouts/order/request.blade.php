<div class="contentBlock listOfApp">
    <div class="container">
        <div id="master">
            <div class="app-item">
                <div class="app-item-author clr">
                    <div class="app-item-author_info clr">
                        <h3><a href="{{ route('master.show', ['id' => $request->master->id]) }}">{{ $request->master->user->name }}</a></h3>
                        <div class="app-item-reviews site_icon-reviews">Відгуків:
                            <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="">{{ count($request->master->reviews) }}</a>
                            @if(count($request->master->negative_reviews) > 0)
                                <a href="{{ route('master.show.reviews', ['id' => $request->master->id]) }}" class="site_color-red" title="Негативні відгуки">(-{{ count($request->master->negative_reviews) }})</a>
                            @endif
                        </div>
                        <a href="{{ route('master.show', ['id' => $request->master->id]) }}" class="app-item-author_img">
                            <img src="{{ $request->master->getAvatarSrc() }}" alt="{{ $request->master->user->name }}">
                        </a>
                        {{--TODO: make url filter maker--}}
                        <div class="app-item-loc site_icon-location"><a href="{{ route('master.index') . '/' . $request->master->region->slug }}" title="Переглянути інших спеціалістів із цього регіону">
                                {{ $request->master->main_office->address ? explode(',', $request->master->main_office->address)[0] : $request->master->region->name }}
                            </a></div>
                    </div>
                    {{--TODO: make final_label attribute in OrderRequest model--}}
                    <div class="app-item-price">{{ $final }} <span>${{ $request->price }}</span></div>
                </div>
                <div class="app-item-content">
                    @foreach($request->prices as $price)
                            <h4>{{ $price->type->name }}: ${{ $price->price }}</h4>
                            <p style="margin-left:40px;white-space:pre-wrap;">{!! nl2br(e($price->comment)) !!}</p>
                    @endforeach

                    {!! $buttons !!}
                </div>
            </div>
        </div>
    </div>
</div>