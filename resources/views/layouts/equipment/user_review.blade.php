<div class="review-company first-review-company">
    <div class="review-text">
        <span class="review-title">{{ $review->title }}</span>
        <p>{{ $review->body }}</p>
        @if(Auth::check() && Auth::user()->id == $review->user_id)
            <div class="review-btn">
                <a href="#delete-review" class="remove-review" data-id="{{ $review->id }}">
                    Видалити
                </a>
                <!-- /.remove-review -->

                <a href="{{ route('brand.review.edit', ['equipment_type' => $equipment_type,
                'brand_slug' => $brand->slug , 'id' => $review->id]) }}" class="edit-review">
                    Редагувати
                </a>
                <!-- /.edit-review -->
            </div>
        @endif
    <!-- /.review-btn -->
    </div>
    <!-- /.title-review -->
    <div class="rating-company">
        <div class="star-rating-company">
            <div class="stars-container">
                <div class="stars-wrapper">
                    <div class="stars-off stars">
                        <div class="stars-on stars" style="width:calc({{ $review->quality_rating }}*100%/5)"></div>
                    </div>
                </div>
                <div class="stars-wrapper">
                    <div class="stars-off stars">
                        <div class="stars-on stars" style="width:calc({{ $review->price_rating }}*100%/5)"></div>
                    </div>
                </div>
                <div class="stars-wrapper">
                    <div class="stars-off stars">
                        <div class="stars-on stars" style="width:calc({{ $review->service_rating }}*100%/5)"></div>
                    </div>
                </div>
            </div>
            <!-- /.stars-wrap -->
            <div class="overal-quality">
                <span>Якість</span>
                <span>Ціна</span>
                <span>Сервіс</span>
            </div>
            <!-- /.quality -->
        </div>
        <!-- /.star-rating -->

        <div class="status-review">
            @if ($review->user->master)
                <p>Відгук від компанії: <a href="{{ route('master.show', ['id' => $review->user->master->id]) }}" class="name-company">{{ $review->user->name }}</a></p>
            @else
                <p>Відгук від клієнта: <a style="cursor: text" class="name-company">{{ $review->user->name }}</a></p>
            @endif
            <span class="date-review">{{ $review->created_at->format('d.m.Y') }}</span>
            <!-- /.date-review -->
            @if(Auth::check() && Auth::user()->id == $review->user_id)
                <p class="on-verification">
                    @if ($review->status->slug == 'verified')
                        Активний
                    @elseif ($review->status->slug == 'rejected')
                        Відхилено
                    @else
                        {{ $review->status->name }}
                    @endif
                </p>
            @endif
        <!-- /.status -->
        </div>
        <!-- /.status-review -->
    </div>
    <!-- /.rating-company -->
</div>