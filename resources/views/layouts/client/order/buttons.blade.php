<div class="container controlBtn textCenter">
    <a href="{{ route('client.order.index') }}" class="site_button-orange onlyBorder">Назад</a>
    <a href="{{ route('client.order.edit', ['id' => $order->id]) }}" class="site_button-orange onlyBorder">Редагувати</a>
    {!! $button !!}
</div>