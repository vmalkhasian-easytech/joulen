@extends('order.show', ['order' => $order_request->order])

@section('user-section')
    <div class="contentBlock">
        <div class="container">
            <h3>Подайте вашу заявку</h3>
            <div>
                <div id="setApplication">
                    <h4>Оцініть вартість по кожному пункту, який цікавить клієнта:</h4>
                    <form id="formsm" class="validate_form" action="@yield('request-edit-action')" method="POST">
                        <div>
                            <div>
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                {{--TODO: add label output (nowhere to get it dymanically)--}}
                                {{--TODO: output only prices that correspond to order blocks--}}
                                @foreach($order_request->prices as $price)
                                    {{--TODO: hide price type if block in order wasn't selected--}}
                                    <div class="setApplicationBlock">
                                        <h5>{{ $price->type->name }}:</h5>
                                        <div class="clr">
                                            <div class="setApp-price">
                                                <div class="label">Ціна</div>
                                                <div class="field">
                                                    <input class="request_price only-number validate_number" id="devices_price" name="prices[{{ $price->type->id }}]" step="any" type="number" value="{{ $price->price }}"> <strong><b>$</b></strong>
                                                </div>
                                            </div>
                                            <div class="setApp-desc">
                                                <div class="label">{{ $price->description }}</div>
                                                <textarea name="comments[{{ $price->type->id }}]">{{ $price->comment }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div id="" class="checkboxChoices clr">
                                <input name="final" type="checkbox" id="isNegative" value="1" {{ $order_request->final ? 'checked' : '' }}>
                                <label for="isNegative">
                                    <div class="clr">
                                        <span>Попередня заявка</span>
                                        <span>Остаточна заявка</span>
                                    </div>
                                </label>
                            </div>

                            <div class="appDifference">
                                <a>Яка різниця між попереднєю та остаточною заявками</a>
                                <div class="appDiffDesc">
                                    <div>
                                        <strong>Попередню заявку</strong> вибирайте в тому випадку, якщо ви точно не впевнені у остаточній вартості або якщо у вас є запитання до клієнта.
                                    </div>
                                    <div>
                                        <strong>Остаточну заявку</strong> вибирайте тоді, коли ви вже узгодили з клієнтом всі питання і вже домовилися щодо остаточної ціни.”
                                    </div>
                                </div>
                            </div>

                            <div class="sumarnaVartist">
                                <h2 id="moneysum">Сумарна вартість: $<span class="request_price_sum">{{ $order_request->price }}</span></h2>
                                <div>
                                    <div class="comissionBlock">
                                        {{--TODO: get subcategory factor--}}
                                        <a href="{{ route('master.commission') }}">Комісія сервісу</a> = {{ $order_request->order->subcategory->subcategoryFactor->percents }}%
                                        <a href="#" class="open_tooltip">
                                            <span class="tooltip_info">
                                                <p>Комісія сплачується після того, як клієнт повністю із вами розрахується і залишить вам позитивний відгук</p>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="buttonsWrapper">
                                <input type="submit" value="Зберегти" class="site_button-orange"/>
                                <a href="{{ route('master.order.index') }}" class="site_button-orange onlyBorder">Скасувати</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('footer_scripts')
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/master/request/create.js') }}"></script>
    @endpush

@endsection