<li class="menu-item">
    <a class="orders_menu" href="#">Замовлення <span class="arrow"></span></a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="sub-menu">
        <li>
            <a href="{{ route('master.order.index') }}">Всі замовлення</a>
        </li>
        <li>
            <a href="{{ route('master.order.private.index') }}">Закріплені за мною</a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a class="company_menu" href="#">Ваша компанія <span class="arrow"></span></a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="sub-menu">
        <li>
            <a href="{{route('master.profile_pages.general')}}">Загальна інформація</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.contact')}}">Ваші контакти</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.services')}}">Послуги вашої компанії</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.portfolio')}}">Портфоліо</a>
        </li>
    </ul>
</li>
<li class="menu-item">
    <a class="settings_menu" href="#">Налаштування <span class="arrow"></span></a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="sub-menu">
        <li>
            <a href="{{route('master.profile_pages.subscribe')}}">Розсилка</a>
        </li>
        <li>
            <a href="{{route('master.settings.private')}}">Обліковий запис</a>
        </li>
        <li>
            <a href="{{route('master.profile_pages.partnership')}}">Дані для співпраці</a>
        </li>
        <li>
            <a href="{{ route('master.commission')}}">Комісія сервісу</a>
        </li>
        @if(Auth::user()->master && Auth::user()->master->status->slug == 'unverified')
            <li>
                <a href="{{route('master.profile_pages.verification_request')}}">Стати "Перевіреним"</a>
            </li>
        @endif
    </ul>
</li>
<li class="menu-item">
    <a class="faq_menu" href="{{ route('master.faq') }}">Поширенні запитання</a>
</li>