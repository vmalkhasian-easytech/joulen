<li class="menu-item">
    <a class="settings_menu" href="#">Налаштування <span class="arrow"></span></a>
    {{--<span class="showSubMenu"></span>--}}
    <ul class="sub-menu">
        <li>
            <a href="{{route('distributor.masters')}}">Мої дилери</a>
        </li>
        <li>
            <a href="{{ route('distributor.edit')}}">Про компанію</a>
        </li>
        <li>
            <a href="{{route('distributor.settings')}}">Обліковий запис</a>
        </li>
    </ul>
</li>