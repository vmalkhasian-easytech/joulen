<div class="remodal" data-remodal-id="add-brand-panel">
    <div class="modal-header">
        <h4>Додати бренд (Сонячні панелі)</h4>
    </div>

    <div class="validation_errors_modal backend_validation_errors">
        @component('layouts.validation_errors')
        @endcomponent
    </div>

    <div class="modal-body">
        <form class="validate_form" data-step='brand' action="{{ Auth::user()->isAdmin() ? route('admin.master.store.equipment') : route('master.store.equipment') }}" method="POST" id="add-panel-form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $master->id }}" name="master_id">
            <input type="hidden" name="equipment_type">

            <div class="modal-form-block">
                <label for="add_panel_brand_panel">Назва бренду:</label>

                <select name="equipment_id" class="select-modal brands-list" id="add_panel_brand_panel">

                    @foreach($panel_type_equipment as $panel)
                        <option value="{{ $panel->id }}">{{ $panel->title }}</option>
                    @endforeach
                </select>

            </div>
            <div class="modal-form-block">
                <label for="add_panel_warranty_duration">Гарантія на панелі: </label>
                <input type="text" class="warranty-input" name="warranty_duration" id="add_panel_warranty_duration"> років.

            </div>
            <!-- /.modal-form-block -->
               <div class="modal-form-block">
                   <p>Хто буде займатися ремонтом/заміною обладнання при настанні гарантійного випадку?</p>

                       @foreach($guarantee_repairs as $id=>$repair)
                       <div class="styled-radio">
                           <input type="radio" name="warranty_case" id="add_panel_warranty_case{{ $id }}" value="{{ $id }}">
                           <label for="add_panel_warranty_case{{ $id }}">{{ $repair }}</label>
                       </div>
                       @endforeach

                   <!-- /.styled-radio -->
               </div>
            <!-- /.modal-form-block -->
            <div class="form-help-wrap">
                <div class="modal-form-block">
                    <p>Де знаходиться <span class="tooltip">обмінний фонд@component('layouts.profile_help')
                            @endcomponent</span> обладнання/комплектуючих?</p>
                    @foreach($exchange_funds as $id => $fund)
                        <div class="styled-radio">
                            <input type="radio" name="exchange_fund_loc" id="add_panel_exchange_fund_loc{{ $id }}" value="{{ $id }}">
                            <label for="add_panel_exchange_fund_loc{{ $id }}">{{ $fund }}</label>
                        </div>
                        <!-- /.styled-radio -->
                    @endforeach
                </div>

            </div>
            <div class="modal-form-block">
                <label for="built">Скільки сонячних станцій із використанням панелей цього бренду ви збудували?</label>
                <input type="text" name="built" id="built">
                <span class="form-info">Вказане число станцій повинно збігатися із кількістю об'єктів, доданих у вашому портфоліо</span>
            </div>
            <!-- /.modal-form-block -->

            <div class="btn-modal">
                <button class="btn btn-confirm" id="add-panel-confirm">Зберегти</button>
                <button class="btn btn-cancel" data-remodal-action="cancel">Скасувати</button>

            </div>
        </form>
    </div>
</div>