<div class="remodal" data-remodal-id="view-brands" id="view-brands">
    <div class="modal_header">
        <p class="title-brand"></p>
        <a class="brand-delete close-window" data-remodal-action="cancel"></a>
    </div>
    <div class="modal_body">
        <p class="guarantee"></p>
        <p class="increase-text">Чи є можливість збільшити/продовжити гарантійний строк?</p>
        <p class="increase"></p>
        <p class="repair-text">Хто буде займатися ремонтом/заміною обладнання при настанні гарантійного випадку?</p>
        <p class="repair"></p>
        <p class="fund-text">Де знаходиться обмінний фонд обладнання/комплектуючих?</p>
        <p class="fund"></p>
        <p class="count_sun_station-text">Скільки сонячних станцій із використанням панелей цього бренду ви
            збудували?</p>
        <p class="count_sun_station"></p>
    </div>
    <div class="btn-modal-apply">
        <a class="btn btn-primary" data-remodal-action="cancel">ОК</a>
    </div>
</div>