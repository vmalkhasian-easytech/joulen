<header class="page-header-wrap" role="banner">
    <div class="container">
        <div class="page-header">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('images/logo_joulen.png') }}" alt="logo">
            </a>

            @include('layouts.menu')
        </div>
        <!-- /.header-button -->

    </div>
</header>


<!--# end header #-->