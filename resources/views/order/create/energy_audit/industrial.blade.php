@extends('order.create.energy_audit')

@section('energy-audit-img-class', 'cost_header-industrial')

@section('energy-audit-header-title', 'Для підприємства')

@section('energy-audit-about', 'Про підприємство')

@section('energy-audit-explotation', 'Експлуатація підприємства')

@section('energy-audit-explotation-true', 'Підприємство функціонує')

@section('energy-audit-explotation-false', 'Підприємство ще не добудовано (не функціонує)')

@section('energy-audit-photo', 'Прикріпити фото підприємства')

@section('energy-audit-where-title', 'Де знаходиться ваш підприємство')

@section('energy-audit-other-info', 'Інша інформація про підприємство')

@section('energy-audit-subcategory-hidden')
    <input type="hidden" name="subcategory" class="validate_required" value="{{ $order_subcategories['industrial']->id }}">
@endsection

@section('site-title')
    @parent
    на підприємствах та заводах - дізнатись вартість
@endsection