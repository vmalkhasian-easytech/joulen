@extends('order.create.energy_audit')

@section('energy-audit-img-class', 'cost_header-commerce')

@section('energy-audit-header-title', 'Для торгово-офісного центру')

@section('energy-audit-about', 'Про заклад')

@section('energy-audit-explotation', 'Експлуатація закладу')

@section('energy-audit-explotation-true', 'Заклад функціонує')

@section('energy-audit-explotation-false', 'Заклад ще не добудовано (не функціонує)')

@section('energy-audit-photo', 'Прикріпити фото закладу')

@section('energy-audit-where-title', 'Де знаходиться ваш заклад')

@section('energy-audit-other-info', 'Інша інформація про заклад')

@section('energy-audit-subcategory-hidden')
    <input type="hidden" class="validate_required" name="subcategory" value="{{ $order_subcategories['commerce']->id }}">
@endsection

@section('site-title')
    @parent
    в торгово-офісних центрах - дізнатись вартість
@endsection