@extends('order.create.ses')

@section('ses-title', 'Оцінка вартості будівництва Гібридної СЕС')

@section('ses-img-class', 'cost_header-hybrid')

@section('ses-header-title', 'Гібридна СЕС')

@section('ses-header-description', 'Використовується коли електромережа нестабільна. Надлишки електроенергії можна продавати. Потрібні аккумулятори.')

@section('ses-legal-checkbox')
    <div>
        <input type="checkbox" id="c3" name="price_types[3]" value="3" data-switchBlock=".data-doc" class="checkbox-special" />
        <label for="c3">Документальний супровід (Підключення та оформлення “зеленого” тарифу)</label>
    </div>
@endsection

@section('ses-power-block')
    <div class="calc1 clr">
        <div class="calc1_wrapper">
            <div class="error-block">
                <div class="calc_label">Яка потужність фотомодулів вам потрібна?</div>
                <div class="calc_field small">
                    <input type="number" min="0.1" class="photocell_power_input autonomous-photosell" id="powerf" value="0" name="photocell_power"/> кВт
                </div>
            </div>
            <div class="error-block">
                <div class="calc_label">Яка ємність акумуляторів вам потрібна?</div>
                <div class="calc_field small">
                    <input id="battery_capacity" class="photocell_power_input" type="number" min="0.1" value="0" name="battery_capacity"/> кВт·год
                </div>
            </div>

            <div class="afterField">
                ДЛЯ ЦЬОГО НЕОБХІДНА ПЛОЩА ≈
                <strong><b><span><span class="squad-info" id="sliderRes"></span>М<sup><small>2</small></sup></span></b></strong>
                <input type="hidden" class="input-squad" name="required_area">
            </div>
        </div>
        {{--TODO: add power calculation route name--}}
        <a href="{{ route('order.calculator') }}" class="site_button-orange button-small onlyBorder">Розрахувати</a>
        <span style="margin-top: 20px; margin-left: -10px;" class="tooltip" data-tooltip="Розрахунок покаже ПРИБЛИЗНІ рекомендовані параметри потужності панелей та ємності акумуляторів.
Точний розрахунок повинен провести монтажник, попередньо ознайомившись із вашим конкретним об’єктом.">?</span>
    </div>
@endsection

@section('ses-delivery-appointment')
    <div id="ses-delivery-appointment"  class="montageBlock"></div>
@endsection

@section('ses-where-title', 'Де потрібно встановити сонячну станцію?')

@section('ses-where-text')
    <div style="margin-top: 0"><p>Поставте маркер <img src="{{ asset('/images/map-marker.png') }}"> на ваш будинок або подвір'я -
            в залежності від того, де буде монтуватися станція.</p>
        <div>
            <p><strong>Для чого це потрібно?</strong><br>
                Вартість монтажу залежить від складності монтажу (конструкція даху, планування ділянки, тощо).<br>
                Вкажіть точне розміщення, і монтажник зможе оцінити вартість монтажу,
                а також зможе відразу надати рекомендації у вашому конкретному випадку.
            </p>
        </div>
    </div>
    <div style="display: none; margin-top: 12px; color: red" class="error-input-map">
        Потрібно обов'язково вказати точне місце встановлення станції.<br>
        Цю інформацію буде бачити лише адміністратор сервісу та авторизовані монтажники.
    </div>
@endsection

@section('ses-house-info-title', 'Інформація про ваш будинок')

@section('ses-area-title')
    Приблизна площа даху, <span class="toLowerCase">м.кв.</span>
@endsection

@section('ses-photo-title', 'Прикріпити фото будинку')

@section('ses-other-info', 'Інша інформація про ваш будинок')

@section('ses-legal-block')
    <div class="contentBlock bordered data-doc" id="block-jur">
        <div class="container">
            <h2>Документальний супровід</h2>
            <div>
                <div>
                    <input type="checkbox" class="styledChk" id="c150" name="green_tariff" value="1"/>
                    <label for="c3">Підключення до енергомережі та отримання “зеленого” тарифу</label>
                </div>
            </div>
            <div id="legal_dynamic_blocks" class="montageBlock"></div>
            <div id="location_legal" style="display: none" class="location-block">
                <h3 class="site_icon-location">Де знаходиться об'єкт?</h3>
                <div>
                    <div class="label">Область</div>
                    <div class="field">
                        <select id="selectregion" name="region_id" class="validate_required">
                            <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}">{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="label">Населений пункт</div>
                    <div class="field">
                        <input id="city" type="text" class="validate_required only-letter" value="" name="city"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ses-subcategory-hidden')
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['hybrid']->id }}">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/order/calculator_fill.js') }}"></script>
@endpush

@section('site-title')
    Гібридні (резервні) сонячні електростанції в Україні - дізнатися вартість
@endsection