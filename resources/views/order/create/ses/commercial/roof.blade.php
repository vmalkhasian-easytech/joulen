@extends('order.create.ses')

@section('ses-title', 'Оцінка вартості будівництва дахової комерційної СЕС')

@section('ses-img-class', 'cost_header-roof')

@section('ses-header-title', 'Дахова комерційна СЕС')

@section('ses-header-description', 'Потужна СЕС (> 30 кВт) для продажу електроенергії у мережу по “зеленому” тарифу.')

@section('ses-legal-checkbox')
    <div>
        <input name="price_types[3]" type="checkbox" id="c3" data-switchBlock=".data-doc" class="checkbox-special" value="3" />
        <label for="c3">Документальний супровід (Підключення та оформлення “зеленого” тарифу)</label>
    </div>
@endsection

@section('ses-power-block')
    <div class="clr">
        <div class="label">Яка потужність фотомодулів вас цікавить?
            <div class="field error-block">
                <input type="number" min="1" value="" id="powerf" class="power-value roof-photosell only-number validate_ses_network validate_number" name="photocell_power"/> <strong><b>кВт</b></strong></div>
        </div>
        <div class="afterField">
            Для цього необхідна площа ≈
            <strong><b><span><span class="squad-info">0</span> м.кв.</span></b></strong>
            <input type="hidden" class="input-squad" name="required_area">
        </div>
    </div>
@endsection

@section('ses-delivery-appointment')
    <div id="ses-delivery-appointment"  class="montageBlock"></div>
@endsection

@section('ses-where-title', 'Де знаходиться приміщення')

@section('ses-where-text')
    <div style="margin-top: 0"><p>Поставте маркер <img src="{{ asset('/images/map-marker.png') }}"> на приміщення де буде монтуватися станція.</p>
        <div>
            <p><strong>Для чого це потрібно?</strong><br>
                Вартість будівництва станції залежить від складності монтажу (тип даху, його конструкція тощо).
                Вкажіть точне розміщення, і монтажник зможе оцінити вартість монтажу, а також зможе відразу
                надати рекомендації у вашому конкретному випадку.
            </p>
        </div>
    </div>
    <div style="display: none; margin-top: 12px; color: red" class="error-input-map">
        Потрібно обов'язково вказати точне місце встановлення станції.<br>
        Цю інформацію буде бачити лише адміністратор сервісу та авторизовані монтажники.
    </div>
@endsection

@section('ses-house-info-title', 'Інформація про дах приміщення')

@section('ses-area-title', 'Площа даху')

@section('ses-area-unit')
    <label for="m">
        м.кв
        <input type='hidden' name="area_unit" value="m" readonly>
    </label>
@endsection

@section('ses-photo-title', 'Прикріпіть фото/план даху')

@section('ses-other-info', 'Інша інформація про дах приміщення')

@section('ses-legal-block')
    <div class="contentBlock bordered data-doc" id="block-jur">
        <div class="container">
            <h2>Документальний супровід</h2>
            <div>
                <div>
                    <input type="checkbox" class="styledChk" id="c150" name="green_tariff" value="1"/>
                    <label for="c3">Потрібна допомога у підключенні до енергомережі та оформленні “зеленого” тарифу.</label>
                </div>
            </div>
            <div id="legal_dynamic_blocks" class="montageBlock"></div>
            <div id="location_legal" style="display: none" class="location-block">
                <h3 class="site_icon-location">Де знаходиться об'єкт?</h3>
                <div>
                    <div class="label">Область</div>
                    <div class="field">
                        <select id="selectregion" name="region_id" class="validate_required">
                            <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}">{{ $region->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="label">Населений пункт</div>
                    <div class="field">
                        <input id="city" type="text" class="validate_required only-letter" value="" name="city"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ses-subcategory-hidden')
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['commercial']->id }}">
@endsection

@section('ses-ground-hidden')
    <input type="hidden" name="ground" value="0">
@endsection

@section('site-title')
    Комерційні сонячні електростанції в Україні - дізнатися вартість
@endsection