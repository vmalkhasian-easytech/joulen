@extends('order.create')

@section('category-content')
    <div class="container pageTitle withBlock">
        <h1>@yield('ses-title')</h1>
        <div class="additionalBlock">
            <a href="{{ route('temp.step1')}}" class="site_button-orange button-small onlyBorder">Обрати інший тип СЕС</a>
        </div>
    </div>

    <div class="contentBlock">
        <div class="container">
            <div class="cost_header @yield('ses-img-class')">
                <h4>@yield('ses-header-title')</h4>
                <p>@yield('ses-header-description')</p>
            </div>
        </div>
    </div>

    @component('layouts.validation_errors')
    @endcomponent

    <div class="contentBlock ckeckboxesBlock">
        <div class="container">

            <h2>Оберіть послуги, які вас цікавлять:</h2>
            <div>
                <div>
                    <input type="checkbox" id="c1" name="price_types[1]" value="1" data-switchblock=".data-cost" data-block_id="delivery" class="checkbox-special toggle-order-block" />
                    <label for="c1">Поставка обладнання та комплектуючих</label>
                </div>
                <div>
                    <input type="checkbox" id="c2" name="price_types[2]" value="2" data-switchblock=".data-montage" data-block_id="installation" class="checkbox-special toggle-order-block" />
                    <label for="c2">Монтаж та налагодження СЕС</label>
                </div>
                @yield('ses-legal-checkbox')
            </div>
        </div>
    </div>

    <div class="contentBlock bordered cost_offlineStation data-cost" id="delivery">
        <div class="container">
            <h2>Поставка обладнання та комплектуючих</h2>
            <div>
                <div id="powerBlock" class="clr">
                    @yield('ses-power-block')
                </div>
                <div>
                    <div class="label">Яка цінова категорія обладнання вас цікавить?</div>
                    <div class="field calc1_radioBtns">
                        {{--TODO: implement move this to viewComposer--}}
                        @foreach(\App\Models\SesPriceCategory::all() as $category)
                            <input type="radio" name="ses_price_category" id="order-ses-price-{{ $category->id }}" value="{{ $category->id }}" {{ $category->slug == 'optimal' ? 'checked' : '' }}/>
                            <label for="order-ses-price-{{ $category->id }}">{{ $category->name }}</label>
                        @endforeach
                    </div>
                </div>
                <div id="paramsDetails">
                    <div class="label">Вкажіть конкретні параметри інвертора, сонячних панелей та комплектуючих, які вас цікавлять</div>
                    <div class="field">
                        <textarea name="parts_comment" class="validate_required parts_comment_input validate_length"></textarea>
                    </div>
                </div>
                <div id="location_delivery" style="display: none" class="location-block">
                    <h3 class="site_icon-location">Куди доставити обладнання?</h3>
                    <div>
                        <div class="label">Область</div>
                        <div class="field">
                            <select id="selectregion" name="region_id" class="validate_required">
                                <option value="0" data-display="Виберіть область" selected disabled>Виберіть область</option>
                                @foreach($regions as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">Населений пункт</div>
                        <div class="field">
                            <input id="city" type="text" class="validate_required only-letter" value="" name="city"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contentBlock bordered montageBlock data-montage" id="installation">
        <div class="container">
            <h2>Монтаж та налагодження СЕС</h2>
            <div id="installOnly_power_block_wrapper">
                <div id="installOnly_power_block" hidden>
                    {{--TODO: remove h3 make margins via css--}}
                    <h3></h3>
                    <div class="label">Яка потужність сонячної станції?</div>
                    <div class="field">
                        <input id="installOnly_power" type="number" class="validate_required only-number" value="" name="installOnly_power" />
                        <span>КВТ</span>
                    </div>
                </div>
            </div>
            <div id="installation-wrapper">
                <div>
                    <h3 class="site_icon-location">@yield('ses-where-title')</h3>
                    @yield('ses-where-text')
                    <div style="display: none; margin-top: 12px; color: red" class="error-input-city">
                        Вкажіть населений пункт.
                    </div>
                    <div class="gmap-block client-map" style="display: block">
                        <!-- /.marker-info -->
                        <input id="pac-input" class="controls pac-input validate_required search-map-input" type="text" placeholder="Введіть адресу тут ...">
                        <div id="gmap"></div>
                    </div>

                    <input type="hidden" class="geo_coords_lat" name="lat" value="">
                    <input type="hidden" class="geo_coords_lng" name="lng" value="">
                    <input id="city_name" type="hidden" value="" name="city_name"/>
                    <input id="region_name" type="hidden" value="" name="region_name"/>
                    <input type="hidden" name="zoom" value="">
                </div>
                <div>
                    <h3 class="site_icon-options">@yield('ses-house-info-title')</h3>
                    <div>
                        <div class="label">@yield('ses-area-title')</div>
                        <div class="field">
                            <input id="area_house" type="number" min="1" class="validate_number only-number" value="" name="area"/>
                            @yield('ses-area-unit')
                        </div>
                    </div>
                    <div>
                        <div class="label">@yield('ses-photo-title') <span class="textNormal">(Максимум 5 фото)</span></div>

                        <div id="fine-uploader-manual-trigger"></div>

                    </div>
                    <div>
                        <div class="label">@yield('ses-other-info')</div>
                        <div class="field">
                            <textarea id="about_house" class="validate_length" name="house_comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('ses-legal-block')

    <input type="hidden" name="category" value="{{ 'ses' }}">

    @yield('ses-subcategory-hidden')

    @yield('ses-ground-hidden')

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/fine-uploader/fine-uploader-new.css') }}"/>
@endpush

@push('footer_scripts')
    <script type="text/javascript">
        var markerSrc = '{{ asset('images/map-marker.png') }}',
            geonamesURL = '{{ env('GEONAMES_URL') }}',
            username = '{{ env('GEONAMES_USERNAME') }}';
    </script>
    <script type="text/javascript" src="{{ asset('js/fine-uploader/jquery.fine-uploader.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/order/ses.js')}}"></script>
    <script type="text/template" id="qq-template-manual-trigger">
    <div class="upload-photo-block qq-uploader-selector qq-uploader" qq-drop-area-text="Перетягніть файли сюди">

        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>

        <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Процес завантаження файлів...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
        <div class="uploaded_photos qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <div class="uploaded-photo">
                <img class="qq-thumbnail-selector" qq-max-size="400" qq-server-scale>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel remove-uploaded-photo site_icon-delete">Cancel</button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                <div class="status-info">
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Повторити</button>
                </div>
            </div>
        </div>
        <div class="buttons upload_photo_buttons">
            <div class="qq-upload-button-selector qq-upload-button">
                <div class="upload_photo"><span>+</span> ДОДАТИ ЗОБРАЖЕННЯ</div>
            </div>
            <button type="button" id="trigger-upload" class="btn btn-primary">
                Upload
            </button>
        </div>
        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector site_button-orange">ОК</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">No</button>
                <button type="button" class="qq-ok-button-selector">Yes</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
    <script type="text/template" id="preloader-order-template">
        <div class="preloader-order-wrap">
            <div class="preloader-order">
                <h4>Дочекайтесь заватаження фото</h4>
                <div class="lds-css ng-scope">
                    <div class="lds-spinner" style="width: 100%;height:100%">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <!-- /.preloader-order -->

        </div>
        <!-- /.d -->
    </script>

    <script type="text/javascript" src="{{ asset('js/order/create/uploader-photo.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&libraries=places"
        defer></script>
    <script type="text/javascript" src="{{ asset('js/order/ses_dynamic_blocks.js')}}"></script>


@endpush