@extends('order.station-info-category.station-commercial')

@section('station-title', 'дахова комерційна')

@section('station-cost-depends')
    Вартість дахової станції залежить від <b>потужності.</b>
@endsection

@section('slider-power')
    <div class="slider-block power-station">
        <div id="power-roof-station" data-min="60" data-max="1000"></div>
        <!-- /#power-station -->
    </div>
@endsection

@section('station-type-photo')
    <img style="margin-bottom: 9px" src="{{ asset('images/roof.png') }}" alt="image">
@endsection

@section('station-area-type', 'даху')

@section('tooltip-text', 'Площа розрахована для плаского даху із незначними затіненнями від димарів/витяжок. Якщо ж у вас похилий дах на Південну сторону, то вам для цієї потужності знадобиться в 2 або в 3 рази менша площа')

@section('station-max-income')
    <div class="param-item">
        <img src="{{ asset('images/income.png') }}" alt="image">
        <p>
            Максимальний дохід за рік:
        </p>
        <strong>$2 000</strong>
        <span>(Якщо власне споживання = 0)</span>
    </div>
@endsection

@section('station-faq-info')
    <div class="station-info-block left-image right-arrow">
        <div class="station-info-img">
            <img src="{{ asset('images/roof-station.png') }}" alt="image">
        </div>
        <!-- /.station-info-img -->
        <h2>Чому дахові комерційні станції такі дорогі?</h2>
        <p>
            Основна причина, чому комерційні станції дорожчі від домашніх - це необхідність проходження багатьох бюрократичних процедур для отримання "зеленого" тарифу.
            <br><br>
            Саме тому дахові комерційні станції варто будувати потужністю від 100 кВт.<br><br>
            При цьому варто пам'ятати, що для будівництва комерційних станцій можна отримати дуже хороші
            умови кредитування.
        </p>
    </div>
@endsection

@section('ses-link')
    <li><a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'commercial', 'ground' => 'roof']) }}" class="choise-check">
            <img src="{{ asset('images/check.png') }}">
            Хочу дізнатися точну ціну на дахову комерційну станцію для мого приміщення</a></li>
@endsection

@section('site-title', 'Скільки коштує дахова комерційна сонячна електростанція під ключ для продажу енергії по “зеленому” тарифу - калькулятор, ціни, виробіток')