@extends('order.station-info-category.station-home')

@section('station-class', 'network-station')

@section('station-title', 'мережева')

@section('station-cost-depends')
    Вартість мережевої станції в першу чергу залежить від <b>потужності.</b>
@endsection

@section('station-methods-calc')
    <p class="info">А розрахувати необхідну потужність можна декількома способами:</p>
    <ol class="calculation-info">
        <li><span>1.</span> в залежності від того, яку суму ви можете інвестувати;</li>
        <li><span>2.</span> в залежності від площі даху/подвір'я, де ви можете розмістити станцію.</li>
    </ol>
    <p class="info">Скористайтеся калькулятором нижче, щоб приблизно зрозуміти яка потужність станції вам потрібна:</p>
@endsection

@section('slider-power')
    <div class="slider-block power-station" >
        <div id="power-network-station" data-min="3" data-max="30"></div>
        <!-- /#power-station -->
    </div>
@endsection

@section('station-max-income')
    <div class="param-item">
        <img src="{{ asset('images/income.png') }}" alt="image">
        <p>
            Максимальний дохід за рік:
        </p>
        <strong class="max-income">&euro;2 000</strong>
        <span>(Якщо власне споживання = 0)</span>
    </div>
@endsection

@section('alert')
    <div class="alert alert-danger">
        <b>Увага!</b> Тут дуже приблизні дані щодо доходів. Якщо у вас споживання та виробіток станції будуть
        однакові, то чистих доходів така
        станція не принесе, а лише дозволить економити.
    </div>
@endsection

@section('station-link')
    <p>Якщо ви вважаєте, що вам потрібні акумулятори (та заробіток на "зеленому" тарифі), то вам потрібна
        <a href="{{ route('assistant.hybrid') }}">гібридна сонячна електростанція</a>.</p>
@endsection

@section('ses-link')
    <li><a href="{{ route('client.order.create', ['category' => 'ses', 'subcategory' => 'network']) }}" class="choise-check">
            <img src="{{ asset('images/check.png') }}">
            Хочу дізнатися точну ціну на мережеву станцію для мого будинку</a></li>
@endsection

@section('consult-link')
    <li><a href="{{ route('services.consulting') }}"><img src="{{asset('images/chat-message.png')}}" alt=""> Замовити послуги персонального консультанта</a></li>
@endsection

@section('site-title', 'Скільки коштує мережева сонячна електростанція під ключ для продажу енергії по “зеленому” тарифу - калькулятор, ціни, виробіток')