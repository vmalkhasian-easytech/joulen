@extends('order.station-info')

@section('station-class', 'network-station roof-station')

@section('station-methods-calc')
    <p class="info">А розрахувати необхідну потужність можна декількома способами:</p>
    <ol class="calculation-info">
        <li><span>1.</span> в залежності від того, яку суму ви можете інвестувати;</li>
        <li><span>2.</span> в залежності від площі @yield('station-area-type'), де ви можете розмістити станцію.</li>
    </ol>
    <p class="info">Скористайтеся калькулятором нижче, щоб приблизно зрозуміти яка потужність станції вам потрібна:</p>
@endsection

@section('station-power')
    <div class="station-power-block ">
        <div class="enter-other-power">
            <p class="title">Потужність станції:</p>
        </div>

        @yield('slider-power')
        <!-- /.slider-block -->

        <!-- /.title -->

        <!-- /.info -->
        <div class="params-block">
            <div class="param-item">
                @yield('station-type-photo')
                <p>Необхідна площа @yield('station-area-type'):
                    <a class="tooltip-btn">
                        ?
                        <span class="tooltip">@yield('tooltip-text')</span>
                        <!-- /.tooltip -->
                    </a>
                </p>
                <strong><b class="square-text">25 га</b></strong>
            </div>
            <!-- /.param-item -->
            <div class="param-item">
                <img src="{{ asset('images/price_icon.png') }}" alt="image">
                <p>
                    Приблизна вартість станції:
                    <a class="tooltip-btn">
                        ?
                        <span class="tooltip">Вартість вказана за станцію "Під ключ" - тобто із всіма проектами, дозволами та підключенням.<br>
Зверніть увагу! Оскільки в процесі роботи станції ПДВ буде компенсовано, то для розрахунку терміну окупності потрібно від цієї суми відняти 17%.</span>
                        <!-- /.tooltip -->
                    </a>
                </p>
                <strong>$
                    <span class="min-cost" style="display: inline; font-weight: 600; font-size: 22px; color: #32394a;">13 000</span>
                    - <span class="max-cost" style="display: inline; font-weight: 600; font-size: 22px; color: #32394a;">17 000</span></strong>
            </div>
            <!-- /.param-item -->
            <div class="param-item">
                <img src="{{ asset('images/generation.png') }}" alt="image">
                <p>
                    Приблизна річна генерація:
                </p>
                <strong class="year-generation">17 тис. кВт&middot;год</strong>
            </div>
            <!-- /.param-item -->
            <div class="param-item">
                <img src="{{ asset('images/income.png') }}" alt="image">
                <p>
                    Максимальний дохід за рік:
                </p>
                <strong class="max-income">&euro; 2 000</strong>
            </div>
            <!-- /.param-item -->
        </div>
        <!-- /.params-block -->
        @yield('alert')
        <!-- /.alert alert-danger -->
    </div>
@endsection

@section('station-max-income')
    <div class="param-item">
        <img src="{{ asset('images/income.png') }}" alt="image">
        <p>
            Максимальний дохід за рік:
        </p>
        <strong>$2 000</strong>
        <span>(Якщо власне споживання = 0)</span>
    </div>
@endsection