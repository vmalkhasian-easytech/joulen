@extends( !Auth::check() || (Auth::check() && (Auth::user()->isClient() ||
Auth::user()->isAdmin())) ? 'layouts.old.app' : 'layouts.master.app')

@section('content')
    <div id="content">
        <div class="projectHeader projectCat-{{ $order->getOrderSesType() }}">
            <div class="container">
                <h1>{{ $order->title }}</h1>

                <div class="projectStatus">
                    <span class="label_status">Статус:</span>
                    <span class="site_icon-status_{{ $order->status->slug }}">{{ $order->status->name }}</span>
                </div>

                @yield('activation-date')

                <div class="additionalBlock">
                    @yield('order-header-additional-block')
                </div>

            </div>
        </div>

        @if(count($order->category->subcategories) == 1)
            @include("order.show.{$order->category->slug}", ['order' => $order, $order->category->slug => $order->orderable])
        @else
            @include("order.show.{$order->category->slug}.{$order->subcategory->slug}", ['order' => $order, $order->category->slug => $order->orderable])
        @endif

        @if(!empty($order->client_comment))
            <div class="contentBlock">
                <div class="container">
                    <div class="client_comment">
                        <h4>Коментар клієнта щодо замовлення:</h4>
                        <p>{!! nl2br(e($order->client_comment)) !!}</p>
                    </div>
                </div>
            </div>
        @endif

        @yield('user-section')

        @yield('admin-masters-requests')

    </div>
@endsection

@section('site-title')
    {{ $order->title }}
@endsection

@push('styles')
@if(Auth::check() && Auth::user()->isMaster())
<link rel="stylesheet" type="text/css" href="{{ asset('css/order-show.css') }}" />
@endif
@endpush