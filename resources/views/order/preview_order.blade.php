<div id="content" class="preview-order" style="display: none">
    <div class="contentBlock">
        <div class="container">
            <p>Шановний(-на) <strong>{{ Auth::user()->name }}</strong>,
            </p>
        </div>
    </div>

    {{--<div class="contentBlock bordered deliveryBlock" style="display: none">--}}
        {{--<div class="container">--}}
            {{--<h2>Поставка обладнання та комплектуючих</h2>--}}
            {{--<div>--}}
                {{--<ul>--}}
                    {{--<li>Необхідна потужність СЕС: <b><span class="delivery-power"></span></b> кВт</li>--}}
                    {{--<li class="delivery-capacity" style="display: none"></li>--}}
                    {{--<li class="delivery-price-category"></li>--}}
                    {{--<li class="delivery-location" style="display: none"></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="contentBlock bordered installationBlock" style="display: none">--}}
        {{--<div class="container">--}}
            {{--<h2>Монтаж та налагодження</h2>--}}
            {{--<div>--}}
                {{--<div class="projectMap">--}}
                    {{--<div id="projectMap"></div>--}}
                {{--</div>--}}

                {{--<ul>--}}
                    {{--<li class="installation-power" style="display: none"></li>--}}
                    {{--<li>Місце розташування об’єкту: <span class="installation-location"></span></li>--}}
                    {{--<li>Площа ділянки: <span class="installation-area"></span></li>--}}
                {{--</ul>--}}

                {{--<div class="installation-house-block" style="display: none">--}}
                {{--Інша інформація про будинок:--}}
                {{--<p style="white-space:pre-wrap" class="installation-house-comment"></p>--}}
                {{--</div>--}}

                {{--<div class="project_photo" style="display: none">--}}
                    {{--@yield('ses-photo-title')--}}
                    {{--<div class="installation-photo"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="contentBlock bordered documentationBlock" style="display: none">--}}
        {{--<div class="container">--}}
            {{--<h2>Документальний супровід</h2>--}}
            {{--<div>--}}
                {{--<ul>--}}
                    {{--<li>Потрібна допомога у підключенні до енергомережі та оформленні “зеленого” тарифу</li>--}}
                    {{--<li class="documentation-power" style="display: none"></li>--}}
                    {{--<li class="documentation-location" style="display: none"></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="contentBlock previewBlock">
        <div class="container">
            {{--<p>Для вашого проекту (<span class="order-title"></span>) відібрано <strong class="selected-masters"></strong>--}}
                {{--компанії.--}}
            {{--</p>--}}
            <h2 class="selected-masters"></h2>
            <p>зможуть реалізувати ваш проект</p>
            <h2 style="margin-top: 40px" class="selected-verified-masters"></h2>
            <p>із них ми перевірили особисто, і можемо за них поручитися</p>
            {{--<p>Із них Перевірених компаній: <strong class="selected-verified-masters"></strong></p>--}}
            <p>Ці <strong class="selected-masters"></strong> надішлють свої цінові пропозиції на ваш e-mail:<br>
                <strong>{{ Auth::user()->email }}</strong>
            </p>
            <div>
                Ваш номер телефону <strong>+38{{ Auth::user()->phone }}</strong> бачитимуть:
                <ul>
                    <li>
                        <div>
                            <div>
                                <input checked disabled type="checkbox" class="styledChk" id="show_admin_phone" value="1"/>
                                <label for="show_admin_phone">адміністратор сервісу Джоуль
                                    (обов’язково)</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <div>
                                <input checked type="checkbox" class="styledChk" id="show_phone" name="show_phone" value="1"/>
                                <label for="show_phone">Перевірені компанії (рекомендується)</label>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="contentBlock">
        <div class="container textCenter">
            <input type="button" class="site_button-orange onlyBorder btn-back-create-order" value="Повернутися назад">
            <input type="submit" class="site_button-orange confirm-btn" value="Надіслати заявку компаніям">
            @yield('after-submit')
        </div>
    </div>
</div>