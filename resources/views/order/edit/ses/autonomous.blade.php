@extends('order.edit.ses')

@section('ses-title', 'Оцінка вартості будівництва Автономної СЕС')

@section('ses-img-class', 'cost_header-autonomous')

@section('ses-header-title', 'Автономна СЕС')

@section('ses-header-description', 'Використовується коли нема підключення до центральної електромережі. Потрібні акумулятори')

@section('ses-power-block')
    <div class="calc1 clr">
        <div class="calc1_wrapper">
            <div class="error-block">
                <div class="calc_label">Яка потужність фотомодулів вам потрібна?</div>
                <div class="calc_field small">
                    <input type="number" class="photocell_power_input autonomous-photosell" min="0.1" id="powerf" value="{{ $order->orderable->photocell_power }}" name="photocell_power"/> кВт
                </div>
            </div>
            <div class="error-block">
                <div class="calc_label">Яка ємність акумуляторів вам потрібна?</div>
                <div class="calc_field small">
                    <input id="battery_capacity" class="photocell_power_input" type="number" min="0.1" value="{{ $order->orderable->battery_capacity }}" name="battery_capacity"/> кВт·год
                </div>
            </div>

            <div class="afterField">
                ДЛЯ ЦЬОГО НЕОБХІДНА ПЛОЩА ≈
                <strong><b><span><span class="squad-info" id="sliderRes">{{ $order->orderable->required_area }}</span>М<sup><small>2</small></sup></span></b></strong>
                <input type="hidden" class="input-squad" name="required_area" value="{{ $order->orderable->required_area }}">
            </div>
        </div>
        {{--TODO: add power calculation route name--}}
        <a href="{{ route('order.calculator') }}" class="site_button-orange button-small onlyBorder">Розрахувати</a>
        <span style="margin-top: 20px; margin-left: -10px;" class="tooltip" data-tooltip="Розрахунок покаже ПРИБЛИЗНІ рекомендовані параметри потужності панелей та ємності акумуляторів.
Точний розрахунок повинен провести монтажник, попередньо ознайомившись із вашим конкретним об’єктом.">?</span>
    </div>
@endsection

@section('ses-delivery-appointment')
    <div id="ses-delivery-appointment"  class="montageBlock"></div>
@endsection

@section('ses-where-title', 'Де потрібно встановити сонячну станцію?')

@section('ses-where-text')
    <div style="margin-top: 0"><p>Поставте маркер <img src="{{ asset('/images/map-marker.png') }}"> на ваш будинок або подвір'я -
            в залежності від того, де буде монтуватися станція.</p>
        <div>
            <p><strong>Для чого це потрібно?</strong><br>
                Вартість монтажу залежить від складності монтажу (конструкція даху, планування ділянки, тощо).<br>
                Вкажіть точне розміщення, і монтажник зможе оцінити вартість монтажу,
                а також зможе відразу надати рекомендації у вашому конкретному випадку.
            </p>
        </div>
    </div>
    <div style="display: none; margin-top: 12px; color: red" class="error-input-map">
        Потрібно обов'язково вказати точне місце встановлення станції.<br>
        Цю інформацію буде бачити лише адміністратор сервісу та авторизовані монтажники.
    </div>
@endsection

@section('ses-house-info-title', 'Інформація про ваш будинок')

@section('ses-area-title')
    Приблизна площа даху, <span class="toLowerCase">м.кв.</span>
@endsection

@section('ses-photo-title', 'Прикріпити фото будинку')

@section('ses-other-info', 'Інша інформація про ваш будинок')

@section('ses-subcategory-hidden')
    {{--TODO: rename to subcategory_id--}}
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['autonomous']->id }}">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/order/calculator_fill.js') }}"></script>
@endpush