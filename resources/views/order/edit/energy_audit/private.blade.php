@extends('order.edit.energy_audit')

@section('energy-audit-img-class', 'cost_header-private')

@section('energy-audit-header-title', 'Для приватного будинку')

@section('energy-audit-about', 'Про ваш будинок')

@section('energy-audit-explotation', 'Експлуатація будинку')

@section('energy-audit-explotation-true', 'У будинку зараз проживають')

@section('energy-audit-explotation-false', 'Будинок ще не добудовано (не експлуатується)')

@section('energy-audit-photo', 'Прикріпити фото будинку')

@section('energy-audit-residents')
    <div class="input_block">
        <div class="label">Кількість мешканців</div>
        <div class="field small_field">
            <input class="validate_required" type="number" min="1" value="{{ $order->orderable->residents }}" name="residents">
        </div>
    </div>
@endsection

@section('energy-audit-where-title', 'Де потрібно встановити сонячну станцію?')

@section('energy-audit-other-info', 'Інша інформація про ваш будинок')

@section('energy-audit-subcategory-hidden')
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['private']->id }}">
@endsection