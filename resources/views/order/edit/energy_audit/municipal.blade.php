@extends('order.edit.energy_audit')

@section('energy-audit-img-class', 'cost_header-municipal')

@section('energy-audit-header-title', 'Для бюджетного закладу')

@section('energy-audit-about', 'Про заклад')

@section('energy-audit-explotation', 'Експлуатація закладу')

@section('energy-audit-explotation-true', 'Заклад функціонує')

@section('energy-audit-explotation-false', 'Заклад ще не добудовано (не функціонує)')

@section('energy-audit-photo', 'Прикріпити фото закладу')

@section('energy-audit-where-title', 'Де знаходиться ваш заклад')

@section('energy-audit-other-info', 'Інша інформація про заклад')

@section('energy-audit-subcategory-hidden')
    <input type="hidden" name="subcategory" class="validate_required" value="{{ $order_subcategories['municipal']->id }}">
@endsection