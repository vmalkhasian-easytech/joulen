@extends('order.edit')

@section('category-content')
    <div class="container pageTitle withBlock">
        <h1>Оцінка вартості будівництва сміттєсортувальної лінії</h1>
    </div>

    <div class="contentBlock">
        <div class="container">
            <div class="cost_header cost_header-waste_sorting">
                <h4>Сміттєсортувальна лінія</h4>
                <p>Використовується для механічного
                    відбору вторсировини (пластик, папір, скло)
                    з побутових відходів для подальшого продажу.</p>
            </div>
        </div>
    </div>

    @component('layouts.validation_errors')
    @endcomponent

    <div class="contentBlock ckeckboxesBlock">
        <div class="container">
            <h2>Оберіть послуги, які вас цікавлять:</h2>
            <div>
                <div>
                    <input type="checkbox" id="c1" name="c1" data-switchBlock="#block-supply" class="checkbox-special" value="1" {{ $order->orderable->delivery_block ? 'checked' : '' }}/><label for="c1">
                        Поставка обладнання (сміттєсортувальної лінії)
                    </label>
                </div>
                <div>
                    <input type="checkbox" id="c2" name="c2" data-switchBlock="#block-montage"  class="checkbox-special" value="1" {{ $order->orderable->installation_block ? 'checked' : '' }}/><label for="c2">
                        Загальнобудівельні роботи і налагодження роботи «під ключ»
                    </label>
                </div>
                <div>
                    <input type="checkbox" id="c3" name="c3" data-switchBlock="#block-jur" class="checkbox-special block-jur" value="1" {{ $order->orderable->legal_block ? 'checked' : '' }}/><label for="c3">
                        Документальний та переговорний супровід (послуги проектного менеджменту)
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="contentBlock bordered cost_offlineStation contentSwitch" id="block-supply">
        <div class="container">

            <div class="slider_block">
                <div class="slider_img">
                    <h2>Поставка сміттєсортувальної лінії</h2>
                    {{--TODO: think on where to put this extra information about size so slides can be outputted in loop --}}
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="{{ asset('images/pic_big_station@2x.png') }}" alt="1">
                                <div class="sortingline_info">
                                    <p class="title">Велика сміттєсортувальна лінія</p>
                                    <p>Орієнтовна вартість обладнання:</p>
                                    <span>$400 000</span>
                                    <p>Необхідний об’єм відходів:</p>
                                    <span>90 000 - 100 000 тонн на рік</span>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('images/pic_meddium_station@2x.png') }}" alt="1">
                                <div class="sortingline_info">
                                    <p class="title">Середня сміттєсортувальна лінія</p>
                                    <p>Орієнтовна вартість обладнання:</p>
                                    <span>$200 000</span>
                                    <p>Необхідний об’єм відходів:</p>
                                    <span>40 000 - 60 000 тонн на рік</span>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('images/pic_small_station@2x.png') }}" alt="1">
                                <div class="sortingline_info">
                                    <p class="title">Малопотужна сміттєсортувальна лінія</p>
                                    <p>Орієнтовна вартість обладнання:</p>
                                    <span>$100 000</span>
                                    <p>Необхідний об’єм відходів:</p>
                                    <span>25 000 - 35 000 тонн на рік</span>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('images/pic_mini_station@2x.png') }}" alt="1">
                                <div class="sortingline_info">
                                    <p class="title">Міні сміттєсортувальна лінія</p>
                                    <p>Орієнтовна вартість обладнання:</p>
                                    <span>$50 000</span>
                                    <p>Необхідний об’єм відходів:</p>
                                    <span>15 000 - 20 000 тонн в рік</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider_information">
                    <p class="title">Яка потужність сміттєсортувальної
                        лінії вас цікавить?</p>
                    <div class="swiper-scrollbar_rotate">
                        <div class="swiper-scrollbar"></div>
                        <p class="line title1 item_line_swiper" data-item="1">Велика (40 т/г.)</p>
                        <p class="line title2 item_line_swiper" data-item="2">Середня (20 т/г.)</p>
                        <p class="line title3 item_line_swiper" data-item="3">Малопотужна (10 т/г.)</p>
                        <p class="line title4 item_line_swiper" data-item="4">Міні (до 5 т/г.)</p>
                        <div class="question_block" >
                            <a href="#" class="question"  data-remodal-target="modal"></a>
                            <p>Не знаєте, які об’єми відходів генеруються у вашому регіоні?</p>
                        </div>
                        @if(Auth::user()->isClient())
                            <a href="{{ route('client.preorder.create', ['category' => 'waste_audit']) }}" class="question_link">Замовте попередній аудит ринку сміття для вашого регіону</a>
                        @endif
                    </div>
                    {{--TODO: find out why there are two hidden size inputs and remove one--}}
                    <input type="hidden" name="size" value="{{ $order->orderable->size_id }}">
                </div>
            </div>
            {{--TODO: find out why there are two hidden size inputs and remove one--}}
            <input type="hidden" class="waste_input" name="size_id" value="{{ $order->orderable->size_id }}">
            <div class="contentBlock">
                <div class="showHideBlock">
                    <div class="showHideLabel">Додати коментар</div>
                    <div class="showHideContent">
                        <textarea class="" name="size_comment">{{ $order->orderable->size_comment }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contentBlock bordered montageBlock contentSwitch" id="block-montage">
        <div class="container">
            <h2>Загальнобудівельні роботи</h2>
            <div>
                <div>
                    <div>
                        <div class="label">Виберіть Область</div>
                        <div class="field">
                            <select class="custom_selec hidden_validate" id="region" name="region_id">
                                <option value="0" selected disabled>Виберіть область</option>
                                @foreach($regions as $region)
                                    <option value="{{ $region->id }}" {{ $order->region_id == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="label">Вкажіть район або Населений пункт</div>
                        <div class="field">
                            <input type="text" value="{{ $order->city }}" class="validate_required" name="city">
                        </div>
                    </div>
                </div>
                <div>
                    <div>
                        <div class="label">Інша інформація щодо будівництва</div>
                        <div class="field">
                            <textarea class="" name="about_comment">{{ $order->orderable->about_comment }}</textarea>
                        </div>
                    </div>
                    <div class="question_block">
                        <a href='#' class='question'  data-remodal-target="modal"></a>
                        <p>Не знаєте, в якому населеному пункті доцільніше будувати сміттєсортувальну лінію?</p>
                        {{--TODO: set right url--}}
                        <a href="#">Замовте попередній аудит ринку сміття для вашого регіону</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!--# end contentBlock #-->

    <div class="contentBlock bordered jurBlock contentSwitch" id="block-jur">
        <div class="container">
            <h2>Послуги проектного менеджера</h2>
            <div>
                <div>
                    <input type="checkbox" class="styledChk project_meneger" name="pm_help" id="c123"  value="1" {{ $order->orderable->legal_block ? 'checked' : '' }}/>
                    <label for="c3">Необхідна допомога в документальному оформленні та у відносинах з органами місцевого самоврядування</label>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="category" value="{{ 'waste_sorting' }}">
    <input type="hidden" name="subcategory" value="{{ $order_subcategories['waste_sorting_line']->id }}">

    <div class="remodal-wrapper">
        <div class="remodal information_modal" data-remodal-id="modal" tabindex="-1">
            <button data-remodal-action="close" class="remodal-close"></button>
            <p class="blue_line">
                Попередній аудит ринку сміття
                покаже всі переваги та недоліки
                будівництва сміттєсортувальної
                лінії у вашому регіоні.
            </p>
        </div>
    </div>
@endsection

@section('after-submit')
    @if(Auth::user()->isClient())
        <a href="{{ route('client.preorder.create', ['category' => 'waste_audit']) }}" class="button-simple underline waste_align_btn">Замовити попередній аудит</a>
    @endif
    <a href="#" class="question-light" data-remodal-target="modal"></a>
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/swiper/swiper.jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/remodal/remodal.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/order/edit/waste_sorting.js')}}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/swiper/swiper.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}"/>
@endpush