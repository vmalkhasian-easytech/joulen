@extends('order.show.ses')

@section('ses-area')
    {{--TODO: use area_unit here--}}
    <li>Площа даху: {{$ses->area}} м.кв.</li>
@endsection

@section('ses-house-comment-title')
    <h5>Інша інформація про будинок:</h5>
@endsection

@section('ses-photo-title')
    <h5>Фото будинку:</h5>
@endsection

@section('site-title')
    @parent
@endsection