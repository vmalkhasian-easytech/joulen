@if($order->orderable->delivery_block)
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Поставка сміттєсортувальної лінії</h2>
            <div>
                <p>Необхідна потужність сміттєсортувальної лінії: {{ $waste_sorting->waste_sorting_size->power }} тон/год</p>
                <h5 class="margin_bottom">Коментар клієнта: </h5>
                <p>{!! nl2br(e($waste_sorting->size_comment)) !!}</p>
            </div>
        </div>
    </div>
@endif

@if($order->orderable->installation_block)
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Загальнобудівельні роботи</h2>
            <div>
                <h5>Необхідно провести загальнобудівельні роботи «під ключ»</h5>
                <p><span>Область:</span> {{ count($order->region) > 0 ? $order->region->name : ''}}</p>
                <p><span>Район або населений пункт:</span> {{ $order->city }}</p>
                <p><span>Інша інформація щодо будівництва:</span> {!! nl2br(e($waste_sorting->about_comment)) !!}</p>
            </div>
        </div>
    </div>
@endif

@if($order->orderable->legal_block)
    <div class="contentBlock bordered">
        <div class="container">
            <h2>Послуги проектного менеджера</h2>
            <div>
                <p>Необхідна допомога в документальному оформленні та у відносинах з органами місцевого самоврядування.</p>
            </div>
        </div>
    </div>
@endif