@extends('order.show.energy_audit')

@section('energy-audit-about-title')
    <h2>Про підприємство</h2>
@endsection

@section('energy-audit-about-title')
    <li>Експлуатація підприємства:
        <span>{{ $energy_audit->exploitation ? 'Підприємство функціонує' : 'Підприємство ще не добудовано (не функціонує)' }}</span>
    </li>
@endsection

@section('energy-audit-photo-title')
    <h5>Фото підприємства:</h5>
@endsection

@section('energy-audit-other-info-title')
    <p>Інша інформація про підприємство: </p>
@endsection