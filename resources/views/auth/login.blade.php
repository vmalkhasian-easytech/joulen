@extends('layouts.app')

@section('content')
    <div class="auth-items-wrap select-category">
        <div class="container">

            <div class="auth-items">
                <div class="register-items">
                    <!-- /.register-item -->
                    <h2>Авторизація</h2>
                    <div class="authorized-items">
                        <!-- /.authorized-item -->
                        <div class="auth-facebook">
                            <a class="login-facebook" href="{{ route('facebook.login') }}">
                                <img src="{{ asset('images/facebook.svg') }}" alt="image">
                                <span>Увійти через Facebook</span>
                            </a>
                        </div>
                        <div class="login-email">
                            <form class="formLogin register-validate" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <h3>Вхід через e-mail:</h3>
                                @if (count($errors) > 0)
                                    <div class="error" id="error_block">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li style="color: red; list-style: none;">{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (session()->has('status'))
                                    <div class="error" >
                                        <ul>
                                            <li style="color: red; list-style: none;">{{ session()->get('status') }}</li>
                                        </ul>
                                    </div>
                                @endif
                                <label for="email">E-mail:</label>
                                <input type="email" id="email" class="email_required"
                                       name="email" value="{{ old('email') }}">
                                <label for="password">Пароль:</label>
                                <input type="password" id="password" class="password_required"
                                       name="password" value="">

                            <!-- /.repeat-password -->
                            <div class="login-btn">
                                <input type="submit" class="login" value="Ввійти" />
                                <span class="forgot-password" data-remodal-target="modal-2">Забули пароль?</span>
                            </div>
                            <div class="info-register">
                                <div class="unregistered">
                                    <span>Ще не зареєстровані?</span> <span>Зареєструйтеся:
                                    <a href="{{ route('client.register') }}">як клієнт</a>/
                                    <a href="{{ route('master.register') }}">як монтажник</a></span>
                                </div>
                            </div>
                            </form>

                            <!-- /.come-in -->

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--# end modals #-->

    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('js/remodal/remodal-default-theme.css') }}" />
    @endpush

    @push('footer_scripts')
    <script type="text/javascript">
        var clientRegisterFacebookURL = '{{ route('client.register') }}',
            orderCreateHomeSesURL = '{{ url('/user/search/step2') }}',
            orderCreateCommercialSesURL = '{{ url('/user/search/step22') }}';
    </script>
    <script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
    <script src="{{ asset('js/remodal/remodal.min.js') }}"></script>
    <script src="{{ asset('js/client/auth/client-login.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('body').on('click', '.send-letter', function (e) {
                e.preventDefault();
                if($('#password_recovery').valid()) {
                    $('#password_recovery').submit();
                }
            });
        });
    </script>
    @endpush

    @push('modals')
    <div class="remodal registration-facebook" data-remodal-id="modal-2">
        <button data-remodal-action="close" class="remodal-close close-btn"></button>
        <h3>Забули пароль?</h3>
        <div class="enter-email">
            <form action="{{ route('password.email') }}" method="POST" id="password_recovery" class="formLogin register-validate">
                {{ csrf_field() }}

                <p>Введіть Ваш e-mail, і ми надішлемо Вам на пошту
                посилання для відновлення паролю</p>
                <label for="input-email">E-mail:</label>
                <input type="email" placeholder="E-mail" class="email_required"
                       id="input-email" name="email" value="" style="width: 100%">
                <div class="forgot-password-btn">
                    <input type="submit" class="accept-btn remodal-confirm send-letter" value="Надіслати" >
                    <a class="backward-btn remodal-cancel" data-remodal-action="cancel">Назад</a>
                </div>
            </form>
        </div>
    </div>
    @endpush
@endsection

@section('site-title')
    Авторизація / Реєстрація у сервісі {{ env('DOMAIN') }}
@endsection