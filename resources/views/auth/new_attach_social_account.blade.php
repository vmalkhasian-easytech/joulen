@if ( !Auth::user()->profileType->profile_id )
    <a href="{{ route('facebook.attach') }}" class="button-small">Приєднати Facebook
        акаунт</a>
@else
    Facebook - аккаунт: <a href="">{{ Auth::user()->name }}</a>
    <a href="{{ route('facebook.detach') }}" class="btn btn-secondary">
        Від'єднати
    </a>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif