@extends('layouts.app')

@section('content')
        <div class="select-category change-password-wrap">
            <div class="container">
                <div class="auth-items">
                    <div class="register-items">
                        <div class="new-password-items">
                            <h2>Змінити пароль</h2>
                            <form class="change-password register-validate" method="POST" action="{{ route('password.reset') }}">
                                {{ csrf_field() }}

                                <input type="hidden" name="token" value="{{ $token }}">

                                <input id="email" type="hidden" name="email" value="{{ $email }}">

                                @if(isset($errors))
                                    @foreach($errors->all() as $error)
                                        <span style="color: red">{{$error}}</span>
                                    @endforeach
                                @endif

                                @if(session()->has('success'))
                                    <span style="color: red">{{ session()->get('success') }}</span>
                                @endif
                                <div class="new-password">
                                    <label for="new-password">Новий пароль</label>
                                    <input type="password"  class='password password_required' id="new-password" name="password" >
                                    <a class="show-password-link"><img src="{{ asset('images/view-password.svg') }}" alt="image"></a>
                                </div>
                                <div class="confirm-new-password">
                                    <label for="confirm-password">Підтвердіть пароль</label>
                                    <input type="password" class='password password_required pasword_equal'  id="confirm-password" name="password_confirmation" >
                                    <a class="show-password-link"><img src="{{ asset('images/view-password.svg') }}" alt="image"></a>
                                </div>
                                <!-- /.confirm-new-password -->

                                <div class="login-btn">
                                    <input type="submit" class="btn btn-default" value="Змінити пароль">
                                    <a href="{{ url()->previous() }}" class="btn btn-primary" style="text-align: center;">Скасувати</a>
                                </div>
                            </form>
                        </div>
                        <div class="change-password-img">
                            <img src="{{ asset('images/change-password.svg') }}" alt="image">
                        </div>
                        <!-- /.change-password-img -->
                    </div>
                    <!-- /.register-items -->
                </div>
                <!-- /.auth-items -->
            </div>
            <!-- /.container -->
        </div>

    @push('footer_scripts')
        <script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
        <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
        <script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
    @endpush
@endsection
