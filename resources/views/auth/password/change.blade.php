@extends(Auth::user()->isMaster() || Auth::user()->isDistributor() ? 'layouts.master.app' : 'layouts.app')

@section('content')
    @if(Auth::user()->isMaster() || Auth::user()->isDistributor())
    <div id="content">

        <div class="container">
            <div id="changePassword" class="passForm">
                <h1><span>Змінити пароль</span></h1>
                <div>
                    <form method="POST" class="register-validate" action="{{ route('password.change') }}">
                        {{ csrf_field() }}
                        @if(isset($errors))
                            @foreach($errors->all() as $error)
                                <span style="color: red">{{$error}}</span>
                            @endforeach
                        @endif

                        @if(session()->has('success'))
                            <span style="color: red">{{ session()->get('success') }}</span>
                        @endif

                        <div>
                            <div class="label">Новый пароль</div>
                            <div class="field">
                                <input name="password" type="password" value="" class="password_required" />
                                <span class="togglePass"></span>
                            </div>
                        </div>
                        <div>
                            <div class="label">Підтвердіть пароль</div>
                            <div class="field">
                                <input name="password_confirmation" type="password" value="" class="password_required pasword_equal" />
                                <span class="togglePass"></span>
                            </div>
                        </div>
                        <div class="space-top">
                            <a href="{{ url()->previous() }}" class="site_button-orange onlyBorder btn btn-secondary">Скасувати</a>
                            <input type="submit" class="site_button-orange btn btn-primary" value="Змінити пароль" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @else

    <div class="select-category change-password-wrap">
        <div class="container">
            <div class="auth-items">
                <div class="register-items">
                    <div class="new-password-items">
                        <h2>Змінити пароль</h2>
                        <form class="change-password register-validate" method="POST" action="{{ route('password.change') }}">
                            {{ csrf_field() }}
                            @if(isset($errors))
                                @foreach($errors->all() as $error)
                                    <span style="color: red">{{$error}}</span>
                                @endforeach
                            @endif

                            @if(session()->has('success'))
                                <span style="color: red">{{ session()->get('success') }}</span>
                            @endif
                            <div class="new-password">
                                <label for="new-password">Новий пароль</label>
                                <input type="password"  class='password password_required' id="new-password" name="password" >
                                <a class="show-password-link"><img src="{{ asset('images/view-password.svg') }}" alt="image"></a>
                            </div>
                            <div class="confirm-new-password">
                                <label for="confirm-password">Підтвердіть пароль</label>
                                <input type="password" class='password password_required pasword_equal'  id="confirm-password" name="password_confirmation" >
                                <a class="show-password-link"><img src="{{ asset('images/view-password.svg') }}" alt="image"></a>
                            </div>
                            <!-- /.confirm-new-password -->

                            <div class="login-btn">
                                <input type="submit" class="btn btn-default" value="Змінити пароль">
                                <a href="{{ url()->previous() }}" class="btn btn-primary" style="text-align: center;">Скасувати</a>
                            </div>
                        </form>
                    </div>
                    <div class="change-password-img">
                        <img src="{{ asset('images/change-password.svg') }}" alt="image">
                    </div>
                    <!-- /.change-password-img -->
                </div>





                <!-- /.register-items -->
            </div>
            <!-- /.auth-items -->
        </div>
        <!-- /.container -->
    </div>
    @endif
    @push('footer_scripts')
    <script src="{{ asset('js/lightbox/js/lightbox.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_uk.js') }}"></script>
    <script src="{{ asset('js/registration-fb/validationFormFacebook.js') }}"></script>
    @endpush
@endsection
