<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'not_deleted' => 'Ваш обліковий запис був видалений',
    'not_blocked' => 'Ваш кабінет заблоковано, для розблокування зверніться до адміністрації сайту',
    'above_zero' => 'Поле :attribute повинно мати значення більше нуля.',
    'order_attachments_limit' => 'Ви не можете завантажити більше 5 фото.',
    'no_source' => 'Поле :attribute необхідно заповнити.',

    'accepted'             => 'Поле :attribute повинно бути прийнято.',
    'active_url'           => ':attribute не є дійсним URL.',
    'after'                => 'Дата :attribute повинна бути датою після :date.',
    'after_or_equal'       => 'Дата :attribute повинна бути датою :date або датою після.',
    'alpha'                => 'Поле :attribute має містити тільки букви.',
    'alpha_dash'           => 'Поле :attribute має містити тільки букви, цифри та тире.',
    'alpha_num'            => 'Поле :attribute має містити тільки букви та цифри.',
    'array'                => ':attribute має бути масивом.',
    'before'               => 'Дата :attribute має бути датою перед :date.',
    'before_or_equal'      => 'Дата :attribute має бути датою :date. або датою перед',
    'between'              => [
        'numeric' => 'Значення поля :attribute має бути від :min до :max.',
        'file'    => 'Розмір файлу :attribute має бути :min від :max кілобайтів.',
        'string'  => 'Поле :attribute має містити від :min до :max символів.',
        'array'   => ':attribute має містити від :min до :max елементів.',
    ],
    'boolean'              => 'Поле :attribute має містити тількт значення так або ні.',
    'confirmed'            => 'Поле :attribute не підтверджено.',
    'date'                 => 'Дата :attribute не дійсна.',
    'date_format'          => 'Поле :attribute не відповідає формату :format.',
    'different'            => 'Поля :attribute і :other мають відрізнятись.',
    'digits'               => 'Значення поля :attribute має містити :digits знаків.',
    'digits_between'       => 'Значення поля :attribute має містити від :min до :max знаків.',
    'dimensions'           => 'Картинка :attribute не відповідає розмірам.',
    'distinct'             => 'Поле :attribute має дублююче значення.',
    'email'                => 'Поле :attribute повинно бути дійсною email адрессою.',
    'exists'               => 'Вибране поле :attribute недійсне.',
    'file'                 => ':attribute має бути файлом.',
    'filled'               => 'Поле :attribute потрібно заповнити.',
    'image'                => 'Поле :attribute повинно містити картинку.',
    'in'                   => 'Вибране поле :attribute не дійсне.',
    'in_array'             => 'Поля :attribute немає в :other.',
    'integer'              => 'Поле :attribute має бути integer.',
    'ip'                   => 'Поле :attribute повинно бути дійсною IP адрессою.',
    'json'                 => 'Поле :attribute повинно бути в JSON.',
    'max'                  => [
        'numeric' => 'Поле :attribute не повинно бути більш ніж :max.',
        'file'    => 'Файл :attribute не повинен бути більш ніж :max кілобайтів.',
        'string'  => 'Поле :attribute не повинно містити більш ніж :max символів.',
        'array'   => ':attribute не повинен містити :max елементів.',
    ],
    'mimes'                => 'Поле :attribute має містити файл типу: :values.',
    'mimetypes'            => 'Поле :attribute має містити файл типу: :values.',
    'min'                  => [
        'numeric' => 'Поле :attribute повинно бути щонайменше :min.',
        'file'    => 'Файл :attribute повинен бути щонайменше :min кілобайтів.',
        'string'  => 'У полі :attribute повинно бути щонайменше :min символів.',
        'array'   => 'Поле :attribute має містити щонайменше :min елементів.',
    ],
    'not_in'               => 'Вибране поле :attribute не дійсне.',
    'numeric'              => 'Поле :attribute повинно бути числом.',

    'present'              => 'Поле :attribute повинно бути присутнє.',
    'regex'                => 'Невірний формат поля :attribute.',
    'required'             => 'Поле :attribute необхідно заповнити.',
    'required_if'          => 'Поле :attribute необхідно заповнити.',
    'required_unless'      => 'Поле :attribute необхідно заповнити.',
    'required_with'        => 'Поле :attribute необхідно заповнити.',
    'required_with_all'    => 'Поле :attribute необхідно заповнити.',
    'required_without'     => 'Поле :attribute необхідно заповнити.',
    'required_without_all' => 'Поле :attribute необхідно заповнити.',
    'same'                 => 'Поля :attribute і :other мають співпадати.',
    'size'                 => [
        'numeric' => 'Поле :attribute має бути :size.',
        'file'    => 'Файл :attribute має бути :size kilobytes.',
        'string'  => 'Поле :attribute повинно містити :size символів.',
        'array'   => 'Поле :attribute повинно містити :size елементів.',
    ],
    'string'               => 'Поле :attribute повинно бути строкою.',
    'timezone'             => 'Поле :attribute повинно бути існуючою часовою зоною.',
    'unique'               => ':attribute вже зайнято.',
    'uploaded'             => ':attribute не вдалось завантажити.',
    'url'                  => ':attribute невірний формат.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'email' => [
            'exists' => 'Користувача з таким email не виявлено',
        ],
        'main_office_address' => [
            'required_if' => 'Поле :attribute необхідно заповнити',
        ],
        'second_office_address' => [
            'required_if' => 'Поле :attribute необхідно заповнити',
        ],
        'third_office_address' => [
            'required_if' => 'Поле :attribute необхідно заповнити',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'area' => 'Площа',
        'floors' => 'Кількість поверхів',
        'exploitation' => 'Експлуатація будинку',
        'residents' => 'Кількість мешканців',
        'electricity_per_year' => 'Споживання електроенергії за рік',
        'water_per_year' => 'Споживання води за рік',
        'gas_per_year' => 'Споживання газу за рік',
        'heating_type' => 'Тип опалення',
        'region_id' => 'Область',
        'city' => 'Населений пункт',
        'house_comment' => 'Інша інформація про ваш будинок',
        'other_problems' => 'Інша проблема',
        'category' => 'Категорія',
        'subcategory' => 'Підкатегорія',
        'client_comment' => 'Коментар до замовлення',
        'photocell_power' => 'Потужність фотомодулів',
        'required_area' => 'Необхідна площа',
        'ses_price_category' => 'Цінова категорія обладнання',
        'parts_comment' => 'Параметри комплектуючих',
        'lat' => 'Широта',
        'lng' => 'Довгота',
        'zoom' => 'Зум',
        'green_tariff' => 'Документальний супровід',
        'battery_capacity' => 'Ємність акумуляторів',
        'pm_help' => 'Послуги проектного менеджера',
        'high_costs' => 'Високі витрати на енергоносії',
        'cold_winter' => 'Холодно взимку',
        'hot_summer' => 'Спекотно влітку',
        'mold' => 'У приміщенні пліснява/грибки',
        'draft' => 'Протяги',
        'window_fogging' => 'Потіють вікна',
        'disbalance' => 'Невідбалансована система опалення',
        'blackout' => 'Часто \"вибиває\" пробки',
        'provider_problems' => 'Проблеми з енергопостачальниками',
        'thermal_examination' => 'Тепловізійне обстеження',
        'blower_door_test' => 'Проведення тесту на герметичність',
        'detailed_calculation' => 'Провести детальний розрахунок',
        'technical_supervision' => 'Технічний нагляд під час утеплення/реконструкції',
        'about_comment' => 'Інша інформація щодо будівництва',
        'qqfile' => 'зображення',
        'tel' => 'Телефон',
        'phone' => 'Телефон',
        'name' => 'Ім\'я',
        'work_email' => 'Робочий E-MAIL',
        'work_location' => 'В яких областях ви готові надавати свої послуги',
        'work_location.*' => 'В яких областях ви готові надавати свої послуги',
        'services' => 'Які послуги Ви надаєте?',
        'location' => 'Звідки Ви?',
        'installOnly_power' => 'Потужність СЕС',
        // master profile
        'company_name' => 'Назва компанії',
        'master_avatar' => '"Ваш логотип"',
        'about' => 'Про Компанію',
        'work_phone' => 'Робочий номер телефону',
        'second_work_phone' => 'Робочий номер телефону 2',
        'site' => 'Сайт',
        'main_office_address' => 'Адреса головного офісу',
        'second_office_address' => 'Адреса офісу 2',
        'third_office_address' => 'Адреса офісу 3',
        'main_office_room' => 'Номер головного офісу',
        'second_office_room' => 'Номер офісу 2',
        'third_office_room' => 'Номер офісу 3',
        'manager_name' => 'Ім\'я менеджера',
        'owner_name' => 'Ім\'я керівника',
        'manager_phone' => 'Телефон менеджера',
        'owner_phone' => 'Телефон керівника',
        'owner_email' => 'Email керівник',
        'manager_email' => 'Email менеджера',
        'edrpou' => 'Код ЄДРПОУ',
        'ipn' => 'Індивідуальний податковий номер',
        'commercial_ses' => 'Комерційні сес',
        'home_ses' => 'Домашні сес',
        'exchange_fund' => 'Обмінний фонд',
        'car' => 'Корпоративний автомобіль',
        'project_department' => 'Проектний відділ',
        'machinery_photo' => 'Фото будівельної техніки',
        'car_photo' => 'Фото корпоративного автомобіля',
        'eco_energy' => 'Акредитація ЕКО-ЕНЕРГІЯ',
        'exchange_fund_text' => 'Опис обладнання на складі',
        'message' => 'Повідомлення',
        'user_name' => 'Ім\'я',
        'user_phone' => 'Телефон',
        'user_email' => 'E-mail',
        'captcha' => 'капча',
        'portfolio_power' => 'Вкажіть потужність станції',
        'portfolio_month' => 'Вкажіть місяць введення в експлуатацію',
        'portfolio_year' => 'Вкажіть рік введення в експлуатацію',
        'portfolio_address' => 'Розміщення станції',
        'portfolio_subcategory_id' => 'Тип станції',
        'portfolio_subcategory_type_id' => 'Тип станції',
        'portfolio_invertor_id' => 'Вкажіть який бренд інвертора ви використали для цієї станції',
        'portfolio_panel_id' => 'Бренд панелей',
        'portfolio_installation' => 'Вкажіть чи займалася ваша компанія монтажем цієї станції',
        'portfolio_delivery' => 'Вкажіть чи займалася ваша компанія поставкою обладнання для цієї станції',
        'invertor_photo' => 'Фото інвертора',
        'panel_photo' => 'Фото панелі',
        'equipment_id' => 'Назва бренду',
        'warranty_duration' => 'Базова гарантія',
        'warranty_increase' => 'Чи є можливість продовжити гарантійний строк',
        'exchange_fund_loc' => 'Де знаходиться обмінний фонд обладнання',
        'warranty_case' => 'Хто буде займатися ремонтом',
        'built' => 'Скільки сонячних станцій з використанням бренду ви збудували',
        'machinery' => 'Будівельна техніка',
        'main_office_added' => 'Головний офіс',
        'second_office_added' => 'Другий офіс',
        'third_office_added' => 'Третій офіс',
        'main_office_lat' => 'Координати широти головного офісу',
        'main_office_lng' => 'Координати довготи головного офісу',
        'second_office_lat' => 'Координати широти другого офісу',
        'second_office_lng' => 'Координати довготи другого офісу',
        'third_office_lat' => 'Координати широти третього офісу',
        'third_office_lng' => 'Координати довготи третього офісу',
        'main_office_photo' => 'Фото головного офісу',
        'second_office_photo' => 'Фото другого офісу',
        'third_office_photo' => 'Фото третього офісу',
        'manager_mailing' => 'Отримувати емейли для менеджера',
        'owner_mailing' => 'Отримувати емейли для власника',
        'favorite_work_location' => 'Із яких областей ви б хотіли отримувати замовлення перш за все?',
        'favorite_work_location.1' => 'із яких областей ви б хотіли отримувати замовлення перш за все',
        'favorite_work_location.*' => 'Із яких областей ви б хотіли отримувати замовлення перш за все?',
        'portfolio_contractor' => 'Вкажіть чи були Ви генеральним підрядником при будівництві цієї станції',
        'portfolio_designing' => 'Вкажіть чи займалися Ви проектуванням та отриманням дозвільної документації для цієї СЕС',
        'portfolio_documentation' => 'Вкажіть чи займалася Ваша компанія документальним супроводом для даної станції',
        'main_email' => 'Такий e-mail вже використовується',

    ],

];