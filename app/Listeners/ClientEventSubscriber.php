<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 20.06.17
 * Time: 14:30
 */

namespace App\Listeners;


use App\Events\Client\OrderCanceled as OrderCanceledEvent;
use App\Events\Client\OrderClosed;
use App\Events\ClientLeftReview;
use App\Events\ClientOrderCanceled;
use App\Events\ClientOrderClosed;
use App\Events\ClientOrderCreated;
use App\Events\ClientOrderRestored;
use App\Events\ClientProfileDeleted;
use App\Events\ClientProjectPublished;
use App\Events\Client\PreorderCreated;
use App\Mail\ClientOrderAcceptedToReview;
use App\Mail\Admin\OrderCanceled as OrderCanceledMail;
use App\Mail\GuestSendMailToMaster;
use App\Models\OrderStatus;
use App\Models\User;
use App\Models\UserRole;
use App\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;
use App\Mail\ClientSendMailToMaster;
use App\Events\Guest\GuestMailToMaster;

class ClientEventSubscriber
{
    protected $user_repository;

    protected $admins;


    public function __construct()
    {
        $this->user_repository = resolve(UserRepository::class);
        $this->admins = $this->user_repository->getAllAdmins();
    }

    public function onClientDeleted(ClientProfileDeleted $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\ClientProfileDeleted($event->user))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onOrderCreated(ClientOrderCreated $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $admin_message = (new \App\Mail\ClientOrderCreated($event->user, $event->order, 'Нове замовлення - ' . $event->order->title))
            ->onConnection('database')
            ->onQueue('emails');

        $client_message = (new \App\Mail\ClientOrderAcceptedToReview($event->user, $event->order, 'Ваша заявка прийнята до розгляду'))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($admin_message);
        Mail::to($event->user)->queue($client_message);
    }

    public function onProjectPublished(ClientProjectPublished $event)
    {
        $message = (new \App\Mail\ClientProjectPublished($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->user)->queue($message);
    }

    public function onOrderCanceled(OrderCanceledEvent $event)
    {
        $message = (new OrderCanceledMail($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($this->admins)->queue($message);

    }

    public function onOrderClosed(OrderClosed $event)
    {
        $message = (new OrderCanceledMail($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($this->admins)->queue($message);
    }


    public function onOrderRestored(ClientOrderRestored $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\ClientOrderRestored($event->user, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }

    public function onLeftReview(ClientLeftReview $event)
    {
        $event->order->status_id = OrderStatus::where('slug', 'done')->first()->id;

        $event->order->save();

        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $masterMessage = (new \App\Mail\ClientLeftReview($event->user, $event->order, $event->review))
            ->onConnection('database')
            ->onQueue('emails');

        $adminMessage = (new \App\Mail\Admin\ClientLeftReview($event->user, $event->order, $event->review))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($adminMessage);
        Mail::to($event->user)->queue($masterMessage);
    }

    // TODO: rename this events and mails
    public function onGuestMailToMaster($event)
    {
        $clientMessage = (new GuestSendMailToMaster($event->user_message, $event->user_phone, $event->user_email, $event->user_name))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->master->user)->queue($clientMessage);
    }

    public function onClientMailToMaster($event)
    {
        $clientMessage = (new ClientSendMailToMaster($event->client, $event->clientEmail, $event->order))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($event->master->user)->queue($clientMessage);
    }

    public function onPreorderCreated(PreorderCreated $event)
    {
        $user = resolve(UserRepository::class);
        $admins = $user->getAllAdmins();

        $message = (new \App\Mail\Admin\PreorderCreated($event->preorder, 'Нове замовлення на ' . $event->preorder->title))
            ->onConnection('database')
            ->onQueue('emails');

        Mail::to($admins)->queue($message);
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'App\Events\ClientProfileDeleted',
            'App\Listeners\ClientEventSubscriber@onClientDeleted'
        );

        $events->listen(
            'App\Events\ClientOrderCreated',
            'App\Listeners\ClientEventSubscriber@onOrderCreated'
        );

        $events->listen(
            'App\Events\ClientProjectPublished',
            'App\Listeners\ClientEventSubscriber@onProjectPublished'
        );

        $events->listen(
            'App\Events\Client\OrderCanceled',
            'App\Listeners\ClientEventSubscriber@onOrderCanceled'
        );

        $events->listen(
            'App\Events\Client\OrderClosed',
            'App\Listeners\ClientEventSubscriber@onOrderClosed'
        );

        $events->listen(
            'App\Events\ClientOrderRestored',
            'App\Listeners\ClientEventSubscriber@onOrderRestored'
        );

        $events->listen(
            'App\Events\ClientLeftReview',
            'App\Listeners\ClientEventSubscriber@onLeftReview'
        );

        $events->listen(
            'App\Events\Client\ClientMailToMaster',
            'App\Listeners\ClientEventSubscriber@onClientMailToMaster'
        );

        $events->listen(
            'App\Events\Client\PreorderCreated',
            'App\Listeners\ClientEventSubscriber@onPreorderCreated'
        );

        $events->listen(
            'App\Events\Guest\GuestMailToMaster',
            'App\Listeners\ClientEventSubscriber@onGuestMailToMaster'
        );
    }
}