<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 16.06.17
 * Time: 20:30
 */

namespace App\Repositories;


use App\Models\Payment\Payment;
use App\Models\Payment\Status;
use App\Models\Payment\Transaction;
use Bosnadev\Repositories\Eloquent\Repository;

class PaymentStatusRepository extends BaseRepository
{
//    TODO: move this functionality to PaymentRepository
    
    protected static $successStatus = null;
    protected static $failureStatus = null;
    protected static $inProgressStatus = null;

    public function model()
    {
        return Status::class;
    }

    public function getSuccessStatus()
    {
        return $this->model->where('name', Status::STATUS_SUCCESS)->first() ?: false;
    }

    public function getFailureStatus()
    {
        return $this->model->where('name', Status::STATUS_FAILURE)->first() ?: false;
    }

    public function getInProgressStatus()
    {
        return $this->model->where('name', Status::STATUS_IN_PROGRESS)->first() ?: false;
    }

    public function getManualStatus()
    {
        return $this->model->where('name', Status::STATUS_MANUAL)->first() ?: false;
    }

    public function getCanceledStatus()
    {
        return $this->model->where('name', Status::STATUS_CANCELED)->first() ?: false;
    }
}