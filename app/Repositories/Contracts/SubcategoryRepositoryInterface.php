<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 12.06.17
 * Time: 18:40
 */

namespace App\Repositories\Contracts;


interface SubcategoryRepositoryInterface
{
    public function getAllWithFactors();
}