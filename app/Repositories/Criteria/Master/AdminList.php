<?php

namespace App\Repositories\Criteria\Master;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\DB;

class AdminList extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model
            ->select(
                'ses_moc.category_id AS ses',
                'ses_moc.mailing AS ses_mailing',
                'energy_audit_moc.category_id AS energy_audit',
                'energy_audit_moc.mailing AS energy_audit_mailing',
                'waste_sorting_moc.category_id AS waste_sorting',
                'waste_sorting_moc.mailing AS waste_sorting_mailing',
                'users.name',
                'users.created_at',
                'users.updated_at',
                'masters.*'
            )
            ->withCount([
                'order_requests',
                'order_requests_considering',
                'orders_in_progress',
                'orders_done',
            ])
            ->leftJoin('users', 'users.id', '=', 'masters.user_id')
            ->leftJoin(DB::raw('(SELECT * FROM master_order_category WHERE category_id = 1) AS ses_moc'), 'ses_moc.master_id', '=', 'masters.id')
            ->leftJoin(DB::raw('(SELECT * FROM master_order_category WHERE category_id = 2) AS energy_audit_moc'), 'energy_audit_moc.master_id', '=', 'masters.id')
            ->leftJoin(DB::raw('(SELECT * FROM master_order_category WHERE category_id = 3) AS waste_sorting_moc'), 'waste_sorting_moc.master_id', '=', 'masters.id');

        return $query;
    }
}