<?php

namespace App\Repositories\Criteria\Master\Equipment;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\DB;

class EquipmentList extends Criteria {
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model
            ->select(
                'equipment.*'
            )
            ->leftJoin('equipment_master', 'equipment_id', '=', 'equipment.id');
//            ->leftJoin('equipment_types', 'id', '=', 'equipment.type_id');


        return $query;
    }
}