<?php

namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class Attribute extends Criteria {

    protected $attribute;
    protected $value;
    protected $option;

    public function __construct($attribute, $value, $option = '=')
    {
        if(is_null($attribute) || is_null($value) || is_null($option))
            throw new InvalidArgumentException('attribute and value must be set');

        $this->attribute = $attribute;
        $this->value = $value;
        $this->option = $option;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->where($this->attribute, $this->option, $this->value);

        return $query;
    }
}