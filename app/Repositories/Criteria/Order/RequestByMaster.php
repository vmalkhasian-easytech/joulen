<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 19.06.17
 * Time: 15:45
 */

namespace App\Repositories\Criteria\Order;


use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Criteria\Criteria;
use InvalidArgumentException;

class RequestByMaster extends Criteria
{
    protected $master_id;

    public function __construct($master_id)
    {
        if (is_null($master_id))
            throw new InvalidArgumentException('master_id must be set');

        $this->master_id = $master_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with(['request' => function ($request) {
            $request->ofMaster($this->master_id);
        }])->has('request');
    }
}