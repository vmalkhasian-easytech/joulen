<?php 

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class ClientList extends Criteria {

    protected $client_id;

    public function __construct($client_id)
    {
        if(is_null($client_id))
            throw new InvalidArgumentException('client_id must be set');

        $this->client_id = $client_id;
    }
    
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with('status', 'subcategory.category', 'orderable')
            ->withCount(['requests' => function ($query) {
                $query->whereHas('master.user', function ($query){
                    $query->where(['deleted' => false, 'blocked' => false]);
                });
            }])
            ->where('orderable_type', 'ses')
            ->where(['client_id' => $this->client_id, ])
            ->whereHas('status', function ($query){
                    $query->where('slug', '!=', 'deactivated');
            })
            ->orderBy('created_at', 'desc');
        
        return $query;
    }
}