<?php

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class InMastersRegions extends Criteria {

    protected $master_id;

    public function __construct($master_id)
    {
        if(is_null($master_id))
            throw new InvalidArgumentException('master_id must be set');

        $this->master_id = $master_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->whereHas('region', function ($query) {
            $query->whereHas('mastersWork', function ($query) {
                $query->where('master_id', $this->master_id);
            });
        });

        
        return $query;
    }
}