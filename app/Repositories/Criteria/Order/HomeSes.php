<?php

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class HomeSes extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with('subcategory')->whereHas('subcategory', function ($subcategory_query){
            $subcategory_query->whereIn('slug', ['hybrid', 'autonomous', 'network']);
        });

        return $query;
    }
}