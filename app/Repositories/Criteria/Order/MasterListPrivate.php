<?php 

namespace App\Repositories\Criteria\Order;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use InvalidArgumentException;

class MasterListPrivate extends Criteria {

    protected $master_id;

    public function __construct($master_id)
    {
        if(is_null($master_id))
            throw new InvalidArgumentException('master_id must be set');

        $this->master_id = $master_id;
    }
    
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
//        TODO: get chosen master's request with score
        $query = $model->with([
                'subcategory.category',
                'orderable',
            ])->where('orderable_type', 'ses')->whereHas('status', function ($query){
                $query->where('master_id', $this->master_id)->where(function ($query){
                    $query->where('slug', 'in_progress')->orWhere('slug', 'done');
                });
            })->orderBy('created_at', 'desc');
        
        return $query;
    }
}