<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 20.06.17
 * Time: 16:46
 */

namespace App\Repositories;


use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function getAllAdmins()
    {
        return User::where('role_id', 1)->whereHas('admin', function ($admin_query){
            $admin_query->where('mailing', true);
        })->get();
    }
}