<?php

namespace App\Repositories;

use App\EmployeeType;
use App\Models\Employee;
use App\Models\User;
use App\Models\User\Distributor;
use App\Models\UserRole;
use Illuminate\Http\Request;

class DistributorRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Distributor::class;
    }

    public function create(array $data)
    {
        return parent::create($data);
    }

    public function updateOrCreateManager($request, Distributor $distributor)
    {
        $manager_second_phone = $request->input('manager_second_phone');
        $manager = Employee::updateOrCreate([
            'id' => $distributor->user->manager->id,
            'user_id' => $distributor->user->id,
            'type_id' => EmployeeType::MANAGER_ID
        ], [
            'name' => $request->input('manager_name'),
            'email' => $request->input('manager_email'),
            'phone' => phone_format($request->input('manager_phone')),
            'second_phone' => is_null($manager_second_phone) ? $manager_second_phone : phone_format($manager_second_phone),
            'mailing' => false
        ]);

        return $manager;
    }
}