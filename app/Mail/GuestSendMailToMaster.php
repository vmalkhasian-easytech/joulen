<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 29.06.17
 * Time: 15:13
 */

namespace App\Mail;


use App\Models\Client;
use App\Models\ClientEmail;
use App\Models\Master;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class GuestSendMailToMaster extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    private $user_message;

    /**
     * @var
     */
    private $user_phone;

    /**
     * @var
     */
    private $user_name;

    /**
     * @var
     */
    private $user_email;

    /**
     * Create a new message instance.
     *
     * @param $user_message
     * @param $user_phone
     * @param $user_email
     * @param $user_name
     * @param Master $master
     */
    public function __construct($user_message, $user_phone, $user_email, $user_name)
    {
        $this->user_message = $user_message;
        $this->user_email = $user_email;
        $this->user_name = $user_name;
        $this->user_phone = $user_phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Нове повідомлення від клієнта ')->view('emails.master.from_client_or_guest')->with([
            'user_message' => $this->user_message,
            'user_email' => $this->user_email,
            'user_name' => $this->user_name,
            'user_phone' => $this->user_phone
        ]);
    }
}