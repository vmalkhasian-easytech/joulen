<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 17:49
 */

namespace App\Mail;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ClientProjectPublished extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $order;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ваш проект опублікованний')->view('emails.client.order.published')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
    }
}