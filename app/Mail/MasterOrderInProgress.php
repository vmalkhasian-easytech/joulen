<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 12:19
 */

namespace App\Mail;


use App\Models\Master;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MasterOrderInProgress extends Mailable {
    use Queueable, SerializesModels;

    public $master;
    public $order;


    /**
     * Create a new message instance.
     * @param Master $master
     * @param Order $order
     */
    public function __construct(Master $master, Order $order)
    {
        $this->master = $master;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Вас обрали виконавцем')->view('emails.master.order.status.in_progress')->with([
            'master' => $this->master,
            'order' => $this->order
        ]);
        return $view;
    }
}