<?php

namespace App\Mail;

use App\Models\Equipment\EquipmentReview;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class AdminRejectedEquipmentReview extends Mailable
{
    use Queueable, SerializesModels;

    public $review;
    public $brand_route;
    public $subject;


    /**
     * Create a new message instance.
     * @param EquipmentReview $review
     * @param string $brand_route
     * @param string $subject
     */
    public function __construct(EquipmentReview $review, string $brand_route, $subject = 'Ваш відгук відхилено')
    {
        $this->review = $review;
        $this->brand_route = $brand_route;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->subject)->view('emails.user.admin_rejected_equipment_review')->with([
            'brand_title' => $this->review->equipment->title,
            'brand_route' => $this->brand_route,
            'rejection_reason' => $this->review->rejection_reason
        ]);
        return $view;
    }
}