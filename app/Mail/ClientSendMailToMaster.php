<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 29.06.17
 * Time: 15:13
 */

namespace App\Mail;


use App\Models\Client;
use App\Models\ClientEmail;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ClientSendMailToMaster extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    private $client;

    /**
     * @var
     */
    private $clientEmail;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new message instance.
     *
     * @param Client $client
     * @param ClientEmail $clientEmail
     * @param Order $order
     * @internal param Master $master
     * @internal param User $user
     */
    public function __construct(Client $client, ClientEmail $clientEmail, Order $order)
    {
        $this->client = $client;
        $this->clientEmail = $clientEmail;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Нове повідомлення від клієнта щодо проекту: ' . $this->order->title)->view('emails.client.order.emails.to_master')->with([
            'client' => $this->client,
            'order' => $this->order,
            'clientEmail' => $this->clientEmail
        ]);
    }
}