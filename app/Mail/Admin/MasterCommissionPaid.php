<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 23.06.17
 * Time: 15:48
 */

namespace App\Mail\Admin;


use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use App\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MasterCommissionPaid  extends Mailable {
    use Queueable, SerializesModels;

    public $user;
    public $order;


    /**
     * Create a new message instance.
     * @param user $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject('Спеціаліст сплатив комісію')->view('emails.admin.master_commission_paid')->with([
            'user' => $this->user,
            'order' => $this->order
        ]);
        return $view;
    }
}