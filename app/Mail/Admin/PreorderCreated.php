<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:59
 */

namespace App\Mail\Admin;


use App\Mail\Mailable;
use App\Models\Preorder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ClientOrderCreated
 * @package App\Mail
 */
class PreorderCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $preorder;
    public $subject;


    /**
     * Create a new message instance.
     * @param Preorder $preorder
     */
    public function __construct(Preorder $preorder, $subject = 'Нове замовлення')
    {
        $this->preorder = $preorder;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->subject)->view('emails.admin.preorder.created')->with([
            'preorder' => $this->preorder
        ]);
        return $view;
    }


}