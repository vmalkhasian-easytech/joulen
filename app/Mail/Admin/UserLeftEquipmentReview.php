<?php

namespace App\Mail\Admin;


use App\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserLeftEquipmentReview extends Mailable
{
    use Queueable, SerializesModels;

    public $brand_title;
    public $review_route;
    public $subject;


    /**
     * Create a new message instance.
     * @param string $brand_title
     * @param string $review_route
     * @param string $subject
     */
    public function __construct(string $brand_title, string $review_route, $subject = 'Новий відгук для бренду')
    {
        $this->brand_title = $brand_title;
        $this->review_route = $review_route;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->subject($this->subject)->view('emails.admin.user_left_equipment_review')->with([
            'brand_title' => $this->brand_title,
            'review_route' => $this->review_route
        ]);
        return $view;
    }
}