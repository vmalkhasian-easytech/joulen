<?php

namespace App\Events\Master;

use App\Models\OrderRequest;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class OrderRequestMade
{
    use SerializesModels;

    public $order_request;

    /**
     * Create a new event instance.
     *
     * @param OrderRequest $order_request
     */
    public function __construct(OrderRequest $order_request)
    {
        $this->order_request = $order_request;
    }
}