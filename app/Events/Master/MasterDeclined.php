<?php

namespace App\Events\Master;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class MasterDeclined
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}