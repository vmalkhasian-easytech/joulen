<?php

namespace App\Events\Equipment;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class EquipmentReviewStore
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $equipment_review_id;

    /**
     * Create a new event instance.
     *
     * @param int $equipment_review_id
     * @param User|Authenticatable $user
     * @internal param int $equipment_id
     * @internal param int $user_id
     */
    public function __construct(int $equipment_review_id, Authenticatable $user)
    {
        $this->equipment_review_id = $equipment_review_id;
        $this->user = $user;
    }
}
