<?php

namespace App\Events\User;


use Illuminate\Queue\SerializesModels;

class UserLeftEquipmentReview
{
    use SerializesModels;

    public $brand_title;
    public $review_route;

    /**
     * Create a new event instance.
     *
     * @param string $brand_title
     * @param string $review_route
     */
    public function __construct(string $brand_title, string $review_route)
    {
        $this->brand_title = $brand_title;
        $this->review_route = $review_route;
    }
}