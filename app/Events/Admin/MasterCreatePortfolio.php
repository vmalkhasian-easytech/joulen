<?php

namespace App\Events\Admin;

use App\Models\Master;
use Illuminate\Queue\SerializesModels;

class MasterCreatePortfolio
{
    use SerializesModels;

    public $master;
    public $route;

    /**
     * Create a new event instance.

     * @param Master $master
     * @param $route
     */
    public function __construct(Master $master, $route)
    {
        $this->master = $master;
        $this->route = $route;
    }
}