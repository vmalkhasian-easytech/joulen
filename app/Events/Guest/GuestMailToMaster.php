<?php

namespace App\Events\Guest;

use App\Models\ClientMasterEmail;
use App\Models\Master;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class GuestMailToMaster
{
    use SerializesModels;

    public $user_phone;

    public $user_email;

    public $user_name;

    public $user_message;

    public $master;

    public function __construct($user_message, $user_phone, $user_email, $user_name, $master)
    {
        $this->user_message = $user_message;
        $this->user_phone = $user_phone;
        $this->user_name = $user_name;
        $this->user_email = $user_email;
        $this->master = $master;
    }
}