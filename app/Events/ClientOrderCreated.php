<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 21.06.17
 * Time: 15:58
 */

namespace App\Events;


use App\Models\Order;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class ClientOrderCreated
{
    use SerializesModels;

    public $user;
    public $order;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $order
     * @internal param User $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }
}