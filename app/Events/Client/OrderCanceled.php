<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 22.06.17
 * Time: 19:32
 */

namespace App\Events\Client;


use App\Models\Order;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class OrderCanceled
{
    use SerializesModels;

    public $user;
    public $order;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }
}