<?php

namespace App\Policies;

use App\Models\User;
use App\Models\OrderRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the order_request.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\OrderRequest $order_request
     * @return mixed
     */
    public function update(User $user, OrderRequest $order_request)
    {
        if ($user->isAdmin() || $user->isPartner() || $user->isClient()) {
            return false;
        } elseif ($user->isMaster() && $user->master->id == $order_request->master_id && $order_request->order->status->slug == 'considering') {
            return true;
        } else {
            return false;
        }
    }
}
