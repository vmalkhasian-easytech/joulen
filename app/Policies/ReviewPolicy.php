<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Review;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user own the review.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Review $review
     * @return mixed
     */
    public function update(User $user, Review $review)
    {
        if ( $user->isPartner() || $user->isMaster()) {
            return false;
        } elseif ($user->isAdmin()) {
            return true;
        } elseif ($user->isClient()) {
            return $user->client->id === $review->client_id;
        } else {
            return false;
        }
    }
}
