<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Master;
use Illuminate\Auth\Access\HandlesAuthorization;

class MasterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can edit master.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Master $master
     * @return boolean
     */
    public function edit(User $user, Master $master)
    {
        return $user->isAdmin();
    }
}
