<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\MasterPortfolio;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Attachment;

trait ManagePortfolio
{
    public function savePortfolio(Request $request)
    {
        $master_id = Auth::user()->isMaster() ? Auth::user()->master->id : session('master_id');
        
        $portfolio = MasterPortfolio::create([
            'description' => $request->get('description'),
            'master_id' => $master_id
        ]);
        Session::put('portfolio_id', $portfolio->id);

        return response($portfolio->id, 200);
    }

    public function editPortfolio(Request $request)
    {
        $array_rules = [
            'portfolio_id' => 'exists:master_portfolios,id',
            'description' => 'required',
        ];
        $validator = Validator::make($request->all(), $array_rules);

        if ($validator->fails()) {
            return Response($validator->messages(), 422);
        }

        $sanitize_description = htmlentities($request->input('description'), ENT_QUOTES, 'UTF-8', false);
        $portfolioId = $request->get('portfolio_id');

        $portfolio = MasterPortfolio::find($portfolioId);
        $portfolio->description = $sanitize_description;
        $portfolio->save();

        Session::put('portfolio_id', $portfolio->id);

        return response($portfolioId, 200);
    }

    public function deleteImagePortfolio(Request $request)
    {
        $attachmentId = $request->get('attachment_id');
        Attachment::find($attachmentId)->deleteStorageFile();
        Attachment::find($attachmentId)->delete();

        return response($attachmentId, 200);
    }

    public function deletePorfolio(Request $request)
    {
        //TODO check if portfolio_id in portfolio table
        $master = Auth::user()->master;
//        $masterPortfolio->attachments()->create(array('link' => $imageFullName));
        $portfolioId = $request->get('portfolio_id');
        $attachments = MasterPortfolio::find($portfolioId)->attachments;
        foreach ( $attachments as $attachment ) {
            Attachment::find($attachment->id)->deleteStorageFile();
            Attachment::find($attachment->id)->delete();
        }
        MasterPortfolio::find($portfolioId)->delete();
//        $album->photos()->detach();

        return response(json_encode($attachments), 200);
    }

    public function attachImagePortfolio(Request $request)
    {
        $array_rules = [
            'qqfile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:15000',
        ];

        $validator = Validator::make($request->all(), $array_rules);

        if ($validator->fails()) {
            return Response($validator->messages(), 422);
        }

        $attach_count = MasterPortfolio::find(session('portfolio_id'))->attachments()->count();

        if($attach_count > 4) {
            return Response('Ви не можете завантажити більше п\'яти фотографій', 422);
        }
        if ($request->session()->has('portfolio_id')) {

            $file = $request->file('qqfile');

            $attachment = new Attachment();
            $attachment->attachable_id = session('portfolio_id');
            $attachment->attachable_type = 'master_portfolio';
            $attachment->saveFromPost($file);
            $attachment->save();

            return json_encode([
                "success" => true,
                "uuid" => $_REQUEST['qquuid'],
                'attachment_id' => $attachment->id
            ]);
        }
        return response('error', 500);
    }
}