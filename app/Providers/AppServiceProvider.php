<?php

namespace App\Providers;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Relations\Relation;
use DB;
use InvalidArgumentException;
use Laravel\Dusk\DuskServiceProvider;
use View;
use App;
use Illuminate\Support\Facades\Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Relation::morphMap([
            'ses' => 'App\Models\OrderSes',
            'energy_audit' => 'App\Models\OrderEnergyAudit',
            'waste_sorting' => 'App\Models\OrderWasteSorting',
            'waste_audit' => 'App\Models\PreorderWasteAudit',
            'master' => 'App\Models\Master',
            'office' => 'App\Models\MasterOffice',
            'master_portfolio' => 'App\Models\MasterPortfolio',
            'order' => 'App\Models\Order',
            'equipment' => 'App\Models\Equipment\Equipment',
            'distributor' => 'App\Models\User\Distributor'
        ]);

        if (env('DISPLAY_SQL', false) && env('APP_DEBUG', false) && $this->app->environment('local')) {
            DB::listen(function ($query) {
                var_dump($query->sql);
            });
        }

        Validator::extend('not_deleted', function ($attribute, $value, $parameters, $validator){
            $user = User::where('email', $value)->first();

            if (is_null($user))
                return true;

            return !$user->deleted;
        });

        Validator::extend('not_blocked', function ($attribute, $value, $parameters, $validator){
            $user = User::where('email', $value)->first();

            if (is_null($user))
                return true;

            return !$user->blocked;
        });

        Validator::extend('above_zero', function ($attribute, $value, $parameters, $validator) {
            return (float) $value > 0;
        });

        Validator::extend('order_attachments_limit', function ($attribute, $value, $parameters, $validator) {
            if (Session::get('order_id')) {
                $order = App\Models\Order::find(Session::get('order_id'));

                return ($order->attachments->count() < 5);
            }
            return false;
        });

        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            if (!is_null($value)) {
                // validate recaptcha
                $guzzle_client = new Client(['base_uri' => env('RECAPTCHA_URL')]);

                $response = $guzzle_client->post('/recaptcha/api/siteverify', [
                    'form_params' => [
                        'secret' => env('NOCAPTCHA_SECRET'),
                        'response' => $value
                    ]
                ]);

                return \GuzzleHttp\json_decode($response->getBody()->getContents())->success;
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Contracts\OrderRepositoryInterface', 'App\Repositories\OrderRepository');
        $this->app->bind('App\Repositories\Contracts\MasterRepositoryInterface', 'App\Repositories\MasterRepository');
        $this->app->bind('App\Repositories\Contracts\SubcategoryRepositoryInterface', 'App\Repositories\SubcategoryRepository');

        // Regions data
        App::singleton(App\Http\Composers\RegionComposer::class);
        View::composer([
            'order.create.ses', 
            'order.create.energy_audit', 
            'order.create.waste_sorting',
            'order.create.ses.network',
            'order.create.ses.hybrid',
            'order.create.ses.commercial',
            'order.create.ses.commercial.roof',
            'order.create.ses.commercial.ground',
            'order.edit.ses.network',
            'order.edit.ses.hybrid',
            'order.edit.ses.commercial',
            'order.edit.ses.commercial.roof',
            'order.edit.ses.commercial.ground',
            'order.edit.ses',
            'order.edit.energy_audit',
            'order.edit.waste_sorting',
            'admin.order.index', 
            'master.index',
            'master.create',
            'admin.master.edit', 
            'admin.master.index',
            'admin.master.portfolio.index',
            'master.settings.public',
            'master.profile',
            'admin.master.profile',
            'master.profile_pages.service',
            'master.profile_pages.contacts',
            'master.profile_pages.portfolio',
            'admin.master.portfolio.index'
        ], App\Http\Composers\RegionComposer::class);

        // Order category data
        App::singleton(App\Http\Composers\OrderCategoryComposer::class);
        View::composer(['client.order.index', 'master.order.index', 'master.create', 'admin.master.index', 'admin.master.edit', 'master.settings.public'], App\Http\Composers\OrderCategoryComposer::class);

        // Order subcategory data
        App::singleton(App\Http\Composers\OrderSubcategoryComposer::class);
        View::composer(['order.create.*', 'order.edit.*', 'admin.order.index', 'admin.master.*', 'admin.master.profile.index'], App\Http\Composers\OrderSubcategoryComposer::class);

        // Order status data
        App::singleton(App\Http\Composers\OrderStatusComposer::class);
        View::composer(['admin.order.index', 'admin.order.show'], App\Http\Composers\OrderStatusComposer::class);

        // Preorder subcategory data
        App::singleton(App\Http\Composers\PreorderSubcategoryComposer::class);
        View::composer(['preorder.create.*'], App\Http\Composers\PreorderSubcategoryComposer::class);

        App::singleton(App\Http\Composers\InvertorComposer::class);
        View::composer(['admin.master.portfolio.*', 'equipment.index.inverter'], App\Http\Composers\InvertorComposer::class);

        App::singleton(App\Http\Composers\PanelComposer::class);
        View::composer(['admin.master.portfolio.*', 'equipment.index.solar-panel'], App\Http\Composers\PanelComposer::class);

        App::singleton(App\Http\Composers\EquipmentComposer::class);
        View::composer(['admin.equipment.index', 'admin.equipment-review.index'], App\Http\Composers\EquipmentComposer::class);

        App::singleton(App\Http\Composers\StatusComposer::class);
        View::composer(['admin.equipment-review.*'], App\Http\Composers\StatusComposer::class);

        App::singleton(App\Http\Composers\EquipmentTypeComposer::class);
        View::composer(['admin.equipment.*'], App\Http\Composers\EquipmentTypeComposer::class);

        App::singleton(App\Http\Composers\BlogNewsComposer::class);
        View::composer(['temp.*', 'index'], App\Http\Composers\BlogNewsComposer::class);

        App::singleton(App\Http\Composers\BlogFaqsComposer::class);
        View::composer(['temp.*', 'index'], App\Http\Composers\BlogFaqsComposer::class);

        if ($this->app->environment('local')) {
            $this->app->register('Reliese\Coders\CodersServiceProvider');
//            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        }

        $this->app->singleton(App\Client\NbuClient::class, function ($app) {
            return new App\Client\NbuClient();
        });

        $this->app->singleton(App\Client\LiqPay::class, function ($app) {
            $liqpay = null;

            try {
                $privateKey = env('LIQPAY_PRIVATE_KEY', '');
                $publicKey = env('LIQPAY_PUBLIC_KEY', '');

                $liqpay = new App\Client\LiqPay($publicKey, $privateKey);
            } catch (InvalidArgumentException $e) {
                Log::error('Liqpay private key and public key does not set');
            }
            return $liqpay;
        });

        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
