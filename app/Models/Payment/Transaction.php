<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models\Payment;

use App\Models\Order;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaction
 * @package App\Models\Payment
 */
class Transaction extends Eloquent
{
    const STATUS_SANDBOX = 'sandbox';
    const STATUS_SUCCESS = 'success';

    public $timestamps = false;

    protected $table = "payment_transactions";

    protected $guarded = [
        'id'
    ];

    protected $dates = [
        'create_date',
        'end_date',
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public static function isValidStatus($status)
    {
        return $status == self::STATUS_SUCCESS or $status == self::STATUS_SANDBOX;
    }
}
