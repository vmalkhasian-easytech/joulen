<?php
/**
 * Created by PhpStorm.
 * User: maximdrobonoh
 * Date: 15.06.17
 * Time: 14:09
 */

namespace App\Models\Payment;


use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILURE = 'failure';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_MANUAL = 'manual';
    const STATUS_CANCELED = 'canceled';

    protected $table = "payment_statuses";

    protected $guarded = [];
}