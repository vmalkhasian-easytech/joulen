<?php

namespace App\Models\Equipment;

use App\Models\AttachmentType;
use App\Models\Master;
use App\Models\MasterStatus;
use App\Models\OrderSubcategory;
use App\Models\User\Distributor;
use App\Models\User\Master\Status;
use App\Models\UserRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Equipment extends Model
{
    const EQUIPMENT_TYPE_TO_SLUG_MAPPING = [
        'solar-panel' => 'solar_panel',
        'inverter' => 'inverter',
    ];

    public $timestamps = false;

    protected $casts = [
        'type_id' => 'int',
    ];

    protected $fillable = [
        'type_id',
        'title',
        'slug',
        'comment',
        'description',
        'website',
        'foundation_year',
        'headquarters',
        'production',
        'ukraine_office'
    ];

    public function type()
    {
        return $this->belongsTo(EquipmentType::class);
    }

    public function masters()
    {
        return $this->belongsToMany(Master::class)->whereHas('status', function ($query) {
            $query->whereIn('slug', MasterStatus::$validStatuses);
        })->withCount('order_requests_considering')
            ->leftJoin(DB::raw('(SELECT
                master_id,
                SUM(amount) AS sum
            FROM
                payments
            LEFT OUTER JOIN payment_statuses ON payments.status_id = payment_statuses.id
            WHERE payment_statuses.name = \'success\' OR payment_statuses.name = \'manual\'
            GROUP BY
                master_id) AS payments'), 'payments.master_id', '=', 'masters.id')
            ->orderByDesc('payments.sum')->orderByDesc('order_requests_considering_count');
    }

    public function distributors()
    {
        return $this->belongsToMany(Distributor::class, 'distributor_equipment');
    }

    public function logo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', AttachmentType::TYPE_EQUIPMENT_LOGO)->first()->id);
    }

    public function inverter_portfolios()
    {
        return $this->hasMany('App\Models\MasterPortfolio', 'invertor_id')->whereHas('attachments', function ($query) {
            $query->where('public', true);
        })->scopes(['verifiedStatus', 'homeSes'])->orderByDesc('created_at');
    }

    public function solar_panel_portfolios()
    {
        return $this->hasMany('App\Models\MasterPortfolio', 'panel_id')->whereHas('attachments', function ($query) {
            $query->where('public', true);
        })->scopes(['verifiedStatus', 'homeSes'])->orderByDesc('created_at');
    }

    public function reviews()
    {
        return $this->hasMany(EquipmentReview::class)->scopes(['userEquipmentReviews', 'activateStatus']);
    }

    public function master_reviews()
    {
        return $this->reviews()->whereHas('user', function ($query) {
            $query->whereHas('role', function ($q) {
                $q->where('name', UserRole::MASTER);
            });
        })->orderBy('created_at', 'desc');
    }

    public function client_reviews()
    {
        return $this->reviews()->whereHas('user', function ($query) {
            $query->whereHas('role', function ($q) {
                $q->where('name', UserRole::CLIENT);
            });
        })->orderBy('created_at', 'desc');
    }

    public function getPortfoliosAttribute()
    {
        return $this->type_id === 1 ? $this->inverter_portfolios : $this->solar_panel_portfolios;
    }

    public function getPriceRatingAttribute()
    {
        return $this->reviews()->pluck('price_rating')->avg() * 100 / 5;
    }

    public function getQualityRatingAttribute()
    {
        return $this->reviews()->pluck('quality_rating')->avg() * 100 / 5;
    }

    public function getServiceRatingAttribute()
    {
        return $this->reviews()->pluck('service_rating')->avg() * 100 / 5;
    }

    public function getSumRatingAttribute()
    {
        $rating = ($this->service_rating + $this->quality_rating + $this->price_rating) / 3;

        return $rating;
    }
}
