<?php

namespace App\Models\Equipment;

use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model
{
    const TYPE_INVERTER = 'inverter';
    const TYPE_SOLAR_PANEL = 'solar_panel';
    const INVERTER_ID = 1;
    const PANEL_ID = 2;

    protected $fillable = [
        'title',
        'slug',
    ];

    public function equipment()
    {
        return $this->hasMany(Equipment::class);
    }
}
