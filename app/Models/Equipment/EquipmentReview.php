<?php

namespace App\Models\Equipment;

use App\Models\User;
use App\Models\User\Master\Status;
use App\Models\UserRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EquipmentReview extends Model
{
    protected $fillable = [
        'equipment_id',
        'user_id',
        'positive',
        'title',
        'quality_rating',
        'price_rating',
        'service_rating',
        'body',
        'status_id',
        'user_IP'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }

    public function scopeUserEquipmentReviews($query)
    {
        $query->whereNotNull('user_id')->whereHas('user', function ($user) {
            $user->whereHas('role', function ($role) {
                $role->where('name', '!=', UserRole::ADMIN);
            });
        });
    }

    public function scopeActivateStatus($query)
    {
        $query->whereHas('status', function ($status) {
            $status->where('slug', Status::VERIFIED_STATUS);
        });
    }

    public function getSumRatingAttribute()
    {
        $rating = ($this->service_rating + $this->quality_rating + $this->price_rating) * 100 / 15;

        return $rating;
    }
}
