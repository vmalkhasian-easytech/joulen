<?php

namespace App\Models;

use App\EmployeeType;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'second_phone',
        'email',
        'mailing',
        'user_id',
        'type_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->hasOne(EmployeeType::class);
    }
}
