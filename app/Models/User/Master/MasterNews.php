<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterNews extends Model
{
    protected $fillable = [
        'master_id',
        'title',
        'url',
    ];

    public function masters()
    {
        return $this->hasMany(Master::class);
    }
}
