<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use App\EmployeeType;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Models\User\Distributor;
use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\User;
/**
 * Class Master
 * 
 * @property int $id
 * @property int $user_id
 * @property int $region_id
 * @property bool $company
 * @property string $work_email
 * @property string $website
 * @property string $about
 * @property string $photo	
 * 
 * @property \App\Models\Region $region
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $client_emails
 * @property \Illuminate\Database\Eloquent\Collection $portfolios
 * @property \Illuminate\Database\Eloquent\Collection $regions
 * @property \Illuminate\Database\Eloquent\Collection $order_requests
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $reviews
 * @property \Illuminate\Database\Eloquent\Collection $service_categories
 *
 * @package App\Models
 */
class Master extends User
{
	public $timestamps = false;

	public static $default_avatar = '/images/specialist.png';
	public static $default_photo = '/images/default_photo.jpg';
	const ACTIVE_STATUSES = ['unverified','verified'];

	protected $casts = [
		'user_id' => 'int',
		'region_id' => 'int',
		'company' => 'bool',
	];

	protected $fillable = [
		'user_id',
		'region_id',
		'work_phone',
		'second_work_phone',
		'work_email',
		'website',
		'about',
		'photo',
        'exchange_fund',
        'edrpou',
        'ipn',
        'project_department',
        'construction_machinery',
        'eco_energy',
        'year_started',
        'profile_stage',
        'status_id',
        'news_mailing',
	];

    public function scopeWithActiveStatuses($query)
    {
        $query->whereHas('status', function ($status)  {
            $status->whereIn('slug', self::ACTIVE_STATUSES);
        });
    }

	public function getWebsiteAttribute($value)
	{
		return substr( $value, 0, 4 ) === "http" ? $value : 'http://' . $value;
	}

	public function getCreatedAtAttribute($value)
	{
		return date('d.m.Y', strtotime($value));
	}

	public function getCreatedAtTimeAttribute()
	{
		return date('H:i', strtotime($this->created_at));
	}

	public function getUpdatedAtAttribute($value)
	{
		return date('d.m.Y', strtotime($value));
	}

	public function getUpdatedAtTimeAttribute()
	{
		return date('H:i', strtotime($this->updated_at));
	}

    public function region()
	{
		return $this->belongsTo(\App\Models\Region::class)->withDefault();
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class)->withDefault();
	}

	public function client_emails()
	{
		return $this->hasMany(\App\Models\ClientEmail::class);
	}

	public function portfolios()
	{
		return $this->hasMany(\App\Models\MasterPortfolio::class);
	}
    //working regions
	public function regions()
	{
		return $this->belongsToMany(\App\Models\Region::class, 'master_region', 'master_id', 'region_id')->withPivot('favorite');
	}

	public function service_categories()
	{
		return $this->belongsToMany(\App\Models\OrderCategory::class, 'master_order_category', 'master_id', 'category_id')->withPivot('mailing');
	}

    public function subcategories()
    {
        return $this->belongsToMany(\App\Models\OrderSubcategory::class, 'master_order_subcategory', 'master_id', 'subcategory_id')->withPivot('mailing');
    }

    public function subcategoriesWithMailingTrue()
    {
        return $this->belongsToMany(\App\Models\OrderSubcategory::class, 'master_order_subcategory', 'master_id', 'subcategory_id')->withPivot('mailing')->where('mailing', true);
    }

	public function order_requests()
	{
		return $this->hasMany(\App\Models\OrderRequest::class);
	}

	public function order_requests_considering()
	{
		return $this->hasMany(\App\Models\OrderRequest::class)->whereHas('order.status', function ($status_query){
			$status_query->where('slug', 'considering');
		});
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}

	public function orders_in_progress()
	{
		return $this->hasMany(\App\Models\Order::class)->whereHas('status', function($order_query){
			$order_query->where('slug', 'in_progress');
		});
	}

	public function orders_done()
	{
		return $this->hasMany(\App\Models\Order::class)->whereHas('status', function($order_query){
			$order_query->where('slug', 'done');
		});
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment\Payment::class);
	}

	public function reviews()
	{
		return $this->hasMany(\App\Models\Review::class);
	}

	public function negative_reviews()
	{
		return $this->hasMany(\App\Models\Review::class)->where('positive', false);
	}

    public function avatar()
	{
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'master_photo')->first()->id);
    }

    public function car_photo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'car_photo')->first()->id);
    }

    public function contract()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'contract')->first()->id)->withDefault();
    }

    public function machinery_photo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'machinery_photo')->first()->id);
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function publicAttachments()
    {
        return $this->attachments()->where('public', 1);
    }

    public function getAllPublicAttachments()
    {
        $portfoliosPublicAttachments = [];
        foreach ($this->portfolios as $portfolio) {
            foreach ($portfolio->publicAttachments as $attachment) {
                $portfoliosPublicAttachments[] = $attachment;
            }
        }
        $allPublicAttachments = $this->publicAttachments->merge($portfoliosPublicAttachments);

        return $allPublicAttachments;
    }

    public function main_office()
    {
        return $this->hasOne(MasterOffice::class)->where('index', 1)->withDefault();
    }

    public function second_office()
    {
        return $this->hasOne(MasterOffice::class)->where('index', 2)->withDefault();
    }

    public function third_office()
    {
        return $this->hasOne(MasterOffice::class)->where('index', 3)->withDefault();
    }

    public function offices()
    {
        return $this->hasMany(MasterOffice::class);
    }

    public function news()
    {
        return $this->hasMany(MasterNews::class);
    }

    public function verification_request()
    {
        return $this->hasOne(User\Master\VerificationRequest::class);
    }

    public function getAvatarSrc()
	{
        return $this->avatar ? $this->avatar->getPath(true) : null;
    }

    public function getCarPhotoSrc()
    {
        return $this->car_photo ? $this->car_photo->getPath() : null;
    }

    public function getMachineryPhotoSrc()
    {
        return $this->machinery_photo ? $this->machinery_photo->getPath() : null;
    }

    public function getContractSrc()
    {
        return $this->contract() ? $this->contract->getPath() : null;
    }

    public function HasUnpaidCommission()
	{
//        TODO: use relations
		$unpaid_reviews = DB::table('orders')
            ->select(DB::raw('count(*) AS count'))
            ->leftJoin('order_statuses', 'orders.status_id', '=', 'order_statuses.id')
            ->leftJoin('payments', function($join) {
                $join->on('payments.master_id', '=', 'orders.master_id');
                $join->on('payments.order_id', '=', 'orders.id');
            })
			->leftJoin('payment_statuses', 'payments.status_id', '=', 'payment_statuses.id')
            ->leftJoin('reviews', function($join) {
                $join->on('reviews.master_id', '=', 'orders.master_id');
                $join->on('reviews.order_id', '=', 'orders.id');
            })
            ->where('orders.master_id', '=', $this->id)
            ->where(function ($query) {
                $query->where('order_statuses.slug', '=', 'in_progress')
                    ->orWhere('order_statuses.slug', '=', 'done');
            })
			->whereNotNull('reviews.id')
            ->where(function ($query) {
                $query->whereNull('payments.id')
					->where(function ($query) {
						$query->where('payment_statuses.name', '!=', 'success')
						->orWhere('payment_statuses.name', '!=', 'manual');
					});
            })
            ->first();

        return $unpaid_reviews->count > 0;
    }

    public function deletePortfolios()
    {
        foreach ($this->portfolios as $portfolio) {
            $portfolio->delete();
        }
    }

    public function deleteStorageDirectory()
    {
        \File::deleteDirectory($this->getStorageDirectoryPath());
    }

    public function getStorageDirectoryPath()
    {
        return storage_path()
            . DIRECTORY_SEPARATOR . 'app'
            . DIRECTORY_SEPARATOR . 'public'
            . DIRECTORY_SEPARATOR . 'master'
            . DIRECTORY_SEPARATOR . $this->id;
    }

    public function equipment() {
	    return $this->belongsToMany(Equipment::class)->withPivot([
	            'warranty_increase',
                'warranty_duration',
                'warranty_case',
                'exchange_fund_loc',
                'built'
            ]);
    }

    public function getProfileStageAttribute($value)
    {
        switch ($value) {
            case 1: return 'general'; break;
            case 2: return 'contacts'; break;
            case 3: return 'cooperation'; break;
            case 4: return 'services'; break;
            case 5: return 'completed'; break;
            default: return 'empty';
        }
    }

    public function status()
    {
        return $this->belongsTo(MasterStatus::class)->withDefault();
    }

    public function distributors()
    {
        return $this->belongsToMany( Distributor::class);
    }

    public function hasValidStatus()
    {
        return in_array($this->status->slug, MasterStatus::$validStatuses);
    }

    public function getSesTypes()
    {
        if ($this->subcategories->contains('home', '0') == true && $this->subcategories->contains('home', '1') == true) {
            return 'both';
        }
        elseif ($this->subcategories->contains('home', '0') == true) {
            return 'commercial_ses';
        }
        elseif ($this->subcategories->contains('home', '1') == true) {
            return 'home_ses';
        }
        else {
            return 'none';
        }
    }

    public function getHomeAttribute()
    {
        return $this->getSesTypes() == 'both' || $this->getSesTypes() == 'home_ses';
    }

    public function getCommercialAttribute()
    {
        return $this->getSesTypes() == 'both' || $this->getSesTypes() == 'commercial_ses';
    }

    public function getFavoriteRegions()
    {
        return $this->regions()->where('favorite', '1')->pluck('id');
    }

    public function getHomePortfolios()
    {
        $home_subcategories_ids = OrderSubcategory::where('home', 1)->get()->pluck('id');

        return $this->portfolios()->whereIn('subcategory_id', $home_subcategories_ids)->get();
    }

    public function getCommercialPortfolios()
    {
        $commercial_subcategories_id = OrderSubcategory::where('slug', 'commercial')->first()->id;

        return $this->portfolios()->where('subcategory_id', $commercial_subcategories_id)->get();
    }

    public function getCommercialPortfoliosCoordinatesAttribute()
    {
        $commercial_portfolios = $this->getCommercialPortfolios();
        $commercial_portfolios_coords = $commercial_portfolios->map(function ($portfolio) {
            return collect($portfolio->toArray())
                ->only(['id', 'lat', 'lng'])
                ->all();
        });

        return $commercial_portfolios_coords;
    }

    public function getHomePortfoliosCoordinatesAttribute()
    {
        $home_portfolios = $this->getHomePortfolios();
        $home_portfolios_coords = $home_portfolios->map(function ($portfolio) {
            return collect($portfolio->toArray())->only(['id', 'lat', 'lng'])->all();
        });

        return $home_portfolios_coords;
    }

    public function invertors()
    {
        return $this->equipment()->where('type_id', 1);
    }

    public function panels()
    {
        return $this->equipment()->where('type_id', 2);
    }

    public function getNotAddedInvertors()
    {
        $master_invertors = $this->invertors->pluck('id')->toArray();
        $notAddedInvertors = Equipment::where('type_id', 1)
            ->whereNotIn('id', $master_invertors)->orderBy('title')->get();

        return $notAddedInvertors;
    }

    public function getNotAddedPanels()
    {
        $master_panels = $this->panels->pluck('id')->toArray();
        $notAddedPanels = Equipment::where('type_id', 2)
            ->whereNotIn('id', $master_panels)->orderBy('title')->get();

        return $notAddedPanels;
    }

    public function getHomeMailingAttribute()
    {
        $home_mailing = false;

        foreach ($this->subcategories as $subcategory) {
            if($subcategory->home && $subcategory->pivot->mailing)
                $home_mailing = true;
        }

        return $home_mailing;
    }

    public function getCommercialMailingAttribute()
    {
        $commercial = $this->subcategories()->where('subcategory_id', OrderSubcategory::where('slug', 'commercial')->first()->id)->first();

        return $commercial ? $commercial->pivot->mailing : false;
    }

    public function getActiveEmails()
    {
        $emails = [];
        if ($this->user->manager->mailing) {
            $emails[] = $this->user->manager->email;
        }
        if ($this->user->owner->mailing) {
            $emails[] = $this->user->owner->email;
        }

        return $emails;
    }

    public function scopeMastersWithOrderSubcategory($query, $request)
    {
        return $query->whereHas('subcategories', function ($category_query) use ($request) {
            $category_query->where(['subcategory_id' => $request->subcategory]);
        });
    }

    public function scopeActiveMasters($query)
    {
        return $query->whereHas('user', function ($user_query) {
            $user_query->where(['blocked' => false, 'deleted' => false]);
        });
    }

    public function scopeValidStatusMasters($query)
    {
        return $query->whereHas('status', function ($status_query) {
            $status_query->whereIn('slug', MasterStatus::$validStatuses);
        });
    }

    public function scopeVerifiedStatusMasters($query)
    {
        return $query->whereHas('status', function ($status_query) {
            $status_query->where('slug', 'verified');
        });
    }

    public function scopeMastersWithOrderRegion($query, $region_id)
    {
        return $query->whereHas('regions', function ($region_query) use ($region_id) {
            $region_query->where('id', $region_id);
        });
    }

    public function scopeSelectedMastersForOrder($query, $request, $region_id)
    {
        return $query->mastersWithOrderSubcategory($request)->activeMasters()->validStatusMasters()->mastersWithOrderRegion($region_id);
    }

    public function isInactive()
    {
        return $this->status->slug === MasterStatus::INACTIVE;
    }
}
