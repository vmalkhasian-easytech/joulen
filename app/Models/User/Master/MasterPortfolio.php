<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use App\Models\User\Master\Status;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MasterPortfolio
 * 
 * @property int $id
 * @property int $master_id
 * @property string $photo
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Master $Master
 *
 * @package App\Models
 */
class MasterPortfolio extends Eloquent
{
    protected $appends = [
        'invertor_photo_src',
        'panel_photo_src',
        'has_photos',
        'is_home'
    ];

    public static $default_photo = '/images/default_photo.jpg';

	protected $casts = [
		'master_id' => 'int',
        'power' => 'float(8,1)'
 	];

	protected $fillable = [
        'master_id',
        'region_id',
        'month',
        'year',
        'address',
        'lat',
        'lng',
        'power',
        'subcategory_id',
        'ground',
        'invertor_id',
        'panel_id',
        'general_contractor',
        'designing',
        'installation',
        'delivery',
        'documentation',
        'status_id'
	];

	public function master()
	{
		return $this->belongsTo(\App\Models\Master::class);
	}

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function publicAttachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable')->where('public', true);
    }

    public function delete()
    {
        \File::deleteDirectory($this->getStorageDirectoryPath());
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }

        parent::delete();
    }

    public function getStorageDirectoryPath()
    {
        return storage_path()
            . DIRECTORY_SEPARATOR . 'app'
            . DIRECTORY_SEPARATOR . 'public'
            . DIRECTORY_SEPARATOR . 'master_portfolio'
            . DIRECTORY_SEPARATOR . $this->id;
    }

    public function subcategory()
    {
        return $this->belongsTo(\App\Models\OrderSubcategory::class, 'subcategory_id')->withDefault();
    }

    public function invertor()
    {
        return $this->belongsTo(Equipment::class, 'invertor_id')->withDefault();
    }

    public function panel()
    {
        return $this->belongsTo(Equipment::class, 'panel_id')->withDefault();
    }

    public function invertor_photo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'invertor_photo')->first()->id);
    }

    public function panel_photo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')->where('attachment_type_id', AttachmentType::where('name', 'panel_photo')->first()->id);
    }

    public function getInvertorPhotoSrcAttribute()
    {
        return $this->invertor_photo ? $this->invertor_photo->getPath() : null;
    }

    public function getPanelPhotoSrcAttribute()
    {
        return $this->panel_photo ? $this->panel_photo->getPath() : null;
    }

    public function getHasPhotosAttribute()
    {
        return $this->attachments->count() > 0;
    }

    public function getIsHomeAttribute()
    {
        $result = false;

        if ($this->subcategory) {
            $result = $this->subcategory->home ? true : false;
        }

        return $result;
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function getCity()
    {
        $address = $this->address;
        if (strpos($address,',') == true) {
            $city = stristr($address, ',', true);
        }
        else {
            $city = $address;
        }
        return $city;
    }

    public function status()
    {
        return $this->belongsTo(Status::class)->withDefault();
    }

    public function scopeVerifiedStatus($query)
    {
        $status_id = Status::where('slug', Status::VERIFIED_STATUS)->first()->id;
        return $query->where('status_id', $status_id);
    }

    public function scopeHomeSes($query)
    {
        $commercial_id = OrderSubcategory::where('slug', OrderSubcategory::COMMERCIAL)->first()->id;
        return $query->where('subcategory_id', '!=', $commercial_id);
    }
}
