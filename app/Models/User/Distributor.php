<?php

namespace App\Models\User;

use App\Models\AttachmentType;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Models\Master;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class Distributor extends User
{
    const STATUS_MAPPING = [
        0 => 'Неактивний',
        1 => 'Активний'
    ];

    protected $fillable = [
        'user_id',
        'company_name',
        'description',
        'slug',
        'website',
        'corporate_email',
        'phone',
        'second_phone',
        'balance',
        'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function equipment()
    {
        return $this->belongsToMany(Equipment::class, 'distributor_equipment');
    }

    public function inverters()
    {
        return $this->equipment()->where('type_id', EquipmentType::INVERTER_ID);
    }

    public function panels()
    {
        return $this->equipment()->where('type_id', EquipmentType::PANEL_ID);
    }

    public function masters()
    {
        return $this->belongsToMany(Master::class)->withCount('order_requests_considering')
            ->leftJoin(DB::raw('(SELECT master_id, SUM(amount) AS sum
            FROM payments
            LEFT OUTER JOIN payment_statuses ON payments.status_id = payment_statuses.id
            WHERE payment_statuses.name = \'success\' OR payment_statuses.name = \'manual\'
            GROUP BY master_id) AS payments'), 'payments.master_id', '=', 'masters.id')
            ->orderByDesc('payments.sum')->orderByDesc('order_requests_considering_count');
    }

    public function avatar()
    {
        return $this->morphOne('App\Models\Attachment', 'attachable')
            ->where('attachment_type_id', AttachmentType::where('name', 'distributor_photo')->first()->id);
    }

    public function getAvatarSrc()
    {
        return $this->avatar ? $this->avatar->getPath(true) : asset('images/default_photo.jpg');
    }
}
