<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class MarketData extends Eloquent
{
    public $timestamps = false;

    protected $table = 'market_data';

    protected $fillable = [
        'green_tariff_home_ses',
        'green_tariff_roof_ses',
        'green_tariff_ground_ses',
        'avg_cost_network_ses_5kW',
        'avg_cost_network_ses_30kW',
        'avg_cost_auto_ses_5kW',
        'avg_cost_auto_ses_30kW',
        'ratio_hybrid_station',
        'avg_cost_roof_ses_30kW',
        'avg_cost_roof_ses_100kW',
        'avg_cost_ground_ses_100kW',
        'avg_cost_ground_ses_1000kW',
        'avg_cost_gel_batteries'
    ];

    protected $casts = [
        'green_tariff_home_ses' => 'float(9,3)',
        'green_tariff_roof_ses' => 'float(9,3)',
        'green_tariff_ground_ses' => 'float(9,3)',
        'avg_cost_network_ses_5kW' => 'float(9,3)',
        'avg_cost_network_ses_30kW' => 'float(9,3)',
        'avg_cost_auto_ses_5kW' => 'float(9,3)',
        'avg_cost_auto_ses_30kW' => 'float(9,3)',
        'ratio_hybrid_station' => 'float(5,2)',
        'avg_cost_roof_ses_30kW' => 'float(9,3)',
        'avg_cost_roof_ses_100kW' => 'float(9,3)',
        'avg_cost_ground_ses_100kW' => 'float(9,3)',
        'avg_cost_ground_ses_1000kW' => 'float(9,3)',
        'avg_cost_gel_batteries' => 'float(9,3)'
    ];

}
