<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PreorderWasteAudit
 * 
 * @property int $id
 * @property int $preorder_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 * 
 * @property \App\Models\Preorder $preorder
 *
 * @package App\Models
 */
class PreorderWasteAudit extends Eloquent
{
	protected $table = 'preorder_waste_audit';
	public $timestamps = false;

	protected $casts = [
		'preorder_id' => 'int'
	];

	protected $fillable = [
		'preorder_id',
		'name',
		'phone',
		'email',
		'comment'
	];

	public function preorder()
	{
		return $this->morphOne(\App\Models\Preorder::class, 'preorderable');
	}
}
