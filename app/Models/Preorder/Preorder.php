<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Preorder
 * 
 * @property int $id
 * @property int $subcategory_id
 * @property int $preorderable_id
 * @property string $preorderable_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\PreorderSubcategory $subcategory
 * @property \App\Models\PreorderWasteAudit $preorderable
 *
 * @package App\Models
 */
class Preorder extends Eloquent
{
	protected $casts = [
		'subcategory_id' => 'int',
		'preorderable_id' => 'int'
	];

	protected $fillable = [
		'subcategory_id',
		'preorderable_id',
		'preorderable_type'
	];

	public function getTitleAttribute()
	{
		return $this->makeTitle();
	}

	public function getCreatedAtAttribute($value)
	{
		return date('d.m.Y', strtotime($value));
	}

	public function subcategory()
	{
		return $this->belongsTo(\App\Models\PreorderSubcategory::class, 'subcategory_id');
	}

	public function getCategoryAttribute()
	{
		return $this->subcategory->category;
	}

	public function preorderable()
	{
		return $this->morphTo('preorderable', 'preorderable_type', 'preorderable_id');
	}

	protected function makeTitle()
	{
		return $this->category->name;
	}
}
