<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PreorderSubcategory
 * 
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 * 
 * @property \App\Models\PreorderCategory $category
 * @property \Illuminate\Database\Eloquent\Collection $preorders
 *
 * @package App\Models
 */
class PreorderSubcategory extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'category_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'name',
		'slug'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\PreorderCategory::class, 'category_id');
	}

	public function preorders()
	{
		return $this->hasMany(\App\Models\Preorder::class, 'subcategory_id');
	}
}
