<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PreorderCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $partners
 * @property \Illuminate\Database\Eloquent\Collection $subcategories
 *
 * @package App\Models
 */
class PreorderCategory extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function partners()
	{
		return $this->belongsToMany(\App\Models\Partner::class, 'partner_preorder_category', 'category_id', 'partner_id' );
	}

	public function subcategories()
	{
		return $this->hasMany(\App\Models\PreorderSubcategory::class, 'category_id');
	}
}
