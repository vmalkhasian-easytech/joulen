<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderSubcategory
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 *
 * @property \App\Models\OrderCategory $category
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $partners
 *
 * @package App\Models
 */
class OrderSubcategory extends Eloquent
{
    //TODO:: use this constant instead of string slugs. (replace existing)
    const COMMERCIAL = "commercial";

    public $timestamps = false;

    protected $casts = [
        'category_id' => 'int'
    ];

    protected $fillable = [
        'category_id',
        'name',
        'slug'
    ];

    public function getTitleAttribute()
    {
        return $this->makeTitle();
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\OrderCategory::class, 'category_id')->withDefault();
    }

    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class, 'subcategory_id');
    }

    public function partners()
    {
        return $this->belongsToMany(\App\Models\Partner::class, 'order_subcategory_partner', 'subcategory_id', 'partner_id');
    }

    public function portfolios()
    {
        return $this->hasMany(\App\Models\MasterPortfolio::class, 'subcategory_id');
    }

    public function subcategoryFactor()
    {
        return $this->hasOne(SubcategoryFactor::class,'subcategory_id');
    }

    public function getCommissionFactorAttribute()
    {
        return $this->subcategoryFactor->factor;
    }

    public function getCommissionPercentAttribute()
    {
        return strval($this->subcategoryFactor->factor * 100);
    }

	protected function makeTitle(){
		$category = $this->category;

		$title = '';

		if($category->slug == 'ses'){
			$title = implode(' ', [$this->name, $category->name]);
		} elseif ($category->slug == 'energy_audit'){
			$title = implode(' ', [$category->name, $this->name]);
		} elseif ($category->slug == 'waste_sorting'){
			$title = $this->name;
		}

		return $title;
	}
}
