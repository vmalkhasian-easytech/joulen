<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderAttachment
 * 
 * @property int $id
 * @property int $order_id
 * @property string $link
 * 
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class OrderAttachment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'order_id' => 'int'
	];

	protected $fillable = [
		'order_id',
		'link'
	];

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}
}
