<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRequestPrice
 * 
 * @property int $id
 * @property int $request_id
 * @property int $type_id
 * @property float $price
 * @property string $comment
 * 
 * @property \App\Models\OrderRequest $order_request
 * @property \App\Models\OrderRequestPriceType $type
 *
 * @package App\Models
 */
class OrderRequestPrice extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'request_id' => 'int',
		'type_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'request_id',
		'type_id',
		'price',
		'comment'
	];

	public function order_request()
	{
		return $this->belongsTo(\App\Models\OrderRequest::class, 'request_id');
	}

	public function type()
	{
		return $this->belongsTo(\App\Models\OrderRequestPriceType::class, 'type_id');
	}
}
