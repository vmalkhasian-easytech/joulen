<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:33 +0000.
 */

namespace App\Models;

use App\Client\NbuClient;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRequest
 *
 * @property int $id
 * @property int $master_id
 * @property int $order_id
 * @property bool $final
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Master $Master
 * @property \App\Models\Order $order
 * @property \Illuminate\Database\Eloquent\Collection $prices
 *
 * @package App\Models
 */
class OrderRequest extends Eloquent
{
    protected $casts = [
        'master_id' => 'int',
        'order_id' => 'int',
        'final' => 'bool'
    ];

    protected $fillable = [
        'master_id',
        'order_id',
        'final'
    ];

    protected static $currency;

    public function master()
    {
        return $this->belongsTo(\App\Models\Master::class);
    }

    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class);
    }

    public function prices()
    {
        return $this->hasMany(\App\Models\OrderRequestPrice::class, 'request_id');
    }

    public function scopeOfMaster($query, $masterId)
    {
        return $query->where('master_id', $masterId);
    }

    public function paymentByMasterAndOrder()
    {
        return Payment::where('master_id', $this->master_id)->where('order_id', $this->order_id)->first();
    }

    public function getPriceAttribute()
    {
        return $this->prices->sum(function (OrderRequestPrice $orderRequestPrice) {
            return $orderRequestPrice->price;
        });
    }

    public function getCommissionAttribute()
    {
        return $this->calculateCommission();
    }


    public function exchangePrice($rate)
    {
        return $this->price * $rate;
    }

    private function calculateCommission()
    {
        $factor = $this->order->subcategory->commission_factor;

        $nbuClient = resolve(NbuClient::class);
        $currency = $nbuClient->getLastUpdatedCurrency();

        if (is_null($currency)) {
            throw  new \Exception('Object of '.Currency::class.' is null. Can not convert order request price without USD rate');
        }

        return round($this->exchangePrice($currency->rate) * $factor, 2);
    }
}
