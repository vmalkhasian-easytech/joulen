<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class WasteSortingSize
 * 
 * @property int $id
 * @property int $power
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $order_waste_sortings
 *
 * @package App\Models
 */
class WasteSortingSize extends Eloquent
{
	protected $table = 'waste_sorting_size';
	public $timestamps = false;

	protected $casts = [
		'power' => 'int'
	];

	protected $fillable = [
		'power',
		'name',
		'slug'
	];

	public function order_waste_sortings()
	{
		return $this->hasMany(\App\Models\OrderWasteSorting::class, 'size_id');
	}
}
