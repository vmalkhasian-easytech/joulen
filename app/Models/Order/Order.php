<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 17 May 2017 13:24:34 +0000.
 */

namespace App\Models;

use App\Models\Payment\Payment;
use App\Models\Payment\Status;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 *
 * @property int $id
 * @property int $client_id
 * @property int $master_id
 * @property int $region_id
 * @property int $status_id
 * @property int $subcategory_id
 * @property int $orderable_id
 * @property string $orderable_type
 * @property string $city
 * @property string $client_comment
 * @property string $manager_comment
 * @property string $cancellation_comment
 * @property bool $paid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $activated_at
 *
 * @property \App\Models\Client $client
 * @property \App\Models\Master $master
 * @property \App\Models\Region $region
 * @property \App\Models\OrderStatus $status
 * @property \App\Models\OrderSubcategory $subcategory
 * @property \App\Models\OrderSes|\App\Models\OrderEnergyAudit|\App\Models\OrderWasteSorting $orderable
 * @property \App\Models\OrderRequest $request_by_master
 * @property \App\Models\Review $review_by_master
 * @property \App\Models\Payment $payment_by_master
 * @property \Illuminate\Database\Eloquent\Collection $client_emails
 * @property \Illuminate\Database\Eloquent\Collection $attachments
 * @property \Illuminate\Database\Eloquent\Collection $requests
 * @property \Illuminate\Database\Eloquent\Collection $util_requests_by_master
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $util_payments_by_master
 * @property \Illuminate\Database\Eloquent\Collection $reviews
 * @property \Illuminate\Database\Eloquent\Collection $util_reviews_by_master
 *
 * @package App\Models
 */
class Order extends Eloquent
{
    protected $casts = [
        'client_id' => 'int',
        'master_id' => 'int',
        'region_id' => 'int',
        'status_id' => 'int',
        'subcategory_id' => 'int',
        'orderable_id' => 'int',
        'paid' => 'bool',
        'ground' => 'bool',
        'show_phone' => 'bool'
    ];

    //todo:  remove paid column

    protected $fillable = [
        'client_id',
        'master_id',
        'region_id',
        'status_id',
        'subcategory_id',
        'orderable_id',
        'orderable_type',
        'city',
        'client_comment',
        'manager_comment',
        'cancellation_comment',
        'paid',
        'activated_at',
        'ground',
        'show_phone'
    ];

    public function getTitleAttribute()
    {
        return $this->makeTitle();
    }

    public function getEmailTitleAttribute()
    {
        return $this->makeEmailTitle();
    }

//    public function getPaidAttribute()
//    {
//        return !empty($this->payment_by_master) && ($this->payment_by_master->status->name == 'success' || $this->payment_by_master->status->name == 'manual');
//    }

    public function getCategoryAttribute()
    {
        return $this->subcategory->category;
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d.m.Y', strtotime($value));
    }

    public function getCreatedAtTimeAttribute()
    {
        return date('H:i', strtotime($this->created_at));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d.m.Y', strtotime($value));
    }

    public function getUpdatedAtTimeAttribute()
    {
        return date('H:i', strtotime($this->updated_at));
    }

    public function getActivatedAtNoTimeAttribute()
    {
        return $this->activated_at ? date('d.m.Y', strtotime($this->activated_at)) : null;
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function master()
    {
        return $this->belongsTo(\App\Models\Master::class);
    }

    public function region()
    {
        return $this->belongsTo(\App\Models\Region::class);
    }

    public function status()
    {
        return $this->belongsTo(\App\Models\OrderStatus::class, 'status_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(\App\Models\OrderSubcategory::class, 'subcategory_id');
    }

    public function client_emails()
    {
        return $this->hasMany(\App\Models\ClientEmail::class);
    }

    public function attachments()
    {
        return $this->morphMany(\App\Models\Attachment::class, 'attachable');
    }

    public function requests()
    {
        return $this->hasMany(\App\Models\OrderRequest::class);
    }

    public function requests_from_active_masters()
    {
        return $this->hasMany(\App\Models\OrderRequest::class)
            ->whereHas('master.status', function ($status_query) {
            $status_query->whereIn('slug', MasterStatus::$validStatuses);
        })
            ->whereHas('master.user', function ($user_query) {
            $user_query->where(['blocked' => false, 'deleted' => false]);
        });
    }

    public function util_requests_by_master()
    {
        return $this->hasMany(\App\Models\OrderRequest::class, 'master_id', 'master_id');
    }

    public function getRequestByMasterAttribute()
    {
        return $this->requests->intersect($this->util_requests_by_master)->first();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function util_payments_by_master()
    {
        return $this->hasMany(Payment::class, 'master_id', 'master_id');
    }

    public function getPaymentByMasterAttribute()
    {
        return $this->payments->intersect($this->util_payments_by_master)->first();
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    public function util_reviews_by_master()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    public function getReviewByMasterAttribute()
    {
        return $this->reviews->intersect($this->util_reviews_by_master)->first();
    }

    public function orderable()
    {
        return $this->morphTo('orderable', 'orderable_type', 'orderable_id');
    }

    public function orderRequestByMaster()
    {
        return $this->requests->where('master_id', $this->master_id)->first();
    }

    public function scopeOfMaster($query, $masterId)
    {
        return $query->where('master_id', $masterId);
    }

//    Used only with RequestByMaster and CommssionList Criteria because of non-working scope. Don't use it anywhere and better just fix it
    public function request()
    {
        return $this->hasOne(OrderRequest::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function getPriceAttribute()
    {
        //todo: remove this to repository
        if (is_null($this->master_id)) {
            return 0;
        }
//        TODO:resolve this with scopes
        $request = $this->requests()->where('master_id', $this->master_id)->first();

        if (is_null($request)) {
            return 0;
        }
        return $request->price;
    }

    public function getCommissionAttribute()
    {
        $commission = 0;

        if (is_null($this->master)) {
            return $commission;
        }

        if ($this->payment_by_master) {
            $commission = $this->payment_by_master->amount;

            return $commission;
        }


        if ($orderRequest = $this->orderRequestByMaster()) {
            $commission = $orderRequest->commission;
        }

        return $commission;
    }

    // makeTitle has been done so bad because the owner said it needs to be done quickly and it will be remade
    protected function makeTitle()
    {
        $subcategory = $this->subcategory;
            if (!$subcategory)
                return false;

        $category = $subcategory->category ?? OrderCategory::where('slug', 'ses')->first();
        $price_types = $this->price_types->pluck('name')->toArray();
        $price_types_string = '';
        $title = '';

        if ($category->slug == 'ses') {
            if ($this->price_types) {
                if ($this->price_types->count() == 1) {
                    if ($this->price_types->contains('slug', 'ses_parts')) {
                        $price_types_string = '(лише поставка обладнання)';
                    }
                    if ($this->price_types->contains('slug', 'ses_installation')) {
                        $price_types_string = '(лише монтаж)';
                    }
                    if ($this->price_types->contains('slug', 'ses_legal_support')) {
                        $price_types_string = '(лише документальний супровід)';
                    }
                } elseif ($this->price_types->count() == 2) {
                    if ($this->price_types->contains('slug', 'ses_installation') && $this->price_types->contains('slug', 'ses_legal_support')) {
                        $price_types_string = '(лише монтаж та супровід)';
                    }
                    if ($this->price_types->contains('slug', 'ses_parts') && $this->price_types->contains('slug', 'ses_legal_support')) {
                        $price_types_string = '(без монтажу)';
                    }
                    if ($this->price_types->contains('slug', 'ses_parts') && $this->price_types->contains('slug', 'ses_installation') && $this->subcategory->slug != 'autonomous') {
                        $price_types_string = '(без документального супроводу)';
                    }

                }
            }

            $commercial_type = '';
            if($this->ground == '1') {
                $commercial_type = 'Наземна';
            }
            elseif ($this->ground == '0') {
                $commercial_type = 'Дахова';
            }
            $title = implode(' ', [
                $commercial_type,
                $subcategory->name,
                $category->name,
                $this->orderable->photocell_power,
                'кВт',
                $price_types_string
            ]);
        } elseif ($category->slug == 'energy_audit') {
            $title = implode(' ', [$category->name, $subcategory->name]);
        } elseif ($category->slug == 'waste_sorting') {
            $title = $subcategory->name;
        }

        return $title;
    }

    protected function makeEmailTitle()
    {
        $subcategory = $this->subcategory;
        $category = $subcategory->category;
        $title = 'Тип';

        if ($category->slug == 'ses' || $category->slug == 'energy_audit') {
            $title .= ' ' . $category->name . ': ' . $subcategory->name;
        } elseif ($category->slug == 'waste_sorting') {
            $title .= ': ' . $subcategory->name;
        }

        return $title;
    }

    public function isNeedPay()
    {
        $this->paymentByMaster;
        return is_null($this->paymentByMaster);

    }

    public function isPaid()
    {
        if (!$this->payment_by_master)
            return false;

        return $this->payment_by_master->status->name == Status::STATUS_SUCCESS || $this->payment_by_master->status->name == Status::STATUS_MANUAL;
    }

    public function price_types()
    {
        return $this->belongsToMany(\App\Models\OrderRequestPriceType::class, 'order_price_type_pivot', 'order_id', 'price_type_id');
    }

    public function getOrderSesType()
    {
        $ses_type = $this->subcategory->slug;

        if($ses_type == 'commercial') {
            if($this->ground == 1) {
                $ses_type = 'ground';
            }
            elseif ($this->ground == 0) {
                $ses_type = 'roof';
            }
        }

        return $ses_type;
    }
}
