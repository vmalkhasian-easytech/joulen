<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File as FileHelper;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentType extends Model
{
    const TYPE_EQUIPMENT_LOGO = 'equipment_logo';
    const TYPE_MASTER = 'master';
    const TYPE_PANEL_PHOTO = 'panel_photo';
    const TYPE_INVERTER_PHOTO = 'invertor_photo';
    const DISTRIBUTOR_PHOTO = 'distributor_photo';

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'attachment_type_id');
    }
}
