<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\ClientMasterEmail;
use App\Models\Master;
use App\Events\Guest\GuestMailToMaster;


class GuestController extends Controller
{
    public function sendMasterEmail(Request $request)
    {
        $this->validate($request, [
            'master_id' => 'exists:masters,id',
            'user_phone' => 'required|regex:/^\+38[\(]\d{3}[\)]\s{1}\d{3}-\d{2}\-\d{2}$/',
            'user_name' => 'required|max:50',
            'user_email' => 'required|string|email|max:100',
            'captcha' => 'required',
            'message' => 'required|string|max:500'
        ]);

        $master_id = $request->input('master_id');
        $master = Master::find($master_id);
        $user_email = $request->input('user_email');
        $user_phone = $request->input('user_phone');
        $user_name = $request->input('user_name');
        $sanitized_message = htmlentities($request->input('message'), ENT_QUOTES, 'UTF-8', false);

        if ($master) {
            ClientMasterEmail::create([
                'master_id' => $master_id,
                'user_phone' => $user_phone,
                'user_email' => $user_email,
                'user_name' => $user_name,
                'message' => $sanitized_message
            ]);

            event(new GuestMailToMaster($sanitized_message, $user_phone, $user_email, $user_name, $master));

            return Response('Successfully', 200);
        }else {
            return Response('False', 404);
        }
    }
}
