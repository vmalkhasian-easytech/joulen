<?php

namespace App\Http\Controllers\Admin;

use App\Events\Master\MasterActivated;
use App\Events\Master\MasterDeclined;
use App\Events\MasterBlocked;
use App\Http\Requests\Master\EquipmentRequest;
use App\Http\Requests\Master\PortfolioRequest;
use App\Http\Requests\Master\ProfileRequest;
use App\Models\Equipment\Equipment;
use App\Models\EquipmentExchangeFund;
use App\Models\EquipmentGuaranteeRepair;
use App\Models\Equipment\EquipmentType;
use App\Models\Master;
use App\Models\MasterNews;
use App\Models\MasterStatus;
use App\Models\OrderCategory;
use App\Models\OrderSubcategory;
use App\Models\User;
use App\Repositories\Criteria\Attribute;
use App\Repositories\Criteria\Master\AdminList;
use App\Repositories\Criteria\RelationAttribute;
use App\Models\MasterPortfolio;
use App\Repositories\MasterRepository;
use App\Traits\ManagePortfolio;
use App\Traits\RestoreUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attachment;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class MasterController extends Controller
{
    use RestoreUser;
    use ManagePortfolio;

    /**
     * @var MasterRepository
     */
    protected $masterRepository;

    /**
     * @var string
     */
    protected $redirect_after_restore;

    public function __construct(MasterRepository $masterRepository)
    {
        $this->masterRepository = $masterRepository;
        $this->redirect_after_restore = route('admin.master.index');
    }

    /**
     * Display a list of masters.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //TODO: add validation
        //TODO: optimise sql queries

        $filter = $request->all();
        $this->masterRepository->pushCriteria(new AdminList());

        if(!empty($filter['status_id'])){
            if($filter['status_id'] == 'deleted') {
                $this->masterRepository->pushCriteria(new RelationAttribute('user', 'deleted', true));
            }
            elseif ($filter['status_id'] == 'blocked') {
                $this->masterRepository->pushCriteria(new RelationAttribute('user', 'blocked', true));
            }
            else {
                $this->masterRepository->pushCriteria(new RelationAttribute('status', 'id', $filter['status_id']));
            }
        }

//        TODO: refactoring after know what to do with order subcategory
        if(!empty($filter['category_id']))
            $this->masterRepository->pushCriteria(new RelationAttribute('subcategories', 'order_subcategories.id', $filter['category_id']));

        if(!empty($filter['region_id']))
            $this->masterRepository->pushCriteria(new RelationAttribute('offices', 'region_id', $filter['region_id']));

        if(!empty($filter['regions_id']))
            $this->masterRepository->pushCriteria(new RelationAttribute('regions', 'region_id', $filter['regions_id']));

        if(!empty($filter['master_email']))
            $this->masterRepository->pushCriteria(new RelationAttribute('user', 'email', $filter['master_email']));

        if(!empty($filter['company_name']))
            $this->masterRepository->pushCriteria(new RelationAttribute('user', 'name', $filter['company_name']));

        $masters = $this->masterRepository->orderBy($request->input('sortfield', 'updated_at'), $request->input('sortorder', 'desc'));
        if(in_array($request->input('sortfield', 'updated_at'), OrderCategory::select('slug')->get()->pluck('slug')->toArray())){
            $masters = $masters->orderBy($request->input('sortfield', 'updated_at') . '_mailing', $request->input('sortorder', 'desc'));
        }
        $masters = $masters->paginate($request->input('perpage', 20));
        $master_statuses = MasterStatus::all();
        $masters_name = Master::all();

        $request->flash();

        return view('admin.master.index', compact('masters',
            'master_statuses', 'masters_name'));
    }

    /**
     * Show the form for editing the specified master.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $master = Master::with('user.manager', 'user.owner')->findOrFail($id);
        $invertor_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->get();
        $panel_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->get();

        $subcategories = OrderSubcategory::all()->pluck('name', 'id');
        $exchange_funds = EquipmentExchangeFund::all()->pluck('name', 'id');
        $guarantee_repairs = EquipmentGuaranteeRepair::all()->pluck('name', 'id');
        $equipment = Equipment::all()->pluck('title', 'id');
        $master_statuses = MasterStatus::all()->keyBy('slug');

        return view('admin.master.edit', compact('invertor_type_equipment', 'panel_type_equipment', 'equipment', 'master',
            'exchange_funds', 'guarantee_repairs', 'subcategories', 'master_statuses'));
    }

    /**
     * Update the specified order in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validate($request, [
            'main_email' => [
                'required',
                Rule::unique('users', 'email')->ignore($master->user->id),
            ],
            'company_status' => 'required'
        ]);
        $this->masterRepository->updateProfile($request, $master);
        $this->newsSync($request, $master);
        if ($request->input('home_ses_mailing') && $request->input('commercial_ses_mailing')) {
            foreach ($master->subcategories as $subcategory) {
                $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => true]);
            };
        }
        else if ($request->input('home_ses_mailing')) {
            foreach ($master->subcategories as $subcategory) {
                if ($subcategory->home == true) {
                    $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => true]);
                }
                else {
                    $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => false]);
                }
            };
        }
        else if ($request->input('commercial_ses_mailing')) {
            foreach ($master->subcategories as $subcategory) {
                if ($subcategory->home == false) {
                    $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => true]);
                }
                else {
                    $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => false]);
                }
            };
        }
        else {
            foreach ($master->subcategories as $subcategory) {
                $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => false]);
            };
        }

        $master->user->email = $request->main_email;
        $master->user->blocked = $request->input('blocked', false);
        if($master->user->blocked == 1) {
            event(new MasterBlocked($master->user));
        }
        $master->user->deleted = $request->input('deleted', false);
        $master->user->save();

        return redirect()->route('admin.master.index');
    }

    public function equipmentStore(EquipmentRequest $request)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $master->equipment()->syncWithoutDetaching([$request->equipment_id => [
            'warranty_increase' => $request->warranty_increase,
            'warranty_duration' => $request->warranty_duration,
            'warranty_case' => $request->warranty_case,
            'exchange_fund_loc' => $request->exchange_fund_loc,
            'built' => $request->built,
        ]]);

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function equipmentUpdate(EquipmentRequest $request)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $master->equipment()->syncWithoutDetaching([$request->equipment_id => [
            'warranty_increase' => $request->warranty_increase,
            'warranty_duration' => $request->warranty_duration,
            'warranty_case' => $request->warranty_case,
            'exchange_fund_loc' => $request->exchange_fund_loc,
            'built' => $request->built,
        ]]);

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function equipmentDelete(Request $request, $id)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $master->equipment()->detach($id);

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function getEquipment(Request $request, $id)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $equipment = $master->equipment->keyBy('id')->get($id);

        return $equipment;
    }

    public function portfolioStore(PortfolioRequest $request)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $portfolio = $this->masterRepository->portfolioStore($request, $master);

        return $portfolio;
    }

    public function getPortfolio($id)
    {
        $portfolio = MasterPortfolio::findOrFail($id);

        return $portfolio;
    }

    public function portfolioUpdate(PortfolioRequest $request)
    {
        $master = Master::findOrFail($request->input('master_id'));
        $portfolio = $this->masterRepository->portfolioUpdate($request, $master);

        return $portfolio;
    }

    public function portfolioDelete($id)
    {
        $portfolio = MasterPortfolio::findOrFail($id);
        $portfolio->delete();

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function statusUpdate($id, $status_id, Request $request)
    {
        $master = Master::where('id', $id)->with('status')->first();
        $master->status_id = $status_id;
        if ($status_id == MasterStatus::where("slug", 'canceled')->first()->id) {
            $master->rejection_reason = $request->rejection_reason;
            $master->save();
            event(new MasterDeclined($master->user));
        }
        if ($status_id == MasterStatus::where("slug", 'canceled')->first()->id || $status_id == MasterStatus::where("slug", 'inactive')->first()->id) {
            $master->profile_stage = 0;
            $master->save();
        }

        if ($status_id == MasterStatus::where("slug", 'unverified')->first()->id) {
            $master->activation_date = Carbon::now();
            $master->save();
            event(new MasterActivated($master->user));
        }
        return $master;
    }

    public function getBrands(Request $request, $type_id)
    {
        $master = Master::findOrFail($request->input('master_id'));

        $invertor_type = EquipmentType::where('slug', EquipmentType::TYPE_INVERTER)->first()->id;
        $panel_type = EquipmentType::where('slug', EquipmentType::TYPE_SOLAR_PANEL)->first()->id;

        if ($type_id == $invertor_type) {
            $equipment = $master->getNotAddedInvertors();
        }
        elseif ($type_id == $panel_type) {
            $equipment = $master->getNotAddedPanels();
        }

        return $equipment;
    }

    public function newsSync(Request $request, Master $master)
    {
        $master_news = $master->news->pluck('id')->toArray();
        $updated_news = is_array($request->news_title_updated) ? array_keys($request->news_title_updated) : [];
        $news_to_delete = array_diff($master_news, $updated_news);
        MasterNews::whereIn('id', $news_to_delete)->delete();

        if (is_array($request->news_title_added)) {
            foreach ($request->news_title_added as $key => $title) {
                if ($title) {
                    MasterNews::create([
                        'master_id' => $master->id,
                        'title' => $title,
                        'url' => $request->news_url_added[$key]
                    ]);
                }
            }
        }

        if (is_array($request->news_title_updated)) {
            foreach ($request->news_title_updated as $key => $title) {
                $news = MasterNews::findOrFail($key);
                $news->title = $title;
                $news->url = $request->news_url_updated[$key];
                $news->save();
            }
        }
    }
}
