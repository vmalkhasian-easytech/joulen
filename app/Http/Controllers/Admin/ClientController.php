<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ClientStoreRequest;
use App\Models\Client;
use App\Models\User;
use App\Models\UserRole;
use App\Repositories\ClientRepository;
use App\Traits\RestoreUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ClientController extends Controller
{
    use RestoreUser;

    /**
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     * @var string
     */
    protected $redirect_after_restore;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->redirect_after_restore = route('admin.order.index');
    }

    public function index(Request $request)
    {
        $clients_query = User::whereHas('client')
            ->where('role_id', UserRole::CLIENT_ID)
            ->with('client.orders')->withCount(['orders']);

        $names = (clone $clients_query)->get()->pluck('name')->unique();
        $emails = (clone $clients_query)->get()->pluck('email');
        $phones = (clone $clients_query)->get()->pluck('phone')->unique();

        $filter = $request->all();

        if(!empty($filter['name']))
            $clients_query->where('name', 'like', $filter['name']);

        if(!empty($filter['email']))
            $clients_query->where('email', 'like', $filter['email']);

        if(!empty($filter['phone']))
            $clients_query->where('phone', $filter['phone']);

        $clients = $clients_query
            ->orderBy($request->input('sortfield', 'name'), $request->input('sortorder', 'asc'))
            ->paginate(20);

        $request->flash();

        return view('admin.client.index', compact('clients', 'names', 'emails', 'phones'));
    }

    /**
     * Show the form for editing the specified client.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        
        return view('admin.client.edit', compact('client'));
    }

    /**
     * Update the specified client in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->clientRepository->update($request->all(), $id);

        return redirect()->route('admin.order.index');
    }

    public function create()
    {
        return view('admin.client.create');
    }

    public function store(ClientStoreRequest $request)
    {
        $this->clientRepository->store($request->all());

        return redirect()->route('admin.customer.index');
    }
}
