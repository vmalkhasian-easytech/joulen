<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EquipmentRequest;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentType;
use App\Repositories\AttachmentRepository;
use App\Repositories\Criteria\Attribute;
use App\Repositories\EquipmentRepository;
use App\Repositories\MasterRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EquipmentController extends Controller
{
    /**
     * @var EquipmentRepository
     */
    protected $equipmentRepository;

    /**
     * @var AttachmentRepository
     */
    protected $attachmentRepository;
    /**
     * @var MasterRepository
     */
    protected $masterRepository;

    /**
     * EquipmentController constructor.
     * @param EquipmentRepository $equipmentRepository
     * @param AttachmentRepository $attachmentRepository
     */
    public function __construct(EquipmentRepository $equipmentRepository, AttachmentRepository $attachmentRepository)
    {
        $this->equipmentRepository = $equipmentRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        if(!empty($filter['types']))
            $this->equipmentRepository->pushCriteria(new Attribute('type_id', $filter['types']));

        if(!empty($filter['brand']))
            $this->equipmentRepository->pushCriteria(new Attribute('id', $filter['brand']));

        $equipment = $this->equipmentRepository
            ->select(DB::raw('equipment.*, ifnull(COALESCE(portfolio_inverter.inverter_count, portfolio_panel.panel_count), 0) as portfolios_count'))
            ->leftJoin(DB::raw('
                (SELECT invertor_id, COUNT(*) AS inverter_count
	            FROM `master_portfolios` 
	            WHERE `master_portfolios`.`subcategory_id` != 4 
	            AND `master_portfolios`.`status_id` = 2
	            AND `master_portfolios`.`id` IN(
    	            SELECT `attachments`.`attachable_id` 
    	            FROM `attachments`
		            WHERE `attachments`.`public` = true)
	            GROUP BY invertor_id) as portfolio_inverter'), 'portfolio_inverter.invertor_id', '=', 'equipment.id')
            ->leftJoin(DB::raw('
                (SELECT panel_id, COUNT(*) as panel_count
	            FROM `master_portfolios` 
	            WHERE `master_portfolios`.`subcategory_id` != 4 
	            AND `master_portfolios`.`status_id` = 2
	            AND `master_portfolios`.`id` IN(
    	            SELECT `attachments`.`attachable_id` 
    	            FROM `attachments`
		            WHERE `attachments`.`public` = true)
	            GROUP BY panel_id) as portfolio_panel'), 'portfolio_panel.panel_id', '=', 'equipment.id')
            ->with(['masters', 'reviews'])
            ->withCount('masters', 'reviews')
            ->orderBy($request->input('sortfield', 'title'), $request->input('sortorder', 'asc'))
            ->paginate(20);

        $equipment_slug_mapping = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING;
        $request->flash();

        return view('admin.equipment.index', compact('equipment', 'equipment_slug_mapping'));
    }

    public function create()
    {
        return view('admin.equipment.create');
    }

    /**
     * @param EquipmentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EquipmentRequest $request)
    {
        $slug = str_slug($request->input('title'));
        $duplicate_equipment = Equipment::where('type_id', $request->input('type_id'))->where('slug', $slug)->first();

        if ( $duplicate_equipment ) {
            $error = $duplicate_equipment->type->slug == EquipmentType::TYPE_INVERTER
                ? 'Інвертор з таким ім\'ям вже існує' : 'Сонячна панель з таким ім\'ям вже існує';
            return redirect()->back()->withInput($request->all())->withErrors([$error]);
        }

        $request->merge(['slug' => $slug]);
        $brand = Equipment::create($request->all());

        $this->equipmentRepository->storeImage($request, $brand);
//        if ($request->hasFile('equipment_logo')) {
//            $image = $request->file('equipment_logo');
//
//            $attachment = $this->attachmentRepository->saveAttachment($brand, 'equipment', AttachmentType::where('name', AttachmentType::TYPE_EQUIPMENT_LOGO)->first()->id, $image, true);
//            $this->attachmentRepository->resizePhoto($attachment, null, Attachment::EQUIPMENT_LOGO_HEIGHT);
//        }

        return redirect()->route('admin.equipment.index');
    }

//    public function getEquipment()
//    {
//        $equipment = Equipment::all();
//        return $equipment->keyBy('id');
//    }

    public function edit($id) {
        $brand = Equipment::findOrFail($id);

        return view('admin.equipment.edit', compact('brand'));
    }
    /**
     * @param EquipmentRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EquipmentRequest $request, $id)
    {
        $brand = Equipment::findOrFail($id);
        $brand->fill($request->all())->save();

        $this->equipmentRepository->storeImage($request, $brand);

        return redirect()->route('admin.equipment.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $equipment = Equipment::findOrFail($id);
        $equipment->delete();

        return redirect()->route('admin.equipment.index');
    }
}
