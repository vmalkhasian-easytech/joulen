<?php

namespace App\Http\Controllers\Admin;

use App\Client\NbuClient;
use App\Events\MasterCommissionPaid;
use App\Events\MasterOrderInProgress;
use App\Events\MasterOrderStatusСonsidering;
use App\Http\Controllers\Controller as Controller;
use App\Mail\ClientProjectPublished;
use App\Models\Client;
use App\Models\OrderCategory;
use App\Models\OrderStatus;
use App\Models\OrderSubcategory;
use App\Models\Payment\Payment;
use App\Repositories\ClientRepository;
use App\Repositories\Criteria\CriteriaHelper;
use App\Repositories\Criteria\Attribute;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\Criteria\Order\AdminList;
use App\Repositories\OrderRepository;
use App\Repositories\MasterRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\PaymentStatusRepository;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Master;
use App\Models\Attachment;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var MasterRepository
     */
    protected $masterRepository;

    /**
     * @var PaymentRepository
     */
    protected $paymentRepository;

    /**
     * @var PaymentRepository
     */
    protected $paymentStatusRepository;

    /**
     * @var NbuClient
     */
    protected $nbuClient;

    /**
     * @var clientRepository
     */
    protected $clientRepository;

    public function __construct(OrderRepository $orderRepository, MasterRepository $masterRepository,
                                PaymentRepository $paymentRepository, PaymentStatusRepository $paymentStatusRepository,
                                NbuClient $nbuClient, ClientRepository $clientRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->masterRepository = $masterRepository;
        $this->paymentRepository = $paymentRepository;
        $this->paymentStatusRepository = $paymentStatusRepository;
        $this->nbuClient = $nbuClient;
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a list of orders.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //TODO: add validation
        //TODO: optimise queries (18 is way too many)
        
        $filter = $request->request->all();
        $this->orderRepository->pushCriteria(new AdminList());

        if (isset($filter['name'])) {
            $this->orderRepository->pushCriteria(new RelationAttribute('client.user', 'name', $filter['name']));
        }

        if (isset($filter['email'])) {
            $this->orderRepository->pushCriteria(new RelationAttribute('client.user', 'email', $filter['email']));
        }

        if (isset($filter['master_id_requests'])) {
            $this->orderRepository->pushCriteria(new RelationAttribute('requests', 'master_id', $filter['master_id_requests']));
            $master_id_requests = $filter['master_id_requests'];
            unset($filter['master_id_requests']);
        }

        if (isset($filter['phone'])) {
            $this->orderRepository->pushCriteria(new RelationAttribute('client.user', 'phone', '%'.$filter['phone'].'%', 'like'));
        }

        if (isset($filter['created_at'])) {
            $this->orderRepository->pushCriteria(new Attribute('created_at', $filter['created_at'] . '%', 'like'));
        }

        foreach ($filter as $attribute => $value) {
            if($attribute != 'phone') {
                if (CriteriaHelper::isAttribute($value, $attribute)) {
                    $this->orderRepository->pushCriteria(new Attribute($attribute, $value));
                }
            }
        }

        $orders = $this->orderRepository->paginate(20);

        $orders_id = Order::all()->sortByDesc('id')->pluck('id');

        $orders_date = Order::pluck('created_at')->transform(function ($item) {
            return date('Y-m', strtotime($item));
        })->unique();

        $masters = $this->masterRepository->pushCriteria(new RelationAttribute('user', 'deleted', false))->all();

        $request->flash();

        return view('admin.order.index', compact('orders', 'masters',
            'master_id_requests', 'orders_id', 'orders_date'));
    }

    /**
     * Display the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->with([
            'subcategory.category',
            'orderable',
            'requests.master.user',
            'requests.master',
            'status',
            'requests',
            'requests.order.subcategory'
        ])->findOrFail($id);
        $another_masters_requests = $order->requests->keyBy('master.id')->forget($order->master_id);

        return view('admin.order.show', compact('order', 'another_masters_requests'));
    }

    /**
     * Show the form for editing the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with('subcategory.category')->findOrFail($id);

        $category = $order->subcategory->category->slug;
        $subcategory = $order->subcategory->slug;

        if (count(OrderCategory::where('slug', $category)->first()->subcategories) == 1)
            return view("order.edit.$category", compact('order'));

        if($order->ground == '1') {
            return view("order.edit.{$category}.{$subcategory}.ground", compact('order'));
        }
        elseif ($order->ground == '0') {
            return view("order.edit.{$category}.{$subcategory}.roof", compact('order'));
        }

        return view("order.edit.{$category}.{$subcategory}", compact('order'));
    }

    /**
     * Update the specified order in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
// TODO dont working figure out why
        // if (!Auth::user()->can('update', $order))
        //     return response(json_encode(['redirectTo' => route('home')]), 200);

        $this->orderRepository->update($request->all(), $order);

        session(['order_id' => $order->id]);
        return response(json_encode(['redirectTo' => route('admin.order.index')]), 200);
    }


    /**
     * Update the specified order's meta information (master_id, status, manager's comment, cancellation comment, commission) in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateMeta(Request $request, $id)
    {
//        TODO: add validation
        $order = $this->orderRepository->updateMeta($request, $id);

        $defaultCommission = 0;
        if ($request->get('master_id')) {

            if ($request->get('paid', false)) {

                //todo: check if master exist
                $payment = Payment::firstOrCreate([
                    'order_id' => $id,
                    'master_id' => $request->input('master_id'),
                ], [
                    'amount' => $request->input('commission', $defaultCommission),
                    'status_id' => $this->paymentStatusRepository->getManualStatus()->id,
                ]);

                if ($payment->amount != $request->input('commission', $defaultCommission) || !$payment->hasValidStatus()) {
                    $payment->amount = $request->input('commission', $defaultCommission);
                    $payment->status_id = $this->paymentStatusRepository->getManualStatus()->id;
                    $payment->save();
                    event(new MasterCommissionPaid($payment->master->user, $order));
                }
            } else {
                $payment = Payment::where([
                    'order_id' => $id,
                    'master_id' => $request->input('master_id'),
                ])->first();
                if ($payment) {
                    $payment->amount = $request->input('commission', $defaultCommission);
                    $payment->status_id = $this->paymentStatusRepository->getCanceledStatus()->id;
                    $payment->save();
                }
            }
        }

        return redirect()->route('admin.order.index');
    }

    /**
     * Shows list of order's requests
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function requests($id)
    {
        $order = Order::findOrFail($id);

        return view('admin.order.requests', compact('order'));
    }

    /**
     * Changes order's subcategory (only within same category)
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function changeCategory(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $commercial = OrderSubcategory::where('slug', 'commercial')->first()->id;
        if($request->input('new_subcategory_id') == 'ground') {
            $new_subcategory = OrderSubcategory::findOrFail($commercial);
            $ground = 1;
        }
        elseif ($request->input('new_subcategory_id') == 'roof') {
            $new_subcategory = OrderSubcategory::findOrFail($commercial);
            $ground = 0;
        }
        else {
            $new_subcategory = OrderSubcategory::findOrFail($request->input('new_subcategory_id'));
            $ground = null;
        }

        if($order->category->slug == $new_subcategory->category->slug){
            $order->subcategory_id = $new_subcategory->id;
            $order->ground = $ground;
            $order->save();
        }

        return redirect()->route('admin.order.index');
    }

    public function deletePhoto(Request $request)
    {
        $attachmentId = $request->get('attachment_id');
        Attachment::find($attachmentId)->deleteStorageFile();
        Attachment::find($attachmentId)->delete();

        return response($attachmentId, 200);
    }


    public function attachmentUpload(Request $request)
    {
        if ($request->session()->has('order_id')) {

            $file = $request->file('qqfile');

            if ($file) {
                $attachment = new Attachment();
                $attachment->attachable_id = session('order_id');
                $attachment->attachable_type = 'order';
                $attachment->saveFromPost($file);
                $attachment->save();
            }
            return json_encode([
                "success" => true,
                "uuid" => $_REQUEST['qquuid']
            ]);
        }
        return response('error', 500);
    }

    public function finishUploadSession(Request $request)
    {
        $request->session()->forget('order_id');

        return route('admin.order.index');
    }

    public function activate($id)
    {
        $order = Order::findOrFail($id);
        $client = Client::findOrFail($order->client_id)->user;
        event(new MasterOrderStatusСonsidering(Order::find($id)));
        event(new ClientProjectPublished($client, Order::find($id)));
        $considering_status_id = OrderStatus::where('slug', 'considering')->first()->id;
        $order->status_id = $considering_status_id;
        $order->activated_at = Carbon::now();
        $order->save();

        return redirect()->route('admin.order.index');
    }
}


