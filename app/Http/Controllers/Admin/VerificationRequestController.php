<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterStatus;
use App\Models\User\Master\VerificationRequest;
use App\Models\User\Master\Status;
use App\Repositories\Criteria\Attribute;
use App\Repositories\MasterRepository;
use Illuminate\Http\Request;


class VerificationRequestController extends Controller
{

    public function index()
    {
        $requests = VerificationRequest::with('master')->paginate(20);
        $request_statuses = Status::all()->pluck('id', 'slug');

        return view('admin.master.verification', compact('requests', 'request_statuses'));
    }

    public function update(Request $request, $id)
    {
        $verification_request = VerificationRequest::findOrFail($id);
        $verification_request->status_id = $request->input('request_status_id');
        $verified_request_status_id = Status::where('slug', 'verified')->first()->id;
        $verified_master_status_id = MasterStatus::where('slug', 'verified')->first()->id;

        if ($request->request_status_id == $verified_request_status_id) {
            $verification_request->master->status_id = $verified_master_status_id;
            $verification_request->master->save();
        }

        $verification_request->save();

        return back();
    }

    public function storeComment(Request $request, $id)
    {
        $this->validate($request, [
            'comment' => 'required|string|max:500'
        ]);

        $verification_request = VerificationRequest::findOrFail($id);
        $verification_request->comment = $request->input('comment');
        $verification_request->save();

        return $verification_request;
    }

    public function getComment($id)
    {
        $verification_request = VerificationRequest::findOrFail($id);
        $comment = $verification_request->comment;

        return $comment;
    }

}
