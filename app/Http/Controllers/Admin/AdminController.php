<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MarketData;
use App\Models\OrderStatus;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\MasterOrderInProgress;
use App\Models\ClientMasterEmail;
use App\Models\ClientEmail;
use App\Models\MasterPortfolio;
use App\Models\MasterStatus;
use App\Models\User\Master\Status;
use App\Models\Master;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use Carbon\Carbon;

class AdminController extends Controller
{

    /**
     * Show the form for editing admin's settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('admin.settings', ['user' => Auth::user()]);
    }

    /**
     * Update admin's settings.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        TODO: add validation
        $user = Auth::user();

        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
        $user->admin->mailing = $request->input('mailing', false);
        $user->admin->save();
        $user->save();

        return back();
    }

    public function showMarketData()
    {
        $market = MarketData::all();

        return view('admin.market_data', compact('market'));
    }

    public function updateMarketData(Request $request, $id)
    {
        $market = MarketData::find($id);

        $market->fill([
            'green_tariff_home_ses' => $request->green_tariff_home_ses,
            'green_tariff_roof_ses' => $request->green_tariff_roof_ses,
            'green_tariff_ground_ses' => $request->green_tariff_ground_ses,
            'avg_cost_network_ses_5kW' => $request->avg_cost_network_ses_5kW,
            'avg_cost_network_ses_30kW' => $request->avg_cost_network_ses_30kW,
            'avg_cost_auto_ses_5kW' => $request->avg_cost_auto_ses_5kW,
            'avg_cost_auto_ses_30kW' => $request->avg_cost_auto_ses_30kW,
            'ratio_hybrid_station' => $request->ratio_hybrid_station,
            'avg_cost_roof_ses_30kW' => $request->avg_cost_roof_ses_30kW,
            'avg_cost_roof_ses_100kW' => $request->avg_cost_roof_ses_100kW,
            'avg_cost_ground_ses_100kW' => $request->avg_cost_ground_ses_100kW,
            'avg_cost_ground_ses_1000kW' => $request->avg_cost_ground_ses_1000kW,
            'avg_cost_gel_batteries'  => $request->avg_cost_gel_batteries
        ])->save();

        return back();
    }

    public function kpi()
    {
        $valid_statuses = [3, 4, 5, 8];

        $months = [
            '01' => 'Січень',
            '02' => 'Лютий',
            '03' => 'Березень',
            '04' => 'Квітень',
            '05' => 'Травень',
            '06' => 'Червень',
            '07' => 'Липень',
            '08' => 'Серпень',
            '09' => 'Вересень',
            '10' => 'Жовтень',
            '11' => 'Листопад',
            '12' => 'Грудень',
        ];

        $start_ym = Order::oldest()->first()->created_at;
        $end_ym = Carbon::now();
        $period = new DatePeriod(new DateTime($start_ym), DateInterval::createFromDateString('1 month'), (new DateTime($end_ym)));

        $dates = array_map( function($item) {
            return $item->format('Ym');
        }, iterator_to_array($period));

        $dates = array_reverse($dates);

        $statistics = collect();

        $all_orders = Order::select('*', DB::raw('EXTRACT(YEAR_MONTH FROM created_at) as ym'))
            ->with('payment')->withCount('requests', 'client_emails')
            ->orderBy('ym','DESC')->get()->groupBy('ym');

        $master_emails = ClientMasterEmail::select(DB::raw('EXTRACT(YEAR_MONTH FROM created_at) as ym'))->get()->groupBy('ym');

        $client_emails = ClientEmail::select(DB::raw('EXTRACT(YEAR_MONTH FROM created_at) as ym'))->get()->groupBy('ym');

        $masters = Master::withActiveStatuses()->select(DB::raw('EXTRACT(YEAR_MONTH FROM activation_date) as ym'))->get();

        $master_portfolios = MasterPortfolio::verifiedStatus()->select(DB::raw('EXTRACT(YEAR_MONTH FROM created_at) as ym'))->get();

        foreach ($dates as $ym) {
            $orders = $all_orders->get($ym);

            $collection = collect();
            $count_orders_in_valid_statuses = 0;
            $sum_payments = 0;
            $requests_count = 0;
            $client_emails_count = $client_emails->get($ym) ? $client_emails->get($ym)->count() : 0;
            $master_emails_count = $master_emails->get($ym) ? $master_emails->get($ym)->count() : 0;
            $all_emails = $client_emails_count + $master_emails_count;

            if ($orders) {
                $orders->groupBy('status_id')->map(function ($items, $key) use ($collection, $valid_statuses, &$count_orders_in_valid_statuses, $ym) {
                    if (in_array($key, $valid_statuses))
                        $count_orders_in_valid_statuses += $items->count();

                    $collection->put($key, ['count_by_status' => $items->count()]);
                });

                $collection->put('count_all_orders', $orders->count());

                $collection->put('count_orders_in_valid_statuses', $count_orders_in_valid_statuses);

                $collection->put('percentage_orders_in_valid_statuses',
                    (int) round($count_orders_in_valid_statuses * 100 / $collection->get('count_all_orders')));

                $collection->map(function ($item, $status_id) use ($collection, $valid_statuses, $ym) {
                    if (in_array($status_id, $valid_statuses))
                        $collection->put($status_id, array_merge($item, ['percentage_by_status' =>
                            (int) round($item['count_by_status'] * 100 / $collection->get('count_orders_in_valid_statuses'))]));
                });

                $orders->map(function ($item) use (&$sum_payments, &$requests_count) {
                    $requests_count += $item->requests_count;
                    if ($item->paid && !empty($item->payment))
                        $sum_payments += $item->payment->amount;
                });
            }

            $collection->put('payment_sum', $sum_payments);
            $collection->put('requests_count', $requests_count);
            $collection->put('avg_requests_count', $count_orders_in_valid_statuses ? (int) round($requests_count/$count_orders_in_valid_statuses) : 0);
            $collection->put('client_emails_count', $client_emails_count);
            $collection->put('master_emails_count', $master_emails_count);
            $collection->put('all_emails', $all_emails);
            $collection->put('active_masters', $masters->where('ym', '<=', $ym)->count());
            $collection->put('active_masters_by_month', $masters->where('ym', $ym)->count());
            $collection->put('active_master_portfolios', $master_portfolios->where('ym', '<=', $ym)->count());
            $collection->put('active_masters_portfolios_by_month', $master_portfolios->where('ym', $ym)->count());
            $collection->put('year', substr($ym, 0, -2));
            $collection->put('month', $months[substr($ym, 4)]);
            $collection->put('date', substr($ym, 0, -2) . '-' . substr($ym, 4));

            $statistics->put($ym, $collection);
        }

       return view('admin.kpi', compact('statistics'));
    }
}
