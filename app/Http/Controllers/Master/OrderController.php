<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller as Controller;
use App\Models\Order;
use App\Models\OrderRequest;
use App\Repositories\Criteria\Order\HomeSes;
use App\Repositories\Criteria\Order\InMastersRegions;
use App\Repositories\Criteria\Order\MasterList;
use App\Repositories\Criteria\Order\MasterListPrivate;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->middleware('commission');

        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a list of orders.
     *
     * @param  string $filter
     * @return \Illuminate\Http\Response
     */
    public function index($filter = null)
    {
        $master_id = Auth::user()->master->id;
        $master = Auth::user()->master;
        $this->orderRepository->pushCriteria(new MasterList($master_id));

        if($master->getSesTypes() == 'commercial_ses') {
            if($filter == 'region_filter_on') {
                $filter = 'commercial/region_filter_on';
            }
            else {
                $filter = 'commercial';
            }
        }
        else if($master->getSesTypes() == 'home_ses') {
            if($filter == 'region_filter_on') {
                $filter = 'home/region_filter_on';
            }
            else {
                $filter = 'home';
            }
        }
        if(!$this->orderRepository->validateUrlFilter($filter))
            abort(404);

        $filter = $this->orderRepository->decodeUrlFilter($filter);

        if(isset($filter['category']))
            $this->orderRepository->pushCriteria(new RelationAttribute('subcategory.category', 'slug', $filter['category']));

        if(isset($filter['subcategory']))
            $this->orderRepository->pushCriteria(new RelationAttribute('subcategory', 'slug', $filter['subcategory']));

        if(isset($filter['home']))
            $this->orderRepository->pushCriteria(new HomeSes('subcategory', 'slug', $filter['home']));

        if(isset($filter['region_filter_on']))
            $this->orderRepository->pushCriteria(new InMastersRegions($master_id));

        $orders = $this->orderRepository->paginate(20);

        return view('master.order.index', compact('orders', 'filter', 'master'));
    }

    /**
     * Display a list of orders where master selected as executor.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPrivate()
    {
        $master_id = Auth::user()->master->id;

        $orders = $this->orderRepository->pushCriteria(new MasterListPrivate($master_id))->all();

        return view('master.order.private.index', compact('orders'));
    }

    /**
     * Display the specified order.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->with(['subcategory.category', 'orderable', 'subcategory.category.request_price_types', 'status'])->findOrFail($id);

        if(Auth::user()->can('view', $order)){
            if(Auth::user()->can('own', $order))
                return redirect()->route('master.order.private.show', ['id' => $order->id]);

//            TODO: query in controller is not good (use request relation)
            $request = OrderRequest::with(['master' => function($master) {
                $master->withCount(['reviews', 'negative_reviews']);
            }, 'prices'])->where('master_id', Auth::user()->master->id)->where('order_id', $order->id)->first();

            if(isset($request)) {
                return view('master.request.show', compact('order', 'request'));
            } else {
                return view('master.request.create', compact('order'));
            }
        }else{
            return 'Проект закрито';
        }
    }

    /**
     * Display the specified order where master selected as executor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPrivate($id)
    {
        $order = Order::with([
            'subcategory.category',
            'orderable.order.region',
            'subcategory.category.request_price_types',
            'status',
            'region'
        ])->findOrFail($id);

        if(Auth::user()->can('own', $order)) {
            return view('master.order.private.show', compact('order'));
        } else {
            return 'Доступ заблоковано';
        }
    }
}
