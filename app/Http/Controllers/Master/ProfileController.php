<?php

namespace App\Http\Controllers\Master;

use App\Events\Admin\MasterCreatePortfolio;
use App\Events\Admin\MasterVerificationRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Master\EquipmentRequest;
use App\Http\Requests\Master\PortfolioRequest;
use App\Http\Requests\Master\ProfileRequest;
use App\Models\AttachmentType;
use App\Models\Equipment\Equipment;
use App\Models\EquipmentExchangeFund;
use App\Models\EquipmentGuaranteeRepair;
use App\Models\Equipment\EquipmentType;
use App\Models\Master;
use App\Models\MasterOffice;
use App\Models\OrderCategory;
use App\Models\OrderSubcategory;
use App\Models\Region;
use App\Models\User\Master\VerificationRequest;
use App\Models\User\Master\Status;
use App\Repositories\AttachmentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MasterRepository;
use App\Traits\ManagePortfolio;
use Illuminate\Support\Facades\Session;
use App\Models\MasterPortfolio;
use App\Models\Attachment;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    use ManagePortfolio;

    /**
     * @var MasterRepository
     * @var AttachmentRepository
     */
    protected $masterRepository;
    protected $attachmentRepository;


    public function __construct(MasterRepository $masterRepository, AttachmentRepository $attachmentRepository)
    {
        $this->masterRepository = $masterRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    public function create() {

        // close this form for masters who has been already verified
        if (Auth::user()->master && Auth::user()->master->hasValidStatus()) {
            return redirect()->route('home');
        }
        if (Auth::user()->master && Auth::user()->master->status->slug == 'wait_activation') {
            $wait_activation_message = trans('errors.wait_activation');
        }

        $invertor_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->orderBy('title')->get();
        $panel_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->orderBy('title')->get();

        $subcategories = OrderSubcategory::all()->pluck('name', 'id');
        $exchange_funds = EquipmentExchangeFund::all()->pluck('name', 'id');
        $guarantee_repairs = EquipmentGuaranteeRepair::all()->pluck('name', 'id');
        $equipment = Equipment::all()->pluck('title', 'id');
        $master = Auth::user()->master ?? new Master();

        $homePortfolioCoordinates = $this->getHomePortfoliosCoordinates();
        $commercialPortfolioCoordinates = $this->getCommercialPortfoliosCoordinates();

        return view('master.profile', compact(
            'invertor_type_equipment', 'panel_type_equipment', 'equipment', 'master',
            'exchange_funds', 'guarantee_repairs', 'equipment_master', 'subcategories',
            'commercialPortfolioCoordinates', 'homePortfolioCoordinates', 'wait_activation_message'));
    }

    public function store(ProfileRequest $request)
    {
        $this->masterRepository->createFromRequest($request);
    }

    public function update(ProfileRequest $request)
    {
        $master = Auth::user()->master;

        if ($request->input('profile_stage') == 5) {
            if ($master->getSesTypes() == 'both' || $master->getSesTypes() == 'home_ses') {
                if ($master->getHomePortfolios()->count() < 3)
                    return new JsonResponse(['errors'=> [trans('errors.home_ses_portfolios')]], 422);
            }
            if ($master->getSesTypes() == 'both' || $master->getSesTypes() == 'commercial_ses') {
                if ($master->getCommercialPortfolios()->count() < 2)
                    return new JsonResponse(['errors'=> [trans('errors.commercial_ses_portfolios')]], 422);
            }
            $redirectTo = route('master.profile_pages.verification_request');
        }
        $this->masterRepository->updateProfile($request, $master);

        return $redirectTo ?? null;
    }

    public function profilePreview($id) {
        $master = Master::withCount([
            'reviews',
            'negative_reviews',
            'orders_in_progress',
        ])
            ->with([
                'orders_in_progress' => function ($order_query) {
                    $order_query->with(['region', 'subcategory.category', 'orderable']);
                },
                'regions',
                'portfolios',
                'reviews' => function ($review_query) {
                    $review_query->with(['client.user', 'order' => function ($order_query) {
                        $order_query->with(['region', 'subcategory.category', 'orderable']);
                    }]);
                },
            ])
            ->findOrFail($id);

        if($master->user->deleted)
            return 'спеціаліст видалений';

        if($master->user->blocked)
            return 'спеціаліст заблокований';

        return view('master.show', compact('master'));
    }

    public function generalInfo()
    {
        return view('master.profile_pages.general_information', ['master' => Auth::user()->master]);
    }

    public function pageGeneralInfoUpdate(ProfileRequest $request)
    {
        $master = Auth::user()->master;
        $master->user->name = $request->input('company_name');
        $master->about = $request->input('about');
        $master->user->save();
        $master->save();
        if ($request->hasFile('master_avatar')) {
            if ($master->avatar)
                $master->avatar->delete();
            $image = $request->file('master_avatar');
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->attachment_type_id = AttachmentType::where('name', 'master_photo')->first()->id;
            $attachment->saveFromPost($image);
            $attachment->public = true;
            $attachment->save();

            $this->attachmentRepository->resizePhoto($attachment, null, Attachment::MASTER_LOGO_HEIGHT);
        }

        return back();
    }

    public function contacts()
    {
        return view('master.profile_pages.contacts', ['master' => Auth::user()->master]);
    }

    public function pageContactsUpdate(ProfileRequest $request)
    {
        $regions = Region::all()->pluck('name', 'id')->toArray();
        $main_region_id = Region::where('slug', 'kyiv')->first()->id;
        $second_region_id = Region::where('slug', 'kyiv')->first()->id;
        $third_region_id = Region::where('slug', 'kyiv')->first()->id;

        foreach ($regions as $region_id => $region) {
            $region_to_compare = explode(' ', $region)[0];
            similar_text($region_to_compare, $request->input('main_office_region'), $percentage);
            if ($percentage > 75)
                $main_region_id = $region_id;
            similar_text($region_to_compare, $request->input('second_office_region'), $percentage);
            if ($percentage > 75)
                $second_region_id = $region_id;
            similar_text($region_to_compare, $request->input('third_office_region'), $percentage);
            if ($percentage > 75)
                $third_region_id = $region_id;
        }

        $master = Auth::user()->master;
        $master->work_email = $request->input('work_email');
        $master->website = $request->input('website');
        $work_phone = str_replace(array('+', ' ', '(' , ')', '-'), '', $request->input('work_phone'));
        if(strpos($work_phone, '38') === 0) {
            $work_phone = substr($work_phone, 2);
        }
        $master->work_phone = $work_phone;
        $second_work_phone = str_replace(array('+', ' ', '(' , ')', '-'), '', $request->input('second_work_phone'));
        if(strpos($second_work_phone, '38') === 0) {
            $second_work_phone = substr($second_work_phone, 2);
        }
        $master->second_work_phone = $second_work_phone;
        $master->save();

        if ($request->input('main_office_added')) {
            $main_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 1
            ], [
                'lat' => $request->input('main_office_lat'),
                'lng' => $request->input('main_office_lng'),
                'address' => $request->input('main_office_address'),
                'region_id' => $main_region_id,
                'room' => $request->input('main_office_room'),
            ]);
            if ($request->hasFile('main_office_photo')) {
                if ($main_office->picture || $request->input('is_deleted_main_office_photo'))
                    $main_office->picture->delete();
                $image = $request->file('main_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $main_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->main_office->delete();
        }


        if ($request->input('second_office_added')) {
            $second_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 2
            ], [
                'lat' => $request->input('second_office_lat'),
                'lng' => $request->input('second_office_lng'),
                'address' => $request->input('second_office_address'),
                'region_id' => $second_region_id,
                'room' => $request->input('second_office_room'),
            ]);
            if ($request->hasFile('second_office_photo')) {
                if ($second_office->picture || $request->input('is_deleted_second_office_photo'))
                    $second_office->picture->delete();
                $image = $request->file('second_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $second_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->second_office->delete();
        }

        if ($request->input('third_office_added')) {
            $third_office = MasterOffice::updateOrCreate([
                'master_id' => $master->id,
                'index' => 3
            ], [
                'lat' => $request->input('third_office_lat'),
                'lng' => $request->input('third_office_lng'),
                'address' => $request->input('third_office_address'),
                'region_id' => $third_region_id,
                'room' => $request->input('third_office_room'),
            ]);

            if ($request->hasFile('third_office_photo')) {
                if ($third_office->picture || $request->input('is_deleted_third_office_photo'))
                    $third_office->picture->delete();
                $image = $request->file('third_office_photo');
                $attachment = new Attachment();
                $attachment->attachable_id = $third_office->id;
                $attachment->attachable_type = 'office';
                $attachment->saveFromPost($image);
                $attachment->save();
            }
        }
        else {
            $master->third_office->delete();
        }

        return back();
    }

    public function cooperationData()
    {
        return view('master.profile_pages.cooperation_data', ['master' => Auth::user()->master]);
    }

    public function updatePartnership(ProfileRequest $request)
    {
        $master = Auth::user()->master;

        $master->user->manager->name = $request->input('manager_name');
        $master->user->manager->phone = $request->input('manager_phone');
        $master->user->manager->email = $request->input('manager_email');
        $master->user->manager->mailing = $request->input('manager_mailing', false);
        $master->user->manager->save();

        $master->user->owner->name = $request->input('owner_name');
        $master->user->owner->phone = $request->input('owner_phone');
        $master->user->owner->email = $request->input('owner_email');
        $master->user->owner->mailing = $request->input('owner_mailing', false);
        $master->user->owner->save();

        $master->edrpou = $request->input('edrpou');
        $master->ipn = $request->input('ipn');
        $master->save();

        return back();

    }

    public function subscribe()
    {
        return view('master.profile_pages.subscribe', ['master' => Auth::user()->master]);
    }

    public function subscriptionUpdate(Request $request)
    {
        $master = Auth::user()->master;
        $master->news_mailing = $request->input('news_mailing');
        $master->save();
        if ($request->input('orders_mailing')) {
            foreach ($master->subcategories as $subcategory) {
                $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => true]);
            };
        } else {
            foreach ($master->subcategories as $subcategory) {
                $master->subcategories()->updateExistingPivot($subcategory->id, ['mailing' => false]);
            };
        }

        return redirect()->back();
    }

    public function verificationRequest()
    {
        $master = Auth::user()->master;
        $status = $master->verification_request ? trans('errors.verification_sent') : null;

        return view('master.profile_pages.verification_request', compact('master', 'status'));
    }

    public function verificationRequestStore()
    {
        $master = Auth::user()->master;

        if (!($master->edrpou || $master->ipn)) {
            return back()->withErrors(['not_registered' => trans('errors.not_registered')]);
        }
        if (!$master->main_office->address) {
            return back()->withErrors(['no_office' => trans('errors.no_office')]);
        }

        $status_id = Status::where('slug', 'checking')->first()->id;

        VerificationRequest::create([
            'master_id' => $master->id,
            'status_id' => $status_id
        ]);

        event(new MasterVerificationRequest($master));

        return back()->with(['status' => trans('errors.verification_sent')]);
    }

    public function service()
    {
        $invertor_type_equipment = Equipment::where ('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->get();

        $panel_type_equipment = Equipment:: where ('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->get();

        $master = Auth::user()->master;

        $exchange_funds = EquipmentExchangeFund::all()->pluck('name', 'id');

        $guarantee_repairs = EquipmentGuaranteeRepair::all()->pluck('name', 'id');

        return view('master.profile_pages.service', compact('master', 'panel_type_equipment', 'invertor_type_equipment',
            'exchange_funds', 'guarantee_repairs'));
    }

    public function pageServiceUpdate(ProfileRequest $request)
    {
        $master = Auth::user()->master;
        $master->car = $request->input('car', false);
        $master->project_department = $request->input('project_department', 0);
        $master->construction_machinery = $request->input('machinery', false);
        $master->exchange_fund_bool = $request->input('exchange_fund', false);
        $master->exchange_fund = $request->input('exchange_fund_text');
        $master->eco_energy = $request->input('eco_energy', false);
        $master->year_started = $request->input('year_started');
        $master->save();

        $ses_category_id = OrderCategory::where('slug', 'ses')->first()->id;
        $ses_home_subcategories = OrderSubcategory::where(['home' => true, 'category_id' => $ses_category_id])->get()->pluck('id')->toArray();
        $ses_commercial_categories = OrderSubcategory::where(['home' => false, 'category_id' => $ses_category_id])->get()->pluck('id')->toArray();

        if ($request->input('home_ses')) {
            $master->subcategories()->syncWithoutDetaching($ses_home_subcategories);
        } else {
            $master->subcategories()->detach($ses_home_subcategories);
        }

        if ($request->input('commercial_ses')) {
            $master->subcategories()->syncWithoutDetaching($ses_commercial_categories);
        } else {
            $master->subcategories()->detach($ses_commercial_categories);
        }

        $work_locations = $request->input('work_location') ?? [];
        $master->regions()->detach();
        $master->regions()->sync($work_locations);
        $favorite_work_locations = array_filter($request->input('favorite_work_location')) ?? [];
        $favorite = array_fill(0, count($favorite_work_locations), ['favorite' => true]);
        $favorite_work_locations = array_combine($favorite_work_locations, $favorite);
        $master->regions()->syncWithoutDetaching($favorite_work_locations);


        $car_photo = $request->file('car_photo');

        if ($car_photo) {
            if ($master->car_photo)
                $master->car_photo->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->public = true;
            $attachment->attachment_type_id = AttachmentType::where('name', 'car_photo')->first()->id;
            $attachment->saveFromPost($car_photo);
            $attachment->save();
        }

        $machinery_photo = $request->file('machinery_photo');

        if ($machinery_photo) {
            if ($master->machinery_photo)
                $master->machinery_photo->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->public = true;
            $attachment->attachment_type_id = AttachmentType::where('name', 'machinery_photo')->first()->id;
            $attachment->saveFromPost($machinery_photo);
            $attachment->save();
        }

        $contract = $request->file('contract');

        if ($contract) {
            if ($master->contract)
                $master->contract->delete();
            $attachment = new Attachment();
            $attachment->attachable_id = $master->id;
            $attachment->attachable_type = 'master';
            $attachment->attachment_type_id = AttachmentType::where('name', 'contract')->first()->id;
            $attachment->saveFromPost($contract);
            $attachment->public = true;
            $attachment->save();
        }

        return back();
    }

    public function portfolio()
    {
        $invertor_type_equipment = Equipment::where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_INVERTER);
        })->orderBy('title')->get();

        $panel_type_equipment = Equipment:: where('type_id', function ($query) {
            $query->select('id')->from('equipment_types')->where('slug', EquipmentType::TYPE_SOLAR_PANEL);
        })->orderBy('title')->get();

        $master = Auth::user()->master;

        $subcategories = OrderSubcategory::all()->pluck('name', 'id');

        return view('master.profile_pages.portfolio', compact('master', 'invertor_type_equipment',
            'panel_type_equipment', 'subcategories'));
    }

    public function portfolioStore(PortfolioRequest $request)
    {
        $master = Auth::user()->master;
        $portfolio = $this->masterRepository->portfolioStore($request, $master);

        $route = route('admin.portfolio.index');

        event(new MasterCreatePortfolio($master, $route));

        return $portfolio;
    }

    public function getPortfolio($id)
    {
        $master = Auth::user()->master;
        $portfolio = $master->portfolios->keyBy('id')->get($id);

        return $portfolio;
    }

    public function portfolioUpdate(PortfolioRequest $request)
    {
        $master = Auth::user()->master;
        $portfolio = $this->masterRepository->portfolioUpdate($request, $master);

        return $portfolio;
    }

    public function portfolioDelete($id)
    {
        $master = Auth::user()->master;
        $portfolio = $master->portfolios()->find($id);
        $deleted_portfolio = $portfolio;
        if ($portfolio->attachments()->count() > 0)
            $portfolio->attachments()->delete();
        $portfolio->delete();

        return $deleted_portfolio;
    }

    public function equipmentStore(EquipmentRequest $request)
    {
        $master = Auth::user()->master;
        $master->equipment()->syncWithoutDetaching([$request->equipment_id => [
            'warranty_increase' => $request->warranty_increase,
            'warranty_duration' => $request->warranty_duration,
            'warranty_case' => $request->warranty_case,
            'exchange_fund_loc' => $request->exchange_fund_loc,
            'built' => $request->built,
        ]]);

        return new JsonResponse(['status' => 'success'], 200);
    }

    // returns masters equipment with pivot data
    public function getEquipment($id)
    {
        $master = Auth::user()->master;
        $equipment = $master->equipment->keyBy('id')->get($id);

        return $equipment;
    }

    public function equipmentUpdate(EquipmentRequest $request)
    {
        $master = Auth::user()->master;
        $master->equipment()->syncWithoutDetaching([$request->equipment_id => [
            'warranty_increase' => $request->warranty_increase,
            'warranty_duration' => $request->warranty_duration,
            'warranty_case' => $request->warranty_case,
            'exchange_fund_loc' => $request->exchange_fund_loc,
            'built' => $request->built,
        ]]);

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function equipmentDelete(Request $request, $id)
    {
        $master = Auth::user()->master;
        $master->equipment()->detach($id);

        return new JsonResponse(['status' => 'success'], 200);
    }

    public function getHomePortfoliosCoordinates()
    {
        $master = Auth::user()->master ?? new Master();

        return $master->home_portfolios_coordinates;
    }

    public function getCommercialPortfoliosCoordinates()
    {
        $master = Auth::user()->master ?? new Master();

        return $master->commercial_portfolios_coordinates;
    }

    public function getBrands(Request $request, $type_id) {
        $master = Master::findOrFail($request->input('master_id'));

        $invertor_type = EquipmentType::where('slug', EquipmentType::TYPE_INVERTER)->first()->id;
        $panel_type = EquipmentType::where('slug', EquipmentType::TYPE_SOLAR_PANEL)->first()->id;
        if($type_id == $invertor_type) {
            $equipment = $master->getNotAddedInvertors();
        }
        elseif ($type_id == $panel_type) {
            $equipment = $master->getNotAddedPanels();
        }

        return $equipment;
    }
}
