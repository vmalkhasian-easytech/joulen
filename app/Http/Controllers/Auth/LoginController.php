<?php

namespace App\Http\Controllers\Auth;

use App\Events\Equipment\EquipmentReviewStore;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\UserProfileType;
use App\Models\UserProfile;
use App\Models\UserRole;
use App\Models\User;
use Socialite;
use Exception;
use View;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


//    TODO: rename this method
    public function showLoginForm()
    {
        return view('auth/login');
    }

    public function login(Request $request)
    {
//        TODO: move validation to custom request

        $array_rules = [
            'email' => 'required|string|email|max:100|exists:users,email|not_deleted|not_blocked',
            'password' => 'required|string|min:6',
        ];

        $validator = Validator::make($request->all(), $array_rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            if (Session::has('equipment_review_id')) {
                event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
                Session::forget('equipment_review_id');
            }
            if (Auth::user()->isTempMaster() || (Auth::user()->master && !Auth::user()->master->hasValidStatus())) {
                return redirect()->route('master.profile');
            }
            if (Auth::user()->isDistributor() && !Auth::user()->distributor->active) {
                Auth::logout();
                return redirect()->back()->withInput()->withErrors([
                    'inactive_account' => 'Ваш аккаунт неактивний. Зверніться в службу підтримки сервісу'
                ]);
            } elseif (Auth::user()->isDistributor() && Auth::user()->distributor->active) {
                return redirect()->route('distributor.edit');
            }
//          TODO: use default intended method  
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }

            return redirect()->route(Auth::user()->role->getDefaultPageRoute());
        } else {
            return back()->withInput()->withErrors([
                'password' => 'Невірний пароль'
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}
