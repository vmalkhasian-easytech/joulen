<?php

namespace App\Http\Controllers\Auth;

use App\Events\Equipment\EquipmentReviewStore;
use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\Master;
use App\Models\MasterStatus;
use App\Models\User;
use App\Models\UserTokens;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use App\Repositories\RegisterRepository;
use App\Traits\SendSms;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\JsonResponse;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserProfileType;
use App\Models\UserProfile;
use Jrean\UserVerification\Facades\UserVerification;
use Socialite;
use Illuminate\Support\Facades\Session;
use Response;

use Jrean\UserVerification\Traits\VerifiesUsers;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    use VerifiesUsers;

    /*
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    public $redirectAfterVerification = "/master/profile";

    protected $registerRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RegisterRepository $registerRepository)
    {
        $this->registerRepository = $registerRepository;
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
//        ]);
    }

    public function showRegisterClientForm()
    {
        return view('client.auth.register');
    }

    public function showRegistrationForm()
    {
        return view('master.auth.register');
    }

//    TODO move to OAuthController
    public function facebookClient()
    {
        Session::put('facebook', 'client_register');

        return Socialite::driver('facebook')->redirect();
    }
//    TODO move to OAuthController
    public function facebookMaster()
    {
        Session::put('facebook', 'master_register');

        return Socialite::driver('facebook')->redirect();
    }


//    if data from facebook not enought we use modal window
//      to add more info This method handle modal form
    public function facebookClientCreate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:100|unique:users',
            'tel' => 'required|min:11|max:25',
            'profile_id' => 'required|min:5',
            'profile_url' => 'string',
        ]);

        if(empty($request->compare_code)) {
            $unique_code = mt_rand(0000, 9999);
            $sms_response = $this->registerRepository->sendSms($request->tel, $unique_code);

            if($sms_response) {
                return $unique_code;
            }
            else {
                return ['status' => 'failed_sms_send'];
            }
        }
        else {
            if($this->registerRepository->confirmPhone($request)) {
                if (Session::has('equipment_review_id')) {
                    event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
                    Session::forget('equipment_review_id');
                }
                if(Session::get('intended_path', false)) {
                    $intended_path = Session::get('intended_path', false);
                    Session::forget('intended_path');
                    return ['redirect' => url($intended_path)];
                }

                return ['status' => 'success'];
            }
            else {
                return ['errors' => 'Невірний код підтвердження'];
            }
        }
    }

//    if data from facebook not enought we use modal window
//      to add more info This method handle modal form
    public function facebookMasterCreate(Request $request)
    {
        $array_rules = [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:100|unique:users',
            'profile_id' => 'required|min:5',
            'profile_url' => 'string',
        ];
        $messages = [
            'email' => 'Поле Email не пройшло валідацію!',
            'email.required' => 'Поле Email обовязкове для заповнення!',
            'email.unique' => 'Такий Email вже використовується!',
        ];
        $validator = Validator::make($request->all(), $array_rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
//
        $roleId = UserRole::where('name', 'master')->first()->id;
        $statusId = MasterStatus::where('slug', 'inactive')->first()->id;
        $facebookProfileTypeId = UserProfileType::where('slug', 'facebook')->first()->id;
        $user = User::create(['email' => $request->input('email'), 'name' => 'undefined', 'role_id' => $roleId]);

        $master = Master::create(['user_id' => $user->id, 'status_id' => $statusId]);

        $profileUser = UserProfile::create([
            'profile_id' => $request->input('profile_id'),
            'user_id' => $user->id,
            'type_id' => $facebookProfileTypeId,
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'profile_url' => $request->input('profile_url')
        ]);

        Auth::login($user);
        if (Session::has('equipment_review_id')) {
            event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
            Session::forget('equipment_review_id');
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }
        }
        return response()->json(['redirect' => route('master.profile')]);
    }

    //client creation
    //TODO add proper validation message
    public function clientCreate(Request $request)
    {
        $this->middleware('guest');

        $this->validate($request, [
            'name' => 'required|string|max:50',
            'tel' => 'required|min:11|max:18',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if(empty($request->compare_code)) {
            $unique_code = mt_rand(0000, 9999);
            $sms_response = $this->registerRepository->sendSms($request->tel, $unique_code);

            if($sms_response) {
                return $unique_code;
            }
            else {
                return ['status' => 'failed_sms_send'];
            }
        }
        else {
            if($this->registerRepository->confirmPhone($request)) {
                if (Session::has('equipment_review_id')) {
                    event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
                    Session::forget('equipment_review_id');
                }
                if(Session::get('intended_path', false)) {
                    $intended_path = Session::get('intended_path', false);
                    Session::forget('intended_path');

                    return ['redirect' => url($intended_path)];
                }

                return ['status' => 'success'];
            }
            else {
                return ['errors' => 'Невірний код підтвердження'];
            }
        }

    }


    //first form master creation
    public function masterCreate(Request $request)
    {
        $this->middleware('guest');

        $array_rules = [
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
        $messages = [
            'email' => 'Поле Email не пройшло валідацію!',
            'email.required' => 'Поле Email обовязкове для заповнення!',
            'email.unique' => 'Такий Email вже використовується!',
            'password.required' => 'Поле Пароль обовязкове для заповнення!',
            'password.min' => 'Пароль повинен бути більше 6 символів!',
            'g-recaptcha-response.required' => 'Каптча обов`язкова для заповнення!',
            'g-recaptcha-response.recaptcha' => 'Каптча не вірна!'
        ];

        $validator = Validator::make($request->all(), $array_rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $name = '';
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));
        $roleId = UserRole::where('name', 'master')->first()->id;
        $statusId = MasterStatus::where('slug', 'inactive')->first()->id;

        $user = User::create(['email' => $email, 'password' => $password, 'name' => $name, 'role_id' => $roleId]);

        $master = Master::create(['user_id' => $user->id, 'status_id' => $statusId]);
        UserVerification::generate($user);
        UserVerification::send($user, 'Реєстрація у сервісі Джоуль');

        Auth::login($user);
        if (Session::has('equipment_review_id')) {
            event(new EquipmentReviewStore(Session::get('equipment_review_id'), Auth::user()));
            Session::forget('equipment_review_id');
            if(Session::get('intended_path', false)) {
                $intended_path = Session::get('intended_path', false);
                Session::forget('intended_path');
                return redirect($intended_path);
            }
        }
        return redirect('/master/register')
            ->with('email', 'На ваш email щойно було відправлено повідомлення для підтвердження реєстрації.');
    }
}
