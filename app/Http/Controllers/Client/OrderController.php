<?php

namespace App\Http\Controllers\Client;

use App\Events\Client\OrderCanceled;
use App\Events\Client\OrderClosed;
use App\Events\ClientOrderCreated;
use App\Events\ClientOrderRestored;
use App\Events\MasterOrderInProgress;
use App\Http\Controllers\Controller as Controller;
use App\Mail\MasterSelectedToOrder;
use App\Models\Attachment;
use App\Models\Master;
use App\Models\MasterStatus;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Region;
use App\Repositories\Criteria\Order\ClientList;
use App\Repositories\Criteria\RelationAttribute;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use App\Models\OrderCategory;
use App\Models\OrderSubcategory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateRequest;
use App\Http\Requests\Order\AttachmentRequest;
use Illuminate\Support\Str;

class OrderController extends Controller
{

    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a list of orders.
     *
     * @param  string $filter
     * @return \Illuminate\Http\Response
     */
    public function index($filter = null)
    {
        $client_id = Auth::user()->client->id;

        $this->orderRepository->pushCriteria(new ClientList($client_id));

        if(!$this->orderRepository->validateUrlFilter($filter))
            abort(404);

        $filter = $this->orderRepository->decodeUrlFilter($filter);

        if(isset($filter['category']))
            $this->orderRepository->pushCriteria(new RelationAttribute('subcategory.category', 'slug', $filter['category']));

        $orders = $this->orderRepository->paginate(20);

        return view('client.order.index', compact('orders', 'filter'));
    }

    /**
     * Show the form for creating a new order.
     *
     * @param string $category
     * @param string $subcategory
     * @param string $ground
     * @return \Illuminate\Http\Response
     */
    public function create($category, $subcategory = null, $ground = null)
    {
//        TODO: make helper method for this or hide it somewhere (may be all this code should be moved to validator)
        $categories = OrderCategory::all('slug')->pluck('slug')->all();

        if (!in_array($category, $categories))
            abort(404);

//        TODO: make helper method for this or hide it somewhere
        if (count(OrderCategory::where('slug', $category)->first()->subcategories) == 1)
            return view("order.create.$category");

        $subcategories = OrderSubcategory::whereHas('category', function ($query) use ($category) {
            $query->where('slug', $category);
        })->get(['slug'])->pluck('slug')->all();

        if (!in_array($subcategory, $subcategories))
            abort(404);

        if($ground === 'ground') {
            return view("order.create.$category.$subcategory.$ground");
        }
        elseif ($ground === 'roof') {
            return view("order.create.$category.$subcategory.$ground");
        }

        return view("order.create.$category.$subcategory");
    }





    public function attachmentUpload(AttachmentRequest $request)
    {
        if ($request->session()->has('order_id')) {

            $file = $request->file('qqfile');
            $order = Order::find(session('order_id'));

            if ($file) {
                $attachment = new Attachment();
                $attachment->attachable_id = session('order_id');
                $attachment->attachable_type = 'order';
                $attachment->saveFromPost($file);
                $attachment->save();
            }
            return json_encode([
                "success" => true,
                "uuid" => $_REQUEST['qquuid']
            ]);
        }
        return response('error', 500);
    }

    public function finishUploadSession(Request $request)
    {
        $request->session()->forget('order_id');

        return route('client.order.index');
    }

    public function deletePhoto(Request $request)
    {
        $attachmentId = $request->get('attachment_id');
        $attachment = Attachment::find($attachmentId);
        if ($attachment) {
            $attachment->deleteStorageFile();
            $attachment->delete();
        }

        return response($attachmentId, 200);
    }

    /**
     * Display the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::withCount(['requests_from_active_masters'])
            ->with([
                'subcategory.category',
                'orderable',
                'requests_from_active_masters.master' => function ($master_query) {
                    $master_query->withCount(['reviews', 'negative_reviews']);
                },
                'requests.master' => function ($master_query) {
                    $master_query->withCount(['reviews', 'negative_reviews']);
                },
                'status',
            ])
            ->findOrFail($id);

        $another_masters_requests = $order->requests->keyBy('master.id')->forget($order->master_id);

        if (!Auth::user()->can('view', $order))
            return redirect()->route('order.show', ['id' => $id]);

//        TODO: check status with constants
        if ($order->status->slug == 'deactivated')
            abort(404);

        return view("client.order.show.{$order->status->slug}", compact('order', 'another_masters_requests'));
    }

    /**
     * Show the form for editing the specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with('subcategory.category')->findOrFail($id);

        if (!Auth::user()->can('update', $order))
            return redirect()->route('home');

        $category = $order->subcategory->category->slug;
        $subcategory = $order->subcategory->slug;
        if($order->ground == '1') {
            return view("order.edit.{$category}.{$subcategory}.ground", compact('order'));
        }
        elseif ($order->ground == '0') {
            return view("order.edit.{$category}.{$subcategory}.roof", compact('order'));
        }

        if (count(OrderCategory::where('slug', $category)->first()->subcategories) == 1)
            return view("order.edit.$category", compact('order'));

        return view("order.edit.{$category}.{$subcategory}", compact('order'));
    }

    /**
     * Store a newly created order in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();

        $clientId = Auth::user()->client->id;

        $data['client_id'] = $clientId;
        $order = $this->orderRepository->create($data);

        event(new ClientOrderCreated(Auth::user(), $order));

        if(empty(Auth::user()->remember_token)) {
            Auth::user()->remember_token = Str::random(60);
            Auth::user()->save();
        }

        session(['order_id' => $order->id]);

        return response(json_encode(['redirectTo' => route('client.order.index')]), 200);
    }

    /**
     * Update the specified order in storage.
     *
     * @param  UpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $order = Order::findOrFail($id);

        if (!Auth::user()->can('update', $order))
            return response(json_encode(['redirectTo' => route('home')]), 200);

        $this->orderRepository->update($request->all(), $order);

        session(['order_id' => $order->id]);
        return response(json_encode(['redirectTo' => route('client.order.index')]), 200);

    }

    /**
     * Change order status to canceled.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $order = $this->orderRepository->findOrFail($id);

//        TODO: save status id with constants
        if (Auth::user()->can('change_status', $order)) {
            $order->status_id = 6;
            $order->save();
        }

        event(new OrderCanceled(Auth::user(), $order));

        return redirect()->route('client.order.index');
    }

    /**
     * Change order status to closed.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function close($id)
    {
        $order = $this->orderRepository->findOrFail($id);

//        TODO: save status id with constants
        if (Auth::user()->can('change_status', $order)) {
            $order->status_id = 8;
            $order->save();
        }

        event(new OrderClosed(Auth::user(), $order));

        return redirect()->route('client.order.index');
    }

    /**
     * Change order status to checking.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    //todo: rename method to store
    public function runChecking($id)
    {
        $order = $this->orderRepository->findOrFail($id);

//        TODO: save status id with constants
        if (Auth::user()->can('change_status', $order)) {
            $order->status_id = 1;
            $order->save();
            event(new ClientOrderRestored(Auth::user(), $order));
        }

        return redirect()->route('client.order.index');
    }

    /**
     * Change order status to considering.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function runConsidering($id)
    {
        $order = $this->orderRepository->findOrFail($id);

//        TODO: save status id with constants
        if (Auth::user()->can('change_status', $order)) {
            $order->status_id = 3;
            $order->save();
            event(new ClientOrderRestored(Auth::user(), $order));
        }

        return redirect()->route('client.order.index');
    }

    /**
     * Select master as executor of order.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function selectMaster(Request $request, $id)
    {
//        TODO: add validation
        $order = $this->orderRepository->findOrFail($id);

//        TODO: check if master has request for this order
        if (Auth::user()->can('own', $order)) {
            $order->master_id = $request->master_id;
            $order->status_id = OrderStatus::where('slug', 'in_progress')->first()->id;
            $order->save();
        }

        //todo:
        event(new MasterOrderInProgress(Master::find($order->master_id), $order));

        return redirect()->route('client.order.index');
    }

    public function getSelectedMasters(Request $request)
    {
        $regions = Region::all()->pluck('id')->toArray();
        if(!in_array($request->region_id, $regions)) {
            $regions = Region::all()->pluck('name', 'id')->toArray();
            $region_id = Region::where('slug', 'kyiv')->first()->id;

            foreach ($regions as $id => $region) {
                $region_to_compare = explode(' ', $region)[0];
                similar_text($region_to_compare, $request->input('region_id'), $percentage);
                if ($percentage > 79) {
                    $region_id = $id;
                }
            }
        }
        else {
            $region_id = $request->region_id;
        }

        $selected_masters = Master::selectedMastersForOrder($request, $region_id)->count();

        $selected_verified_masters = Master::selectedMastersForOrder($request, $region_id)->verifiedStatusMasters()->count();

        return ['selected_masters' => $selected_masters, 'selected_verified_masters' => $selected_verified_masters];
    }
}
