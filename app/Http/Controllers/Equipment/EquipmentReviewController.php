<?php

namespace App\Http\Controllers\Equipment;

use App\Events\User\UserLeftEquipmentReview;
use App\Http\Controllers\Controller;
use App\Http\Requests\EquipmentReviewRequest;
use App\Models\Equipment\Equipment;
use App\Models\Equipment\EquipmentReview;
use App\Models\Equipment\EquipmentType;
use App\Models\User\Master\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class EquipmentReviewController extends Controller
{
    public function create(string $equipment_type, string $brand_slug)
    {
        $slug = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING[$equipment_type];

        $brand = Equipment::where('slug', $brand_slug)->whereHas('type', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->first();

        $invertor_type_id = EquipmentType::where('slug', EquipmentType::TYPE_INVERTER)->first()->id;

        return view('equipment.review.create', compact('brand', 'invertor_type_id'));
    }

    public function store(EquipmentReviewRequest $request)
    {
        $brand = Equipment::find($request->input('equipment_id'));
        $brand_type = array_search(EquipmentType::find($brand->type_id)->slug, Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING);
        $status_id = Status::where('slug', Status::CHECKING_STATUS)->first()->id;
        $user_id = Auth::check() ? Auth::user()->id : null;

        $request->merge([
            'status_id' => $status_id,
            'user_id' => $user_id,
            'user_IP' => $request->getClientIp()
        ]);

        $equipment_review = EquipmentReview::create($request->all());

        if (Auth::check()) {
            event(new UserLeftEquipmentReview($brand->title, route('admin.equipment.review.edit',
                ['equipment_type' => $brand_type,
                    'brand_slug' => $brand->slug, 'id' => $equipment_review->id])));
            return redirect()->to(route('brand.show', ['equipment_type' => $brand_type, 'brand_slug' => $brand->slug]) . '#equipment-reviews');
        }

        $request->session()->put('equipment_review_id', $equipment_review->id);
        $request->session()->put('intended_path', url(route('brand.show', ['equipment_type' => $brand_type, 'brand_slug' => $brand->slug]) . '#equipment-reviews'));
        return redirect()->route('login');
    }

    public function edit(string $equipment_type, string $brand_slug, $id)
    {
        $slug = Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING[$equipment_type];

        $brand = Equipment::where('slug', $brand_slug)->whereHas('type', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->first();

        $invertor_type_id = EquipmentType::where('slug', EquipmentType::TYPE_INVERTER)->first()->id;
        $review = EquipmentReview::findOrFail($id);

        if (!Auth::check() || Auth::user()->id != $review->user_id)
            abort(404);

        return view('equipment.review.edit', compact('review', 'brand', 'invertor_type_id'));
    }

    public function update(EquipmentReviewRequest $request, $id)
    {
        $brand = Equipment::find($request->input('equipment_id'));
        $brand_type = array_search(EquipmentType::find($brand->type_id)->slug, Equipment::EQUIPMENT_TYPE_TO_SLUG_MAPPING);

        $request->merge([
            'user_IP' => $request->ip()
        ]);

        $review = EquipmentReview::findOrFail($id);
        $review->fill($request->all())->save();

        return redirect()->to(route('brand.show', ['equipment_type' => $brand_type, 'brand_slug' => $brand->slug]) . '#equipment-reviews');
    }

    public function destroy($id)
    {
        $review = EquipmentReview::findOrFail($id);

        if (!Auth::check() || Auth::user()->id != $review->user_id)
            abort(404);

        $review->delete();

        return redirect()->back();
    }
}
