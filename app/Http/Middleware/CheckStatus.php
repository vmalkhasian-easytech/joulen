<?php

namespace App\Http\Middleware;

use App\Models\MasterStatus;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $master = Auth::user()->master;

        if ($master->id && $master->hasValidStatus())
            return $next($request);

        if ($master->id && $master->status->slug == 'wait_activation')
            return redirect()->route('master.profile');

        return redirect()->route('master.profile');
    }
}
