<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if ( (Auth::check() && ( Auth::user()->blocked || Auth::user()->deleted) )) {
            Auth::logout();
        }

        if ( (Auth::check() && Auth::user()->hasAnyRole($roles)) || (!Auth::check() && in_array('guest', $roles)) ) {
            return $next($request);
        }

        if(!Auth::check()){
            Session::put('intended_path', $request->path());
            return redirect()->route('login');
        }

        return redirect()->route('home');
    }
}
