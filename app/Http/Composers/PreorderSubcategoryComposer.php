<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\PreorderSubcategory;

/**
 * Preorder subcategory composer
 *
 * @package App\Http\Composers
 */
class PreorderSubcategoryComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = PreorderSubcategory::with('category')->get()->keyBy('slug');

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('preorder_subcategories', $this->getData());
    }
}

