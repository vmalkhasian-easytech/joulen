<?php

namespace App\Http\Composers;


use App\Models\Equipment\EquipmentType;
use Illuminate\Contracts\View\View;

class EquipmentTypeComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = EquipmentType::all();

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('equipment_types', $this->getData());
    }
}