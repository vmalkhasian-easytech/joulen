<?php

namespace App\Http\Requests\Distributor;

use App\Models\User;
use App\Models\User\Distributor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slug = $email = '';
        $distributor = Distributor::find($this->route()->parameter('distributor')) ?? Auth::user()->distributor;

        if (array_key_exists('slug', $this->validationData())) {
            $slug = 'required';

            if ($distributor->slug !== $this->validationData()['slug']) {
                $slug .= '|unique:distributors,slug';
            }
        }

        $user = User::find($this->validationData()['user_id']);

        if (array_key_exists('email', $this->validationData()) && $user) {
            $email = 'required';

            if ($user->email !== $this->validationData()['email']) {
                $email .= '|unique:users,email';
            }
        }

        return [
            'company_name' => 'string',
            'description' => 'string',
            'slug' => $slug,
            'website' => 'string',
            'corporate_email' => 'string',
            'phone' => 'string',
            'second_phone' => 'string|nullable',
            'balance' => 'integer',
            'active' => 'boolean',
            'email' => $email,
            'password' => 'nullable|min:6',
            'masters' => 'array',
            'masters.*' => 'required_with:masters|exists:masters,id',
            'equipment' => 'array',
            'equipment.*' => 'required_with:equipment|exists:equipment,id',
        ];
    }
}