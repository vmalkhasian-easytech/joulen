<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PreorderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subcategory_id' => 'exists:order_subcategories,id',
            'name' => 'required|regex:/^[\p{L}\s-]+$/u|string|max:50',
            'phone' => 'required|string|max:18',
            'email' => 'required|string|email|max:100',
            'comment' => [
                'string',
                'max:500',
                'nullable',
                "regex:/^[ 0-9\p{L}\s\.!;:,\?-]{0,500}$/u"
            ]
        ];
    }
}