<?php

use Illuminate\Support\Facades\Auth;

function roleRoute($name, $parameters = [], $absolute = true, $role = null) {
    if(is_null($role) && !is_null(Auth::user())) {
        $role = Auth::user()->role->name;
    }
        
    if(is_null($role)){
        return route($name, $parameters, $absolute);
    }else{
        return route($role . '.' . $name, $parameters, $absolute);
    }
}

function phone_format($phone)
{
    $phone = str_replace(array('+', ' ', '(', ')', '-'), '', $phone);
    if (strpos($phone, '38') === 0) {
        $phone = substr($phone, 2);
    }

    return $phone;
}

function array_filter_null(array $data)
{
    return array_filter($data, function ($value) {
        return !is_null($value);
    });
}

function blog_route(string $route = '')
{
    $blog_prefix = config('app.blog_url_prefix');

    return $route[0] === '/' ? "{$blog_prefix}{$route}" : "{$blog_prefix}/{$route}";
}