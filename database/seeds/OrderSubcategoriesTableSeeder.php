<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('order_subcategories')->truncate();
            DB::table('order_subcategories')->insert([
                'id' => 1,
                'category_id' => 1,
                'name' => 'Мережева',
                'slug' => 'network',
                'home' => '1'
            ]);
            DB::table('order_subcategories')->insert([
                'id' => 2,
                'category_id' => 1,
                'name' => 'Гібридна',
                'slug' => 'hybrid',
                'home' => '1'
            ]);
            DB::table('order_subcategories')->insert([
                'id' => 3,
                'category_id' => 1,
                'name' => 'Автономна',
                'slug' => 'autonomous',
                'home' => '1'
            ]);
            DB::table('order_subcategories')->insert([
                'id' => 4,
                'category_id' => 1,
                'name' => 'Комерційна',
                'slug' => 'commercial',
                'home' => '0'
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
