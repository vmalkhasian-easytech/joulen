<?php

use Illuminate\Database\Seeder;

class MasterStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('master_statuses')->truncate();
            DB::table('master_statuses')->insert([
                'id' => 1,
                'name' => 'Неактивний',
                'slug' => 'inactive',
            ]);
            DB::table('master_statuses')->insert([
                'id' => 2,
                'name' => 'Неактивний (очікує активації)',
                'slug' => 'wait_activation',
            ]);
            DB::table('master_statuses')->insert([
                'id' => 3,
                'name' => 'Активний (неперевірений)',
                'slug' => 'unverified',
            ]);
            DB::table('master_statuses')->insert([
                'id' => 4,
                'name' => 'Активний (перевірений)',
                'slug' => 'verified',
            ]);
            DB::table('master_statuses')->insert([
                'id' => 5,
                'name' => 'Відхилено',
                'slug' => 'canceled',
            ]);
        });
    }
}
