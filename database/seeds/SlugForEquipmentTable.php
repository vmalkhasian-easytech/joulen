<?php

use Illuminate\Database\Seeder;

class SlugForEquipmentTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $equipment = \App\Models\Equipment\Equipment::all();

        foreach ($equipment as $brand) {
            $name = $brand->title;
            $slug = str_slug($name);
            DB::table('equipment')->where('title', $name)->update(['slug' => $slug]);
        }
    }
}
