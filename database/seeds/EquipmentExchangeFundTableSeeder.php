<?php

use Illuminate\Database\Seeder;

class EquipmentExchangeFundTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::table('equipment_exchange_fund')->truncate();
            DB::table('equipment_exchange_fund')->insert([
                'id' => 1,
                'name' => 'На складах вашої компанії в Україні',
                'slug' => 'company_storage',
            ]);
            DB::table('equipment_exchange_fund')->insert([
                'id' => 2,
                'name' => 'На складах офіційного представника в Україні',
                'slug' => 'representative_storage',
            ]);
            DB::table('equipment_exchange_fund')->insert([
                'id' => 3,
                'name' => 'На території України підмінний фонд відсутній',
                'slug' => 'absent_storage',
            ]);
            DB::table('equipment_exchange_fund')->insert([
                'id' => 4,
                'name' => 'Інше',
                'slug' => 'other',
            ]);
        });
    }
}
