<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreorderCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('preorder_categories')->truncate();
            DB::table('preorder_categories')->insert([
                'id' => 1,
                'name' => 'Аудит ринку сміття',
                'slug' => 'waste_audit',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
