<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('EquipmentTypesTableSeeder');
        $this->call('EnergyAuditHeatingTypeTableSeeder');
        $this->call('OrderCategoriesTableSeeder');
        $this->call('OrderRequestPriceTypesTableSeeder');
        $this->call('OrderStatusesTableSeeder');
        $this->call('OrderSubcategoriesTableSeeder');
        $this->call('PaymentStatusesTableSeeder');
        $this->call('PreorderCategoriesTableSeeder');
        $this->call('PreorderSubcategoriesTableSeeder');
        $this->call('RegionsTableSeeder');
        $this->call('SesPriceCategoryTableSeeder');
        $this->call('SubcategoryFactorsTableSeeder');
        $this->call('UserRolesTableSeeder');
        $this->call('UserProfileTypesTableSeeder');
        $this->call('WasteSortingSizeTableSeeder');
        $this->call('EquipmentExchangeFundTableSeeder');
        $this->call('EquipmentGuaranteeRepairTableSeeder');
        $this->call('EmployeeTypesTableSeeder');
        $this->call('AttachmentTypesTableSeeder');
        $this->call('StatusesTableSeeder');
        $this->call('MasterStatusesTableSeeder');
        $this->call('ClientStatusesTableSeeder');
        $this->call('MarketDataSeeder');
    }
}
