<?php

use Illuminate\Database\Seeder;

class AttachmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('attachment_types')->truncate();
            DB::table('attachment_types')->insert([
                'id' => 1,
                'name' => 'master_portfolio'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 2,
                'name' => 'master_photo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 3,
                'name' => 'car_photo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 4,
                'name' => 'contract'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 5,
                'name' => 'machinery_photo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 6,
                'name' => 'invertor_photo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 7,
                'name' => 'panel_photo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 8,
                'name' => 'order'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 9,
                'name' => 'equipment_logo'
            ]);
            DB::table('attachment_types')->insert([
                'id' => 10,
                'name' => 'distributor_photo'
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
