<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_master', function (Blueprint $table) {
            $table->unsignedInteger('distributor_id');
            $table->integer('master_id')->nullable();

            $table->foreign('distributor_id')->references('id')->on('distributors')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('master_id')->references('id')->on('masters')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_master');
    }
}
