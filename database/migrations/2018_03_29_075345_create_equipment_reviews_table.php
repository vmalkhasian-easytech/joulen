<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipment_id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->boolean('positive');
            $table->string('title');
            $table->integer('quality_rating');
            $table->integer('price_rating');
            $table->integer('service_rating');
            $table->text('body');
            $table->integer('status_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('equipment_reviews', function (Blueprint $table) {
            $table->foreign('equipment_id')->references('id')->on('equipment')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment_reviews', function (Blueprint $table) {
            $table->dropForeign('equipment_reviews_equipment_id_foreign');
            $table->dropForeign('equipment_reviews_user_id_foreign');
            $table->dropForeign('equipment_reviews_status_id_foreign');
        });
        Schema::dropIfExists('equipment_reviews');
    }
}
