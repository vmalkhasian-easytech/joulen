<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_data', function (Blueprint $table) {
            $table->increments('id');
            $table->float('green_tariff_home_ses', 9, 3);
            $table->float('green_tariff_roof_ses', 9, 3);
            $table->float('green_tariff_ground_ses', 9, 3);
            $table->float('avg_cost_network_ses_5kW', 9, 3);
            $table->float('avg_cost_network_ses_30kW', 9, 3);
            $table->float('avg_cost_auto_ses_5kW', 9, 3);
            $table->float('avg_cost_auto_ses_30kW', 9, 3);
            $table->float('ratio_hybrid_station', 5, 2);
            $table->float('avg_cost_roof_ses_30kW', 9, 3);
            $table->float('avg_cost_roof_ses_100kW', 9, 3);
            $table->float('avg_cost_ground_ses_100kW', 9, 3);
            $table->float('avg_cost_ground_ses_1000kW', 9, 3);
            $table->float('avg_cost_gel_batteries', 9, 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_data');
    }
}
