<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMasterPortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_portfolios', function (Blueprint $table) {
            $table->string('month');
            $table->string('year');
            $table->string('address');
            $table->decimal('lat', 8, 2)->nullable();
            $table->decimal('lng', 8, 2)->nullable();
            $table->decimal('power', 8, 1);
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->unsignedInteger('invertor_id')->nullable();
            $table->unsignedInteger('panel_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->boolean('general_contractor')->nullable();
            $table->boolean('designing')->nullable();
            $table->boolean('installation')->nullable();
            $table->boolean('delivery')->nullable();
            $table->boolean('documentation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
