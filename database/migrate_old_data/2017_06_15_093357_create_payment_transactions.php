<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action');
            $table->string('status');
            $table->string('paytype');
            $table->string('acq_id');
            $table->string('payment_system_order_id');
            $table->string('payment_system_payment_id');
            $table->string('description');
            $table->string('ip');
            $table->string('sender_card_mask2');
            $table->string('sender_card_bank');
            $table->string('sender_card_type');
            $table->string('sender_card_country');
            $table->string('currency');
            $table->integer('payment_id');
            $table->timestamp('create_date')->nullable();;
            $table->timestamp('end_date')->nullable();;
            $table->string('transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
