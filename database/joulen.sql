-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema joulen
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema joulen
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `joulen` DEFAULT CHARACTER SET utf8 ;
USE `joulen` ;

-- -----------------------------------------------------
-- Table `joulen`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`user_roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `password` CHAR(60) NULL,
  `name` VARCHAR(60) NOT NULL,
  `role_id` INT NOT NULL,
  `phone` VARCHAR(20) NULL,
  `blocked` TINYINT(1) NOT NULL DEFAULT 0,
  `deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `users_role_idx` (`role_id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  CONSTRAINT `users_role_id_foreign`
    FOREIGN KEY (`role_id`)
    REFERENCES `joulen`.`user_roles` (`id`));


-- -----------------------------------------------------
-- Table `joulen`.`regions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`regions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`masters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`masters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `region_id` INT NULL,
  `company` TINYINT(1) NULL DEFAULT 0,
  `work_email` VARCHAR(255) NULL,
  `website` VARCHAR(255) NULL,
  `about` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `master_idx` (`user_id` ASC),
  INDEX `master_order_id_foreign_idx` (`region_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  CONSTRAINT `masters_user_id_foreign`
    FOREIGN KEY (`user_id`)
    REFERENCES `joulen`.`users` (`id`),
  CONSTRAINT `masters_region_id_foreign`
    FOREIGN KEY (`region_id`)
    REFERENCES `joulen`.`regions` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`clients` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `client_user_id_foreign_idx` (`user_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  CONSTRAINT `clients_user_id_foreign`
    FOREIGN KEY (`user_id`)
    REFERENCES `joulen`.`users` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_statuses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_statuses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_subcategories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_subcategories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `order_sybcategory_category_idx` (`category_id` ASC),
  CONSTRAINT `order_sybcategories_category_id_foreign`
    FOREIGN KEY (`category_id`)
    REFERENCES `joulen`.`order_categories` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_id` INT NOT NULL,
  `master_id` INT NULL,
  `region_id` INT NULL,
  `status_id` INT NOT NULL,
  `subcategory_id` INT NOT NULL,
  `orderable_id` INT NULL,
  `orderable_type` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `client_comment` TEXT NULL,
  `manager_comment` TEXT NULL,
  `cancellation_comment` TEXT NULL,
  `paid` TINYINT(1) NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `order_region_idx` (`region_id` ASC),
  INDEX `order_status_idx` (`status_id` ASC),
  INDEX `orders_client_id_foreign_idx` (`client_id` ASC),
  INDEX `orders_master_id_foreign_idx` (`master_id` ASC),
  INDEX `orders_subcategory_id_foreign_idx` (`subcategory_id` ASC),
  CONSTRAINT `orders_client_id_foreign`
    FOREIGN KEY (`client_id`)
    REFERENCES `joulen`.`clients` (`id`),
  CONSTRAINT `orders_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`),
  CONSTRAINT `orders_region_id_foreign`
    FOREIGN KEY (`region_id`)
    REFERENCES `joulen`.`regions` (`id`),
  CONSTRAINT `orders_status_id_foreign`
    FOREIGN KEY (`status_id`)
    REFERENCES `joulen`.`order_statuses` (`id`),
  CONSTRAINT `orders_subcategory_id_foreign`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `joulen`.`order_subcategories` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`payments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`payments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `master_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  `amount` DECIMAL(15,2) NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `payment_order_idx` (`order_id` ASC),
  INDEX `payments_master_id_foreign_idx` (`master_id` ASC),
  CONSTRAINT `payments_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`),
  CONSTRAINT `payments_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_requests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_requests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `master_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  `final` TINYINT(1) NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `master_request_order_idx` (`order_id` ASC),
  INDEX `order_requests_master_id_foreign_idx` (`master_id` ASC),
  CONSTRAINT `order_requests_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`),
  CONSTRAINT `order_requests_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`preorder_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`preorder_categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`preorder_subcategories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`preorder_subcategories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `preorder_subcategory_category_id_idx` (`category_id` ASC),
  CONSTRAINT `preorder_subcategories_category_id_foreign`
    FOREIGN KEY (`category_id`)
    REFERENCES `joulen`.`preorder_categories` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`preorders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`preorders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `subcategory_id` INT NOT NULL,
  `preorderable_id` INT NULL,
  `preorderable_type` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `preorders_subcategory_id_foreign_idx` (`subcategory_id` ASC),
  CONSTRAINT `preorders_subcategory_id_foreign`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `joulen`.`preorder_subcategories` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`reviews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_id` INT NOT NULL,
  `master_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  `text` TEXT NULL,
  `positive` TINYINT(1) NULL DEFAULT 0,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `review_order_id_foreign_idx` (`order_id` ASC),
  INDEX `review_master_id_foreign_idx` (`master_id` ASC),
  INDEX `review_client_id_foreign_idx` (`client_id` ASC),
  CONSTRAINT `reviews_client_id_foreign`
    FOREIGN KEY (`client_id`)
    REFERENCES `joulen`.`clients` (`id`),
  CONSTRAINT `reviews_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`),
  CONSTRAINT `reviews_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`master_region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`master_region` (
  `master_id` INT NULL,
  `region_id` INT NULL,
  INDEX `master_regions_region_idx` (`region_id` ASC),
  INDEX `master_regions_master_id_foreign_idx` (`master_id` ASC),
  CONSTRAINT `master_regions_region_id_foreign`
    FOREIGN KEY (`region_id`)
    REFERENCES `joulen`.`regions` (`id`),
  CONSTRAINT `master_regions_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`partners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`partners` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `partners_user_id_foreing_idx` (`user_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  CONSTRAINT `partners_user_id_foreing`
    FOREIGN KEY (`user_id`)
    REFERENCES `joulen`.`users` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`partner_settings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`partner_settings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `partner_id` INT NOT NULL,
  `key` VARCHAR(45) NOT NULL,
  `value` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `partner_settings_partner_id_foreign_idx` (`partner_id` ASC),
  CONSTRAINT `partner_settings_partner_id_foreign`
    FOREIGN KEY (`partner_id`)
    REFERENCES `joulen`.`partners` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_subcategory_partner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_subcategory_partner` (
  `partner_id` INT NULL,
  `subcategory_id` INT NULL,
  INDEX `partner_order_subcategory_idx` (`subcategory_id` ASC),
  INDEX `partner_order_subcategories_partner_id_foreign_idx` (`partner_id` ASC),
  CONSTRAINT `partner_order_subcategories_subcategory_id_foreign`
    FOREIGN KEY (`subcategory_id`)
    REFERENCES `joulen`.`order_subcategories` (`id`),
  CONSTRAINT `partner_order_subcategories_partner_id_foreign`
    FOREIGN KEY (`partner_id`)
    REFERENCES `joulen`.`partners` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`ses_price_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`ses_price_category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_ses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_ses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` INT NOT NULL,
  `price_category_id` INT NULL,
  `photocell_power` DECIMAL(15,1) NULL DEFAULT 0.00,
  `battery_capacity` INT NULL,
  `required_area` INT NULL,
  `area` INT NULL,
  `area_unit` ENUM('m', 'ha') NULL DEFAULT 'm',
  `parts_comment` TEXT NULL,
  `lat` DECIMAL(8,6) NULL,
  `lng` DECIMAL(8,6) NULL,
  `zoom` INT NULL,
  `house_comment` TEXT NULL,
  `green_tariff` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `order_ses_order_idx` (`order_id` ASC),
  INDEX `order_ses_price_caterogy_id_foreign_idx` (`price_category_id` ASC),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC),
  CONSTRAINT `order_ses_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`),
  CONSTRAINT `order_ses_price_caterogy_id_foreign`
    FOREIGN KEY (`price_category_id`)
    REFERENCES `joulen`.`ses_price_category` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`energy_audit_heating_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`energy_audit_heating_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_energy_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_energy_audit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` INT NOT NULL,
  `heating_type_id` INT NULL,
  `floors` INT NULL,
  `area` INT NULL,
  `area_unit` ENUM('m', 'ha') NULL DEFAULT 'm',
  `exploitation` TINYINT(1) NULL,
  `residents` INT NULL,
  `electricity_per_year` INT NULL,
  `water_per_year` INT NULL,
  `gas_per_year` INT NULL,
  `house_comment` TEXT NULL,
  `high_costs` TINYINT(1) NULL,
  `cold_winter` TINYINT(1) NULL,
  `hot_summer` TINYINT(1) NULL,
  `mold` TINYINT(1) NULL,
  `draft` TINYINT(1) NULL,
  `windows_fogging` TINYINT(1) NULL,
  `disbalance` TINYINT(1) NULL,
  `blackout` TINYINT(1) NULL,
  `provider_problems` TINYINT(1) NULL,
  `other_problems` TEXT NULL,
  `thermal_examination` TINYINT(1) NULL,
  `blower_door_test` TINYINT(1) NULL,
  `energy_passport` TINYINT(1) NULL,
  `detailed_calculation` TINYINT(1) NULL,
  `technical_supervision` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `order_energy_idx` (`order_id` ASC),
  INDEX `order_energy_audit_heating_type_id_foreign_idx` (`heating_type_id` ASC),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC),
  CONSTRAINT `order_energy_audit_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`),
  CONSTRAINT `order_energy_audit_heating_type_id_foreign`
    FOREIGN KEY (`heating_type_id`)
    REFERENCES `joulen`.`energy_audit_heating_type` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`waste_sorting_size`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`waste_sorting_size` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `power` INT NULL,
  `name` VARCHAR(45) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_waste_sorting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_waste_sorting` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` INT NOT NULL,
  `size_id` INT NULL,
  `size_comment` TEXT NULL,
  `about_comment` TEXT NULL,
  `pm_help` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `order_waste_audit_order_idx` (`order_id` ASC),
  INDEX `order_waste_sorting_size_id_foreign_idx` (`size_id` ASC),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC),
  CONSTRAINT `order_waste_sorting_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`),
  CONSTRAINT `order_waste_sorting_size_id_foreign`
    FOREIGN KEY (`size_id`)
    REFERENCES `joulen`.`waste_sorting_size` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`preorder_waste_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`preorder_waste_audit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `preorder_id` INT NOT NULL,
  `name` VARCHAR(60) NULL,
  `phone` VARCHAR(20) NULL,
  `email` VARCHAR(255) NULL,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `preorder_waste_audit_preorder_idx` (`preorder_id` ASC),
  CONSTRAINT `waste_audit_preorders_preorder_id_foreign`
    FOREIGN KEY (`preorder_id`)
    REFERENCES `joulen`.`preorders` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`partner_preorder_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`partner_preorder_category` (
  `partner_id` INT NULL,
  `category_id` INT NULL,
  INDEX `partner_preorder_subcategories_partner_id_foreign_idx` (`partner_id` ASC),
  INDEX `partner_preorder_categories_category_id_foreign_idx` (`category_id` ASC),
  CONSTRAINT `partner_preorder_categories_category_id_foreign`
    FOREIGN KEY (`category_id`)
    REFERENCES `joulen`.`preorder_categories` (`id`),
  CONSTRAINT `partner_preorder_categories_partner_id_foreign`
    FOREIGN KEY (`partner_id`)
    REFERENCES `joulen`.`partners` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`client_emails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`client_emails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_id` INT NOT NULL,
  `master_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  `text` TEXT NULL,
  `comment` TEXT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `client_email_order_idx` (`order_id` ASC),
  INDEX `client_emails_client_id_foreign_idx` (`client_id` ASC),
  INDEX `client_emails_master_id_foreign_idx` (`master_id` ASC),
  CONSTRAINT `client_emails_client_id_foreign`
    FOREIGN KEY (`client_id`)
    REFERENCES `joulen`.`clients` (`id`),
  CONSTRAINT `client_emails_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`),
  CONSTRAINT `client_emails_order_id_foreign`
    FOREIGN KEY (`order_id`)
    REFERENCES `joulen`.`orders` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`settings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`settings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(45) NOT NULL,
  `value` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`master_portfolios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`master_portfolios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `master_id` INT NOT NULL,
  `description` TEXT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `master_portfolio_master_id_foreign_idx` (`master_id` ASC),
  CONSTRAINT `master_portfolios_master_id_foreign`
    FOREIGN KEY (`master_id`)
    REFERENCES `joulen`.`masters` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_request_price_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_request_price_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  `slug` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `order_request_price_type_category_id_foreign_idx` (`category_id` ASC),
  CONSTRAINT `order_request_price_types_category_id_foreign`
    FOREIGN KEY (`category_id`)
    REFERENCES `joulen`.`order_categories` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`order_request_prices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`order_request_prices` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `request_id` INT NOT NULL,
  `type_id` INT NOT NULL,
  `price` DECIMAL(15,2) NULL DEFAULT 0.00,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `order_request_price_request_id_idx` (`request_id` ASC),
  INDEX `order_request_price_type_id_idx` (`type_id` ASC),
  CONSTRAINT `order_request_prices_request_id_foreign`
    FOREIGN KEY (`request_id`)
    REFERENCES `joulen`.`order_requests` (`id`),
  CONSTRAINT `order_request_prices_type_id_foreign`
    FOREIGN KEY (`type_id`)
    REFERENCES `joulen`.`order_request_price_types` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`admins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`admins` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `admins_user_id_foreign_idx` (`user_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  CONSTRAINT `admins_user_id_foreign`
    FOREIGN KEY (`user_id`)
    REFERENCES `joulen`.`users` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joulen`.`attachments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `joulen`.`attachments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `attachable_id` INT NULL,
  `attachable_type` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

