<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get(config('app.blog_url_prefix'), function(){})->name('blog');

Route::get('privacy', 'HomeController@privacyPage')->name('privacy');

Route::post('/commission/LiqPay', 'PaymentController@handleLiqPayCallback')->name('liqpay.callback');

Route::get('/commission/pay/order/{orderId}', 'PaymentController@initTransaction')->name('commission.pay.order');

Route::group(['namespace' => 'Auth'], function () {

    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::get('email-verification/check/{token}', 'RegisterController@getVerification')->name('email-verification.check');

    Route::group(['middleware' => ['guest']], function () {
        // Authentication Routes...
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        // Registration Routes...
        Route::get('client/register', 'RegisterController@showRegisterClientForm')->name('client.register');
        Route::post('client/register', 'RegisterController@clientCreate');
        Route::post('client/facebook/register', 'RegisterController@facebookClientCreate')->name('client.facebook.register');
        Route::post('master/facebook/register', 'RegisterController@facebookMasterCreate')->name('master.facebook.register');
        Route::get('master/register', 'RegisterController@showRegistrationForm')->name('master.register');
        Route::post('master/register', 'RegisterController@masterCreate');

        Route::get('registration', function () {
            return view('auth.facebook_wrong_login');
        })->name('registration');

        // Password Reset Routes...
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
    });

    Route::get('password/reset/{token?}', 'ResetPasswordController@showResetForm')->name('password.reset.form');
    Route::get('password/change', 'ResetPasswordController@showChangeForm')->name('password.change')->middleware('role:admin,client,master,distributor');
    Route::post('password/change', 'ResetPasswordController@change')->name('password.change')->middleware('role:admin,client,master,distributor');

//client creation
    Route::post('user/MyProjects', 'RegisterController@clientCreate')->name('clientCreation');

    Route::group(['prefix' => 'facebook', 'as' => 'facebook.'], function () {

        Route::get('login/', 'OAuthController@facebookLogin')->name('login');
        Route::get('callback', 'OAuthController@facebookLoginCallback')->name('login.callback');
        Route::get('client/', 'RegisterController@facebookClient')->name('client');
        Route::get('master/', 'RegisterController@facebookMaster')->name('master');

        Route::get('attach', 'OAuthController@facebookAttach')->name('attach');
        Route::get('detach', 'OAuthController@facebookDetach')->name('detach');

    });

});

//TODO: change all routes below
Route::group(['middleware' => 'role:client,guest'], function() {
//    steps for creating an order
    Route::get('/user/search', function () {
        return view('temp.search');
    })->name('temp.search');
    Route::get('/user/search/step1', function () {
        return view('temp.step1');
    })->name('temp.step1');
    Route::get('/user/search/step2', function () {
        return view('temp.step2');
    })->name('temp.step2');
    Route::get('/user/search/step22', function () {
        return view('temp.step22');
    })->name('temp.step22');

//    assistant pages
    Route::get('/start', function () {
        return view('temp.step0');
    })->name('temp.step0');
    Route::group(['prefix' => 'faq'], function () {
        Route::get('', function () {
            return view('temp.faq');
        })->name('temp.faq');

        Route::get('/domashni_ses', function () {
            return view('temp.step2_1');
        })->name('assistant.step2_1');

        Route::get('/ses_na_bahatopoverkhivkakh', function () {
            return view('temp.step2_2');
        })->name('assistant.step2_2');

        Route::get('/nazemna_ses', function () {
            return view('temp.step2_3');
        })->name('assistant.step2_3');

        Route::get('/dakhova_ses_dlja_biznesu', function () {
            return view('temp.step2_4');
        })->name('assistant.step2_4');

        Route::get('/ses_dlja_shkil_likaren', function () {
            return view('temp.step2_4');
        });

        Route::get('/investycii_u_sonjachnu_energetyku', function () {
            return view('temp.step2_5');
        })->name('assistant.step2_5');

        Route::get('/ses_dlja_shkil_likaren', function () {
            return view('temp.step2_6');
        })->name('assistant.step2_6');
    });

//    station info
    Route::group(['prefix' => 'assistant', 'as' => 'assistant.'], function () {
        Route::get('merezheva-info', function () {
            return view('order.station-info-category.station-info-subcategory.network');
        })->name('network');

        Route::get('/avtonomna-info', function () {
            return view('order.station-info-category.station-info-subcategory.autonomous');
        })->name('autonomous');

        Route::get('/gibrydna-info', function () {
            return view('order.station-info-category.station-info-subcategory.hybrid');
        })->name('hybrid');

        Route::get('/nazemna-info', function () {
            return view('order.station-info-category.station-info-subcategory.ground');
        })->name('ground');

        Route::get('/dakhova-info', function () {
            return view('order.station-info-category.station-info-subcategory.roof');
        })->name('roof');
    });

    Route::get('services/consulting', function () {
        return view('temp.consulting');
    })->name('services.consulting');

    Route::post('services/consulting', 'HomeController@sendConsultingMail')->name('consulting.mail');
});
//TODO: change all routes under

Route::get('order/calculator', function (){
    return view('order.calculator');
})->name('order.calculator')->middleware('role:admin,client,guest');


Route::get('cancel_mailing/{order}/{token?}', 'Client\ClientController@cancelMailing')->name('cancel_mailing')->middleware('role:client,guest');
Route::get('confirm_mailing/{order}/{token?}', 'Client\ClientController@confirmMailing')->name('confirm_mailing')->middleware('role:client,guest');

Route::resource('order', 'OrderController', ['only' => ['show']]);

Route::group(['prefix' => 'brand', 'as' => 'brand.', 'namespace' => 'Equipment'], function () {
    Route::get('{equipment_type}', 'EquipmentController@index')->name('index')->where('equipment_type', '^(solar-panel|inverter)$');

    Route::post('{equipment_type}', 'EquipmentController@index')->where('equipment_type', '^(solar-panel|inverter)$');

    Route::get('{equipment_type}/{brand_slug}', 'EquipmentController@show')->name('show')
        ->where('equipment_type', '^(solar-panel|inverter)$');

    Route::get('{equipment_type}/{brand_slug}/review/create', 'EquipmentReviewController@create')
        ->name('review.create')->where('equipment_type', '^(solar-panel|inverter)$');

    Route::get('{equipment_type}/{brand_slug}/review/{id}/edit', 'EquipmentReviewController@edit')
        ->name('review.edit')->where('equipment_type', '^(solar-panel|inverter)$');

    Route::resource('review', 'EquipmentReviewController', ['except' => [ 'create', 'edit', 'index' ]]);
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'role:admin'], function () {


    Route::get('portfolio/photo/minimize', 'MasterPortfolioController@minimizePhoto');
//    admin.order.index page request based filtering
    Route::post('order/photo/delete', 'OrderController@deletePhoto')->name('order.photo.delete');

    Route::get('order/activate/{id}', 'OrderController@activate')->name('order.activate');

    Route::post('order/attachment/upload', 'OrderController@attachmentUpload')->name('attachment.upload');

    Route::post('/order/attachment/upload/completed', 'OrderController@finishUploadSession')->name('attachment.upload.complete');
    
    Route::post('order', 'OrderController@index')->name('order.index.filter');

    Route::post('order/{id}/subcategory/change', 'OrderController@changeCategory')->name('order.category.change');

    Route::get('order/{id}/request', 'OrderController@requests')->name('order.request');

    Route::put('order/{id}/meta', 'OrderController@updateMeta')->name('order.update.meta');

    Route::resource('order', 'OrderController', [
        'except' => ['create', 'store', 'destroy']
    ]);

    Route::resource('request', 'OrderRequestController', [
        'only' => ['edit', 'update']
    ]);

    Route::resource('preorder', 'PreorderController', ['only' => ['index', 'update']]);


    Route::resource('client_email', 'ClientEmailController', ['only' => ['index', 'update']]);

    Route::get('client_catalog_list', 'ClientEmailController@showCatalog')->name('client_catalog_list');

    Route::put('client_catalog_list/{id}', 'ClientEmailController@updateCatalog')->name('update_catalog');

//      admin.equipment.index page request based filtering
    Route::resource('equipment', 'EquipmentController', ['only' => ['index']]);

    Route::group(['prefix' => 'equipment', 'as' => 'equipment.'], function () {
        Route::post('', 'EquipmentController@index')->name('index.filter');

        Route::get('create', 'EquipmentController@create')->name('create');

        Route::post('store', 'EquipmentController@store')->name('store');

        Route::get('{id}/edit', 'EquipmentController@edit')->name('edit');

        Route::put('{id}', 'EquipmentController@update')->name('update');

//        Route::get('get', 'EquipmentController@getEquipment')->name('equipment.get');

        Route::delete('{id?}', 'EquipmentController@delete')->name('delete');

        Route::get('/reviews', 'EquipmentReviewController@index')->name('review.index');
        Route::post('/reviews', 'EquipmentReviewController@index')->name('review.index.filter');

        Route::get('{equipment_type}/{brand_slug}/reviews/{id}/edit', 'EquipmentReviewController@edit')
            ->where('equipment_type', '^(solar-panel|inverter)$')->name('review.edit');

        Route::put('reviews/{id}', 'EquipmentReviewController@update')->name('review.update');

        Route::put('reviews/status/{id?}/{status_id?}', 'EquipmentReviewController@statusUpdate')->name('review.status.update');
    });

//    admin.master.index page request based filtering
    Route::post('master', 'MasterController@index')->name('master.index.filter');

    Route::get('master/restore/{id}', 'MasterController@restoreUser')->name('master.restore');

    Route::resource('master/verification', 'VerificationRequestController',  ['only' => ['index', 'update']]);

    Route::put('master/verification/comment/{id?}', 'VerificationRequestController@storeComment')->name('master.comment.store');

    Route::get('master/verification/comment/{id?}', 'VerificationRequestController@getComment')->name('master.comment.get');

    Route::put('master/status/{id?}/{status_id?}', 'MasterController@statusUpdate')->name('master.status.update');

    Route::resource('master', 'MasterController', ['only' => ['index', 'edit', 'update']]);

    Route::get('client/restore/{id}', 'ClientController@restoreUser')->name('client.restore');

    Route::resource('client', 'ClientController', ['only' => ['edit', 'update', 'create', 'store']]);

    Route::match(['get', 'post'], 'customers', 'ClientController@index')->name('customer.index');

    Route::get('kpi','AdminController@kpi')->name('kpi');

    Route::get('settings', 'AdminController@settings')->name('settings');
    Route::put('/', 'AdminController@update')->name('update');

    Route::post('review/create', 'ReviewController@create')->name('review.create');
    Route::post('review/{id}/edit', 'ReviewController@edit')->name('review.edit');

    Route::resource('review', 'ReviewController', ['only' => ['store', 'update']]);
    Route::post('portfolio', 'MasterPortfolioController@index')->name('portfolio.index.filter');
    Route::resource('portfolio', 'MasterPortfolioController', [
        'except' => ['create', 'store', 'edit']
    ]);
    Route::get('portfolio/show/{id?}', 'MasterPortfolioController@show')->name('portfolio.show');
    Route::get('portfolio/get/{id?}', 'MasterPortfolioController@get')->name('portfolio.get');
    Route::post('portfolio/status/update/{id?}', 'MasterPortfolioController@statusUpdate')->name('portfolio.status.update');
    Route::post('portfolio/update/{id?}', 'MasterPortfolioController@update')->name('portfolio.update');
    //OLD PORTFOLIO ROUTES
//    Route::post('settings/portfolio/save', 'MasterController@savePortfolio')->name('settings.portfolio.save');
//    Route::post('settings/portfolio/attache/image', 'MasterController@attachImagePortfolio')->name('portfolio.attach.image');
//    Route::post('settings/portfolio/attachment/edit', 'MasterController@editImagesPortfolio')->name('portfolio.attachment.edit');
//    Route::post('settings/portfolio/delete/image', 'MasterController@deleteImagePortfolio')->name('portfolio.delete.image');
//    Route::post('settings/portfolio/delete', 'MasterController@deletePortfolio')->name('portfolio.delete');
//    Route::post('settings/portfolio/edit', 'MasterController@editPortfolio')->name('portfolio.edit');

    // master profile edit routes
    Route::group(['prefix' => 'master', 'as' => 'master.'], function () {
        Route::post('/profile/equipment', 'MasterController@equipmentStore')->name('store.equipment');
        Route::get('/profile/equipment/{id?}', 'MasterController@getEquipment')->name('get.equipment');
        Route::put('/profile/equipment/{id?}', 'MasterController@equipmentUpdate')->name('update.equipment');
        Route::delete('/profile/equipment/{id?}', 'MasterController@equipmentDelete')->name('delete.equipment');
        Route::get('/profile/brands/{type_id?}', 'MasterController@getBrands')->name('get.brands');
        Route::post('/profile/portfolio', 'MasterController@portfolioStore')->name('store.portfolio');
        Route::get('/profile/portfolio/{id?}', 'MasterController@getPortfolio')->name('get.portfolio');
        Route::post('/profile/portfolio/update/{id?}', 'MasterController@portfolioUpdate')->name('update.portfolio');
        Route::delete('/profile/portfolio/{id?}', 'MasterController@portfolioDelete')->name('delete.portfolio');
        Route::post('/profile/update', 'MasterController@update')->name('update.profile');
    });

    Route::get('/market_condition', 'AdminController@showMarketData')->name('market_condition');
    Route::put('/market_condition/{id}', 'AdminController@updateMarketData')->name('update.market_condition');

    Route::resource('distributors', 'DistributorController');

    Route::post('distributors', 'DistributorController@index')->name('distributors.index.filter');

    Route::post('distributor', 'DistributorController@store')->name('distributors.store');
});

Route::group(['namespace' => 'Master'], function () {

    Route::group(['as' => 'master.'], function () {

        Route::group(['prefix' => 'master', 'middleware' => 'role:master'], function () {

            Route::post('/profile/equipment', 'ProfileController@equipmentStore')->name('store.equipment');
            Route::get('/profile/equipment/{id?}', 'ProfileController@getEquipment')->name('get.equipment');
            Route::put('/profile/equipment/{id?}', 'ProfileController@equipmentUpdate')->name('update.equipment');
            Route::get('/profile/homePortfoliosCoordinates', 'ProfileController@getHomePortfoliosCoordinates')->name('getHomePortfolioCoordinates');
            Route::get('/profile/commercialPortfoliosCoordinates', 'ProfileController@getCommercialPortfoliosCoordinates')->name('getCommercialPortfolioCoordinates');
            Route::delete('/profile/equipment/{id?}', 'ProfileController@equipmentDelete')->name('delete.equipment');
            Route::get('/profile/brands/{type_id?}', 'ProfileController@getBrands')->name('get.brands');
            Route::post('/profile/portfolio', 'ProfileController@portfolioStore')->name('store.portfolio');
            Route::get('/profile/portfolio/{id?}', 'ProfileController@getPortfolio')->name('get.portfolio');
            Route::post('/profile/portfolio/update/{id?}', 'ProfileController@portfolioUpdate')->name('update.portfolio');
            Route::delete('/profile/portfolio/{id?}', 'ProfileController@portfolioDelete')->name('delete.portfolio');
            Route::get('/profile/preview/{id}', 'ProfileController@profilePreview')->name('profile.preview');
            Route::post('/profile/update', 'ProfileController@update')->name('update.profile');

            Route::group(['middleware' => 'master_status'], function () {

                Route::get('commission', 'MasterController@commission')->name('commission');

                //        TODO: recheck this regular expression a few hundreds times
                Route::get('order/{filter?}', 'OrderController@index')->where('filter', '(?!create|private|[0-9]+|[0-9]+\/edit).*')->name('order.index');
                Route::get('order/private', 'OrderController@indexPrivate')->name('order.private.index');
                Route::get('order/private/{id}', 'OrderController@showPrivate')->name('order.private.show');
                Route::resource('order', 'OrderController', ['only' => ['show']]);
                Route::resource('request', 'OrderRequestController', ['only' => ['store', 'edit', 'update']]);
//                Route::get('settings/public', 'MasterController@publicSettings')->name('settings.public');
                Route::get('settings/private', 'MasterController@privateSettings')->name('settings.private');
                Route::get('/general', 'ProfileController@generalInfo')->name('profile_pages.general');
                Route::put('/general', 'ProfileController@pageGeneralInfoUpdate')->name('profile_pages.general.update');
                Route::get('/contact', 'ProfileController@contacts')->name('profile_pages.contact');
                Route::put('/contact', 'ProfileController@pageContactsUpdate')->name('profile_pages.contact.update');
                Route::get('/partnership', 'ProfileController@cooperationData')->name('profile_pages.partnership');
                Route::get('/services', 'ProfileController@service')->name('profile_pages.services');
                Route::put('/services', 'ProfileController@pageServiceUpdate')->name('profile_pages.services.update');
                Route::get('/portfolio', 'ProfileController@portfolio')->name('profile_pages.portfolio');
                Route::put('/partnership/', 'ProfileController@updatePartnership')->name('profile_pages.partnership.update');
                Route::get('/subscribe', 'ProfileController@subscribe')->name('profile_pages.subscribe');
                Route::put('/subscribe', 'ProfileController@subscriptionUpdate')->name('profile_pages.subscription.update');
                Route::get('/verification_request', 'ProfileController@verificationRequest')->name('profile_pages.verification_request');
                Route::post('/verification_request', 'ProfileController@verificationRequestStore')->name('profile_pages.verification_request.store');
                Route::get('/profile/general_information', 'ProfileController@generalInfo')->name('profile.general');
                Route::get('/profile/contacts', 'ProfileController@contacts')->name('profile.contacts');
                Route::get('/profile/cooperation_data', 'ProfileController@cooperationData')->name('profile.cooperationData');
                Route::get('/profile/service', 'ProfileController@service')->name('profile.service');
                Route::get('/profile/portfolio', 'ProfileController@portfolio')->name('profile.portfolio');
                Route::put('/private', 'MasterController@updatePrivate')->name('update.private');
                Route::delete('delete', 'MasterController@delete')->name('delete');
                Route::post('settings/portfolio/save', 'MasterController@savePortfolio')->name('settings.portfolio.save');
                Route::post('settings/portfolio/attache/image', 'MasterController@attachImagePortfolio')->name('portfolio.attach.image');
                Route::post('settings/portfolio/attachment/edit', 'MasterController@editImagesPortfolio')->name('portfolio.attachment.edit');
                Route::post('settings/portfolio/delete/image', 'MasterController@deleteImagePortfolio')->name('portfolio.delete.image');
                Route::post('settings/portfolio/delete', 'MasterController@deletePorfolio')->name('portfolio.delete');
                Route::post('settings/portfolio/edit', 'MasterController@editPortfolio')->name('portfolio.edit');
                Route::get('/faq', 'MasterController@pageFAQ')->name('faq');
            });
        });
        Route::group(['prefix' => 'master', 'middleware' => 'role:temp_master, master'], function () {
            Route::get('profile', 'ProfileController@create')->name('profile');
            Route::post('/profile', 'ProfileController@store')->name('store.profile');
        });

//    TODO: recheck this regular expression a few hundreds times
//        master.index page url based filtering
        Route::get('master/{filter?}', 'MasterController@index')->where('filter', '(?!create|[0-9]+|[0-9]+\/edit).*')->name('index');

//        links to the specific blocks of master.show page
        Route::get('master/{id}#reviews', 'MasterController@show')->name('show.reviews');
        Route::get('master/{id}#orders_in_progress', 'MasterController@show')->name('show.orders_in_progress');

    });

//    TODO: check routes order

    Route::resource('master', 'MasterController', ['only' => ['create', 'store'], 'middleware' => 'role:temp_master']);

    Route::resource('master', 'MasterController', ['only' => ['show']]);

});

Route::group(['prefix' => 'distributor', 'namespace' => 'Distributor', 'as' => 'distributor.'], function () {
    Route::group(['middleware' => 'role:distributor'], function () {
        Route::get('edit', 'DistributorController@edit')->name('edit');
        Route::put('update', 'DistributorController@update')->name('update');

        Route::get('masters', 'DistributorController@myMasters')->name('masters');
        Route::post('masters/update', 'DistributorController@updateMyMasters')->name('masters.update');

        Route::get('settings', 'DistributorController@settings')->name('settings');
        Route::put('', 'DistributorController@updateSettings')->name('settings.update');
    });

    Route::get('{slug}', 'DistributorController@show')->name('show');
});

Route::group(['prefix' => 'client', 'namespace' => 'Client', 'as' => 'client.', 'middleware' => 'role:client'], function () {

    Route::get('order/create/{category}/{subcategory?}/{ground?}', 'OrderController@create')->name('order.create');

    Route::get('order/preview', 'OrderController@getSelectedMasters')->name('order.preview');

    Route::get('order/{filter?}', 'OrderController@index')->where('filter', '(?!create|[0-9]+|[0-9]+\/edit).*')->name('order.index');

    Route::resource('order', 'OrderController', ['except' => ['index', 'create', 'destroy']]);

    Route::get('order/{id}/cancel', 'OrderController@cancel')->name('order.cancel');
    Route::get('order/{id}/close', 'OrderController@close')->name('order.close');
    Route::get('order/{id}/run-checking', 'OrderController@runChecking')->name('order.run.checking');
    Route::get('order/{id}/run-considering', 'OrderController@runConsidering')->name('order.run.considering');
    Route::post('order/{id}/select-master', 'OrderController@selectMaster')->name('order.select.master');

    Route::get('settings', 'ClientController@settings')->name('settings');
    Route::put('/', 'ClientController@update')->name('update');
    Route::delete('delete', 'ClientController@delete')->name('delete');

    Route::post('email/send', 'ClientController@sendEmail')->name('email.send');

        Route::post('master/email/send', 'ClientController@sendMasterEmail')->name('master.email.send');

        Route::post('/order/attachment/upload/completed', 'OrderController@finishUploadSession');
    Route::post('/order/attachment/upload/completed', 'OrderController@finishUploadSession')->name('attachment.upload.complete');
    Route::post('order/photo/delete', 'OrderController@deletePhoto')->name('order.photo.delete');


    Route::post('order/attachment/upload', 'OrderController@attachmentUpload')->name('attachment.upload');

    Route::post('review/create', 'ReviewController@create')->name('review.create');
    Route::post('review/{id}/edit', 'ReviewController@edit')->name('review.edit');

    Route::resource('review', 'ReviewController', ['only' => ['store', 'update']]);

});

Route::group(['prefix' => 'guest', 'as' => 'guest.'], function () {
    Route::group(['namespace' => 'Guest'], function () {

        Route::post('master/email/send', 'GuestController@sendMasterEmail')->name('master.email.send');

    });
});

Route::get('listMasters', function () {
    return redirect('master');
});