$("#formsetfilter").on('submit', function (e) {
    e.preventDefault();
});
$('.formsetfilter_type').on('click', function (e) {
    e.preventDefault();
    $('.formsetfilter_type').removeClass('active');
    $(this).addClass('active');
    applyFilter();
});

function applyFilter(){
    var category = $('.formsetfilter_type.active').first().data('category');
    var filter = [];
    var link = $('.formsetfilter_type').first().attr('href');

    if( typeof category != 'undefined' && category != 'all'){
        filter.push(category);
    }

    link = link + '/' + filter.join('/');
    window.location.href = link;
}