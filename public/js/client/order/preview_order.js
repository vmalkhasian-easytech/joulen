$(document).ready(function () {

    // initMap();
    $('body').on('click', '.btn-preview-order', function () {
        if($('#create-order-form').valid()) {
            $('.create-order').hide();
            $('.preview-order').show();
            var subcategory_id = $('input[name=subcategory]').val();
            var region_id, power, region_name;
            var regionBlock = '#selectregion option:selected';
            var regionInput = $('input[name=region_name]').val();

            if ($(regionBlock).val() != 0) {
                region_id = $(regionBlock).val();
                region_name = $(regionBlock).text();
            }
            else if ($('#location_legal ' + regionBlock).val() != undefined || $('#location_legal ' + regionBlock).val() != 0) {
                region_id = $('#location_legal ' + regionBlock).val();
                region_name = $('#location_legal ' + regionBlock).text();
            }
            if (regionInput) {
                region_id = regionInput;
                region_name = regionInput + ' обл.';
            }

            // if ($('#slider_value').val() != 0) {
            //     power = $('#slider_value').val();
            // }
            // if ($('#installOnly_power').val() != '') {
            //     power = $('#installOnly_power').val();
            // }
            // if ($('#power_block #installOnly_power').val()) {
            //     power = $('#power_block #installOnly_power').val();
            // }

            // receive selected to order masters
            $.ajax({
                method: 'GET',
                data: {subcategory: subcategory_id, region_id: region_id},
                url: selectedMasterURL,
                success: function (response) {
                    var companiesCase1 = CompaniseCases(response.selected_masters);
                    var companiesCase2 = CompaniseCases(response.selected_verified_masters);
                    if (response.selected_verified_masters == 1) {
                        companiesCase2 = ' компанію';
                    }
                    $('.selected-masters').html('<strong>' + response.selected_masters + '</strong><span>' + companiesCase1 + '</span>');
                    $('.selected-verified-masters').html('<strong>' + response.selected_verified_masters + '</strong><span>' + companiesCase2 + '</span>');
                }
            });

            // create title
            // var subcategory_name = subcategories[subcategory_id].name;
            // var commercial_type = '', ground = $('input[name=ground]').val();
            // if (ground === 1) {
            //     commercial_type = 'Наземна';
            // }
            // if (ground === 0) {
            //     commercial_type = 'Дахова';
            // }
            //
            // var title = [
            //     commercial_type,
            //     subcategory_name,
            //     'CEC',
            //     power,
            //     'кВт y ',
            //     region_name
            // ].join(' ');
            //
            // $('.order-title').text(title);
            //
            // $('html, body').animate({
            //     scrollTop: 0
            // }, 800);
            //
            // if ($('#c1').is(':checked')) {
            //     $('.deliveryBlock').show();
            //
            //     var battery_capacity = $('input[name=battery_capacity]').val();
            //     var price_category_id = $('input[name=ses_price_category]:checked').val();
            //     var price_category = $('label[for=order-ses-price-' + price_category_id + ']').text();
            //     var city = $('#city').val();
            //
            //     if (battery_capacity != undefined) {
            //         power = $('input[name=photocell_power]').val();
            //         $('.delivery-capacity').html('Ємність акумуляторів: <b>' + battery_capacity + '</b> кВт*год');
            //         $('.delivery-capacity').show();
            //     }
            //     $('.delivery-power').text(power);
            //     $('.delivery-price-category').html(price_category + '<ul class="specialBlock" style="display: none"></ul>');
            //
            //     if (price_category_id == 4) {
            //         var custom_price_category = $('textarea[name=parts_comment]').val();
            //         $('.delivery-price-category .specialBlock').html('<li style="white-space:pre-wrap">' + custom_price_category + '</li>');
            //         $('.delivery-price-category .specialBlock').show();
            //     }
            //
            //     if (!$('#c2').is(':checked')) {
            //         $('.delivery-location').html('Куди доставити обладнання: ' + city + ', ' + region_name);
            //         $('.delivery-location').show();
            //     }
            // }
            //
            // if ($('#c2').is(':checked')) {
            //     $('.installationBlock').show();
            //
            //     window.dispatchEvent(new Event('resize'));
            //     preview_map.setCenter(latlng);
            //     preview_marker.setPosition(latlng);
            //     preview_marker.setMap(preview_map);
            //
            //     var install_city = $('input[name=city_name]').val();
            //     var install_area = $('#area_house').val();
            //     var install_comment = $('#about_house').val();
            //     var photo_count = $('.uploaded-photo img').size();
            //     var photo_src = [];
            //     $('.installation-location').html(install_city + ', ' + region_name);
            //     $('.installation-area').text(install_area + 'м.кв');
            //     if (install_comment != '') {
            //         $('.installation-house-block').show();
            //         $('.installation-house-comment').text(install_comment);
            //     }
            //     if (photo_count > 0) {
            //         $('.project_photo').show();
            //         for (var i = 0; i < photo_count; i++) {
            //             photo_src = $('.qq-file-id-' + i + ' img').attr('src');
            //             $('.installation-photo').html('<a href="' + photo_src + '" data-lightbox="project-pictures" data-title="">' +
            //                 '<img width="118" src="' + photo_src + '" alt="">' +
            //                 '</a>');
            //         }
            //     }
            //     if (!$('#c1').is(':checked')) {
            //         $('.installation-power').html('Потужність станції: <b>' + power + '</b> кВт');
            //         $('.installation-power').show();
            //     }
            //
            // }
            //
            // if ($('#c3').is(':checked')) {
            //     $('.documentationBlock').show();
            //
            //     var doc_city = $('#location_legal #city').val();
            //
            //     if (!$('#c1').is(':checked') && !$('#c2').is(':checked')) {
            //         $('.documentation-power').html('Потужність станції: <b>' + power + '</b> кВт');
            //         $('.documentation-power').show();
            //         $('.documentation-location').html('Місце розташування об’єкту: ' + doc_city + ', ' + region_name);
            //         $('.documentation-location').show();
            //     }
            // }
        }
    });

    $('body').on('click', '.btn-back-create-order', function () {
        $('.create-order').show();
        if($('.create-order').is(':visible')) {
            window.dispatchEvent(new Event('resize'));
        }
        $('.preview-order').hide();
        $('html, body').animate({
            scrollTop: 0
        }, 800);

        // $('.deliveryBlock').hide();
        // $('.installationBlock').hide();
        // $('.documentationBlock').hide();
        // $('.documentation-location').hide();
        // $('.documentation-power').hide();
        // $('.installation-power').hide();
        // $('.installation-house-block').hide();
        // $('.delivery-location').hide();
        // $('.delivery-price-category .specialBlock').hide();
        // $('.delivery-capacity').hide();
    });
});

// var preview_map, preview_marker, latlng;
// function initMap() {
//     latlng = new google.maps.LatLng(parseFloat(N), parseFloat(E));
//     preview_map = new google.maps.Map(document.getElementById('projectMap'), {
//         center: latlng,
//         zoom: 16,
//         scrollwheel: false,
//         mapTypeId: 'hybrid'
//     });
//
//     preview_marker = new google.maps.Marker({
//         position: latlng,
//         map: preview_map,
//         title: $('input[name=city_name]').val(),
//         icon: markerSrc
//     });
// }

function CompaniseCases(word) {
    var companies = '';
    if (word == 1) {
        companies = ' компанія';
    }
    else if (word == 0 || word > 4) {
        companies = ' компаній';
    }
    else {
        companies = ' компанії';
    }

    return companies;
}
