$(".validate_form").validate({
    lang: 'uk',
    errorElement: 'span'
});

$.validator.addClassRules({
    validate_required: {
        required: true
    }
});