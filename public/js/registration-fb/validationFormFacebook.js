$(".register-validate").each(function () {
    $(this).validate({
        lang: 'uk',
        errorElement: 'span',
        ignore: ':hidden:not(.section-active .hidden_validate, .select_validate), .ignore',
        errorPlacement: function (error, element) {
            if (element.is('select:hidden')) {
                error.insertAfter(element.next('.nice-select'));
            } else {
                error.insertAfter(element);
            }
        }
    });
});


$(".registation-modal").validate({
    lang: 'uk',
    errorElement: 'span',
    ignore: ':hidden:not(.section-active .hidden_validate, .select_validate), .ignore',
    errorPlacement: function (error, element) {
        if (element.is('select:hidden')) {
            error.insertAfter(element.next('.nice-select'));
        } else {
            error.insertAfter(element);
        }
    }
});

$.validator.addMethod("email_required", $.validator.methods.required, "Введіть правильний email");
$.validator.addMethod("name_required", $.validator.methods.required, "Введіть ваше ім'я");
$.validator.addMethod("tel_required", $.validator.methods.required, "Вкажіть номер телефону");

$.validator.addClassRules({
    validate_required: {
        required: true
    },
    name_required: {
        name_required: true,
        maxlength: 150
    },
    email_required: {
        email: true,
        email_required: true,
        maxlength: 150
    },
    tel_required: {
        tel_required: true,
        minlength: 17
    },
    hidden_validate: {
        required: true
    },
    select_validate: {
        required: true
    },
    photocell_power_input: {
        required: true,
        min: 0.5
    },
    password_required: {
        required: true,
        minlength: 6
    },
    pasword_equal: {
        equalTo: ".password_required"
    }

});


$(".fb_required").each(function () {
    if ($(this).val() == '') {
        $(this).addClass('error').next('.error').show();
    } else {
        $(this).removeClass('error').next('.error').hide();
    }

});

$("body").on('change', '.fb_required', function () {
    if ($(this).val() == '') {
        $(this).addClass('error').next('.error').show();
        $('.empty_field').show();
        $('.login-facebook-modal').addClass('disabled');
        $('.remember-password').addClass('disabled');
    } else {
        $(this).removeClass('error').next('.error').hide();
        $('.empty_field').hide();
        $('.login-facebook-modal').removeClass('disabled');
        $('.remember-password').removeClass('disabled');
    }
});
