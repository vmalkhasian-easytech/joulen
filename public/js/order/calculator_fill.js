window.onpageshow = function (event) {
    var batery = getCookie('battery_capacity');
    var poverf = getCookie('photocell_power');
    var pl = Math.round(poverf * 7);

    if (!!batery) {
        $('#battery_capacity').val(batery);
    }
    if (!!poverf) {
        $('#powerf').val(poverf);
        $('#sliderRes').text(pl.toFixed(2));
        $('[name="required_area"]').val(pl.toFixed(2));
    }
    setCookie('battery_capacity', batery, {'path': '/', 'expires': -1});
    setCookie('photocell_power', poverf, {'path': '/', 'expires': -1});
};
