$('#fine-uploader-manual-trigger').fineUploader({
    template: 'qq-template-manual-trigger',
    request: {
        endpoint: userAttachmentHandler,
    },
    thumbnails: {
        placeholders: {
            waitingPath: waitingPath,
            notAvailablePath: notAvailablePath
        }
    },
    validation: {
        itemLimit: 5,
        sizeLimit: 15000000,
        stopOnFirstInvalidFile:false,
        allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
    },
    text: {
        formatProgress: "{percent}% з {total_size}",
        failUpload: "Збій при завантаженні",
        waitingForResponse: "Завантаження...",
        paused: "Пауза"
    },
    messages: {
        tooManyItemsError: "Завантажити можна не більше {itemLimit} фото.",
        noFilesError: "Немає файлів для завантаження",
        typeError: "{file} має невірний формат. Будь ласка, завантажте фото у форматі: {extensions}.",
        sizeError: "{file} занадто великий, максимальний розмір {sizeLimit}."
    },
    callbacks: {
        onCancel: function () {
            $('.upload_photo_buttons').show();
        },
        onStatusChange: function () {
            var submittedFileCount = this.getUploads({status: qq.status.SUBMITTED}).length;
            if (submittedFileCount >= 5) {
                $('.upload_photo_buttons').hide();
            }
        },
        onSubmit: function (id, fileName) {
            this.setParams({
                '_token': $('meta[name="csrf-token"]').attr('content')
            });
        },
        onAllComplete: function (successed,failed) {
            //todo test faieled upload photo
            // if ( failed.length > 0 ) {
            //     alert('Деякі з ваших фото не були завантаженні.');
            //     window.location.href = redirectTo;
            // }
            $.ajax({
                method: 'POST',
                url: userUploadComplete,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (redirectTo) {
                    window.location.href = redirectTo;

                }
            })
        },
        onComplete: function(){

        }
    },
    autoUpload: false
});

$('#trigger-upload').click(function () {
    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
    addOrderPreloader();
});

function addOrderPreloader() {
    $('body').append($('#preloader-order-template').html());
}

