$(document).ready(function () {
    $('#heating_type').val('');
    var selectedOption = $('.exploitation option:selected');
    if (selectedOption.data('value') == 1) {
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_repeat').slideDown("slow");
        $('.house_project').slideDown("slow");
        $('.site_button-orange.disabled').removeClass('disabled');
    }else if(selectedOption.data('value') == 0){
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_project').slideDown("slow");
        $('.house_repeat').hide();
        $('.site_button-orange.disabled').removeClass('disabled');
    }
});

function energyCategory(option) {
    if ($(option).data('value') == 1) {
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_repeat').slideDown("slow");
        $('.house_project').slideDown("slow");
        $('.site_button-orange.disabled').removeClass('disabled');
        var heatingTypeFirstValue = $('.heating_type ul:first li:first').data('value');
        $('#heating_type').val(heatingTypeFirstValue);
    }else if($(option).data('value') == 0){
        $('.house_content').slideDown("slow").addClass('section-active');
        $('.house_project').slideDown("slow");
        $('.house_repeat').hide();
        $('.site_button-orange.disabled').removeClass('disabled');
        $('.residents_amount_input').val('');
        $('.electricity_per_year_input').val('');
        $('.water_per_year_input').val('');
        $('.gas_per_year_input').val('');
        var heatingTypeUl = $('.heating_type ul:first');
        heatingTypeUl.find('.selected').removeClass('selected focus');
        var firstLi = $('.heating_type ul:first li:first');
        var text = firstLi.text();
        firstLi.addClass('selected focus');
        $('.heating_type .current').text(text);
        $('#heating_type').val('');
    }

}

$('body').on('click', '.exploitation .option', function () {
    energyCategory(this);
});