
$(document).ready(function() {
    var swiper = {};
    initSwiper();
    if ($("#c1").is(':checked') || $("#c2").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
    } else {
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
        $('#comm').css('display', 'none');
    }

    if($("#c3").is(':checked')){
        $('#c123').prop('checked', true);
    }else{
        $('#c123').prop('checked', false);
    }
});

function initSwiper(){
    var initial_slide = 0;
    swiper = new Swiper('.swiper-container', {
        direction: 'vertical',
        scrollbar: '.swiper-scrollbar',
        scrollbarHide: false,
        slidesPerView: '1',
        spaceBetween: 20,
        scrollbarDraggable: true,
        scrollbarSnapOnRelease: true,
        observer: true,
        observeParents: true,
        initialSlide: initial_slide,
        onSetTranslate:function(swiper) {
            var wasteIndex;
            if(swiper.activeIndex == 0){
                wasteIndex = 4;
                $('.waste_input').val(wasteIndex);
            }
            if(swiper.activeIndex == 1){
                wasteIndex = 3;
                $('.waste_input').val(wasteIndex);
            }
            if(swiper.activeIndex == 2){
                wasteIndex = 2;
                $('.waste_input').val(wasteIndex);
            }
            if(swiper.activeIndex == 3){
                wasteIndex = 1;
                $('.waste_input').val(wasteIndex);
            }
        },
        onInit:function (swiper) {
            var initIndex = $('.waste_input').val();
            if(initIndex == 1){
                swiper.slideTo(3);
            }
            if(initIndex == 2){
                swiper.slideTo(2);
            }
            if(initIndex == 3){
                swiper.slideTo(1);
            }
            if(initIndex == 4){
                swiper.slideTo(0);
            }
        }
    });
}

$("#c1").change(function () {

    if (this.checked || $("#c2").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });
    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });
    }
});

$("#c2").change(function () {
    if (this.checked || $("#c1").is(':checked') || $("#c3").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });

    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });

    }
});

$("#c3").change(function () {
    if (this.checked || $("#c2").is(':checked') || $("#c1").is(':checked')) {
        $('#comm').css('display', 'block');
        $('#boots').css({
            'background': '#ea6c21',
            'border-bottom-color': '#b54b1a',
            'color': '#fff',
            'cursor': 'pointer'
        });

    } else {
        $('#comm').css('display', 'none');
        $('#boots').css({
            'background': '#ddd',
            'border-bottom-color': '#ccc',
            'color': '#aaa',
            'cursor': 'default'
        });

    }
});
$("#c3").change(function () {
    if($(this).is(':checked')){
        $('.project_meneger').prop('checked', true);
    }else {
        $('.project_meneger').prop('checked', false);
    }
});

$('.item_line_swiper').on('click', function(){
    var slide = $(this).data('item');
    swiper.slideTo(slide - 1 );

}) ;