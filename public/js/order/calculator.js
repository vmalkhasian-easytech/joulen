$(document).ready(function () {

    $(".validate_form").validate({
        lang: 'uk',
        errorElement: 'span',
        ignore: ':hidden:not(.section-active .hidden_validate), .ignore',
        focusInvalid: false,
        errorPlacement: function(error, element) {
            if (element.is('select:hidden')) {
                error.insertAfter(element.next('.nice-select'));
            }else if(element.is('input[name=calc]')){
                error.insertAfter(element.parent('.calcData'));
            }else {
                error.insertAfter(element);
            }

        },
        invalidHandler: function() {
            setTimeout(function () {
                var position = $('span.error').first().offset().top;
                $('html,body').animate({scrollTop: position - 300},'slow');
            },100);
        },
        submitHandler: function() {
            goon();
        }
    });

    $.validator.addClassRules({
        validate_required: {
            required: true
        },
        hidden_validate: {
            required: true
        },
        photocell_power_input:{
            required: true,
            min:0.5,
            maxlength: 6
        },
        validate_number:{
            required: true,
            number:true,
            maxlength: 6
        },
        validate_number_3:{
            required: true,
            number:true,
            maxlength: 3
        },
        validate_ses_network:{
            required: true,
            number:true,
            min: 30
        },
        validate_power:{
            required: true,
            number:true,
            min: 1,
            maxlength: 5
        },
        validate_hours:{
            required: true,
            number:true,
            min: 1,
            max:24,
            maxlength: 2
        },
        validate_hours_week:{
            required: true,
            number:true,
            min: 1,
            max:168,
            maxlength: 3
        }
    });


    $('.only-letter').on('keypress', function() {
        var that = this;
        setTimeout(function() {
            var res = /[^а-яA-ЯїЇєЄіІёЁ ^a-zA-Z-]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        }, 0);
    });


    $('.only-number').on('keypress', function() {
        var that = this;
        setTimeout(function() {
            var res = /[^01234567890]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        }, 0);
    });




//***************** Нажимаєм кнопку Розрахувати*****************
    function goon(){
        // check why msred display block doesn't work
        if($('#radio1').is(':checked') == false && $('#radio2').is(':checked') == false){
            $('#msred').css('display','block');
            scroll(0,0);
            return;
        }
        var batery = (parseFloat($('#powerCalcResult').text())*parseFloat($('#dd').val())).toFixed(1);

        // var Vt = 0;
        // $('.calcRow').each(function () {
        //     if ($(this).children('.styled-checkbox').children('input[type=checkbox]').is(':checked')) {
        //         Vt += parseFloat($(this).find('input[type="text"]').eq(0).val());
        //     }
        // });

        if($('#radio1').is(':checked') == true){
            var poverf = (parseFloat($('#powerCalcResult').text())/3.5).toFixed(1);
        }
        if($('#radio2').is(':checked') == true){
            var poverf = (parseFloat($('#powerCalcResult').text())/1.1).toFixed(1);
        }

        setCookie('battery_capacity', batery, {'path':'/', 'domain':window.location.hostname});
        setCookie('photocell_power', poverf, {'path':'/',  'domain':window.location.hostname});
        // setCookie('station_power', Vt, {'path':'/'});

        history.go(-1);
    }


    $('#radio1').change(function(){
        $('#msred').css('display','none');
    });


    $('#radio2').change(function(){
        $('#msred').css('display','none');
    });


    //калькулятор ********************************
    function calc(elem){
        var Vt = parseFloat($(elem).closest('.calcRow').find('input[type="text"]').eq(0).val());
        var Itm = parseFloat($(elem).closest('.calcRow').find('input[type="text"]').eq(1).val());
        var Dm = parseFloat($(elem).closest('.calcRow').find('input[type="text"]').eq(2).val());

        if(isNaN(Vt)){
            Vt = 0;
        }
        if(isNaN(Itm)){
            Itm = 0;
        }
        if(isNaN(Dm)){
            Dm = 0;
        }
        var hoursType = $(elem).closest('.calc-field-wrap').find('.calc_hours');
        var type = $(elem).closest('.calc-field-wrap').find('select').val();
        if(type == 'Тиждень'){
            var result = (((Vt * Itm * Dm)/7)/1000).toFixed(1);
            hoursType.removeClass('validate_hours').addClass('validate_hours_week');
        }else{
            var result = ((Vt * Itm * Dm)/1000).toFixed(1);
            hoursType.removeClass('validate_hours_week').addClass('validate_hours');
        }
        if(result != 'NaN'){
            $(elem).closest('.calcRow').find('.calc_result').text(result);
            $(elem).closest('.calcRow').find('.calc_result').css({'font-size':'16px','color':'#000'});
        }
        var res = 0;

        $('.calcRow').children('div').children('input[type="checkbox"]').each(function(i,elem) {
            if ($(elem).is(':checked') == true) {

                var cur = parseFloat($(elem).closest('.calcRow').find('.calc_result').text());
                if(!isNaN(cur)){
                    res = res + cur;
                }


            }
        });
        var data = res;

        $('#powerCalcResult').text(data.toFixed(1));
    }
    $('input[type="text"]').keyup(function(){
        calc(this);
    });
    $('select').change(function(){
        calc(this);
    });



    $('input[type="checkbox"]').change(function(){
        var Vt = parseFloat($(this).closest('.calcRow').find('input[type="text"]').eq(0).val());
        var Itm = parseFloat($(this).closest('.calcRow').find('input[type="text"]').eq(1).val());
        var Dm = parseFloat($(this).closest('.calcRow').find('input[type="text"]').eq(2).val());

        var type = $(this).closest('.calcRow').find('select').val();

        if(type == 'Тиждень'){
            var result = (((Vt * Itm * Dm)/7)/1000).toFixed(1);
        }else{
            var result = ((Vt * Itm * Dm)/1000).toFixed(1);
        }

        if(result != 'NaN'){
            $(this).closest('.calcRow').find('.calc_result').text(result);
            $(this).closest('.calcRow').find('.calc_result').css({'font-size':'16px','color':'#000'});
        }

        if($(this).is(':checked') == false){
            $(this).closest('.calcRow').find('.calc_result').text('0');
        }

        var res = 0;
        $('.calcRow').children('div').children('input[type="checkbox"]').each(function(i,elem) {
            if ($(elem).is(':checked') == true) {
                var cur = parseFloat($(elem).closest('.calcRow').find('.calc_result').text());
                if(!isNaN(cur)){
                    res = res + cur;
                }

            }
        });
        var data = res;

        $('#powerCalcResult').text(data.toFixed(1));
    });
});
    