$(document).ready(function () {
    $('.system-meaning--list a').on('click', function (e) {
        e.preventDefault();
        var id= $(this).attr('href');
        var top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});