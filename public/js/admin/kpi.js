$(document).ready(function () {
    var i = 0;
    var max = 12;
    if($('.statistic').length > max) {
        $(".statistic").each(function() {
            i++;
            if(i > max) {
                $(this).addClass('hide-block');
            }
        });
    } else {
        $('.accept-btn').hide();
    }

    $('.accept-btn').on('click', function (e) {
        i = 0;
        e.preventDefault();
        $('.hide-block').each(function() {
            i++;
            if (i <= max) {
                $(this).removeClass('hide-block');
            }
        });
        if ($('.hide-block').length === 0) {
            $('.accept-btn').hide();
        }
    });

    $("#scrollbarX").mCustomScrollbar({
        axis: "yx",
        // setHeight: 400,
        setWidth: "100%",
        scrollbarPosition: "outside",
        theme: "dark",
        advanced: {
            autoExpandHorizontalScroll: true
        }
    });

});