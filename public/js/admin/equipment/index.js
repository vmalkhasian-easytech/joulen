$(document).ready(function () {
    // $(".nice-select").niceSelect();
    $(".select2").select2({
        placeholder: "Почніть вводити назву(лат)",
        width: '100%'
    });

    $('.pagination li:not(.active) a').on('click', function (e) {
        e.preventDefault();
        $('#admin_equipment_filter').attr('action', $(this).attr('href'));
        $('#admin_equipment_filter').submit();
    });

    // $(function () {
    //     $("#myTable").tablesorter();
    // });
    //
    // $('.equipment-item').on('click', function () {
    //     $(this).find('.tablesorter-header-inner').find('p').toggleClass('active');
    //     $('.equipment-item').find('.tablesorter-header-inner').find('p').find('.grade').css('color', '#b9bdc6');
    //     $(this).find('.tablesorter-header-inner').find('p').find('.grade').css('color', '#32394a');
    // });

    $('.equipment-item p').on('click', function () {
        $('.equipment-item p .grade').css('color', '#b9bdc6');
        $(this).find('.grade').css('color', '#32394a');
    });

    $('.equipment-item').on('click', function () {
        var current_sortorder = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortorder');
        var current_sortfield = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortfield');
        var new_sortorder = current_sortorder != 'asc' && current_sortfield == $(this).attr('data-sortfield') ? 'asc' : 'desc';
        $('.equipment-item').removeClass('desc');
        $('.equipment-item').removeClass('asc');
        $('.equipment-item').attr('data-sortorder', '');

        $(this).addClass(new_sortorder);
        $(this).attr('data-sortorder', new_sortorder);

        submit_admin_master_list();
    });

    function submit_admin_master_list(){
        var perpage = $('.amount_page_item.active').first().attr('data-perpage');
        var sortfield = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortfield');
        var sortorder = $('.equipment-item.desc,.equipment-item.asc').first().attr('data-sortorder');
        $('input[name=perpage]').val(perpage);
        $('input[name=sortfield]').val(sortfield);
        $('input[name=sortorder]').val(sortorder);

        $('#admin_equipment_filter').submit();
    }
});



