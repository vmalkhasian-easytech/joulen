$(document).ready(function () {
    $('body').on('click', 'input[type=checkbox]', function () {
        // var input = $(this).parent('div').next('input');
        var input = $(this).parent().next('.input-wrap').find('input');
        var textarea = $(this).parent().next('.textarea-wrap').find('textarea');

        if ($(this).prop('checked')) {
            input.attr('readonly', false);
            input.attr({
                // 'required':"true",
                'data-validation':"required"
            });
            input.css({
                // 'box-shadow': 'inset 0 0 24px 2px rgba(53, 59, 176, 0.15)',
                'border' : '1px solid #727785'
            });

            textarea.attr('readonly', false);
            textarea.attr({
                // 'required':"true",
                'data-validation':"required"
            });
            textarea.css({
                // 'box-shadow': 'inset 0 0 24px 2px rgba(53, 59, 176, 0.15)',
                // 'border' : '1px solid rgba(53, 59, 176, 0.3)'
                'border' : '1px solid #727785'
            });
        } else {
            input.attr('readonly', true);
            input.val('');
            input.attr('required', false);
            input.css({
                'box-shadow': 'none',
                'border' : '1px solid #cfd3dc'
            });


            textarea.attr('readonly', true);
            textarea.text('');
            textarea.attr('required', false);
            textarea.css({
                'box-shadow': 'none',
                'border' : '1px solid #cfd3dc'
            });
        }
    });

    $.validate({
        form: '#review-form',
        validateHiddenInputs: true,
        // rules: {
        //     about: {
        //         required:"#addAbout:checked"
        //     }
        // },
        onValidate: function ($form) {
            $('.styled-checkbox.has-error').parent('.agreement-conditions-item').addClass('error');
            $('.rating.has-error').parent().addClass('error');
        }
    });



    // $.validate({
    //     form: '#review-form',
    //     validateHiddenInputs: true,
    //     // rules: {
    //     //     about: {
    //     //         required:"#addAbout:checked"
    //     //     }
    //     // },
    //     onValidate: function ($form) {
    //         $('.styled-checkbox.has-error').parent('.agreement-conditions-item').addClass('error');
    //         $('.rating.has-error').parent().addClass('error');
    //     }
    // });



    // var $radio = $('input:radio[name="rec"]');
    // $radio.addClass("error");

    //from input to photo block
    function readURL(input, img) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                img.attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

//input file changed
    $(document).on('change', '.logoFile input',  function() {
        var img = $(this).closest('.logoFile').find('img');

        readURL(this, img);
    });

//add photo
    $(document).on('click','.site_icon-download', function() {
        $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
        var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
        hiddenInput.val('0');
    });

//delete photo
    $(document).on('click','.site_icon-delete', function(e) {
        e.preventDefault();

        var input = $(this).closest('.logoImgBlock').find('.logoFile input');
        var hiddenInput = $(this).next('input');
        hiddenInput.val('1');
        var img = $(this).closest('.logoImgBlock').find('.logoFile img');
        input.replaceWith(input.val('').clone(true));
        img.attr('src', '');
    });

    $('.open-modal').on('click', function () {
        $('.validation_errors_modal').empty();
    });

//input[type=file]
    $('.input-file input').on('change',function () {
        var filename = $(this).val();
        if (filename.substring(3,11) == 'fakepath') {
            filename = filename.substring(12);
        }
        $('.file-name').html(filename);
    });
});