$(document).ready(function () {

    $('.select2').select2();

    $('.select2').on('click', function () {
        $('.select2-selection__arrow').toggleClass('active')
    });

    $.validate({
        form: '#review-form',
        validateHiddenInputs: true,
        // rules: {
        //     about: {
        //         required:"#addAbout:checked"
        //     }
        // },
        onValidate: function ($form) {
            $('.styled-checkbox.has-error').parent('.agreement-conditions-item').addClass('error');
            $('.rating.has-error').parent().addClass('error');
        }
    });

    $('#your-review').keyup(function () {
        el = $(this);
        if (el.val().length <= 200) {
            $('#min-your-review').text(200 - el.val().length);
        } else {
            $('#min-your-review').text(0);
        }
    });

    $('#review-title').restrictLength($('#max-review-title'));

    var statusSelect = $('#review-status');

    $('.review-activate').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            method: 'PUT',
            url: reviewActivateStatusURL,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (review) {
                $('.activate-btn').hide();
                $('.reject-btn').hide();
                statusSelect.val(review.status_id).select2();
            }
        });
    });
    $('.review-reject').on('click', function (e) {
        e.preventDefault();
        console.log(reviewActivateStatusURL);
        var rejection_text = $('textarea[name=rejection_reason]').val();
        $.ajax({
            method: 'PUT',
            url: reviewRejectStatusURL,
            data: {rejection_reason: rejection_text},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (review) {
                $('.activate-btn').hide();
                $('.reject-btn').hide();
                var modal = $('[data-remodal-id=review-reject-form]').remodal();
                modal.close();
                statusSelect.val(review.status_id).select2();
            }
        });

    });

});