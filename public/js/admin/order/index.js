$(document).ready(function() {
    $("select").select2();

    $('.pagination li:not(.active) a').on('click', function (e) {
        e.preventDefault();
        $('#admin_order_filter').attr('action', $(this).attr('href'));
        $('#admin_order_filter').submit();
    });
});
