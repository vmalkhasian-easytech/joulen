$(document).ready(function () {
    $('select').niceSelect();
    initialize();

    $('body').on('click', '.delete_portfolio', function () {
        var id = $(this).attr('data-id');
        $('body').on('click', '.delete-portfolio-confirm', function (e) {
            deleteEquipment(deleteEquipmentURL+ '/' +id);
        });
    });
});

function initialize() {
    var lat = portfolio['lat'];
    var lng = portfolio['lng'];
    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(lat, lng),
        scrollwheel: false,
        zoom: 8,
        //Тип карты - обычная дорожная карта
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    };
    //Инициализируем карту
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var myMarker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        title: portfolio['address'],
        map: map
    });
}

var deleteEquipment = function (url) {
    var success = false;
    $.ajax({
        url: url,
        async: false,
        data: {
            _method: 'DELETE'
        },
        method: 'POST',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if(response.status == 'success')
                success = true;
        },
        error: function (response) {
        }
    });

    return success;
};