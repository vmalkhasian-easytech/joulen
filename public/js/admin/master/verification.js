$(document).ready(function () {
    var id;
    $('body').on('click', '.add-comment', function () {
        id = $(this).attr('data-id');
    });

    $('body').on('click', '.update-comment', function () {
        id = $(this).attr('data-id');
        var url = commentGetURL+'/'+ id;
        $.ajax({
            url: url,
            method: 'GET',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('textarea[name=comment]').val(response);
            }
        });
    });

    $('body').on('click', '.comment', function () {
        $('body').on('click', '.comment-confirm', function () {
            var form = $('#comment-form'),
                formData = form.serializeArray(),
                url = commentStoreURL + '/' + id;
            $.ajax({
                url: url,
                data: formData,
                method: 'PUT',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('.status-table tr[data-id=' + id + '] .comment_block').replaceWith(function () {
                        return '<td  data-title="Коментар адміна" data-breakpoints="xs sm md" class="comment_block"' +
                        'style="display: table-cell;">' + response.comment + '&nbsp;' +
                            '<a href="#comment" class="comment update-comment site_icon-edit" data-id="' + id + '"></a></td>';
                    });
                    $('#backend_validation_errors').empty();
                    var modal = $('[data-remodal-id=comment]').remodal();
                    modal.close();
                },
                error: function (response) {
                    var errors = response.responseJSON, errorField;
                    var validationBlock = $('#backend_validation_errors');
                    validationBlock.empty();
                    for (errorField in errors) {
                        validationBlock.append(
                            $('<li>').text(errors[errorField][0])
                        );
                    }
                }
            });
        });
    });
});

$(document).on('closed', '.remodal', function () {
    $('textarea[name=comment]').val('');
});
