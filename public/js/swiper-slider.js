var mySwiper = new Swiper('.slider-portfolio', {
    speed: 400,
    spaceBetween: 15,
    slidesPerView: 4,
    loop: false,
    navigation: {
        nextEl: '.slider-arrow-right',
        prevEl: '.slider-arrow-left',
    },
    breakpoints: {
        1920: {
            slidesPerView: 4,
            spaceBetween: 30
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 10
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        480: {
            slidesPerView: 'auto',
            spaceBetween: 20
        }
        // 320: {
        //     slidesPerView: 1,
        //     spaceBetween: 0
        // }
    }
});