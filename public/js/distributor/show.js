$(document).ready(function () {
    var dealers_count = 20;

    function show_blocks(data) {
        $('.company-dealer_items').children().hide();

        $.each(data, function(index, item){
            $('.dealer-item-' + item.id).show();
        });
    }

    $('#pagination-container').pagination({
        dataSource: dealers,
        pageSize: dealers_count,
        callback: function(data, pagination) {
            // template method of yourself
            show_blocks(data);
        }
    });
});