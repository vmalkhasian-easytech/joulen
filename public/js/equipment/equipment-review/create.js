$(document).ready(function () {

    $.validate({
        form: '#review-form',
        validateHiddenInputs: true,
        // rules: {
        //     about: {
        //         required:"#addAbout:checked"
        //     }
        // },
        onValidate: function ($form) {
            $('.styled-checkbox.has-error').parent('.agreement-conditions-item').addClass('error');
            $('.rating.has-error').parent().addClass('error');
        }
    });

    $('#your-review').keyup(function () {
        el = $(this);
        if (el.val().length <= 200) {
            $('#min-your-review').text(200 - el.val().length);
        } else {
            $('#min-your-review').text(0);
        }
    });

    $('#review-title').restrictLength($('#max-review-title'));


});