$(document).ready(function () {

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        $('body').css('cursor', 'pointer');
    }

    if (left_comment !== undefined) {
        $('.write-review a').attr('disabled', true).css('pointer-events', 'none').css('background-color', 'rgba(128, 128, 128, 0.48)')
            .css('color', 'white').css('box-shadow', 'none');
    }

    var quantity_reviews = $('.reviews-wrapper-company .review-company').length;
    $('.reviews-wrapper-company .review-company').slice(0, 5).show().css('display', 'flex');

    $('.show-more-btn').on('click', function (e) {
        e.preventDefault();
        $('.reviews-wrapper-company .review-company').slice(0, quantity_reviews).slideDown(400).css('display', 'flex');
        $(this).hide();
    });

    $('.read-more').on('click', function (e) {
        e.preventDefault();
        $(this).parent().find('.text-block').css('height', 'auto').addClass('full-text');
        $(this).hide();
    });

    var swiper_completed_projects = new Swiper('.swiper-completed-projects', {
        loop: false,
        spaceBetween: 22,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        slidesPerView: 4,
        breakpoints: {
            1200: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            650: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            425: {
                slidesPerView: 1,
                spaceBetween: 30
            }
        }
    });

    var count_slides = ($('.swiper-completed-projects .swiper-slide').length);
    if ($('.swiper-completed-projects .swiper-slide').length < 4) {
        $(this).addClass('swiper-no-swiping');
        $('.swiper-completed-projects').addClass('swiper-disabled');


        var swiper_completed_projects = new Swiper('.swiper-completed-projects', {
            slidesPerView: 'auto',
            noSwipingClass: 'swiper-no-swiping'
        });
    }
    $('.popup-link').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true,
            tCounter: '<span>%curr%</span> / '+ count_slides,
            preload: [0,1]
        },
        image: {
            markup: '<div class="mfp-figure">' +
            '<div class="mfp-close"></div>' +
            '<div class="mfp-img"></div>' +
            '<div class="mfp-title"></div>' +
            '<div class="mfp-counter"></div>' +
            '<div class="mfp-bottom-bar">' +
            '</div>' +
            '</div>',
            cursor: 'mfp-zoom-out-cur',
            titleSrc: 'data-popup',
            verticalFit: true,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        }
    });

    $('.photo-project').removeAttr('title');

    $('.view-more').on('click', function (e) {
        e.preventDefault();
        $(this).parent().parent().find('li').slideDown(400).css('display', 'block');
        $(this).hide();
    });

    var number_distributors = $('.view .distributors li').length;
    $('.view .distributors li').slice(0, 20).show();

    if (number_distributors < 20 ) {
        $('.view .distributors .view-more-item').css('display', 'none');
    }

    var number_off_dealers = $('.view .official-dealers li').length;
    $('.view .official-dealers li').slice(0, 20).show();

    if (number_off_dealers < 20 ) {
        $('.view .official-dealers .view-more-item').css('display', 'none');
    }

    var number_dealers = $('.view .dealers li').length;
    $('.view .dealers li').slice(0, 20).show();

    if (number_dealers < 20 ) {
        $('.view .dealers .view-more-item').css('display', 'none');
    }

    if ($(window).width() < 993) {
        $('.view-sm .distributors li').slice(0, 8).show().css('display', 'flex');
        $('.view-sm .distributors li').slice(8, number_distributors - 1).hide();
        if (number_distributors < 8 ) {
            $('.distributors .view-more-item').css('display', 'none');
        }

        $('.view-sm .official-dealers li').slice(0, 8).show().css('display', 'flex');
        $('.view-sm .official-dealers li').slice(8, number_off_dealers - 1).hide();
        if (number_off_dealers < 8 ) {
            $('.official-dealers .view-more-item').css('display', 'none');
        }

        $('.view-sm .dealers li').slice(0, 8).show().css('display', 'flex');
        $('.view-sm .dealers li').slice(8, number_dealers - 1).hide();
        if (number_dealers < 8 ) {
            $('.dealers .view-more-item').css('display', 'none');
        }
    }

    // $(window).resize(function () {
    //     if ($(window).width() < 993) {
    //         $('.view-sm .distributors li').slice(0, 8).show().css('display', 'flex');
    //         $('.view-sm .distributors li').slice(8, number_distributors - 1).hide();
    //         if (number_distributors < 8 ) {
    //             $('.distributors .view-more-item').css('display', 'none');
    //         }
    //
    //         $('.view-sm .official-dealers li').slice(0, 8).show().css('display', 'flex');
    //         $('.view-sm .official-dealers li').slice(8, number_off_dealers - 1).hide();
    //         if (number_off_dealers < 8 ) {
    //             $('.official-dealers .view-more-item').css('display', 'none');
    //         }
    //
    //         $('.view-sm .dealers li').slice(0, 8).show().css('display', 'flex');
    //         $('.view-sm .dealers li').slice(8, number_dealers - 1).hide();
    //         if (number_dealers < 8 ) {
    //             $('.dealers .view-more-item').css('display', 'none');
    //         }
    //     }
    // });

    // if ($(window).width() < 767) {
    //     $('.general-list-companies.view-sm li').slice(0, 8).show();
    //     $('.general-list-companies.view-sm li').slice(8, number_companies - 1).hide();
    //     if (number_companies < 11 ) {
    //         $('.view-more-item').css('display', 'none');
    //     }
    // }

    $('body').on('click', '.remove-review', function () {
        var id = $(this).attr('data-id');
        $('#form-delete-review').attr('action', deleteReviewURL + '/' + id);
    });
});
