$(document).ready(function () {
    $('.edit_comment').on('click', function (e) {
        $('#comment_form').attr('action', $(this).data('update_route'));
        $('#comment').val($(this).prev().html());
    });
    $(".modal-validate").validate({
        lang: 'uk',
        errorElement: 'span',
        ignore:[]

    });

    $.validator.addClassRules({
        validate_required: {
            required: true
        }
    });
});
