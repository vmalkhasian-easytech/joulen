$(document).ready(function () {
    $('#general-info').on('submit', function (e) {
        var attr = $(this).find('.logoFile img').attr('src');

        if(attr != '') {
            $(this).find('.show-error').hide();
        }
        else {
            $('#general-info').find('.show-error').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            e.preventDefault();
        }
    });
});

//from input to photo block
function readURL(input, img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            img.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//input file changed
$(document).on('change', '.logoFile input',  function() {
    var img = $(this).closest('.logoFile').find('img');

    readURL(this, img);
});

//add photo
$(document).on('click','.site_icon-download', function() {
    $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
    var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
    hiddenInput.val('0');
});

//delete photo
$(document).on('click','.site_icon-delete', function(e) {
    e.preventDefault();

    var input = $(this).closest('.logoImgBlock').find('.logoFile input');
    var hiddenInput = $(this).next('input');
    hiddenInput.val('1');
    var img = $(this).closest('.logoImgBlock').find('.logoFile img');
    input.replaceWith(input.val('').clone(true));
    img.attr('src', '');
});

//input[type=file]
$('.input-file input').on('change',function () {
    var filename = $(this).val();
    if (filename.substring(3,11) == 'fakepath') {
        filename = filename.substring(12);
    }
    $('.file-name').html(filename);
});

//end input[type=file]

$('body').on('click', function (event) {

    if( $(window).width() <= 992 && $(event.target).closest('.sidebar').length == 0){
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
        $('.sidebar').removeClass('hidden');
        if($(event.target).closest('.toggle-sidebar').length == 0){
            $('.cabinet-page').removeClass('sidebar-hidden');
        }
    }
});

$( window ).scroll(fixedSidebar());

fixedSidebar();
function fixedSidebar() {
    var scroll = $(document).scrollTop();
    if (scroll > 100) {
        $('#mainMenu').css('position', 'fixed');
    } else {
        $('#mainMenu').css('position', 'absolute');
    }
}
