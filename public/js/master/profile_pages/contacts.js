$(document).ready(function () {

    $('body').on('click', '#link_second_phone', function () {
        $('.second_phone').show();
    });

    if ($('#office_map-1').is(":visible") == true) {
        $('#main_office_added').hide();
        $('.plus_second_map').show();
        $("input[name=main_office_added]").val(1);
    }

    $('body').on('click', '#main_office_added', function () {
        $(this).hide();
        $("input[name=main_office_added]").val(1);
        $('#office_map-1').show();
        window.dispatchEvent(new Event('resize'));
        $('.plus_second_map').show();
        myLatlng = new google.maps.LatLng(N, E);
        map1.setCenter(myLatlng);
        map1.setZoom(9);
    });

    if ($('#office_map-2').is(":visible") == true) {
        $('.plus_third_map').show();
        $('.plus_second_map').hide();
        $("input[name=second_office_added]").val(1);
    }

    $('body').on('click', '#second_office_added', function () {
        $(this).hide();
        $("input[name=second_office_added]").val(1);
        $('#office_map-2').show();
        window.dispatchEvent(new Event('resize'));
        $('.plus_third_map').show();
        myLatlng = new google.maps.LatLng(N, E);
        map2.setCenter(myLatlng);
        map2.setZoom(9);
    });

    if ($('#office_map-3').is(":visible") == true) {
        $('#third_office_added').hide();
        $('#second_office_added').hide();
        $("input[name=third_office_added]").val(1);
    }

    $('body').on('click', '#third_office_added', function () {
        $(this).hide();
        $("input[name=third_office_added]").val(1);
        $('#office_map-3').show();
        window.dispatchEvent(new Event('resize'));
        myLatlng = new google.maps.LatLng(N, E);
        map3.setCenter(myLatlng);
        map3.setZoom(9);
    });

    $('body').on('click', 'a[data-hide-id="office_map-1"]', function () {
        $('input[name=main_office_added]').val(0);
        $('#pac-input-1').val('');
        $('#output-1').val('');
        $('#main_office_room').val('');
        $('#main_office_photo_img').attr('src', '');
        setTimeout(function () {
            $('#main_office_added').show();
            $('#office_map-1').fadeOut('fast');
            $('.plus_second_map').fadeOut('fast');
        }, 500);
    });

    $('body').on('click', 'a[data-hide-id="office_map-2"]', function () {
        $('input[name=second_office_added]').val(0);
        $('#pac-input-2').val('');
        $('#output-2').val('');
        $('#second_office_room').val('');
        $('#second_office_photo_img').attr('src', '');
        setTimeout(function () {
            $('#second_office_added').show();
            $('#office_map-2').fadeOut('fast');
            $('.plus_third_map').fadeOut('fast');
        }, 500);
    });

    $('body').on('click', 'a[data-hide-id="office_map-3"]', function () {
        $('input[name=third_office_added]').val(0);
        $('#pac-input-3').val('');
        $('#output-3').val('');
        $('#third_office_room').val('');
        $('#third_office_photo_img').attr('src', '');
        setTimeout(function () {
            $('#third_office_added').show();
            $('#office_map-3').fadeOut('fast')
        }, 500);
    });

    $('.add-second-phone').on('click', function (e) {
        $(this).hide();
        $(".remove-second-phone").show();
        $(".second_phone").show();
    });
    $('.remove-second-phone').on('click', function (e) {
        $(this).hide();
        $(".second_phone").hide();
        $("input[name=second_work_phone]").val('');
        $('.add-second-phone').show();
    });

    if ($('input[type=checkbox]').attr('disabled') == true) {
        $('.tooltip-checkbox').hide();
    }

    $('#contacts').on('submit', function (e) {
        var photos = [], attrs = [], visibleBlock = true;
        photos.push($(this).find('.logoFile img:visible'));
        for (var i = 0; i < photos[0].length; i++) {
            attrs.push($(photos[0][i]).attr('src'));
        }

        for (var i = 0; i < attrs.length; i++) {
            if (!attrs[i]) {
                visibleBlock = false;
            }
        }

        if (visibleBlock) {
            $(this).find('.show-error-hidden-file').hide();
        }
        else {
            e.preventDefault();
            $(this).find('.show-error-hidden-file').show();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
    });

    $('.show-map-block').focusout(function () {
        var lat = $(this).children('.geo_coords_lat').val();
        var lng = $(this).children('.geo_coords_lng').val();
        if(lat == '' && lng == '') {
            $(this).children('.error-block-office').show();
            $('#contacts-confirm').prop('disabled', true);
        }
        else {
            $(this).children('.error-block-office').hide();
            $('#contacts-confirm').prop('disabled', false);
        }
    });

    initialize();
});

///*******************************************
var lat = [], lng = [];
var N = 50.4532;
var E = 30.5255;

$('.geo_coords_lat').each(function () {
    if ($(this).val() == '') {
        lat.push(N);
    }
    else {
        lat.push($(this).val());
    }
});

$('.geo_coords_lng').each(function () {
    if ($(this).val() == '') {
        lng.push(E);
    }
    else {
        lng.push($(this).val());
    }
});
//------------------------------------------
// гугл карта------------------------------------------
var map1, map2, map3;
function initialize() {

    var mapOptions = {
        //Это центр куда спозиционируется наша карта при загрузке
        center: new google.maps.LatLng(N, E),
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false,
        zoom: 9,
        //Тип карты - обычная дорожная карта
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //Инициализируем карту
    map1 = new google.maps.Map(document.getElementById('gmap-1'), mapOptions);
    map2 = new google.maps.Map(document.getElementById('gmap-2'), mapOptions);
    map3 = new google.maps.Map(document.getElementById('gmap-3'), mapOptions);

    //Создание места
    var myPlace = new Place('Киев', N, E, 'Столица Украины');
    //Создание маркера
    var myMarker1 = new google.maps.Marker({
        //расположение на карте
        position: new google.maps.LatLng(lat[0], lng[0]),
        map: map1,
        //То что мы увидим при наведении мышкой на маркер
        title: myPlace.name,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    myLatlng = new google.maps.LatLng(lat[0], lng[0]);
    map1.setCenter(myLatlng);
    if (lat[0] != N && lng[0] != E) {
        var bounds1 = new google.maps.LatLngBounds();
        bounds1.extend(myMarker1.getPosition());
        map1.fitBounds(bounds1);
        google.maps.event.addListener(map1, "idle", function () {
            if (map1.getZoom() > 16) map1.setZoom(17);
        });
    }
    var myMarker2 = new google.maps.Marker({
        //расположение на карте
        position: new google.maps.LatLng(lat[1], lng[1]),
        map: map2,
        //То что мы увидим при наведении мышкой на маркер
        title: myPlace.name,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    myLatlng = new google.maps.LatLng(lat[1], lng[1]);
    map2.setCenter(myLatlng);
    if (lat[1] != N && lng[1] != E) {
        var bounds2 = new google.maps.LatLngBounds();
        bounds2.extend(myMarker2.getPosition());
        map2.fitBounds(bounds2);
        google.maps.event.addListener(map2, "idle", function () {
            if (map2.getZoom() > 16) map2.setZoom(17);
        });
    }
    var myMarker3 = new google.maps.Marker({
        //расположение на карте
        position: new google.maps.LatLng(lat[2], lng[2]),
        map: map3,
        //То что мы увидим при наведении мышкой на маркер
        title: myPlace.name,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: markerSrc
    });
    myLatlng = new google.maps.LatLng(lat[2], lng[2]);
    map3.setCenter(myLatlng);
    if (lat[2] != N && lng[2] != E) {
        var bounds3 = new google.maps.LatLngBounds();
        bounds3.extend(myMarker3.getPosition());
        map3.fitBounds(bounds3);
        google.maps.event.addListener(map3, "idle", function () {
            if (map3.getZoom() > 16) map3.setZoom(17);
        });
    }

    // Create the search box and link it to the UI element.
    var input1 = document.getElementById('pac-input-1');
    var input2 = document.getElementById('pac-input-2');
    var input3 = document.getElementById('pac-input-3');
    var searchBox1 = new google.maps.places.SearchBox(input1);
    var searchBox2 = new google.maps.places.SearchBox(input2);
    var searchBox3 = new google.maps.places.SearchBox(input3);
    map1.controls[google.maps.ControlPosition.TOP_LEFT].push(input1);
    map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input2);
    map3.controls[google.maps.ControlPosition.TOP_LEFT].push(input3);

    // Bias the SearchBox results towards current map's viewport.
    map1.addListener('bounds_changed', function () {
        searchBox1.setBounds(map1.getBounds());
    });
    map2.addListener('bounds_changed', function () {
        searchBox2.setBounds(map2.getBounds());
    });
    map3.addListener('bounds_changed', function () {
        searchBox3.setBounds(map3.getBounds());
    });

    var geocoder1 = new google.maps.Geocoder;
    var geocoder2 = new google.maps.Geocoder;
    var geocoder3 = new google.maps.Geocoder;
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox1.addListener('places_changed', function () {
        var places = searchBox1.getPlaces();
        var homeListener = google.maps.event.addListener(map1, "idle", function () {
            map1.setZoom(17);
            google.maps.event.removeListener(homeListener);
        });
        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            myMarker1.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });

        map1.fitBounds(bounds);
        latlng = myMarker1.getPosition();
        $('#lat-1').val(latlng.lat());
        $('#lng-1').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder1, map1, latlng, '#office_map-1', '#pac-input-1');

    });
    myMarker1.addListener('dragend', function () {
        latlng = myMarker1.getPosition();
        $('#lat-1').val(latlng.lat());
        $('#lng-1').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder1, map1, latlng, '#office_map-1', '#pac-input-1');
    });
    searchBox2.addListener('places_changed', function () {
        var places = searchBox2.getPlaces();
        var homeListener = google.maps.event.addListener(map2, "idle", function () {
            map2.setZoom(17);
            google.maps.event.removeListener(homeListener);
        });
        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            myMarker2.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });

        map2.fitBounds(bounds);
        latlng = myMarker2.getPosition();
        $('#lat-2').val(latlng.lat());
        $('#lng-2').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder2, map2, latlng, '#office_map-2', '#pac-input-2');
    });
    myMarker2.addListener('dragend', function () {
        latlng = myMarker2.getPosition();
        $('#lat-2').val(latlng.lat());
        $('#lng-2').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder2, map2, latlng, '#office_map-2', '#pac-input-2');
    });
    searchBox3.addListener('places_changed', function () {
        var places = searchBox3.getPlaces();
        var homeListener = google.maps.event.addListener(map3, "idle", function () {
            map3.setZoom(17);
            google.maps.event.removeListener(homeListener);
        });
        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            myMarker3.setPosition(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });

        map3.fitBounds(bounds);
        latlng = myMarker3.getPosition();
        $('#lat-3').val(latlng.lat());
        $('#lng-3').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder3, map3, latlng, '#office_map-3', '#pac-input-3');
    });
    myMarker3.addListener('dragend', function () {
        latlng = myMarker3.getPosition();
        $('#lat-3').val(latlng.lat());
        $('#lng-3').val(latlng.lng());

        N = latlng.lat();
        E = latlng.lng();

        geocodeLatLng(geocoder3, map3, latlng, '#office_map-3', '#pac-input-3');
    });
}

//Это класс для удобного манипулирования местами
function Place(name, latitude, longitude, description) {
    this.name = name;  // название
    this.latitude = latitude;  // широта
    this.longitude = longitude;  // долгота
    this.description = description;  // описание места
}

//from input to photo block
function readURL(input, img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            img.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//input file changed
$(document).on('change', '.logoFile input', function () {
    var img = $(this).closest('.logoFile').find('img');

    readURL(this, img);
});

//add photo
$(document).on('click', '.site_icon-download', function () {
    $(this).closest('.logoImgBlock').find('.logoFile input').trigger('click');
    var hiddenInput = $(this).closest('.logoImgBlock').find('div:last-child input');
    hiddenInput.val('0');
});

//delete photo
$(document).on('click', '.site_icon-delete', function (e) {
    e.preventDefault();

    var input = $(this).closest('.logoImgBlock').find('.logoFile input');
    var hiddenInput = $(this).next('input');
    hiddenInput.val('1');
    var img = $(this).closest('.logoImgBlock').find('.logoFile img');
    input.replaceWith(input.val('').clone(true));
    img.attr('src', '');
});

//input[type=file]
$('.input-file input').on('change', function () {
    var filename = $(this).val();
    if (filename.substring(3, 11) == 'fakepath') {
        filename = filename.substring(12);
    }
    $('.file-name').html(filename);
});
//end input[type=file]

$('body').on('click', function (event) {

    if ($(window).width() <= 992 && $(event.target).closest('.sidebar').length == 0) {
        $('.sub-menu').each(function () {
            $(this).hide();
        });
        $('.menu-item').each(function () {
            $(this).removeClass('active');
        });
        $('.sidebar').removeClass('hidden');
        if ($(event.target).closest('.toggle-sidebar').length == 0) {
            $('.cabinet-page').removeClass('sidebar-hidden');
        }
    }
});

$(window).scroll(fixedSidebar());

fixedSidebar();
function fixedSidebar() {
    var scroll = $(document).scrollTop();
    if (scroll > 100) {
        $('#mainMenu').css('position', 'fixed');
    } else {
        $('#mainMenu').css('position', 'absolute');
    }
}

function geocodeLatLng(geocoder, map, latlng, mapBlock, pacInputBlock) {
    var latlng = latlng;
    var address = '', city = '', street = '', number = '', regionName = '';
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === 'OK') {
            $(mapBlock + ' .show-map-block').children('.error-block-office').hide();
            for (var i = 0; i < results.length; i++) {
                if (results[i].types[0] === 'administrative_area_level_1') {
                    var region = results[i].formatted_address.split(' ')[0];
                    $(mapBlock + ' .show-map-block').children('.office-region').val(region);
                }
            }
            if (results[0]) {
                for (var i = 0; i < results[0].address_components.length; i++) {
                    if (results[0].address_components[i].types[0] == 'locality') {
                        city = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'route') {
                        street = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'administrative_area_level_2') {
                        regionName = results[0].address_components[i].long_name;
                    }
                    else if (results[0].address_components[i].types[0] == 'street_number') {
                        number = results[0].address_components[i].long_name;
                    }

                }
                address = (city ? city : regionName) + ', ' + street + ' ' + number;

                map.setCenter(latlng);
                $(mapBlock + ' .show-map-block .office-input').children('.office_output').val(address);
                $(pacInputBlock).val(address);
            } else {
                window.alert('Немає результатів');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}
