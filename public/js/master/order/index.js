$("#formsetfilter").on('submit', function (e) {
    e.preventDefault();
});
$('.formsetfilter_type,.formsetfilter_type_subcategory,.formsetfilter_type_home').on('click', function (e) {
    e.preventDefault();
    $('.formsetfilter_type').removeClass('active');
    $('.formsetfilter_type_subcategory').removeClass('active');
    $('.formsetfilter_type_home').removeClass('active');
    $(this).addClass('active');
    applyFilter();
});
$('#regionFilterChk').on('change', function () {
    applyFilter();
});

function applyFilter(){
    var region_filter = $('#regionFilterChk').val();
    var category = $('.formsetfilter_type.active').first().data('category');
    var subcategory = $('.formsetfilter_type_subcategory.active').first().data('subcategory');
    var home = $('.formsetfilter_type_home.active').first().data('home');
    var filter = [];
    var link = masterOrderRoute;

    if( typeof category != 'undefined' && category != 'all' && $('input[value=all]').prop('disabled') == false){
        filter.push(category);
    }

    if( typeof subcategory != 'undefined' && subcategory != 'all' && $('input[value=commercial]').prop('disabled') == false){
        filter.push(subcategory);
    }

    if( typeof home != 'undefined' && home != 'all' && $('input[value=home]').prop('disabled') == false){
        filter.push(home);
    }

    if($('#regionFilterChk').is(':checked')){
        filter.push(region_filter);
    }

    link = link + '/' + filter.join('/');
    window.location.href = link;
}